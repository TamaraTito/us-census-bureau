*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\ic2021_ay.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xtuit1   $1.
tuition1 5. 
xfee1    $1.
fee1     5. 
xhrchg1  $1.
hrchg1   5. 
xtuit2   $1.
tuition2 5. 
xfee2    $1.
fee2     5. 
xhrchg2  $1.
hrchg2   5. 
xtuit3   $1.
tuition3 5. 
xfee3    $1.
fee3     5. 
xhrchg3  $1.
hrchg3   5. 
xtuit5   $1.
tuition5 5. 
xfee5    $1.
fee5     5. 
xhrchg5  $1.
hrchg5   5. 
xtuit6   $1.
tuition6 5. 
xfee6    $1.
fee6     5. 
xhrchg6  $1.
hrchg6   5. 
xtuit7   $1.
tuition7 5. 
xfee7    $1.
fee7     5. 
xhrchg7  $1.
hrchg7   5. 
xispro1  $1.
isprof1  5. 
xispfe1  $1.
ispfee1  5. 
xospro1  $1.
osprof1  5. 
xospfe1  $1.
ospfee1  5. 
xispro2  $1.
isprof2  5. 
xispfe2  $1.
ispfee2  5. 
xospro2  $1.
osprof2  5. 
xospfe2  $1.
ospfee2  5. 
xispro3  $1.
isprof3  5. 
xispfe3  $1.
ispfee3  5. 
xospro3  $1.
osprof3  5. 
xospfe3  $1.
ospfee3  5. 
xispro4  $1.
isprof4  5. 
xispfe4  $1.
ispfee4  5. 
xospro4  $1.
osprof4  5. 
xospfe4  $1.
ospfee4  5. 
xispro5  $1.
isprof5  5. 
xispfe5  $1.
ispfee5  5. 
xospro5  $1.
osprof5  5. 
xospfe5  $1.
ospfee5  5. 
xispro6  $1.
isprof6  5. 
xispfe6  $1.
ispfee6  5. 
xospro6  $1.
osprof6  5. 
xospfe6  $1.
ospfee6  5. 
xispro7  $1.
isprof7  5. 
xispfe7  $1.
ispfee7  5. 
xospro7  $1.
osprof7  5. 
xospfe7  $1.
ospfee7  5. 
xispro8  $1.
isprof8  5. 
xispfe8  $1.
ispfee8  5. 
xospro8  $1.
osprof8  5. 
xospfe8  $1.
ospfee8  5. 
xispro9  $1.
isprof9  5. 
xispfe9  $1.
ispfee9  5. 
xospro9  $1.
osprof9  5. 
xospfe9  $1.
ospfee9  5. 
xchg1at0 $1.
chg1at0  5. 
xchg1af0 $1.
chg1af0  5. 
xchg1ay0 $1.
chg1ay0  5. 
xchg1at1 $1.
chg1at1  5. 
xchg1af1 $1.
chg1af1  5. 
xchg1ay1 $1.
chg1ay1  5. 
xchg1at2 $1.
chg1at2  5. 
xchg1af2 $1.
chg1af2  5. 
xchg1ay2 $1.
chg1ay2  5. 
xchg1at3 $1.
chg1at3  5. 
xchg1af3 $1.
chg1af3  5. 
xchg1ay3 $1.
chg1ay3  5. 
chg1tgtd 3. 
chg1fgtd 3. 
xchg2at0 $1.
chg2at0  5. 
xchg2af0 $1.
chg2af0  5. 
xchg2ay0 $1.
chg2ay0  5. 
xchg2at1 $1.
chg2at1  5. 
xchg2af1 $1.
chg2af1  5. 
xchg2ay1 $1.
chg2ay1  5. 
xchg2at2 $1.
chg2at2  5. 
xchg2af2 $1.
chg2af2  5. 
xchg2ay2 $1.
chg2ay2  5. 
xchg2at3 $1.
chg2at3  5. 
xchg2af3 $1.
chg2af3  5. 
xchg2ay3 $1.
chg2ay3  5. 
chg2tgtd 3. 
chg2fgtd 3. 
xchg3at0 $1.
chg3at0  5. 
xchg3af0 $1.
chg3af0  5. 
xchg3ay0 $1.
chg3ay0  5. 
xchg3at1 $1.
chg3at1  5. 
xchg3af1 $1.
chg3af1  5. 
xchg3ay1 $1.
chg3ay1  5. 
xchg3at2 $1.
chg3at2  5. 
xchg3af2 $1.
chg3af2  5. 
xchg3ay2 $1.
chg3ay2  5. 
xchg3at3 $1.
chg3at3  5. 
xchg3af3 $1.
chg3af3  5. 
xchg3ay3 $1.
chg3ay3  5. 
chg3tgtd 3. 
chg3fgtd 3. 
xchg4ay0 $1.
chg4ay0  5. 
xchg4ay1 $1.
chg4ay1  5. 
xchg4ay2 $1.
chg4ay2  5. 
xchg4ay3 $1.
chg4ay3  5. 
xchg5ay0 $1.
chg5ay0  5. 
xchg5ay1 $1.
chg5ay1  5. 
xchg5ay2 $1.
chg5ay2  5. 
xchg5ay3 $1.
chg5ay3  5. 
xchg6ay0 $1.
chg6ay0  5. 
xchg6ay1 $1.
chg6ay1  5. 
xchg6ay2 $1.
chg6ay2  5. 
xchg6ay3 $1.
chg6ay3  5. 
xchg7ay0 $1.
chg7ay0  5. 
xchg7ay1 $1.
chg7ay1  5. 
xchg7ay2 $1.
chg7ay2  5. 
xchg7ay3 $1.
chg7ay3  5. 
xchg8ay0 $1.
chg8ay0  5. 
xchg8ay1 $1.
chg8ay1  5. 
xchg8ay2 $1.
chg8ay2  5. 
xchg8ay3 $1.
chg8ay3  5. 
xchg9ay0 $1.
chg9ay0  5. 
xchg9ay1 $1.
chg9ay1  5. 
xchg9ay2 $1.
chg9ay2  5. 
xchg9ay3 $1.
chg9ay3  5.;

input
unitid   
xtuit1   $
tuition1 
xfee1    $
fee1     
xhrchg1  $
hrchg1   
xtuit2   $
tuition2 
xfee2    $
fee2     
xhrchg2  $
hrchg2   
xtuit3   $
tuition3 
xfee3    $
fee3     
xhrchg3  $
hrchg3   
xtuit5   $
tuition5 
xfee5    $
fee5     
xhrchg5  $
hrchg5   
xtuit6   $
tuition6 
xfee6    $
fee6     
xhrchg6  $
hrchg6   
xtuit7   $
tuition7 
xfee7    $
fee7     
xhrchg7  $
hrchg7   
xispro1  $
isprof1  
xispfe1  $
ispfee1  
xospro1  $
osprof1  
xospfe1  $
ospfee1  
xispro2  $
isprof2  
xispfe2  $
ispfee2  
xospro2  $
osprof2  
xospfe2  $
ospfee2  
xispro3  $
isprof3  
xispfe3  $
ispfee3  
xospro3  $
osprof3  
xospfe3  $
ospfee3  
xispro4  $
isprof4  
xispfe4  $
ispfee4  
xospro4  $
osprof4  
xospfe4  $
ospfee4  
xispro5  $
isprof5  
xispfe5  $
ispfee5  
xospro5  $
osprof5  
xospfe5  $
ospfee5  
xispro6  $
isprof6  
xispfe6  $
ispfee6  
xospro6  $
osprof6  
xospfe6  $
ospfee6  
xispro7  $
isprof7  
xispfe7  $
ispfee7  
xospro7  $
osprof7  
xospfe7  $
ospfee7  
xispro8  $
isprof8  
xispfe8  $
ispfee8  
xospro8  $
osprof8  
xospfe8  $
ospfee8  
xispro9  $
isprof9  
xispfe9  $
ispfee9  
xospro9  $
osprof9  
xospfe9  $
ospfee9  
xchg1at0 $
chg1at0  
xchg1af0 $
chg1af0  
xchg1ay0 $
chg1ay0  
xchg1at1 $
chg1at1  
xchg1af1 $
chg1af1  
xchg1ay1 $
chg1ay1  
xchg1at2 $
chg1at2  
xchg1af2 $
chg1af2  
xchg1ay2 $
chg1ay2  
xchg1at3 $
chg1at3  
xchg1af3 $
chg1af3  
xchg1ay3 $
chg1ay3  
chg1tgtd 
chg1fgtd 
xchg2at0 $
chg2at0  
xchg2af0 $
chg2af0  
xchg2ay0 $
chg2ay0  
xchg2at1 $
chg2at1  
xchg2af1 $
chg2af1  
xchg2ay1 $
chg2ay1  
xchg2at2 $
chg2at2  
xchg2af2 $
chg2af2  
xchg2ay2 $
chg2ay2  
xchg2at3 $
chg2at3  
xchg2af3 $
chg2af3  
xchg2ay3 $
chg2ay3  
chg2tgtd 
chg2fgtd 
xchg3at0 $
chg3at0  
xchg3af0 $
chg3af0  
xchg3ay0 $
chg3ay0  
xchg3at1 $
chg3at1  
xchg3af1 $
chg3af1  
xchg3ay1 $
chg3ay1  
xchg3at2 $
chg3at2  
xchg3af2 $
chg3af2  
xchg3ay2 $
chg3ay2  
xchg3at3 $
chg3at3  
xchg3af3 $
chg3af3  
xchg3ay3 $
chg3ay3  
chg3tgtd 
chg3fgtd 
xchg4ay0 $
chg4ay0  
xchg4ay1 $
chg4ay1  
xchg4ay2 $
chg4ay2  
xchg4ay3 $
chg4ay3  
xchg5ay0 $
chg5ay0  
xchg5ay1 $
chg5ay1  
xchg5ay2 $
chg5ay2  
xchg5ay3 $
chg5ay3  
xchg6ay0 $
chg6ay0  
xchg6ay1 $
chg6ay1  
xchg6ay2 $
chg6ay2  
xchg6ay3 $
chg6ay3  
xchg7ay0 $
chg7ay0  
xchg7ay1 $
chg7ay1  
xchg7ay2 $
chg7ay2  
xchg7ay3 $
chg7ay3  
xchg8ay0 $
chg8ay0  
xchg8ay1 $
chg8ay1  
xchg8ay2 $
chg8ay2  
xchg8ay3 $
chg8ay3  
xchg9ay0 $
chg9ay0  
xchg9ay1 $
chg9ay1  
xchg9ay2 $
chg9ay2  
xchg9ay3 $
chg9ay3 ;

label
unitid  ='Unique identification number of the institution' 
xtuit1  ='Imputation field for tuition1 -  In-district average tuition for full-time undergraduates'
tuition1=' In-district average tuition for full-time undergraduates' 
xfee1   ='Imputation field for fee1 -  In-district required fees for full-time undergraduates'
fee1    =' In-district required fees for full-time undergraduates' 
xhrchg1 ='Imputation field for hrchg1 -  In-district per credit hour charge for part-time undergraduates'
hrchg1  =' In-district per credit hour charge for part-time undergraduates' 
xtuit2  ='Imputation field for tuition2 - In-state average tuition for full-time undergraduates'
tuition2='In-state average tuition for full-time undergraduates' 
xfee2   ='Imputation field for fee2 - In-state required fees for full-time undergraduates'
fee2    ='In-state required fees for full-time undergraduates' 
xhrchg2 ='Imputation field for hrchg2 - In-state per credit hour charge for part-time undergraduates'
hrchg2  ='In-state per credit hour charge for part-time undergraduates' 
xtuit3  ='Imputation field for tuition3 - Out-of-state average tuition for full-time undergraduates'
tuition3='Out-of-state average tuition for full-time undergraduates' 
xfee3   ='Imputation field for fee3 - Out-of-state required fees for full-time undergraduates'
fee3    ='Out-of-state required fees for full-time undergraduates' 
xhrchg3 ='Imputation field for hrchg3 - Out-of-state per credit hour charge for part-time undergraduates'
hrchg3  ='Out-of-state per credit hour charge for part-time undergraduates' 
xtuit5  ='Imputation field for tuition5 -  In-district average tuition full-time graduates'
tuition5=' In-district average tuition full-time graduates' 
xfee5   ='Imputation field for fee5 - In-district required fees for full-time graduates'
fee5    ='In-district required fees for full-time graduates' 
xhrchg5 ='Imputation field for hrchg5 - In-district per credit hour charge part-time graduates'
hrchg5  ='In-district per credit hour charge part-time graduates' 
xtuit6  ='Imputation field for tuition6 - In-state average tuition full-time graduates'
tuition6='In-state average tuition full-time graduates' 
xfee6   ='Imputation field for fee6 - In-state required fees for full-time graduates'
fee6    ='In-state required fees for full-time graduates' 
xhrchg6 ='Imputation field for hrchg6 - In-state per credit hour charge part-time graduates'
hrchg6  ='In-state per credit hour charge part-time graduates' 
xtuit7  ='Imputation field for tuition7 - Out-of-state average tuition full-time graduates'
tuition7='Out-of-state average tuition full-time graduates' 
xfee7   ='Imputation field for fee7 - Out-of-state required fees for full-time graduates'
fee7    ='Out-of-state required fees for full-time graduates' 
xhrchg7 ='Imputation field for hrchg7 - Out-of-state per credit hour charge part-time graduates'
hrchg7  ='Out-of-state per credit hour charge part-time graduates' 
xispro1 ='Imputation field for isprof1 - Chiropractic: In-state tuition'
isprof1 ='Chiropractic: In-state tuition' 
xispfe1 ='Imputation field for ispfee1 - Chiropractic: In-state required fees'
ispfee1 ='Chiropractic: In-state required fees' 
xospro1 ='Imputation field for osprof1 - Chiropractic: Out-of-state tuition'
osprof1 ='Chiropractic: Out-of-state tuition' 
xospfe1 ='Imputation field for ospfee1 - Chiropractic: Out-of-state required fees'
ospfee1 ='Chiropractic: Out-of-state required fees' 
xispro2 ='Imputation field for isprof2 - Dentistry: In-state tuition'
isprof2 ='Dentistry: In-state tuition' 
xispfe2 ='Imputation field for ispfee2 - Dentistry: In-state required fees'
ispfee2 ='Dentistry: In-state required fees' 
xospro2 ='Imputation field for osprof2 - Dentistry: Out-of-state tuition'
osprof2 ='Dentistry: Out-of-state tuition' 
xospfe2 ='Imputation field for ospfee2 - Dentistry: Out-of-state required fees'
ospfee2 ='Dentistry: Out-of-state required fees' 
xispro3 ='Imputation field for isprof3 - Medicine: In-state tuition'
isprof3 ='Medicine: In-state tuition' 
xispfe3 ='Imputation field for ispfee3 - Medicine: In-state required fees'
ispfee3 ='Medicine: In-state required fees' 
xospro3 ='Imputation field for osprof3 - Medicine: Out-of-state tuition'
osprof3 ='Medicine: Out-of-state tuition' 
xospfe3 ='Imputation field for ospfee3 - Medicine: Out-of-state required fees'
ospfee3 ='Medicine: Out-of-state required fees' 
xispro4 ='Imputation field for isprof4 - Optometry: In-state tuition'
isprof4 ='Optometry: In-state tuition' 
xispfe4 ='Imputation field for ispfee4 - Optometry: In-state required fees'
ispfee4 ='Optometry: In-state required fees' 
xospro4 ='Imputation field for osprof4 - Optometry: Out-of-state tuition'
osprof4 ='Optometry: Out-of-state tuition' 
xospfe4 ='Imputation field for ospfee4 - Optometry: Out-of-state required fees'
ospfee4 ='Optometry: Out-of-state required fees' 
xispro5 ='Imputation field for isprof5 - Osteopathic Medicine: In-state tuition'
isprof5 ='Osteopathic Medicine: In-state tuition' 
xispfe5 ='Imputation field for ispfee5 - Osteopathic Medicine: In-state required fees'
ispfee5 ='Osteopathic Medicine: In-state required fees' 
xospro5 ='Imputation field for osprof5 - Osteopathic Medicine: Out-of-state tuition'
osprof5 ='Osteopathic Medicine: Out-of-state tuition' 
xospfe5 ='Imputation field for ospfee5 - Osteopathic Medicine: Out-of-state required fees'
ospfee5 ='Osteopathic Medicine: Out-of-state required fees' 
xispro6 ='Imputation field for isprof6 - Pharmacy: In-state tuition'
isprof6 ='Pharmacy: In-state tuition' 
xispfe6 ='Imputation field for ispfee6 - Pharmacy: In-state required fees'
ispfee6 ='Pharmacy: In-state required fees' 
xospro6 ='Imputation field for osprof6 - Pharmacy: Out-of-state tuition'
osprof6 ='Pharmacy: Out-of-state tuition' 
xospfe6 ='Imputation field for ospfee6 - Pharmacy: Out-of-state required fees'
ospfee6 ='Pharmacy: Out-of-state required fees' 
xispro7 ='Imputation field for isprof7 - Podiatry: In-state tuition'
isprof7 ='Podiatry: In-state tuition' 
xispfe7 ='Imputation field for ispfee7 - Podiatry: In-state required fees'
ispfee7 ='Podiatry: In-state required fees' 
xospro7 ='Imputation field for osprof7 - Podiatry: Out-of-state tuition'
osprof7 ='Podiatry: Out-of-state tuition' 
xospfe7 ='Imputation field for ospfee7 - Podiatry: Out-of-state required fees'
ospfee7 ='Podiatry: Out-of-state required fees' 
xispro8 ='Imputation field for isprof8 - Veterinary Medicine: In-state tuition'
isprof8 ='Veterinary Medicine: In-state tuition' 
xispfe8 ='Imputation field for ispfee8 - Veterinary Medicine: In-state required fees'
ispfee8 ='Veterinary Medicine: In-state required fees' 
xospro8 ='Imputation field for osprof8 - Veterinary Medicine: Out-of-state tuition'
osprof8 ='Veterinary Medicine: Out-of-state tuition' 
xospfe8 ='Imputation field for ospfee8 - Veterinary Medicine: Out-of-state required fees'
ospfee8 ='Veterinary Medicine: Out-of-state required fees' 
xispro9 ='Imputation field for isprof9 - Law: In-state tuition'
isprof9 ='Law: In-state tuition' 
xispfe9 ='Imputation field for ispfee9 - Law: In-state required fees'
ispfee9 ='Law: In-state required fees' 
xospro9 ='Imputation field for osprof9 - Law: Out-of-state tuition'
osprof9 ='Law: Out-of-state tuition' 
xospfe9 ='Imputation field for ospfee9 - Law: Out-of-state required fees'
ospfee9 ='Law: Out-of-state required fees' 
xchg1at0='Imputation field for chg1at0 - Published in-district tuition 2018-19'
chg1at0 ='Published in-district tuition 2018-19' 
xchg1af0='Imputation field for chg1af0 - Published in-district fees 2018-19'
chg1af0 ='Published in-district fees 2018-19' 
xchg1ay0='Imputation field for chg1ay0 - Published in-district tuition and fees 2018-19'
chg1ay0 ='Published in-district tuition and fees 2018-19' 
xchg1at1='Imputation field for chg1at1 - Published in-district tuition 2019-20'
chg1at1 ='Published in-district tuition 2019-20' 
xchg1af1='Imputation field for chg1af1 - Published in-district fees 2019-20'
chg1af1 ='Published in-district fees 2019-20' 
xchg1ay1='Imputation field for chg1ay1 - Published in-district tuition and fees 2019-20'
chg1ay1 ='Published in-district tuition and fees 2019-20' 
xchg1at2='Imputation field for chg1at2 - Published in-district tuition 2020-21'
chg1at2 ='Published in-district tuition 2020-21' 
xchg1af2='Imputation field for chg1af2 - Published in-district fees 2020-21'
chg1af2 ='Published in-district fees 2020-21' 
xchg1ay2='Imputation field for chg1ay2 - Published in-district tuition and fees 2020-21'
chg1ay2 ='Published in-district tuition and fees 2020-21' 
xchg1at3='Imputation field for chg1at3 - Published in-district tuition 2021-22'
chg1at3 ='Published in-district tuition 2021-22' 
xchg1af3='Imputation field for chg1af3 - Published in-district fees 2021-22'
chg1af3 ='Published in-district fees 2021-22' 
xchg1ay3='Imputation field for chg1ay3 - Published in-district tuition and fees 2021-22'
chg1ay3 ='Published in-district tuition and fees 2021-22' 
chg1tgtd='Published in-district tuition 2021-22 guaranteed percent increase (if applicable)' 
chg1fgtd='Published in-district fees 2021-22 guaranteed percent increase (if applicable)' 
xchg2at0='Imputation field for chg2at0 - Published in-state tuition 2018-19'
chg2at0 ='Published in-state tuition 2018-19' 
xchg2af0='Imputation field for chg2af0 - Published in-state fees 2018-19'
chg2af0 ='Published in-state fees 2018-19' 
xchg2ay0='Imputation field for chg2ay0 - Published in-state tuition and fees 2018-19'
chg2ay0 ='Published in-state tuition and fees 2018-19' 
xchg2at1='Imputation field for chg2at1 - Published in-state tuition 2019-20'
chg2at1 ='Published in-state tuition 2019-20' 
xchg2af1='Imputation field for chg2af1 - Published in-state fees 2019-20'
chg2af1 ='Published in-state fees 2019-20' 
xchg2ay1='Imputation field for chg2ay1 - Published in-state tuition and fees 2019-20'
chg2ay1 ='Published in-state tuition and fees 2019-20' 
xchg2at2='Imputation field for chg2at2 - Published in-state tuition 2020-21'
chg2at2 ='Published in-state tuition 2020-21' 
xchg2af2='Imputation field for chg2af2 - Published in-state fees 2020-21'
chg2af2 ='Published in-state fees 2020-21' 
xchg2ay2='Imputation field for chg2ay2 - Published in-state tuition and fees 2020-21'
chg2ay2 ='Published in-state tuition and fees 2020-21' 
xchg2at3='Imputation field for chg2at3 - Published in-state tuition 2021-22'
chg2at3 ='Published in-state tuition 2021-22' 
xchg2af3='Imputation field for chg2af3 - Published in-state fees 2021-22'
chg2af3 ='Published in-state fees 2021-22' 
xchg2ay3='Imputation field for chg2ay3 - Published in-state tuition and fees 2021-22'
chg2ay3 ='Published in-state tuition and fees 2021-22' 
chg2tgtd='Published in-state tuition 2021-22 guaranteed percent increase (if applicable)' 
chg2fgtd='Published in-state fees 2021-22 guaranteed percent increase (if applicable)' 
xchg3at0='Imputation field for chg3at0 - Published out-of-state tuition 2018-19'
chg3at0 ='Published out-of-state tuition 2018-19' 
xchg3af0='Imputation field for chg3af0 - Published out-of-state fees 2018-19'
chg3af0 ='Published out-of-state fees 2018-19' 
xchg3ay0='Imputation field for chg3ay0 - Published out-of-state tuition and fees 2018-19'
chg3ay0 ='Published out-of-state tuition and fees 2018-19' 
xchg3at1='Imputation field for chg3at1 - Published out-of-state tuition 2019-20'
chg3at1 ='Published out-of-state tuition 2019-20' 
xchg3af1='Imputation field for chg3af1 - Published out-of-state fees 2019-20'
chg3af1 ='Published out-of-state fees 2019-20' 
xchg3ay1='Imputation field for chg3ay1 - Published out-of-state tuition and fees 2019-20'
chg3ay1 ='Published out-of-state tuition and fees 2019-20' 
xchg3at2='Imputation field for chg3at2 - Published out-of-state tuition 2020-21'
chg3at2 ='Published out-of-state tuition 2020-21' 
xchg3af2='Imputation field for chg3af2 - Published out-of-state fees 2020-21'
chg3af2 ='Published out-of-state fees 2020-21' 
xchg3ay2='Imputation field for chg3ay2 - Published out-of-state tuition and fees 2020-21'
chg3ay2 ='Published out-of-state tuition and fees 2020-21' 
xchg3at3='Imputation field for chg3at3 - Published out-of-state tuition 2021-22'
chg3at3 ='Published out-of-state tuition 2021-22' 
xchg3af3='Imputation field for chg3af3 - Published out-of-state fees 2021-22'
chg3af3 ='Published out-of-state fees 2021-22' 
xchg3ay3='Imputation field for chg3ay3 - Published out-of-state tuition and fees 2021-22'
chg3ay3 ='Published out-of-state tuition and fees 2021-22' 
chg3tgtd='Published out-of-state tuition 2021-22 guaranteed percent increase (if applicable)' 
chg3fgtd='Published out-of-state fees 2021-22 guaranteed percent increase (if applicable)' 
xchg4ay0='Imputation field for chg4ay0 - Books and supplies 2018-19'
chg4ay0 ='Books and supplies 2018-19' 
xchg4ay1='Imputation field for chg4ay1 - Books and supplies 2019-20'
chg4ay1 ='Books and supplies 2019-20' 
xchg4ay2='Imputation field for chg4ay2 - Books and supplies 2020-21'
chg4ay2 ='Books and supplies 2020-21' 
xchg4ay3='Imputation field for chg4ay3 - Books and supplies 2021-22'
chg4ay3 ='Books and supplies 2021-22' 
xchg5ay0='Imputation field for chg5ay0 - On campus, room and board 2018-19'
chg5ay0 ='On campus, room and board 2018-19' 
xchg5ay1='Imputation field for chg5ay1 - On campus, room and board 2019-20'
chg5ay1 ='On campus, room and board 2019-20' 
xchg5ay2='Imputation field for chg5ay2 - On campus, room and board 2020-21'
chg5ay2 ='On campus, room and board 2020-21' 
xchg5ay3='Imputation field for chg5ay3 - On campus, room and board 2021-22'
chg5ay3 ='On campus, room and board 2021-22' 
xchg6ay0='Imputation field for chg6ay0 - On campus, other expenses 2018-19'
chg6ay0 ='On campus, other expenses 2018-19' 
xchg6ay1='Imputation field for chg6ay1 - On campus, other expenses 2019-20'
chg6ay1 ='On campus, other expenses 2019-20' 
xchg6ay2='Imputation field for chg6ay2 - On campus, other expenses 2020-21'
chg6ay2 ='On campus, other expenses 2020-21' 
xchg6ay3='Imputation field for chg6ay3 - On campus, other expenses 2021-22'
chg6ay3 ='On campus, other expenses 2021-22' 
xchg7ay0='Imputation field for chg7ay0 - Off campus (not with family), room and board 2018-19'
chg7ay0 ='Off campus (not with family), room and board 2018-19' 
xchg7ay1='Imputation field for chg7ay1 - Off campus (not with family), room and board 2019-20'
chg7ay1 ='Off campus (not with family), room and board 2019-20' 
xchg7ay2='Imputation field for chg7ay2 - Off campus (not with family), room and board 2020-21'
chg7ay2 ='Off campus (not with family), room and board 2020-21' 
xchg7ay3='Imputation field for chg7ay3 - Off campus (not with family), room and board 2021-22'
chg7ay3 ='Off campus (not with family), room and board 2021-22' 
xchg8ay0='Imputation field for chg8ay0 - Off campus (not with family), other expenses 2018-19'
chg8ay0 ='Off campus (not with family), other expenses 2018-19' 
xchg8ay1='Imputation field for chg8ay1 - Off campus (not with family), other expenses 2019-20'
chg8ay1 ='Off campus (not with family), other expenses 2019-20' 
xchg8ay2='Imputation field for chg8ay2 - Off campus (not with family), other expenses 2020-21'
chg8ay2 ='Off campus (not with family), other expenses 2020-21' 
xchg8ay3='Imputation field for chg8ay3 - Off campus (not with family), other expenses 2021-22'
chg8ay3 ='Off campus (not with family), other expenses 2021-22' 
xchg9ay0='Imputation field for chg9ay0 - Off campus (with family), other expenses 2018-19'
chg9ay0 ='Off campus (with family), other expenses 2018-19' 
xchg9ay1='Imputation field for chg9ay1 - Off campus (with family), other expenses 2019-20'
chg9ay1 ='Off campus (with family), other expenses 2019-20' 
xchg9ay2='Imputation field for chg9ay2 - Off campus (with family), other expenses 2020-21'
chg9ay2 ='Off campus (with family), other expenses 2020-21' 
xchg9ay3='Imputation field for chg9ay3 - Off campus (with family), other expenses 2021-22'
chg9ay3 ='Off campus (with family), other expenses 2021-22';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xtuit1   xfee1    xhrchg1  xtuit2   xfee2    xhrchg2  xtuit3   xfee3    xhrchg3 
xtuit5   xfee5    xhrchg5  xtuit6   xfee6    xhrchg6  xtuit7   xfee7    xhrchg7  xispro1 
xispfe1  xospro1  xospfe1  xispro2  xispfe2  xospro2  xospfe2  xispro3  xispfe3  xospro3 
xospfe3  xispro4  xispfe4  xospro4  xospfe4  xispro5  xispfe5  xospro5  xospfe5  xispro6 
xispfe6  xospro6  xospfe6  xispro7  xispfe7  xospro7  xospfe7  xispro8  xispfe8  xospro8 
xospfe8  xispro9  xispfe9  xospro9  xospfe9  xchg1at0 xchg1af0 xchg1ay0 xchg1at1 xchg1af1
xchg1ay1 xchg1at2 xchg1af2 xchg1ay2 xchg1at3 xchg1af3 xchg1ay3 xchg2at0
xchg2af0 xchg2ay0 xchg2at1 xchg2af1 xchg2ay1 xchg2at2 xchg2af2 xchg2ay2 xchg2at3 xchg2af3
xchg2ay3 xchg3at0 xchg3af0 xchg3ay0 xchg3at1 xchg3af1 xchg3ay1 xchg3at2
xchg3af2 xchg3ay2 xchg3at3 xchg3af3 xchg3ay3 xchg4ay0 xchg4ay1 xchg4ay2
xchg4ay3 xchg5ay0 xchg5ay1 xchg5ay2 xchg5ay3 xchg6ay0 xchg6ay1 xchg6ay2 xchg6ay3 xchg7ay0
xchg7ay1 xchg7ay2 xchg7ay3 xchg8ay0 xchg8ay1 xchg8ay2 xchg8ay3 xchg9ay0 xchg9ay1 xchg9ay2
xchg9ay3  / missing;
format xtuit1  -character-xchg9ay3 $ximpflg.;

Proc Summary print n sum mean min max;
var
tuition1 fee1     hrchg1   tuition2 fee2     hrchg2   tuition3 fee3     hrchg3  
tuition5 fee5     hrchg5   tuition6 fee6     hrchg6   tuition7 fee7     hrchg7   isprof1 
ispfee1  osprof1  ospfee1  isprof2  ispfee2  osprof2  ospfee2  isprof3  ispfee3  osprof3 
ospfee3  isprof4  ispfee4  osprof4  ospfee4  isprof5  ispfee5  osprof5  ospfee5  isprof6 
ispfee6  osprof6  ospfee6  isprof7  ispfee7  osprof7  ospfee7  isprof8  ispfee8  osprof8 
ospfee8  isprof9  ispfee9  osprof9  ospfee9  chg1at0  chg1af0  chg1ay0  chg1at1  chg1af1 
chg1ay1  chg1at2  chg1af2  chg1ay2  chg1at3  chg1af3  chg1ay3  chg1tgtd chg1fgtd chg2at0 
chg2af0  chg2ay0  chg2at1  chg2af1  chg2ay1  chg2at2  chg2af2  chg2ay2  chg2at3  chg2af3 
chg2ay3  chg2tgtd chg2fgtd chg3at0  chg3af0  chg3ay0  chg3at1  chg3af1  chg3ay1  chg3at2 
chg3af2  chg3ay2  chg3at3  chg3af3  chg3ay3  chg3tgtd chg3fgtd chg4ay0  chg4ay1  chg4ay2 
chg4ay3  chg5ay0  chg5ay1  chg5ay2  chg5ay3  chg6ay0  chg6ay1  chg6ay2  chg6ay3  chg7ay0 
chg7ay1  chg7ay2  chg7ay3  chg8ay0  chg8ay1  chg8ay2  chg8ay3  chg9ay0  chg9ay1  chg9ay2 
chg9ay3  ;
run;
