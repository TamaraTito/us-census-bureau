%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let file=ic2021_campuses;
%let libraryName=IPEDICM;
libname &libraryName "&path/SASData";

proc format library=&libraryName;

value $pcstabbr  
'AL'='Alabama' 
'AZ'='Arizona' 
'AR'='Arkansas' 
'CA'='California' 
'CO'='Colorado' 
'CT'='Connecticut' 
'DE'='Delaware' 
'DC'='District of Columbia' 
'FL'='Florida' 
'GA'='Georgia' 
'HI'='Hawaii' 
'ID'='Idaho' 
'IL'='Illinois' 
'IN'='Indiana' 
'KS'='Kansas' 
'KY'='Kentucky' 
'LA'='Louisiana' 
'ME'='Maine' 
'MD'='Maryland' 
'MA'='Massachusetts' 
'MI'='Michigan' 
'MN'='Minnesota' 
'MS'='Mississippi' 
'MO'='Missouri' 
'MT'='Montana' 
'NV'='Nevada' 
'NJ'='New Jersey' 
'NY'='New York' 
'NC'='North Carolina' 
'ND'='North Dakota' 
'OH'='Ohio' 
'OK'='Oklahoma' 
'PA'='Pennsylvania' 
'RI'='Rhode Island' 
'SC'='South Carolina' 
'TN'='Tennessee' 
'TX'='Texas' 
'UT'='Utah' 
'VT'='Vermont' 
'VA'='Virginia' 
'WA'='Washington' 
'WI'='Wisconsin' 
'PR'='Puerto Rico' 
'VI'='Virgin Islands';
value pcfips    
1='Alabama' 
4='Arizona' 
5='Arkansas' 
6='California' 
8='Colorado' 
9='Connecticut' 
10='Delaware' 
11='District of Columbia' 
12='Florida' 
13='Georgia' 
15='Hawaii' 
16='Idaho' 
17='Illinois' 
18='Indiana' 
20='Kansas' 
21='Kentucky' 
22='Louisiana' 
23='Maine' 
24='Maryland' 
25='Massachusetts' 
26='Michigan' 
27='Minnesota' 
28='Mississippi' 
29='Missouri' 
30='Montana' 
32='Nevada' 
34='New Jersey' 
36='New York' 
37='North Carolina' 
38='North Dakota' 
39='Ohio' 
40='Oklahoma' 
42='Pennsylvania' 
44='Rhode Island' 
45='South Carolina' 
47='Tennessee' 
48='Texas' 
49='Utah' 
50='Vermont' 
51='Virginia' 
53='Washington' 
55='Wisconsin' 
72='Puerto Rico' 
78='Virgin Islands';
value pcobereg  
1='New England (CT, ME, MA, NH, RI, VT)' 
2='Mid East (DE, DC, MD, NJ, NY, PA)' 
3='Great Lakes (IL, IN, MI, OH, WI)' 
4='Plains (IA, KS, MN, MO, NE, ND, SD)' 
5='Southeast (AL, AR, FL, GA, KY, LA, MS, NC, SC, TN, VA, WV)' 
6='Southwest (AZ, NM, OK, TX)' 
7='Rocky Mountains (CO, ID, MT, UT, WY)' 
8='Far West (AK, CA, HI, NV, OR, WA)' 
9='Other U.S. jurisdictions (AS, FM, GU, MH, MP, PR, PW, VI)';
value pcopeflag 
1='Participates in Title IV federal financial aid programs' 
2='Branch campus of a main campus that participates in Title IV' 
5='Not currently participating in Title IV, has an OPE ID number';
value pcsector  
1='Public, 4-year or above' 
2='Private not-for-profit, 4-year or above' 
3='Private for-profit, 4-year or above' 
4='Public, 2-year' 
5='Private not-for-profit, 2-year' 
6='Private for-profit, 2-year' 
7='Public, less-than 2-year' 
8='Private not-for-profit, less-than 2-year' 
9='Private for-profit, less-than 2-year';
value pciclevel 
1='Four or more years' 
2='At least 2 but less than 4 years' 
3='Less than 2 years (below associate)';
value pccontrol 
1='Public' 
2='Private not-for-profit' 
3='Private for-profit';
value pchloffer 
1='Award of less than one academic year' 
2='At least 1, but less than 2 academic yrs' 
3='Associate^s degree' 
4='At least 2, but less than 4 academic yrs' 
5='Bachelor^s degree' 
6='Postbaccalaureate certificate' 
7='Master^s degree' 
8='Post-master^s certificate' 
9='Doctor^s degree';
value pcugoffer 
1='Undergraduate degree or certificate offering' 
2='No undergraduate offering';
value pcgroffer 
1='Graduate degree or certificate offering' 
2='No graduate offering';
value pchdegofr1f
11='Doctor^s degree - research/scholarship and professional practice' 
12='Doctor^s degree - research/scholarship' 
13='Doctor^s degree -  professional practice' 
14='Doctor^s degree - other' 
20='Master^s degree' 
30='Bachelor^s degree' 
40='Associate^s degree' 
0='Non-degree granting';
value pcdeggrant
1='Degree-granting' 
2='Nondegree-granting, primarily postsecondary';
value pchbcu    
1='Yes' 
2='No';
value pctribal  
1='Yes' 
2='No';
value pclocale  
11='City: Large' 
12='City: Midsize' 
13='City: Small' 
21='Suburb: Large' 
22='Suburb: Midsize' 
23='Suburb: Small' 
31='Town: Fringe' 
32='Town: Distant' 
33='Town: Remote' 
41='Rural: Fringe' 
42='Rural: Distant' 
43='Rural: Remote';
value pcopenpubl
1='Institution is open to the public';
value $pcact     
'A'='Active - institution active' 
'N'='New (active) - added during the current year';
value pccyactive
1='Yes';
value pcpostsec 
1='Primarily postsecondary institution' 
2='Not primarily postsecondary';
value pcpseflag 
1='Active postsecondary institution' 
2='Not primarily postsecondary or open to public';
value pcpset4flg
1='Title IV postsecondary institution' 
2='Non-Title IV postsecondary institution' 
3='Title IV NOT primarily postsecondary institution';
value pccbsa    
10380='Aguadilla-Isabela, PR' 
10420='Akron, OH' 
10500='Albany, GA' 
10580='Albany-Schenectady-Troy, NY' 
10900='Allentown-Bethlehem-Easton, PA-NJ' 
11020='Altoona, PA' 
11100='Amarillo, TX' 
11640='Arecibo, PR' 
11700='Asheville, NC' 
12060='Atlanta-Sandy Springs-Alpharetta, GA' 
12260='Augusta-Richmond County, GA-SC' 
12420='Austin-Round Rock-Georgetown, TX' 
12540='Bakersfield, CA' 
12580='Baltimore-Columbia-Towson, MD' 
12620='Bangor, ME' 
12940='Baton Rouge, LA' 
13740='Billings, MT' 
13980='Blacksburg-Christiansburg, VA' 
14020='Bloomington, IN' 
14460='Boston-Cambridge-Newton, MA-NH' 
15380='Buffalo-Cheektowaga, NY' 
15620='Cadillac, MI' 
15980='Cape Coral-Fort Myers, FL' 
16540='Chambersburg-Waynesboro, PA' 
16700='Charleston-North Charleston, SC' 
16740='Charlotte-Concord-Gastonia, NC-SC' 
16980='Chicago-Naperville-Elgin, IL-IN-WI' 
17140='Cincinnati, OH-KY-IN' 
17340='Clearlake, CA' 
17460='Cleveland-Elyria, OH' 
17660='Coeur d^Alene, ID' 
17700='Coffeyville, KS' 
17740='Coldwater, MI' 
17780='College Station-Bryan, TX' 
17900='Columbia, SC' 
17980='Columbus, GA-AL' 
18020='Columbus, IN' 
18140='Columbus, OH' 
19100='Dallas-Fort Worth-Arlington, TX' 
19430='Dayton-Kettering, OH' 
19620='Del Rio, TX' 
19660='Deltona-Daytona Beach-Ormond Beach, FL' 
19740='Denver-Aurora-Lakewood, CO' 
19820='Detroit-Warren-Dearborn, MI' 
20020='Dothan, AL' 
20140='Dublin, GA' 
20180='DuBois, PA' 
20260='Duluth, MN-WI' 
20700='East Stroudsburg, PA' 
20940='El Centro, CA' 
21500='Erie, PA' 
21780='Evansville, IN-KY' 
22020='Fargo, ND-MN' 
22060='Faribault-Northfield, MN' 
22220='Fayetteville-Springdale-Rogers, AR' 
22300='Findlay, OH' 
22420='Flint, MI' 
22780='Fort Leonard Wood, MO' 
22900='Fort Smith, AR-OK' 
23060='Fort Wayne, IN' 
23300='Freeport, IL' 
23420='Fresno, CA' 
23900='Gettysburg, PA' 
24220='Grand Forks, ND-MN' 
24340='Grand Rapids-Kentwood, MI' 
24660='Greensboro-High Point, NC' 
24860='Greenville-Anderson, SC' 
25020='Guayama, PR' 
25260='Hanford-Corcoran, CA' 
25420='Harrisburg-Carlisle, PA' 
25540='Hartford-East Hartford-Middletown, CT' 
25620='Hattiesburg, MS' 
25980='Hinesville, GA' 
26420='Houston-The Woodlands-Sugar Land, TX' 
26620='Huntsville, AL' 
26900='Indianapolis-Carmel-Anderson, IN' 
27100='Jackson, MI' 
27140='Jackson, MS' 
27260='Jacksonville, FL' 
27620='Jefferson City, MO' 
28020='Kalamazoo-Portage, MI' 
28140='Kansas City, MO-KS' 
28420='Kennewick-Richland, WA' 
28740='Kingston, NY' 
28940='Knoxville, TN' 
29020='Kokomo, IN' 
29200='Lafayette-West Lafayette, IN' 
29420='Lake Havasu City-Kingman, AZ' 
29460='Lakeland-Winter Haven, FL' 
29540='Lancaster, PA' 
29620='Lansing-East Lansing, MI' 
29820='Las Vegas-Henderson-Paradise, NV' 
30020='Lawton, OK' 
30140='Lebanon, PA' 
30620='Lima, OH' 
30780='Little Rock-North Little Rock-Conway, AR' 
31080='Los Angeles-Long Beach-Anaheim, CA' 
31140='Louisville/Jefferson County, KY-IN' 
31420='Macon-Bibb County, GA' 
31500='Madison, IN' 
31860='Mankato, MN' 
32420='Mayagüez, PR' 
32580='McAllen-Edinburg-Mission, TX' 
32820='Memphis, TN-MS-AR' 
33100='Miami-Fort Lauderdale-Pompano Beach, FL' 
33220='Midland, MI' 
33340='Milwaukee-Waukesha, WI' 
33460='Minneapolis-St. Paul-Bloomington, MN-WI' 
33500='Minot, ND' 
33540='Missoula, MT' 
33700='Modesto, CA' 
33860='Montgomery, AL' 
34620='Muncie, IN' 
34740='Muskegon, MI' 
34900='Napa, CA' 
34980='Nashville-Davidson--Murfreesboro--Franklin, TN' 
35020='Natchez, MS-LA' 
35260='New Castle, PA' 
35300='New Haven-Milford, CT' 
35380='New Orleans-Metairie, LA' 
35620='New York-Newark-Jersey City, NY-NJ-PA' 
35840='North Port-Sarasota-Bradenton, FL' 
35980='Norwich-New London, CT' 
36020='Oak Harbor, WA' 
36460='Olean, NY' 
36740='Orlando-Kissimmee-Sanford, FL' 
37100='Oxnard-Thousand Oaks-Ventura, CA' 
37340='Palm Bay-Melbourne-Titusville, FL' 
37980='Philadelphia-Camden-Wilmington, PA-NJ-DE-MD' 
38060='Phoenix-Mesa-Chandler, AZ' 
38300='Pittsburgh, PA' 
38660='Ponce, PR' 
38860='Portland-South Portland, ME' 
38900='Portland-Vancouver-Hillsboro, OR-WA' 
38940='Port St. Lucie, FL' 
39060='Pottsville, PA' 
39100='Poughkeepsie-Newburgh-Middletown, NY' 
39150='Prescott Valley-Prescott, AZ' 
39300='Providence-Warwick, RI-MA' 
39340='Provo-Orem, UT' 
39460='Punta Gorda, FL' 
39540='Racine, WI' 
39580='Raleigh-Cary, NC' 
39740='Reading, PA' 
39860='Red Wing, MN' 
39980='Richmond, IN' 
40060='Richmond, VA' 
40140='Riverside-San Bernardino-Ontario, CA' 
40220='Roanoke, VA' 
40340='Rochester, MN' 
40380='Rochester, NY' 
40620='Rolla, MO' 
40900='Sacramento-Roseville-Folsom, CA' 
40940='Safford, AZ' 
41100='St. George, UT' 
41180='St. Louis, MO-IL' 
41220='St. Marys, GA' 
41400='Salem, OH' 
41540='Salisbury, MD-DE' 
41620='Salt Lake City, UT' 
41700='San Antonio-New Braunfels, TX' 
41740='San Diego-Chula Vista-Carlsbad, CA' 
41860='San Francisco-Oakland-Berkeley, CA' 
41940='San Jose-Sunnyvale-Santa Clara, CA' 
41980='San Juan-Bayamón-Caguas, PR' 
42020='San Luis Obispo-Paso Robles, CA' 
42340='Savannah, GA' 
42540='Scranton--Wilkes-Barre, PA' 
42660='Seattle-Tacoma-Bellevue, WA' 
43420='Sierra Vista-Douglas, AZ' 
43780='South Bend-Mishawaka, IN-MI' 
44060='Spokane-Spokane Valley, WA' 
44140='Springfield, MA' 
44180='Springfield, MO' 
44300='State College, PA' 
44340='Statesboro, GA' 
44700='Stockton, CA' 
45060='Syracuse, NY' 
45140='Tahlequah, OK' 
45220='Tallahassee, FL' 
45300='Tampa-St. Petersburg-Clearwater, FL' 
45460='Terre Haute, IN' 
45500='Texarkana, TX-AR' 
45820='Topeka, KS' 
45900='Traverse City, MI' 
45940='Trenton-Princeton, NJ' 
45980='Troy, AL' 
46060='Tucson, AZ' 
46520='Urban Honolulu, HI' 
46540='Utica-Rome, NY' 
46620='Uvalde, TX' 
46660='Valdosta, GA' 
46700='Vallejo, CA' 
47180='Vincennes, IN' 
47260='Virginia Beach-Norfolk-Newport News, VA-NC' 
47580='Warner Robins, GA' 
47900='Washington-Arlington-Alexandria, DC-VA-MD-WV' 
48060='Watertown-Fort Drum, NY' 
48140='Wausau-Weston, WI' 
49340='Worcester, MA-CT' 
49500='Yauco, PR' 
49620='York-Hanover, PA' 
49660='Youngstown-Warren-Boardman, OH-PA' 
49740='Yuma, AZ' 
-2='Not applicable';
value pccbsatype
1='Metropolitan Statistical Area' 
2='Micropolitan Statistical Area' 
-2='Not applicable';
value pccsa     
104='Albany-Schenectady, NY' 
107='Altoona-Huntingdon, PA' 
108='Amarillo-Pampa-Borger, TX' 
120='Asheville-Marion-Brevard, NC' 
122='Atlanta--Athens-Clarke County--Sandy Springs, GA-AL' 
144='Bloomington-Bedford, IN' 
148='Boston-Worcester-Providence, MA-RI-NH-CT' 
160='Buffalo-Cheektowaga-Olean, NY' 
163='Cape Coral-Fort Myers-Naples, FL' 
172='Charlotte-Concord, NC-SC' 
176='Chicago-Naperville, IL-IN-WI' 
178='Cincinnati-Wilmington-Maysville, OH-KY-IN' 
184='Cleveland-Akron-Canton, OH' 
192='Columbia-Orangeburg-Newberry, SC' 
194='Columbus-Auburn-Opelika, GA-AL' 
198='Columbus-Marion-Zanesville, OH' 
206='Dallas-Fort Worth, TX-OK' 
212='Dayton-Springfield-Kettering, OH' 
216='Denver-Aurora, CO' 
220='Detroit-Warren-Ann Arbor, MI' 
222='Dothan-Ozark, AL' 
240='Erie-Meadville, PA' 
244='Fargo-Wahpeton, ND-MN' 
258='Fort Wayne-Huntington-Auburn, IN' 
260='Fresno-Madera-Hanford, CA' 
266='Grand Rapids-Kentwood-Muskegon, MI' 
268='Greensboro--Winston-Salem--High Point, NC' 
273='Greenville-Spartanburg-Anderson, SC' 
276='Harrisburg-York-Lebanon, PA' 
278='Hartford-East Hartford, CT' 
279='Hattiesburg-Laurel, MS' 
288='Houston-The Woodlands, TX' 
290='Huntsville-Decatur, AL' 
294='Indianapolis-Carmel-Muncie, IN' 
298='Jackson-Vicksburg-Brookhaven, MS' 
300='Jacksonville-St. Marys-Palatka, FL-GA' 
310='Kalamazoo-Battle Creek-Portage, MI' 
312='Kansas City-Overland Park-Kansas City, MO-KS' 
313='Kennewick-Richland-Walla Walla, WA' 
315='Knoxville-Morristown-Sevierville, TN' 
316='Kokomo-Peru, IN' 
320='Lafayette-West Lafayette-Frankfort, IN' 
332='Las Vegas-Henderson, NV' 
338='Lima-Van Wert-Celina, OH' 
340='Little Rock-North Little Rock, AR' 
348='Los Angeles-Long Beach, CA' 
350='Louisville/Jefferson County--Elizabethtown--Bardstown, KY-IN' 
356='Macon-Bibb County--Warner Robins, GA' 
359='Mankato-New Ulm, MN' 
364='Mayagüez-San Germán, PR' 
365='McAllen-Edinburg, TX' 
368='Memphis-Forrest City, TN-MS-AR' 
370='Miami-Port St. Lucie-Fort Lauderdale, FL' 
376='Milwaukee-Racine-Waukesha, WI' 
378='Minneapolis-St. Paul, MN-WI' 
388='Montgomery-Selma-Alexander City, AL' 
400='Nashville-Davidson--Murfreesboro, TN' 
406='New Orleans-Metairie-Hammond, LA-MS' 
408='New York-Newark, NY-NJ-CT-PA' 
412='North Port-Sarasota, FL' 
422='Orlando-Lakeland-Deltona, FL' 
428='Philadelphia-Reading-Camden, PA-NJ-DE-MD' 
429='Phoenix-Mesa, AZ' 
430='Pittsburgh-New Castle-Weirton, PA-OH-WV' 
434='Ponce-Yauco-Coamo, PR' 
438='Portland-Lewiston-South Portland, ME' 
440='Portland-Vancouver-Salem, OR-WA' 
450='Raleigh-Durham-Cary, NC' 
458='Richmond-Connersville, IN' 
462='Rochester-Austin, MN' 
464='Rochester-Batavia-Seneca Falls, NY' 
466='Rockford-Freeport-Rochelle, IL' 
472='Sacramento-Roseville, CA' 
474='Saginaw-Midland-Bay City, MI' 
476='St. Louis-St. Charles-Farmington, MO-IL' 
480='Salisbury-Cambridge, MD-DE' 
482='Salt Lake City-Provo-Orem, UT' 
484='San Antonio-New Braunfels-Pearsall, TX' 
488='San Jose-San Francisco-Oakland, CA' 
490='San Juan-Bayamón, PR' 
496='Savannah-Hinesville-Statesboro, GA' 
500='Seattle-Tacoma, WA' 
515='South Bend-Elkhart-Mishawaka, IN-MI' 
518='Spokane-Spokane Valley-Coeur d^Alene, WA-ID' 
524='State College-DuBois, PA' 
532='Syracuse-Auburn, NY' 
534='Toledo-Findlay-Tiffin, OH' 
536='Tucson-Nogales, AZ' 
545='Virginia Beach-Norfolk, VA-NC' 
548='Washington-Baltimore-Arlington, DC-MD-VA-WV-PA' 
554='Wausau-Stevens Point-Wisconsin Rapids, WI' 
566='Youngstown-Warren, OH-PA' 
-2='Not applicable';
value pcnecta   
70750='Bangor, ME' 
71650='Boston-Cambridge-Newton, MA-NH' 
73300='Greenfield Town, MA' 
73450='Hartford-East Hartford-Middletown, CT' 
75700='New Haven, CT' 
76450='Norwich-New London-Westerly, CT-RI' 
77200='Providence-Warwick, RI-MA' 
78100='Springfield, MA-CT' 
78700='Waterbury, CT' 
79600='Worcester, MA-CT' 
-2='Not applicable';
value pccountycd
1069='Houston County, AL' 
1089='Madison County, AL' 
1101='Montgomery County, AL' 
1109='Pike County, AL' 
1113='Russell County, AL' 
4003='Cochise County, AZ' 
4009='Graham County, AZ' 
4013='Maricopa County, AZ' 
4015='Mohave County, AZ' 
4019='Pima County, AZ' 
4021='Pinal County, AZ' 
4025='Yavapai County, AZ' 
4027='Yuma County, AZ' 
5007='Benton County, AR' 
5091='Miller County, AR' 
5119='Pulaski County, AR' 
5131='Sebastian County, AR' 
5143='Washington County, AR' 
6001='Alameda County, CA' 
6019='Fresno County, CA' 
6025='Imperial County, CA' 
6029='Kern County, CA' 
6031='Kings County, CA' 
6033='Lake County, CA' 
6037='Los Angeles County, CA' 
6055='Napa County, CA' 
6059='Orange County, CA' 
6061='Placer County, CA' 
6065='Riverside County, CA' 
6067='Sacramento County, CA' 
6071='San Bernardino County, CA' 
6073='San Diego County, CA' 
6077='San Joaquin County, CA' 
6079='San Luis Obispo County, CA' 
6085='Santa Clara County, CA' 
6095='Solano County, CA' 
6099='Stanislaus County, CA' 
6111='Ventura County, CA' 
8005='Arapahoe County, CO' 
8011='Bent County, CO' 
8031='Denver County, CO' 
8089='Otero County, CO' 
9003='Hartford County, CT' 
9009='New Haven County, CT' 
9011='New London County, CT' 
10003='New Castle County, DE' 
10005='Sussex County, DE' 
11001='District of Columbia, DC' 
12009='Brevard County, FL' 
12011='Broward County, FL' 
12015='Charlotte County, FL' 
12031='Duval County, FL' 
12053='Hernando County, FL' 
12057='Hillsborough County, FL' 
12069='Lake County, FL' 
12071='Lee County, FL' 
12073='Leon County, FL' 
12086='Miami-Dade County, FL' 
12095='Orange County, FL' 
12097='Osceola County, FL' 
12099='Palm Beach County, FL' 
12101='Pasco County, FL' 
12103='Pinellas County, FL' 
12105='Polk County, FL' 
12111='St. Lucie County, FL' 
12115='Sarasota County, FL' 
12117='Seminole County, FL' 
12127='Volusia County, FL' 
13021='Bibb County, GA' 
13031='Bulloch County, GA' 
13039='Camden County, GA' 
13051='Chatham County, GA' 
13053='Chattahoochee County, GA' 
13063='Clayton County, GA' 
13067='Cobb County, GA' 
13073='Columbia County, GA' 
13089='DeKalb County, GA' 
13091='Dodge County, GA' 
13095='Dougherty County, GA' 
13113='Fayette County, GA' 
13121='Fulton County, GA' 
13135='Gwinnett County, GA' 
13153='Houston County, GA' 
13175='Laurens County, GA' 
13179='Liberty County, GA' 
13185='Lowndes County, GA' 
13211='Morgan County, GA' 
13215='Muscogee County, GA' 
13231='Pike County, GA' 
13245='Richmond County, GA' 
13303='Washington County, GA' 
15003='Honolulu County, HI' 
16055='Kootenai County, ID' 
17031='Cook County, IL' 
17089='Kane County, IL' 
17097='Lake County, IL' 
17111='McHenry County, IL' 
17177='Stephenson County, IL' 
17197='Will County, IL' 
18003='Allen County, IN' 
18005='Bartholomew County, IN' 
18019='Clark County, IN' 
18035='Delaware County, IN' 
18043='Floyd County, IN' 
18059='Hancock County, IN' 
18063='Hendricks County, IN' 
18067='Howard County, IN' 
18077='Jefferson County, IN' 
18083='Knox County, IN' 
18089='Lake County, IN' 
18095='Madison County, IN' 
18097='Marion County, IN' 
18105='Monroe County, IN' 
18141='St. Joseph County, IN' 
18157='Tippecanoe County, IN' 
18163='Vanderburgh County, IN' 
18167='Vigo County, IN' 
18177='Wayne County, IN' 
20021='Cherokee County, KS' 
20091='Johnson County, KS' 
20125='Montgomery County, KS' 
20177='Shawnee County, KS' 
21015='Boone County, KY' 
21111='Jefferson County, KY' 
21185='Oldham County, KY' 
22005='Ascension Parish, LA' 
22033='East Baton Rouge Parish, LA' 
22051='Jefferson Parish, LA' 
22071='Orleans Parish, LA' 
22089='St. Charles Parish, LA' 
23005='Cumberland County, ME' 
23019='Penobscot County, ME' 
23029='Washington County, ME' 
24003='Anne Arundel County, MD' 
24005='Baltimore County, MD' 
24015='Cecil County, MD' 
24031='Montgomery County, MD' 
24033='Prince George^s County, MD' 
25009='Essex County, MA' 
25011='Franklin County, MA' 
25013='Hampden County, MA' 
25023='Plymouth County, MA' 
25027='Worcester County, MA' 
26013='Baraga County, MI' 
26023='Branch County, MI' 
26045='Eaton County, MI' 
26049='Genesee County, MI' 
26055='Grand Traverse County, MI' 
26065='Ingham County, MI' 
26067='Ionia County, MI' 
26075='Jackson County, MI' 
26077='Kalamazoo County, MI' 
26081='Kent County, MI' 
26099='Macomb County, MI' 
26111='Midland County, MI' 
26121='Muskegon County, MI' 
26125='Oakland County, MI' 
26139='Ottawa County, MI' 
26147='St. Clair County, MI' 
26155='Shiawassee County, MI' 
26163='Wayne County, MI' 
26165='Wexford County, MI' 
27003='Anoka County, MN' 
27005='Becker County, MN' 
27013='Blue Earth County, MN' 
27027='Clay County, MN' 
27037='Dakota County, MN' 
27049='Goodhue County, MN' 
27053='Hennepin County, MN' 
27059='Isanti County, MN' 
27109='Olmsted County, MN' 
27119='Polk County, MN' 
27131='Rice County, MN' 
27137='St. Louis County, MN' 
27159='Wadena County, MN' 
27163='Washington County, MN' 
28001='Adams County, MS' 
28035='Forrest County, MS' 
28049='Hinds County, MS' 
28127='Simpson County, MS' 
29051='Cole County, MO' 
29077='Greene County, MO' 
29095='Jackson County, MO' 
29131='Miller County, MO' 
29161='Phelps County, MO' 
29169='Pulaski County, MO' 
29189='St. Louis County, MO' 
30063='Missoula County, MT' 
30111='Yellowstone County, MT' 
32003='Clark County, NV' 
34003='Bergen County, NJ' 
34005='Burlington County, NJ' 
34013='Essex County, NJ' 
34021='Mercer County, NJ' 
34025='Monmouth County, NJ' 
34031='Passaic County, NJ' 
36009='Cattaraugus County, NY' 
36027='Dutchess County, NY' 
36029='Erie County, NY' 
36039='Greene County, NY' 
36045='Jefferson County, NY' 
36047='Kings County, NY' 
36055='Monroe County, NY' 
36061='New York County, NY' 
36065='Oneida County, NY' 
36067='Onondaga County, NY' 
36081='Queens County, NY' 
36085='Richmond County, NY' 
36093='Schenectady County, NY' 
36105='Sullivan County, NY' 
36111='Ulster County, NY' 
36119='Westchester County, NY' 
37021='Buncombe County, NC' 
37025='Cabarrus County, NC' 
37071='Gaston County, NC' 
37081='Guilford County, NC' 
37119='Mecklenburg County, NC' 
37183='Wake County, NC' 
38101='Ward County, ND' 
39003='Allen County, OH' 
39029='Columbiana County, OH' 
39035='Cuyahoga County, OH' 
39041='Delaware County, OH' 
39045='Fairfield County, OH' 
39049='Franklin County, OH' 
39057='Greene County, OH' 
39061='Hamilton County, OH' 
39063='Hancock County, OH' 
39153='Summit County, OH' 
40001='Adair County, OK' 
40021='Cherokee County, OK' 
40031='Comanche County, OK' 
40093='Major County, OK' 
40135='Sequoyah County, OK' 
42001='Adams County, PA' 
42003='Allegheny County, PA' 
42007='Beaver County, PA' 
42011='Berks County, PA' 
42013='Blair County, PA' 
42017='Bucks County, PA' 
42027='Centre County, PA' 
42029='Chester County, PA' 
42033='Clearfield County, PA' 
42041='Cumberland County, PA' 
42043='Dauphin County, PA' 
42045='Delaware County, PA' 
42049='Erie County, PA' 
42051='Fayette County, PA' 
42055='Franklin County, PA' 
42069='Lackawanna County, PA' 
42071='Lancaster County, PA' 
42073='Lawrence County, PA' 
42075='Lebanon County, PA' 
42077='Lehigh County, PA' 
42079='Luzerne County, PA' 
42085='Mercer County, PA' 
42089='Monroe County, PA' 
42091='Montgomery County, PA' 
42101='Philadelphia County, PA' 
42107='Schuylkill County, PA' 
42129='Westmoreland County, PA' 
42133='York County, PA' 
44007='Providence County, RI' 
45019='Charleston County, SC' 
45045='Greenville County, SC' 
45063='Lexington County, SC' 
45079='Richland County, SC' 
47013='Campbell County, TN' 
47037='Davidson County, TN' 
47093='Knox County, TN' 
47157='Shelby County, TN' 
48029='Bexar County, TX' 
48041='Brazos County, TX' 
48113='Dallas County, TX' 
48157='Fort Bend County, TX' 
48201='Harris County, TX' 
48215='Hidalgo County, TX' 
48375='Potter County, TX' 
48439='Tarrant County, TX' 
48453='Travis County, TX' 
48463='Uvalde County, TX' 
48465='Val Verde County, TX' 
49035='Salt Lake County, UT' 
49041='Sevier County, UT' 
49049='Utah County, UT' 
49053='Washington County, UT' 
50005='Caledonia County, VT' 
51041='Chesterfield County, VA' 
51087='Henrico County, VA' 
51107='Loudoun County, VA' 
51121='Montgomery County, VA' 
51153='Prince William County, VA' 
51179='Stafford County, VA' 
51510='Alexandria City, VA' 
51650='Hampton City, VA' 
51700='Newport News City, VA' 
51710='Norfolk City, VA' 
51770='Roanoke City, VA' 
51810='Virginia Beach City, VA' 
53005='Benton County, WA' 
53011='Clark County, WA' 
53029='Island County, WA' 
53033='King County, WA' 
53061='Snohomish County, WA' 
53063='Spokane County, WA' 
55073='Marathon County, WI' 
55079='Milwaukee County, WI' 
55101='Racine County, WI' 
72005='Aguadilla Municipio, PR' 
72013='Arecibo Municipio, PR' 
72021='Bayam�n Municipio, PR' 
72025='Caguas Municipio, PR' 
72031='Carolina Municipio, PR' 
72053='Fajardo Municipio, PR' 
72057='Guayama Municipio, PR' 
72071='Isabela Municipio, PR' 
72091='Manat�  Municipio, PR' 
72097='Mayag�ez Municipio, PR' 
72099='Moca Municipio, PR' 
72103='Naguabo Municipio, PR' 
72113='Ponce Municipio, PR' 
72119='R�o Grande Municipio, PR' 
72127='San Juan Municipio, PR' 
72153='Yauco Municipio, PR' 
78010='St. Croix Island, VI';
value pccngdstcd
102='AL, District 02' 
103='AL, District 03' 
105='AL, District 05' 
401='AZ, District 01' 
402='AZ, District 02' 
403='AZ, District 03' 
404='AZ, District 04' 
405='AZ, District 05' 
406='AZ, District 06' 
407='AZ, District 07' 
408='AZ, District 08' 
502='AR, District 02' 
503='AR, District 03' 
504='AR, District 04' 
603='CA, District 03' 
604='CA, District 04' 
605='CA, District 05' 
606='CA, District 06' 
608='CA, District 08' 
609='CA, District 09' 
610='CA, District 10' 
613='CA, District 13' 
616='CA, District 16' 
617='CA, District 17' 
619='CA, District 19' 
621='CA, District 21' 
623='CA, District 23' 
624='CA, District 24' 
626='CA, District 26' 
627='CA, District 27' 
629='CA, District 29' 
630='CA, District 30' 
634='CA, District 34' 
636='CA, District 36' 
637='CA, District 37' 
638='CA, District 38' 
643='CA, District 43' 
644='CA, District 44' 
645='CA, District 45' 
647='CA, District 47' 
648='CA, District 48' 
649='CA, District 49' 
650='CA, District 50' 
651='CA, District 51' 
652='CA, District 52' 
801='CO, District 01' 
804='CO, District 04' 
806='CO, District 06' 
901='CT, District 01' 
902='CT, District 02' 
903='CT, District 03' 
905='CT, District 05' 
1000='DE, District 00' 
1198='DC, District 98' 
1202='FL, District 02' 
1204='FL, District 04' 
1206='FL, District 06' 
1207='FL, District 07' 
1208='FL, District 08' 
1209='FL, District 09' 
1210='FL, District 10' 
1211='FL, District 11' 
1212='FL, District 12' 
1213='FL, District 13' 
1214='FL, District 14' 
1215='FL, District 15' 
1216='FL, District 16' 
1217='FL, District 17' 
1218='FL, District 18' 
1219='FL, District 19' 
1220='FL, District 20' 
1223='FL, District 23' 
1224='FL, District 24' 
1225='FL, District 25' 
1226='FL, District 26' 
1227='FL, District 27' 
1301='GA, District 01' 
1302='GA, District 02' 
1303='GA, District 03' 
1304='GA, District 04' 
1305='GA, District 05' 
1306='GA, District 06' 
1307='GA, District 07' 
1308='GA, District 08' 
1310='GA, District 10' 
1311='GA, District 11' 
1312='GA, District 12' 
1313='GA, District 13' 
1501='HI, District 01' 
1601='ID, District 01' 
1701='IL, District 01' 
1702='IL, District 02' 
1704='IL, District 04' 
1707='IL, District 07' 
1708='IL, District 08' 
1711='IL, District 11' 
1714='IL, District 14' 
1717='IL, District 17' 
1801='IN, District 01' 
1802='IN, District 02' 
1803='IN, District 03' 
1804='IN, District 04' 
1805='IN, District 05' 
1806='IN, District 06' 
1807='IN, District 07' 
1808='IN, District 08' 
1809='IN, District 09' 
2002='KS, District 02' 
2003='KS, District 03' 
2103='KY, District 03' 
2104='KY, District 04' 
2201='LA, District 01' 
2202='LA, District 02' 
2206='LA, District 06' 
2301='ME, District 01' 
2302='ME, District 02' 
2401='MD, District 01' 
2402='MD, District 02' 
2404='MD, District 04' 
2408='MD, District 08' 
2501='MA, District 01' 
2502='MA, District 02' 
2503='MA, District 03' 
2508='MA, District 08' 
2601='MI, District 01' 
2602='MI, District 02' 
2603='MI, District 03' 
2604='MI, District 04' 
2605='MI, District 05' 
2606='MI, District 06' 
2607='MI, District 07' 
2608='MI, District 08' 
2609='MI, District 09' 
2610='MI, District 10' 
2611='MI, District 11' 
2613='MI, District 13' 
2614='MI, District 14' 
2701='MN, District 01' 
2702='MN, District 02' 
2703='MN, District 03' 
2704='MN, District 04' 
2706='MN, District 06' 
2707='MN, District 07' 
2708='MN, District 08' 
2803='MS, District 03' 
2804='MS, District 04' 
2901='MO, District 01' 
2903='MO, District 03' 
2904='MO, District 04' 
2905='MO, District 05' 
2907='MO, District 07' 
2908='MO, District 08' 
3000='MT, District 00' 
3204='NV, District 04' 
3403='NJ, District 03' 
3406='NJ, District 06' 
3409='NJ, District 09' 
3410='NJ, District 10' 
3412='NJ, District 12' 
3605='NY, District 05' 
3607='NY, District 07' 
3609='NY, District 09' 
3610='NY, District 10' 
3611='NY, District 11' 
3618='NY, District 18' 
3619='NY, District 19' 
3620='NY, District 20' 
3621='NY, District 21' 
3622='NY, District 22' 
3623='NY, District 23' 
3624='NY, District 24' 
3625='NY, District 25' 
3626='NY, District 26' 
3627='NY, District 27' 
3704='NC, District 04' 
3708='NC, District 08' 
3709='NC, District 09' 
3710='NC, District 10' 
3712='NC, District 12' 
3713='NC, District 13' 
3800='ND, District 00' 
3901='OH, District 01' 
3902='OH, District 02' 
3904='OH, District 04' 
3905='OH, District 05' 
3906='OH, District 06' 
3910='OH, District 10' 
3911='OH, District 11' 
3912='OH, District 12' 
3914='OH, District 14' 
3915='OH, District 15' 
3916='OH, District 16' 
4002='OK, District 02' 
4003='OK, District 03' 
4004='OK, District 04' 
4201='PA, District 01' 
4202='PA, District 02' 
4203='PA, District 03' 
4204='PA, District 04' 
4205='PA, District 05' 
4206='PA, District 06' 
4207='PA, District 07' 
4208='PA, District 08' 
4209='PA, District 09' 
4210='PA, District 10' 
4211='PA, District 11' 
4212='PA, District 12' 
4213='PA, District 13' 
4214='PA, District 14' 
4215='PA, District 15' 
4216='PA, District 16' 
4217='PA, District 17' 
4218='PA, District 18' 
4401='RI, District 01' 
4502='SC, District 02' 
4504='SC, District 04' 
4506='SC, District 06' 
4702='TN, District 02' 
4703='TN, District 03' 
4705='TN, District 05' 
4709='TN, District 09' 
4802='TX, District 02' 
4807='TX, District 07' 
4809='TX, District 09' 
4812='TX, District 12' 
4813='TX, District 13' 
4815='TX, District 15' 
4817='TX, District 17' 
4818='TX, District 18' 
4820='TX, District 20' 
4822='TX, District 22' 
4823='TX, District 23' 
4829='TX, District 29' 
4830='TX, District 30' 
4832='TX, District 32' 
4835='TX, District 35' 
4902='UT, District 02' 
4903='UT, District 03' 
4904='UT, District 04' 
5000='VT, District 00' 
5101='VA, District 01' 
5102='VA, District 02' 
5103='VA, District 03' 
5106='VA, District 06' 
5107='VA, District 07' 
5108='VA, District 08' 
5109='VA, District 09' 
5110='VA, District 10' 
5111='VA, District 11' 
5302='WA, District 02' 
5303='WA, District 03' 
5304='WA, District 04' 
5305='WA, District 05' 
5307='WA, District 07' 
5309='WA, District 09' 
5501='WI, District 01' 
5504='WI, District 04' 
5507='WI, District 07' 
7298='PR, District 98' 
7898='VI, District 98';
value pclevel1f
1='Yes' 
0='Implied no';
value pclevel1a 
1='Yes' 
0='Implied no';
value pclevel1b 
1='Yes' 
0='implied no';
value pclevel2f
1='Yes' 
0='Implied no';
value pclevel3f
1='Yes' 
0='Implied no';
value pclevel4f
1='Yes' 
0='Implied no';
value pclevel5f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel6f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel7f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel8f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel17f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel18f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pclevel19f
1='Yes' 
0='Implied no' 
-2='Not applicable';
value pcft_ftug 
1='Yes' 
2='No';
value pcalloncam
1='Yes' 
2='No' 
-2='Not applicable';
value pcroom    
1='Yes' 
2='No' 
-2='Not applicable';
value $pccipcode1f
'09.0702'='Digital Communication and Media/Multimedia' 
'11.1003'='Computer and Information Systems Security/Auditing/Information Assurance' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0414'='Master Aesthetician/Esthetician' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0608'='Aircraft Powerplant Technology/Technician' 
'48.0508'='Welding Technology/Welder' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'51.0601'='Dental Assisting/Assistant' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0801'='Medical/Clinical Assistant' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'-2'='Not applicable';
value pcprgmsr1f
1='Clock hours' 
2='Credit hours' 
-2='Not applicable';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

options fmtsearch=(&libraryName);

data &libraryName..&file;
infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 
		lrecl=32736;

informat
unitid      6. 
campusid    2. 
pcinstnm    $ 120. 
pcaddr      $ 100. 
pccity      $ 30. 
pcstabbr    $ 2. 
pczip       $ 10. 
pcfips      2. 
pcobereg    2. 
pcchfnm     $ 50. 
pcchftitle  $ 50. 
pcgentele   $ 15. 
pcein       $ 9. 
pcduns      $ 2000. 
pcopeid     $ 8. 
pcopeflag   1. 
oldunitid   6. 
pcwebaddr   $ 150. 
pcadminurl  $ 200. 
pcfaidurl   $ 200. 
pcapplurl   $ 200. 
pcnpricurl  $ 200. 
pcveturl    $ 200. 
pcathurl    $ 150. 
pcdisaurl   $ 200. 
pcsector    2. 
pciclevel   2. 
pccontrol   2. 
pchloffer   2. 
pcugoffer   2. 
pcgroffer   2. 
pchdegofr1  2. 
pcdeggrant  2. 
pchbcu      2. 
pctribal    2. 
pclocale    2. 
pcopenpubl  2. 
pcact       $ 1. 
pccyactive  1. 
pcpostsec   2. 
pcpseflag   2. 
pcpset4flg  2. 
pccbsa      5. 
pccbsatype  2. 
pccsa       3. 
pcnecta     5. 
pccountycd  5. 
pccountynm  $ 30. 
pccngdstcd  4. 
pclongitud  12. 
pclatitude  12. 
pclevel1    2. 
pclevel1a   2. 
pclevel1b   2. 
pclevel2    2. 
pclevel3    2. 
pclevel4    2. 
pclevel5    2. 
pclevel6    2. 
pclevel7    2. 
pclevel8    2. 
pclevel17   2. 
pclevel18   2. 
pclevel19   2. 
pcft_ftug   2. 
pcalloncam  2. 
xpcapplfee  $1.
pcapplfeeu  3. 
pcroom      2. 
xpcchg1at0  $1.
pcchg1at0   5. 
xpcchg1af0  $1.
pcchg1af0   5. 
xpcchg1ay0  $1.
pcchg1ay0   5. 
xpcchg1at1  $1.
pcchg1at1   5. 
xpcchg1af1  $1.
pcchg1af1   5. 
xpcchg1ay1  $1.
pcchg1ay1   5. 
xpcchg1at2  $1.
pcchg1at2   5. 
xpcchg1af2  $1.
pcchg1af2   5. 
xpcchg1ay2  $1.
pcchg1ay2   5. 
xpcchg1at3  $1.
pcchg1at3   5. 
xpcchg1af3  $1.
pcchg1af3   5. 
xpcchg1ay3  $1.
pcchg1ay3   5. 
pcchg1tgtd  3. 
pcchg1fgtd  3. 
xpcchg2at0  $1.
pcchg2at0   5. 
xpcchg2af0  $1.
pcchg2af0   5. 
xpcchg2ay0  $1.
pcchg2ay0   5. 
xpcchg2at1  $1.
pcchg2at1   5. 
xpcchg2af1  $1.
pcchg2af1   5. 
xpcchg2ay1  $1.
pcchg2ay1   5. 
xpcchg2at2  $1.
pcchg2at2   5. 
xpcchg2af2  $1.
pcchg2af2   5. 
xpcchg2ay2  $1.
pcchg2ay2   5. 
xpcchg2at3  $1.
pcchg2at3   5. 
xpcchg2af3  $1.
pcchg2af3   5. 
xpcchg2ay3  $1.
pcchg2ay3   5. 
pcchg2tgtd  3. 
pcchg2fgtd  3. 
xpcchg3at0  $1.
pcchg3at0   5. 
xpcchg3af0  $1.
pcchg3af0   5. 
xpcchg3ay0  $1.
pcchg3ay0   5. 
xpcchg3at1  $1.
pcchg3at1   5. 
xpcchg3af1  $1.
pcchg3af1   5. 
xpcchg3ay1  $1.
pcchg3ay1   5. 
xpcchg3at2  $1.
pcchg3at2   5. 
xpcchg3af2  $1.
pcchg3af2   5. 
xpcchg3ay2  $1.
pcchg3ay2   5. 
xpcchg3at3  $1.
pcchg3at3   5. 
xpcchg3af3  $1.
pcchg3af3   5. 
xpcchg3ay3  $1.
pcchg3ay3   5. 
pcchg3tgtd  3. 
pcchg3fgtd  3. 
xpcchg4ay0  $1.
pcchg4ay0   5. 
xpcchg4ay1  $1.
pcchg4ay1   5. 
xpcchg4ay2  $1.
pcchg4ay2   5. 
xpcchg4ay3  $1.
pcchg4ay3   5. 
xpcchg5ay0  $1.
pcchg5ay0   5. 
xpcchg5ay1  $1.
pcchg5ay1   5. 
xpcchg5ay2  $1.
pcchg5ay2   5. 
xpcchg5ay3  $1.
pcchg5ay3   5. 
xpcchg6ay0  $1.
pcchg6ay0   5. 
xpcchg6ay1  $1.
pcchg6ay1   5. 
xpcchg6ay2  $1.
pcchg6ay2   5. 
xpcchg6ay3  $1.
pcchg6ay3   5. 
xpcchg7ay0  $1.
pcchg7ay0   5. 
xpcchg7ay1  $1.
pcchg7ay1   5. 
xpcchg7ay2  $1.
pcchg7ay2   5. 
xpcchg7ay3  $1.
pcchg7ay3   5. 
xpcchg8ay0  $1.
pcchg8ay0   5. 
xpcchg8ay1  $1.
pcchg8ay1   5. 
xpcchg8ay2  $1.
pcchg8ay2   5. 
xpcchg8ay3  $1.
pcchg8ay3   5. 
xpcchg9ay0  $1.
pcchg9ay0   5. 
xpcchg9ay1  $1.
pcchg9ay1   5. 
xpcchg9ay2  $1.
pcchg9ay2   5. 
xpcchg9ay3  $1.
pcchg9ay3   5. 
pccipcode1  $ 7. 
xpcciplgt1  $1.
pcciplgth1  4. 
pcprgmsr1   3. 
xpcmthcmp1  $1.
pcmthcmp1   3. 
xpcwkcmp1   $1.
pcwkcmp1    4. 
xpclnayhr1  $1.
pclnayhr1   4. 
xpclnaywk1  $1.
pclnaywk1   4. 
xpcchg1py0  $1.
pcchg1py0   5. 
xpcchg1py1  $1.
pcchg1py1   5. 
xpcchg1py2  $1.
pcchg1py2   5. 
xpcchg1py3  $1.
pcchg1py3   5. 
xpcchg4py0  $1.
pcchg4py0   5. 
xpcchg4py1  $1.
pcchg4py1   5. 
xpcchg4py2  $1.
pcchg4py2   5. 
xpcchg4py3  $1.
pcchg4py3   5. 
xpcchg5py0  $1.
pcchg5py0   5. 
xpcchg5py1  $1.
pcchg5py1   5. 
xpcchg5py2  $1.
pcchg5py2   5. 
xpcchg5py3  $1.
pcchg5py3   5. 
xpcchg6py0  $1.
pcchg6py0   5. 
xpcchg6py1  $1.
pcchg6py1   5. 
xpcchg6py2  $1.
pcchg6py2   5. 
xpcchg6py3  $1.
pcchg6py3   5. 
xpcchg7py0  $1.
pcchg7py0   5. 
xpcchg7py1  $1.
pcchg7py1   5. 
xpcchg7py2  $1.
pcchg7py2   5. 
xpcchg7py3  $1.
pcchg7py3   5. 
xpcchg8py0  $1.
pcchg8py0   5. 
xpcchg8py1  $1.
pcchg8py1   5. 
xpcchg8py2  $1.
pcchg8py2   5. 
xpcchg8py3  $1.
pcchg8py3   5. 
xpcchg9py0  $1.
pcchg9py0   5. 
xpcchg9py1  $1.
pcchg9py1   5. 
xpcchg9py2  $1.
pcchg9py2   5. 
xpcchg9py3  $1.
pcchg9py3   5.;

input
unitid      
campusid    
pcinstnm    $ 
pcaddr      $ 
pccity      $ 
pcstabbr    $ 
pczip       $ 
pcfips      
pcobereg    
pcchfnm     $ 
pcchftitle  $ 
pcgentele   $ 
pcein       $ 
pcduns      $ 
pcopeid     $ 
pcopeflag   
oldunitid   
pcwebaddr   $ 
pcadminurl  $ 
pcfaidurl   $ 
pcapplurl   $ 
pcnpricurl  $ 
pcveturl    $ 
pcathurl    $ 
pcdisaurl   $ 
pcsector    
pciclevel   
pccontrol   
pchloffer   
pcugoffer   
pcgroffer   
pchdegofr1  
pcdeggrant  
pchbcu      
pctribal    
pclocale    
pcopenpubl  
pcact       $ 
pccyactive  
pcpostsec   
pcpseflag   
pcpset4flg  
pccbsa      
pccbsatype  
pccsa       
pcnecta     
pccountycd  
pccountynm  $ 
pccngdstcd  
pclongitud  
pclatitude  
pclevel1    
pclevel1a   
pclevel1b   
pclevel2    
pclevel3    
pclevel4    
pclevel5    
pclevel6    
pclevel7    
pclevel8    
pclevel17   
pclevel18   
pclevel19   
pcft_ftug   
pcalloncam  
xpcapplfee  $
pcapplfeeu  
pcroom      
xpcchg1at0  $
pcchg1at0   
xpcchg1af0  $
pcchg1af0   
xpcchg1ay0  $
pcchg1ay0   
xpcchg1at1  $
pcchg1at1   
xpcchg1af1  $
pcchg1af1   
xpcchg1ay1  $
pcchg1ay1   
xpcchg1at2  $
pcchg1at2   
xpcchg1af2  $
pcchg1af2   
xpcchg1ay2  $
pcchg1ay2   
xpcchg1at3  $
pcchg1at3   
xpcchg1af3  $
pcchg1af3   
xpcchg1ay3  $
pcchg1ay3   
pcchg1tgtd  
pcchg1fgtd  
xpcchg2at0  $
pcchg2at0   
xpcchg2af0  $
pcchg2af0   
xpcchg2ay0  $
pcchg2ay0   
xpcchg2at1  $
pcchg2at1   
xpcchg2af1  $
pcchg2af1   
xpcchg2ay1  $
pcchg2ay1   
xpcchg2at2  $
pcchg2at2   
xpcchg2af2  $
pcchg2af2   
xpcchg2ay2  $
pcchg2ay2   
xpcchg2at3  $
pcchg2at3   
xpcchg2af3  $
pcchg2af3   
xpcchg2ay3  $
pcchg2ay3   
pcchg2tgtd  
pcchg2fgtd  
xpcchg3at0  $
pcchg3at0   
xpcchg3af0  $
pcchg3af0   
xpcchg3ay0  $
pcchg3ay0   
xpcchg3at1  $
pcchg3at1   
xpcchg3af1  $
pcchg3af1   
xpcchg3ay1  $
pcchg3ay1   
xpcchg3at2  $
pcchg3at2   
xpcchg3af2  $
pcchg3af2   
xpcchg3ay2  $
pcchg3ay2   
xpcchg3at3  $
pcchg3at3   
xpcchg3af3  $
pcchg3af3   
xpcchg3ay3  $
pcchg3ay3   
pcchg3tgtd  
pcchg3fgtd  
xpcchg4ay0  $
pcchg4ay0   
xpcchg4ay1  $
pcchg4ay1   
xpcchg4ay2  $
pcchg4ay2   
xpcchg4ay3  $
pcchg4ay3   
xpcchg5ay0  $
pcchg5ay0   
xpcchg5ay1  $
pcchg5ay1   
xpcchg5ay2  $
pcchg5ay2   
xpcchg5ay3  $
pcchg5ay3   
xpcchg6ay0  $
pcchg6ay0   
xpcchg6ay1  $
pcchg6ay1   
xpcchg6ay2  $
pcchg6ay2   
xpcchg6ay3  $
pcchg6ay3   
xpcchg7ay0  $
pcchg7ay0   
xpcchg7ay1  $
pcchg7ay1   
xpcchg7ay2  $
pcchg7ay2   
xpcchg7ay3  $
pcchg7ay3   
xpcchg8ay0  $
pcchg8ay0   
xpcchg8ay1  $
pcchg8ay1   
xpcchg8ay2  $
pcchg8ay2   
xpcchg8ay3  $
pcchg8ay3   
xpcchg9ay0  $
pcchg9ay0   
xpcchg9ay1  $
pcchg9ay1   
xpcchg9ay2  $
pcchg9ay2   
xpcchg9ay3  $
pcchg9ay3   
pccipcode1  $ 
xpcciplgt1  $
pcciplgth1  
pcprgmsr1   
xpcmthcmp1  $
pcmthcmp1   
xpcwkcmp1   $
pcwkcmp1    
xpclnayhr1  $
pclnayhr1   
xpclnaywk1  $
pclnaywk1   
xpcchg1py0  $
pcchg1py0   
xpcchg1py1  $
pcchg1py1   
xpcchg1py2  $
pcchg1py2   
xpcchg1py3  $
pcchg1py3   
xpcchg4py0  $
pcchg4py0   
xpcchg4py1  $
pcchg4py1   
xpcchg4py2  $
pcchg4py2   
xpcchg4py3  $
pcchg4py3   
xpcchg5py0  $
pcchg5py0   
xpcchg5py1  $
pcchg5py1   
xpcchg5py2  $
pcchg5py2   
xpcchg5py3  $
pcchg5py3   
xpcchg6py0  $
pcchg6py0   
xpcchg6py1  $
pcchg6py1   
xpcchg6py2  $
pcchg6py2   
xpcchg6py3  $
pcchg6py3   
xpcchg7py0  $
pcchg7py0   
xpcchg7py1  $
pcchg7py1   
xpcchg7py2  $
pcchg7py2   
xpcchg7py3  $
pcchg7py3   
xpcchg8py0  $
pcchg8py0   
xpcchg8py1  $
pcchg8py1   
xpcchg8py2  $
pcchg8py2   
xpcchg8py3  $
pcchg8py3   
xpcchg9py0  $
pcchg9py0   
xpcchg9py1  $
pcchg9py1   
xpcchg9py2  $
pcchg9py2   
xpcchg9py3  $
pcchg9py3  ;

label
unitid     ='Unique identification number of the institution' 
campusid   ='Branch Campus Identifcation number' 
pcinstnm   ='Branch Campus Name' 
pcaddr     ='Street address or post office box' 
pccity     ='City location of institution' 
pcstabbr   ='State abbreviation' 
pczip      ='ZIP code' 
pcfips     ='FIPS state code' 
pcobereg   ='Bureau of Economic Analysis (BEA) regions' 
pcchfnm    ='Name of chief administrator' 
pcchftitle ='Title of chief administrator' 
pcgentele  ='General information telephone number' 
pcein      ='Employer Identification Number' 
pcduns     ='Dun and Bradstreet numbers' 
pcopeid    ='Office of Postsecondary Education (OPE) ID Number' 
pcopeflag  ='OPE Title IV eligibility indicator code' 
oldunitid  ='Former unitid if branch location once reported in IPEDS' 
pcwebaddr  ='Institution^s internet website address' 
pcadminurl ='Admissions office web address' 
pcfaidurl  ='Financial aid office web address' 
pcapplurl  ='Online application web address' 
pcnpricurl ='Net price calculator web address' 
pcveturl   ='Veterans and Military Servicemembers tuition policies web address' 
pcathurl   ='Student-Right-to-Know student athlete graduation rate web address' 
pcdisaurl  ='Disability Services Web Address' 
pcsector   ='Sector of institution' 
pciclevel  ='Level of institution' 
pccontrol  ='Control of institution' 
pchloffer  ='Highest level of offering' 
pcugoffer  ='Undergraduate offering' 
pcgroffer  ='Graduate offering' 
pchdegofr1 ='Highest degree offered' 
pcdeggrant ='Degree-granting status' 
pchbcu     ='Historically Black College or University' 
pctribal   ='Tribal college' 
pclocale   ='Degree of urbanization (Urban-centric locale)' 
pcopenpubl ='Institution open to the general public' 
pcact      ='Status of institution' 
pccyactive ='Institution is active in current year' 
pcpostsec  ='Primarily postsecondary indicator' 
pcpseflag  ='Postsecondary institution indicator' 
pcpset4flg ='Postsecondary and Title IV institution indicator' 
pccbsa     ='Core Based Statistical Area (CBSA)' 
pccbsatype ='CBSA Type Metropolitan or Micropolitan' 
pccsa      ='Combined Statistical Area (CSA)' 
pcnecta    ='New England City and Town Area (NECTA)' 
pccountycd ='Fips County code' 
pccountynm ='County name' 
pccngdstcd ='State and 116TH Congressional District ID' 
pclongitud ='Longitude location of institution' 
pclatitude ='Latitude location of institution' 
pclevel1   ='Certificate of less than 1 year' 
pclevel1a  ='Certificate of less than 12 weeks' 
pclevel1b  ='Certificate of at least 12 weeks, but less than 1 year' 
pclevel2   ='Certifiicate of at least 1 year, but less than 2 years' 
pclevel3   ='Associate^s degree' 
pclevel4   ='Certificate of at least 2 years, but less than 4 years' 
pclevel5   ='Bachelor^s degree' 
pclevel6   ='Postbaccalaureate certificate' 
pclevel7   ='Master^s degree' 
pclevel8   ='Post-master^s certificate' 
pclevel17  ='Doctor^s degree - research/scholarship' 
pclevel18  ='Doctor^s degree - professional practice' 
pclevel19  ='Doctor^s degree - other' 
pcft_ftug  ='Full time first-time degree/certificate-seeking undergraduate students enrolled' 
pcalloncam ='Full-time, first-time degree/certificate-seeking students required to live on campus' 
xpcapplfee  ='Imputation field pcapplfeeu - Undergraduate application fee'
pcapplfeeu ='Undergraduate application fee' 
pcroom     ='Institution provide on-campus housing' 
xpcchg1at0  ='Imputation field pcchg1at0 - Published in-district tuition 2018-19'
pcchg1at0  ='Published in-district tuition 2018-19' 
xpcchg1af0  ='Imputation field pcchg1af0 - Published in-district fees 2018-19'
pcchg1af0  ='Published in-district fees 2018-19' 
xpcchg1ay0  ='Imputation field pcchg1ay0 - Published in-district tuition and fees 2018-19'
pcchg1ay0  ='Published in-district tuition and fees 2018-19' 
xpcchg1at1  ='Imputation field pcchg1at1 - Published in-district tuition 2019-20'
pcchg1at1  ='Published in-district tuition 2019-20' 
xpcchg1af1  ='Imputation field pcchg1af1 - Published in-district fees 2019-20'
pcchg1af1  ='Published in-district fees 2019-20' 
xpcchg1ay1  ='Imputation field pcchg1ay1 - Published in-district tuition and fees 2019-20'
pcchg1ay1  ='Published in-district tuition and fees 2019-20' 
xpcchg1at2  ='Imputation field pcchg1at2 - Published in-district tuition 2020-21'
pcchg1at2  ='Published in-district tuition 2020-21' 
xpcchg1af2  ='Imputation field pcchg1af2 - Published in-district fees 2020-21'
pcchg1af2  ='Published in-district fees 2020-21' 
xpcchg1ay2  ='Imputation field pcchg1ay2 - Published in-district tuition and fees 2020-21'
pcchg1ay2  ='Published in-district tuition and fees 2020-21' 
xpcchg1at3  ='Imputation field pcchg1at3 - Published in-district tuition 2021-22'
pcchg1at3  ='Published in-district tuition 2021-22' 
xpcchg1af3  ='Imputation field pcchg1af3 - Published in-district fees 2021-22'
pcchg1af3  ='Published in-district fees 2021-22' 
xpcchg1ay3  ='Imputation field pcchg1ay3 - Published in-district tuition and fees 2021-22'
pcchg1ay3  ='Published in-district tuition and fees 2021-22' 
pcchg1tgtd ='Published in-district tuition 2021-22 guaranteed percent increase (if applicable)' 
pcchg1fgtd ='Published in-district fees 2021-22 guaranteed percent increase (if applicable)' 
xpcchg2at0  ='Imputation field pcchg2at0 - Published in-state tuition 2018-19'
pcchg2at0  ='Published in-state tuition 2018-19' 
xpcchg2af0  ='Imputation field pcchg2af0 - Published in-state fees 2018-19'
pcchg2af0  ='Published in-state fees 2018-19' 
xpcchg2ay0  ='Imputation field pcchg2ay0 - Published in-state tuition and fees 2018-19'
pcchg2ay0  ='Published in-state tuition and fees 2018-19' 
xpcchg2at1  ='Imputation field pcchg2at1 - Published in-state tuition 2019-20'
pcchg2at1  ='Published in-state tuition 2019-20' 
xpcchg2af1  ='Imputation field pcchg2af1 - Published in-state fees 2019-20'
pcchg2af1  ='Published in-state fees 2019-20' 
xpcchg2ay1  ='Imputation field pcchg2ay1 - Published in-state tuition and fees 2019-20'
pcchg2ay1  ='Published in-state tuition and fees 2019-20' 
xpcchg2at2  ='Imputation field pcchg2at2 - Published in-state tuition 2020-21'
pcchg2at2  ='Published in-state tuition 2020-21' 
xpcchg2af2  ='Imputation field pcchg2af2 - Published in-state fees 2020-21'
pcchg2af2  ='Published in-state fees 2020-21' 
xpcchg2ay2  ='Imputation field pcchg2ay2 - Published in-state tuition and fees 2020-21'
pcchg2ay2  ='Published in-state tuition and fees 2020-21' 
xpcchg2at3  ='Imputation field pcchg2at3 - Published in-state tuition 2021-22'
pcchg2at3  ='Published in-state tuition 2021-22' 
xpcchg2af3  ='Imputation field pcchg2af3 - Published in-state fees 2021-22'
pcchg2af3  ='Published in-state fees 2021-22' 
xpcchg2ay3  ='Imputation field pcchg2ay3 - Published in-state tuition and fees 2021-22'
pcchg2ay3  ='Published in-state tuition and fees 2021-22' 
pcchg2tgtd ='Published in-state tuition 2021-22 guaranteed percent increase (if applicable)' 
pcchg2fgtd ='Published in-state fees 2021-22 guaranteed percent increase (if applicable)' 
xpcchg3at0  ='Imputation field pcchg3at0 - Published out-of-state tuition 2018-19'
pcchg3at0  ='Published out-of-state tuition 2018-19' 
xpcchg3af0  ='Imputation field pcchg3af0 - Published out-of-state fees 2018-19'
pcchg3af0  ='Published out-of-state fees 2018-19' 
xpcchg3ay0  ='Imputation field pcchg3ay0 - Published out-of-state tuition and fees 2018-19'
pcchg3ay0  ='Published out-of-state tuition and fees 2018-19' 
xpcchg3at1  ='Imputation field pcchg3at1 - Published out-of-state tuition 2019-20'
pcchg3at1  ='Published out-of-state tuition 2019-20' 
xpcchg3af1  ='Imputation field pcchg3af1 - Published out-of-state fees 2019-20'
pcchg3af1  ='Published out-of-state fees 2019-20' 
xpcchg3ay1  ='Imputation field pcchg3ay1 - Published out-of-state tuition and fees 2019-20'
pcchg3ay1  ='Published out-of-state tuition and fees 2019-20' 
xpcchg3at2  ='Imputation field pcchg3at2 - Published out-of-state tuition 2020-21'
pcchg3at2  ='Published out-of-state tuition 2020-21' 
xpcchg3af2  ='Imputation field pcchg3af2 - Published out-of-state fees 2020-21'
pcchg3af2  ='Published out-of-state fees 2020-21' 
xpcchg3ay2  ='Imputation field pcchg3ay2 - Published out-of-state tuition and fees 2020-21'
pcchg3ay2  ='Published out-of-state tuition and fees 2020-21' 
xpcchg3at3  ='Imputation field pcchg3at3 - Published out-of-state tuition 2021-22'
pcchg3at3  ='Published out-of-state tuition 2021-22' 
xpcchg3af3  ='Imputation field pcchg3af3 - Published out-of-state fees 2021-22'
pcchg3af3  ='Published out-of-state fees 2021-22' 
xpcchg3ay3  ='Imputation field pcchg3ay3 - Published out-of-state tuition and fees 2021-22'
pcchg3ay3  ='Published out-of-state tuition and fees 2021-22' 
pcchg3tgtd ='Published out-of-state tuition 2021-22 guaranteed percent increase (if applicable)' 
pcchg3fgtd ='Published out-of-state fees 2021-22 guaranteed percent increase (if applicable)' 
xpcchg4ay0  ='Imputation field pcchg4ay0 - Books and supplies 2018-19'
pcchg4ay0  ='Books and supplies 2018-19' 
xpcchg4ay1  ='Imputation field pcchg4ay1 - Books and supplies 2019-20'
pcchg4ay1  ='Books and supplies 2019-20' 
xpcchg4ay2  ='Imputation field pcchg4ay2 - Books and supplies 2020-21'
pcchg4ay2  ='Books and supplies 2020-21' 
xpcchg4ay3  ='Imputation field pcchg4ay3 - Books and supplies 2021-22'
pcchg4ay3  ='Books and supplies 2021-22' 
xpcchg5ay0  ='Imputation field pcchg5ay0 - On campus, room and board 2018-19'
pcchg5ay0  ='On campus, room and board 2018-19' 
xpcchg5ay1  ='Imputation field pcchg5ay1 - On campus, room and board 2019-20'
pcchg5ay1  ='On campus, room and board 2019-20' 
xpcchg5ay2  ='Imputation field pcchg5ay2 - On campus, room and board 2020-21'
pcchg5ay2  ='On campus, room and board 2020-21' 
xpcchg5ay3  ='Imputation field pcchg5ay3 - On campus, room and board 2021-22'
pcchg5ay3  ='On campus, room and board 2021-22' 
xpcchg6ay0  ='Imputation field pcchg6ay0 - On campus, other expenses 2018-19'
pcchg6ay0  ='On campus, other expenses 2018-19' 
xpcchg6ay1  ='Imputation field pcchg6ay1 - On campus, other expenses 2019-20'
pcchg6ay1  ='On campus, other expenses 2019-20' 
xpcchg6ay2  ='Imputation field pcchg6ay2 - On campus, other expenses 2020-21'
pcchg6ay2  ='On campus, other expenses 2020-21' 
xpcchg6ay3  ='Imputation field pcchg6ay3 - On campus, other expenses 2021-22'
pcchg6ay3  ='On campus, other expenses 2021-22' 
xpcchg7ay0  ='Imputation field pcchg7ay0 - Off campus (not with family), room and board 2018-19'
pcchg7ay0  ='Off campus (not with family), room and board 2018-19' 
xpcchg7ay1  ='Imputation field pcchg7ay1 - Off campus (not with family), room and board 2019-20'
pcchg7ay1  ='Off campus (not with family), room and board 2019-20' 
xpcchg7ay2  ='Imputation field pcchg7ay2 - Off campus (not with family), room and board 2020-21'
pcchg7ay2  ='Off campus (not with family), room and board 2020-21' 
xpcchg7ay3  ='Imputation field pcchg7ay3 - Off campus (not with family), room and board 2021-22'
pcchg7ay3  ='Off campus (not with family), room and board 2021-22' 
xpcchg8ay0  ='Imputation field pcchg8ay0 - Off campus (not with family), other expenses 2018-19'
pcchg8ay0  ='Off campus (not with family), other expenses 2018-19' 
xpcchg8ay1  ='Imputation field pcchg8ay1 - Off campus (not with family), other expenses 2019-20'
pcchg8ay1  ='Off campus (not with family), other expenses 2019-20' 
xpcchg8ay2  ='Imputation field pcchg8ay2 - Off campus (not with family), other expenses 2020-21'
pcchg8ay2  ='Off campus (not with family), other expenses 2020-21' 
xpcchg8ay3  ='Imputation field pcchg8ay3 - Off campus (not with family), other expenses 2021-22'
pcchg8ay3  ='Off campus (not with family), other expenses 2021-22' 
xpcchg9ay0  ='Imputation field pcchg9ay0 - Off campus (with family), other expenses 2018-19'
pcchg9ay0  ='Off campus (with family), other expenses 2018-19' 
xpcchg9ay1  ='Imputation field pcchg9ay1 - Off campus (with family), other expenses 2019-20'
pcchg9ay1  ='Off campus (with family), other expenses 2019-20' 
xpcchg9ay2  ='Imputation field pcchg9ay2 - Off campus (with family), other expenses 2020-21'
pcchg9ay2  ='Off campus (with family), other expenses 2020-21' 
xpcchg9ay3  ='Imputation field pcchg9ay3 - Off campus (with family), other expenses 2021-22'
pcchg9ay3  ='Off campus (with family), other expenses 2021-22' 
pccipcode1 ='CIP code of largest program' 
xpcciplgt1  ='Imputation field pcciplgth1 - Total length of largest program'
pcciplgth1 ='Total length of largest program' 
pcprgmsr1  ='Largest program measured in credit or clock hours' 
xpcmthcmp1  ='Imputation field pcmthcmp1 - Average number of months to complete largest program'
pcmthcmp1  ='Average number of months to complete largest program' 
xpcwkcmp1   ='Imputation field pcwkcmp1 - Total length of program in weeks, as completed by a student attending full-time'
pcwkcmp1   ='Total length of program in weeks, as completed by a student attending full-time' 
xpclnayhr1  ='Imputation field pclnayhr1 - Total length of academic year (as used to calculate your Pell budget) in clock or credit hours'
pclnayhr1  ='Total length of academic year (as used to calculate your Pell budget) in clock or credit hours' 
xpclnaywk1  ='Imputation field pclnaywk1 - Total length of academic year (as used to calculate your Pell budget) in weeks'
pclnaywk1  ='Total length of academic year (as used to calculate your Pell budget) in weeks' 
xpcchg1py0  ='Imputation field pcchg1py0 - Published tuition and fees 2018-19'
pcchg1py0  ='Published tuition and fees 2018-19' 
xpcchg1py1  ='Imputation field pcchg1py1 - Published tuition and fees 2019-20'
pcchg1py1  ='Published tuition and fees 2019-20' 
xpcchg1py2  ='Imputation field pcchg1py2 - Published tuition and fees 2020-21'
pcchg1py2  ='Published tuition and fees 2020-21' 
xpcchg1py3  ='Imputation field pcchg1py3 - Published tuition and fees 2021-22'
pcchg1py3  ='Published tuition and fees 2021-22' 
xpcchg4py0  ='Imputation field pcchg4py0 - Books and supplies 2018-19'
pcchg4py0  ='Books and supplies 2018-19' 
xpcchg4py1  ='Imputation field pcchg4py1 - Books and supplies 2019-20'
pcchg4py1  ='Books and supplies 2019-20' 
xpcchg4py2  ='Imputation field pcchg4py2 - Books and supplies 2020-21'
pcchg4py2  ='Books and supplies 2020-21' 
xpcchg4py3  ='Imputation field pcchg4py3 - Books and supplies 2021-22'
pcchg4py3  ='Books and supplies 2021-22' 
xpcchg5py0  ='Imputation field pcchg5py0 - On campus, room and board 2018-19'
pcchg5py0  ='On campus, room and board 2018-19' 
xpcchg5py1  ='Imputation field pcchg5py1 - On campus, room and board 2019-20'
pcchg5py1  ='On campus, room and board 2019-20' 
xpcchg5py2  ='Imputation field pcchg5py2 - On campus, room and board 2020-21'
pcchg5py2  ='On campus, room and board 2020-21' 
xpcchg5py3  ='Imputation field pcchg5py3 - On campus, room and board 2021-22'
pcchg5py3  ='On campus, room and board 2021-22' 
xpcchg6py0  ='Imputation field pcchg6py0 - On campus, other expenses 2018-19'
pcchg6py0  ='On campus, other expenses 2018-19' 
xpcchg6py1  ='Imputation field pcchg6py1 - On campus, other expenses 2019-20'
pcchg6py1  ='On campus, other expenses 2019-20' 
xpcchg6py2  ='Imputation field pcchg6py2 - On campus, other expenses 2020-21'
pcchg6py2  ='On campus, other expenses 2020-21' 
xpcchg6py3  ='Imputation field pcchg6py3 - On campus, other expenses 2021-22'
pcchg6py3  ='On campus, other expenses 2021-22' 
xpcchg7py0  ='Imputation field pcchg7py0 - Off campus (not with family), room and board 2018-19'
pcchg7py0  ='Off campus (not with family), room and board 2018-19' 
xpcchg7py1  ='Imputation field pcchg7py1 - Off campus (not with family), room and board 2019-20'
pcchg7py1  ='Off campus (not with family), room and board 2019-20' 
xpcchg7py2  ='Imputation field pcchg7py2 - Off campus (not with family), room and board 2020-21'
pcchg7py2  ='Off campus (not with family), room and board 2020-21' 
xpcchg7py3  ='Imputation field pcchg7py3 - Off campus (not with family), room and board 2021-22'
pcchg7py3  ='Off campus (not with family), room and board 2021-22' 
xpcchg8py0  ='Imputation field pcchg8py0 - Off campus (not with family), other expenses 2018-19'
pcchg8py0  ='Off campus (not with family), other expenses 2018-19' 
xpcchg8py1  ='Imputation field pcchg8py1 - Off campus (not with family), other expenses 2019-20'
pcchg8py1  ='Off campus (not with family), other expenses 2019-20' 
xpcchg8py2  ='Imputation field pcchg8py2 - Off campus (not with family), other expenses 2020-21'
pcchg8py2  ='Off campus (not with family), other expenses 2020-21' 
xpcchg8py3  ='Imputation field pcchg8py3 - Off campus (not with family), other expenses 2021-22'
pcchg8py3  ='Off campus (not with family), other expenses 2021-22' 
xpcchg9py0  ='Imputation field pcchg9py0 - Off campus (with family), other expenses 2018-19'
pcchg9py0  ='Off campus (with family), other expenses 2018-19' 
xpcchg9py1  ='Imputation field pcchg9py1 - Off campus (with family), other expenses 2019-20'
pcchg9py1  ='Off campus (with family), other expenses 2019-20' 
xpcchg9py2  ='Imputation field pcchg9py2 - Off campus (with family), other expenses 2020-21'
pcchg9py2  ='Off campus (with family), other expenses 2020-21' 
xpcchg9py3  ='Imputation field pcchg9py3 - Off campus (with family), other expenses 2021-22'
pcchg9py3  ='Off campus (with family), other expenses 2021-22';
run;

Proc Freq;
Tables
pcstabbr   pcfips     pcobereg  
pcopeflag 
pcsector   pciclevel  pccontrol  pchloffer  pcugoffer 
pcgroffer  pchdegofr1 pcdeggrant pchbcu     pctribal   pclocale   pcopenpubl pcact      pccyactive pcpostsec 
pcpseflag  pcpset4flg pccbsa     pccbsatype pccsa      pcnecta    pccountycd pccngdstcd
pclevel1   pclevel1a  pclevel1b  pclevel2   pclevel3   pclevel4   pclevel5   pclevel6   pclevel7  
pclevel8   pclevel17  pclevel18  pclevel19  pcft_ftug  pcalloncam xpcapplfee pcroom     xpcchg1at0 xpcchg1af0
xpcchg1ay0 xpcchg1at1 xpcchg1af1 xpcchg1ay1 xpcchg1at2 xpcchg1af2 xpcchg1ay2 xpcchg1at3 xpcchg1af3 xpcchg1ay3
xpcchg2at0 xpcchg2af0 xpcchg2ay0 xpcchg2at1 xpcchg2af1 xpcchg2ay1 xpcchg2at2 xpcchg2af2
xpcchg2ay2 xpcchg2at3 xpcchg2af3 xpcchg2ay3 xpcchg3at0 xpcchg3af0 xpcchg3ay0 xpcchg3at1
xpcchg3af1 xpcchg3ay1 xpcchg3at2 xpcchg3af2 xpcchg3ay2 xpcchg3at3 xpcchg3af3 xpcchg3ay3
xpcchg4ay0 xpcchg4ay1 xpcchg4ay2 xpcchg4ay3 xpcchg5ay0 xpcchg5ay1 xpcchg5ay2 xpcchg5ay3 xpcchg6ay0 xpcchg6ay1
xpcchg6ay2 xpcchg6ay3 xpcchg7ay0 xpcchg7ay1 xpcchg7ay2 xpcchg7ay3 xpcchg8ay0 xpcchg8ay1 xpcchg8ay2 xpcchg8ay3
xpcchg9ay0 xpcchg9ay1 xpcchg9ay2 xpcchg9ay3 pccipcode1 xpcciplgt1 pcprgmsr1  xpcmthcmp1 xpcwkcmp1  xpclnayhr1
xpclnaywk1 xpcchg1py0 xpcchg1py1 xpcchg1py2 xpcchg1py3 xpcchg4py0 xpcchg4py1 xpcchg4py2 xpcchg4py3 xpcchg5py0
xpcchg5py1 xpcchg5py2 xpcchg5py3 xpcchg6py0 xpcchg6py1 xpcchg6py2 xpcchg6py3 xpcchg7py0 xpcchg7py1 xpcchg7py2
xpcchg7py3 xpcchg8py0 xpcchg8py1 xpcchg8py2 xpcchg8py3 xpcchg9py0 xpcchg9py1 xpcchg9py2 xpcchg9py3  / missing;

format xpcapplfee-character-xpcchg9py3 $ximpflg.
pcstabbr  $pcstabbr.
pcfips  pcfips.
pcobereg  pcobereg.
pcopeflag  pcopeflag.
pcsector  pcsector.
pciclevel  pciclevel.
pccontrol  pccontrol.
pchloffer  pchloffer.
pcugoffer  pcugoffer.
pcgroffer  pcgroffer.
pchdegofr1  pchdegofr1f.
pcdeggrant  pcdeggrant.
pchbcu  pchbcu.
pctribal  pctribal.
pclocale  pclocale.
pcopenpubl  pcopenpubl.
pcact  $pcact.
pccyactive  pccyactive.
pcpostsec  pcpostsec.
pcpseflag  pcpseflag.
pcpset4flg  pcpset4flg.
pccbsa  pccbsa.
pccbsatype  pccbsatype.
pccsa  pccsa.
pcnecta  pcnecta.
pccountycd  pccountycd.
pccngdstcd  pccngdstcd.
pclevel1  pclevel1f.
pclevel1a  pclevel1a.
pclevel1b  pclevel1b.
pclevel2  pclevel2f.
pclevel3  pclevel3f.
pclevel4  pclevel4f.
pclevel5  pclevel5f.
pclevel6  pclevel6f.
pclevel7  pclevel7f.
pclevel8  pclevel8f.
pclevel17  pclevel17f.
pclevel18  pclevel18f.
pclevel19  pclevel19f.
pcft_ftug  pcft_ftug.
pcalloncam  pcalloncam.
pcroom  pcroom.
pccipcode1  $pccipcode1f.
pcprgmsr1  pcprgmsr1f.
;

Proc Summary print n sum mean min max;
var
campusid   
pclongitud 
pclatitude 
pcapplfeeu  pcchg1at0   pcchg1af0  
pcchg1ay0   pcchg1at1   pcchg1af1   pcchg1ay1   pcchg1at2   pcchg1af2   pcchg1ay2   pcchg1at3   pcchg1af3   pcchg1ay3  
pcchg1tgtd  pcchg1fgtd  pcchg2at0   pcchg2af0   pcchg2ay0   pcchg2at1   pcchg2af1   pcchg2ay1   pcchg2at2   pcchg2af2  
pcchg2ay2   pcchg2at3   pcchg2af3   pcchg2ay3   pcchg2tgtd  pcchg2fgtd  pcchg3at0   pcchg3af0   pcchg3ay0   pcchg3at1  
pcchg3af1   pcchg3ay1   pcchg3at2   pcchg3af2   pcchg3ay2   pcchg3at3   pcchg3af3   pcchg3ay3   pcchg3tgtd  pcchg3fgtd 
pcchg4ay0   pcchg4ay1   pcchg4ay2   pcchg4ay3   pcchg5ay0   pcchg5ay1   pcchg5ay2   pcchg5ay3   pcchg6ay0   pcchg6ay1  
pcchg6ay2   pcchg6ay3   pcchg7ay0   pcchg7ay1   pcchg7ay2   pcchg7ay3   pcchg8ay0   pcchg8ay1   pcchg8ay2   pcchg8ay3  
pcchg9ay0   pcchg9ay1   pcchg9ay2   pcchg9ay3   pcciplgth1  pcmthcmp1   pcwkcmp1    pclnayhr1  
pclnaywk1   pcchg1py0   pcchg1py1   pcchg1py2   pcchg1py3   pcchg4py0   pcchg4py1   pcchg4py2   pcchg4py3   pcchg5py0  
pcchg5py1   pcchg5py2   pcchg5py3   pcchg6py0   pcchg6py1   pcchg6py2   pcchg6py3   pcchg7py0   pcchg7py1   pcchg7py2  
pcchg7py3   pcchg8py0   pcchg8py1   pcchg8py2   pcchg8py3   pcchg9py0   pcchg9py1   pcchg9py2   pcchg9py3   ;
run;
