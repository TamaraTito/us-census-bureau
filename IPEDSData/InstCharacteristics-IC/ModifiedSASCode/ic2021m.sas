%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let file=ic2021;
%let libraryName=IPEDICM;
libname &libraryName "&path/SASData";

proc format library=&libraryName;
  value peo1istr 1='Yes' 0='Implied no';
  value peo2istr 1='Yes' 0='Implied no';
  value peo3istr 1='Yes' 0='Implied no' -2='Not applicable';
  value peo4istr 1='Yes' 0='Implied no';
  value peo5istr 1='Yes' 0='Implied no';
  value peo6istr 1='Yes' 0='Implied no';
  value cntlaffi 1='Public' 2='Private for-profit' 
    3='Private not-for-profit (no religious affiliation)' 
    4='Private not-for-profit (religious affiliation)';
  value pubprime 1='Federal' 2='State' 3='Territorial' 4='School district' 
    5='County' 7='City' 8='Special district' 9='Other' 
-2='Not applicable';
  value pubsecon 1='Federal' 2='State' 3='Territorial' 4='School district' 
    5='County' 6='Township' 7='City' 8='Special district' 9='Other' 
    0='Implied no' 
-2='Not applicable';
  value relaffil 51='African Methodist Episcopal' 
    24='African Methodist Episcopal Zion Church' 52='American Baptist' 
    22='American Evangelical Lutheran Church' 27='Assemblies of God Church' 
    54='Baptist' 28='Brethren Church' 34='Christ and Missionary Alliance Church' 
    61='Christian Church (Disciples of Christ)' 
    48='Christian Churches and Churches of Christ' 
    55='Christian Methodist Episcopal' 35='Christian Reformed Church' 
    58='Church of Brethren' 57='Church of God' 59='Church of the Nazarene' 
    74='Churches of Christ' 60='Cumberland Presbyterian' 
    50='Episcopal Church, Reformed' 102='Evangelical Christian' 
    36='Evangelical Congregational Church' 
    37='Evangelical Covenant Church of America' 
    38='Evangelical Free Church of America' 39='Evangelical Lutheran Church' 
    64='Free Methodist' 41='Free Will Baptist Church' 65='Friends' 
    105='General Baptist' 91='Greek Orthodox' 42='Interdenominational' 
    80='Jewish' 68='Lutheran Church - Missouri Synod' 
    67='Lutheran Church in America' 43='Mennonite Brethren Church' 
    69='Mennonite Church' 87='Missionary Church Inc' 44='Moravian Church' 
    78='Multiple Protestant Denomination' 106='Muslim' 108='Non-Denominational' 
    45='North American Baptist' 100='Original Free Will Baptist' 
    79='Other Protestant' 47='Pentecostal Holiness Church' 
    107='Plymouth Brethren' 103='Presbyterian' 66='Presbyterian Church (USA)' 
    73='Protestant Episcopal' 49='Reformed Church in America' 
    81='Reformed Presbyterian Church' 30='Roman Catholic' 92='Russian Orthodox' 
    95='Seventh Day Adventist' 75='Southern Baptist' 
    94='The Church of Jesus Christ of Latter-day Saints' 
    97='The Presbyterian Church in America' 88='Undenominational' 
    93='Unitarian Universalist' 84='United Brethren Church' 
    76='United Church of Christ' 71='United Methodist' 89='Wesleyan' 
    33='Wisconsin Evangelical Lutheran Synod' 99='Other (none of the above)' 
-2='Not applicable';
  value level1f 1='Yes' 0='Implied no';
  value level1a 1='Yes' 0='Implied no';
  value level1b 1='Yes' 0='implied no';
  value level2f 1='Yes' 0='Implied no';
  value level3f 1='Yes' 0='Implied no';
  value level4f 1='Yes' 0='Implied no';
  value level5f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level6f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level7f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level8f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level12f 0='Implied no';
  value level17f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level18f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value level19f 1='Yes' 0='Implied no' 
-2='Not applicable';
  value calsys 1='Semester' 2='Quarter' 3='Trimester' 4='Four-one-four plan' 
    5='Other academic year' 6='Differs by program' 7='Continuous' 
-2='Not applicable';
  value ft_ug 1='Yes' 2='No' 
-2='Not applicable';
  value ft_ftug 1='Yes' 2='No' 
-2='Not applicable';
  value ftgdnidp 1='Yes' 2='No' 
-2='Not applicable';
  value pt_ug 1='Yes' 2='No' 
-2='Not applicable';
  value pt_ftug 1='Yes' 2='No' 
-2='Not applicable';
  value ptgdnidp 1='Yes' 2='No' 
-2='Not applicable';
  value docpp 1='Yes' 
-2='Not applicable';
  value docppsp 1='Yes' 2='No' 
-2='Not applicable';
  value openadmp 1='Yes' 2='No' 
-2='Not applicable';
  value vet1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value vet2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value vet3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value vet4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value vet5f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value vet9f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value credits1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value credits2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value credits3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value credits4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo5f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo51f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo52f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo53f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo6f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo7f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo8f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo81f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo82f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo83f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value slo9f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value yrscoll 1='One' 2='Two' 3='Three' 4='Four' 5='Five' 6='Six' 8='Eight' 
-1='Not reported' 
-2='Not applicable';
  value stusrv1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value stusrv2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value stusrv3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value stusrv4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value stusrv8f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value stusrv9f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres5f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres6f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value libres9f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value tuitpl 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value tuitpl1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value tuitpl2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value tuitpl3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value tuitpl4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstnugc 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstnugp 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstnugn 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstngc 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstngp 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstngn 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value distcrs 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value distpgs 1='Yes' 0='Implied no' 
-1='Not applicable' 
-2='Not reported';
  value dstnced1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstnced2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value dstnced3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value distnced 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value disab 1='3 percent or less' 2='More than 3 percent' 
-1='Not reported' 
-2='Not applicable';
  value alloncam 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value tuitvary 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value room 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value board 1='Yes, number of meals in the maximum meal plan offered' 
    2='Yes, number of meals per week can vary' 3='No' 
-1='Not reported' 
-2='Not applicable';
  value athassoc 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value assoc1f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value assoc2f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value assoc3f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value assoc4f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value assoc5f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value assoc6f 1='Yes' 0='Implied no' 
-1='Not reported' 
-2='Not applicable';
  value sport1f 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value confno1f 372='American Athletic Conference' 
    176='American Rivers Conference' 204='American Southwest Conference' 
    316='Appalachian Athletic Conference' 102='Atlantic Coast Conference' 
    135='Atlantic Sun Conference' 104='Big East Conference' 
    105='Big Sky Conference' 106='Big South Conference' 107='Big Ten Conference' 
    108='Big Twelve Conference' 165='Centennial Conference' 
    140='Central Intercollegiate Athletic Association' 
    355='Central States Football League' 
    304='Chicagoland Collegiate Athletic Conference' 
    167='College Conference of Illinois and Wisconsin' 
    110='Colonial Athletic Association' 168='Commonwealth Coast Conference' 
    139='Conference Carolinas' 111='Conference USA' 
    377='Continental Athletic Conference' 113='Division I-A Independents' 
    170='Division III Independents' 151='East Coast Conference' 
    366='Eastern Collegiate Football Conference' 172='Empire Eight' 
    333='The Sun Conference' 173='Freedom Football Conference' 
    352='Frontier Conference' 302='Golden State Athletic Conference' 
    367='Great American Conference' 
    144='Great Lakes Intercollegiate Athletic Conference' 
    371='Great Midwest Athletic Conference' 145='Great Lakes Valley Conference' 
    213='Great Northwest Athletic Conference' 
    311='Great Plains Athletic Conference' 146='Gulf South Conference' 
    320='Heart of America Athletic Conference' 
    175='Heartland Collegiate Athletic Conference' 
    327='Independent Northeast Region' 335='Independent Southeast Region' 
    117='Ivy Group' 309='Kansas Collegiate Athletic Conference' 
    147='Lone Star Conference' 
    179='Massachusetts State College Athletic Association' 
    180='Michigan Intercollegiate Athletic Association' 
    148='Mid-America Intercollegiate Athletic Association' 
    119='Mid-American Conference' 121='Mid-Eastern Athletic Conference' 
    315='Mid-South Conference' 356='Mid-States Football Association' 
    181='Middle Atlantic States Athletic Corporation' 182='Midwest Conference' 
    183='Minnesota Intercollegiate Athletic Conference' 
    123='Missouri Valley Conference' 370='Mountain East Conference' 
    203='Mountain West Conference' 184='New England Football Conference' 
    185='New England Small College Athletic Conference' 
    186='New England Women^s & Men^s Athletic Conference' 
    187='New Jersey Athletic Conference' 189='North Coast Athletic Conference' 
    373='North Star Athletic Association' 153='Northeast 10 Conference' 
    125='Northeast Conference' 359='Northern Athletics Collegiate Conference' 
    155='Northern Sun Intercollegiate Conference' 205='Northwest Conference' 
    191='Ohio Athletic Conference' 126='Ohio Valley Conference' 
    192='Old Dominion Athletic Conference' 127='Pacific-12 Conference' 
    128='Patriot League' 158='Pennsylvania State Athletic Conference' 
    129='Pioneer Football League' 194='Presidents^ Athletic Conference' 
    159='Rocky Mountain Athletic Conference' 340='Sooner Athletic Conference' 
    160='South Atlantic Conference' 130='Southeastern Conference' 
    368='Southern Athletic Association' 
    197='Southern California Intercollegiate Athletic Conference' 
    198='Southern Collegiate Athletic Conference' 131='Southern Conference' 
    161='Southern Intercollegiate Athletic Conference' 132='Southland Conference' 
    133='Southwestern Athletic Conference' 134='Sun Belt Conference' 
    171='USA South Athletic Conference' 200='University Athletic Association' 
    354='Upper Midwest Athletic Conference' 
    201='Upstate Collegiate Athletic Association' 
    163='West Virginia Intercollegiate Athletic Conference' 
    137='Western Athletic Conference' 
    202='Wisconsin Intercollegiate Athletic Conference' 342='Other' 
-1='Not reported' 
-2='Not applicable';
  value sport2f 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value confno2f 214='Allegheny Mountain Collegiate Conference' 
    101='America East' 372='American Athletic Conference' 
    319='American Midwest Conference' 176='American Rivers Conference' 
    204='American Southwest Conference' 316='Appalachian Athletic Conference' 
    103='Atlantic 10 Conference' 102='Atlantic Coast Conference' 
    375='Atlantic East Conference' 135='Atlantic Sun Conference' 
    104='Big East Conference' 105='Big Sky Conference' 106='Big South Conference' 
    107='Big Ten Conference' 108='Big Twelve Conference' 
    109='Big West Conference' 138='California Collegiate Athletic Association' 
    301='California Pacific Conference' 164='Capital Athletic Conference' 
    328='Cascade Collegiate Conference' 165='Centennial Conference' 
    323='Central Atlantic Collegiate Conference' 
    140='Central Intercollegiate Athletic Association' 
    304='Chicagoland Collegiate Athletic Conference' 
    166='City University of New York Athletic Conference' 
    167='College Conference of Illinois and Wisconsin' 
    110='Colonial Athletic Association' 364='Colonial States Athletic Conference' 
    168='Commonwealth Coast Conference' 207='Commonwealth Conference' 
    139='Conference Carolinas' 111='Conference USA' 
    377='Continental Athletic Conference' 305='Crossroads League' 
    141='Division II Independents' 170='Division III Independents' 
    151='East Coast Conference' 115='Eastern College Athletic Conference' 
    172='Empire Eight' 333='The Sun Conference' 208='Freedom Conference' 
    352='Frontier Conference' 302='Golden State Athletic Conference' 
    367='Great American Conference' 
    144='Great Lakes Intercollegiate Athletic Conference' 
    371='Great Midwest Athletic Conference' 145='Great Lakes Valley Conference' 
    174='Great Northeast Athletic Conference' 
    213='Great Northwest Athletic Conference' 
    311='Great Plains Athletic Conference' 337='Gulf Coast Athletic Conference' 
    146='Gulf South Conference' 320='Heart of America Athletic Conference' 
    175='Heartland Collegiate Athletic Conference' 209='Heartland Conference' 
    122='Horizon League' 318='Independent Mid-South Region' 
    322='Independent Midwest Region' 327='Independent Northeast Region' 
    335='Independent Southeast Region' 117='Ivy Group' 
    309='Kansas Collegiate Athletic Conference' 361='Landmark Conference' 
    178='Little East Conference' 147='Lone Star Conference' 
    179='Massachusetts State College Athletic Association' 
    118='Metro Atlantic Athletic Conference' 
    180='Michigan Intercollegiate Athletic Association' 
    148='Mid-America Intercollegiate Athletic Association' 
    119='Mid-American Conference' 121='Mid-Eastern Athletic Conference' 
    315='Mid-South Conference' 181='Middle Atlantic States Athletic Corporation' 
    182='Midwest Conference' 183='Minnesota Intercollegiate Athletic Conference' 
    123='Missouri Valley Conference' 370='Mountain East Conference' 
    203='Mountain West Conference' 363='New England Collegiate Conference' 
    185='New England Small College Athletic Conference' 
    186='New England Women^s & Men^s Athletic Conference' 
    187='New Jersey Athletic Conference' 215='North Atlantic Conference' 
    189='North Coast Athletic Conference' 365='United East Conference' 
    373='North Star Athletic Association' 153='Northeast 10 Conference' 
    125='Northeast Conference' 359='Northern Athletics Collegiate Conference' 
    155='Northern Sun Intercollegiate Conference' 205='Northwest Conference' 
    191='Ohio Athletic Conference' 126='Ohio Valley Conference' 
    192='Old Dominion Athletic Conference' 156='Pacific West Conference' 
    127='Pacific-12 Conference' 128='Patriot League' 157='Peach Belt Conference' 
    158='Pennsylvania State Athletic Conference' 
    194='Presidents^ Athletic Conference' 353='Red River Athletic Conference' 
    374='River States' 159='Rocky Mountain Athletic Conference' 
    196='Skyline Conference' 340='Sooner Athletic Conference' 
    160='South Atlantic Conference' 130='Southeastern Conference' 
    368='Southern Athletic Association' 
    197='Southern California Intercollegiate Athletic Conference' 
    198='Southern Collegiate Athletic Conference' 131='Southern Conference' 
    161='Southern Intercollegiate Athletic Conference' 
    334='Southern States Athletic Conference' 132='Southland Conference' 
    133='Southwestern Athletic Conference' 
    195='St. Louis Intercollegiate Athletic Conference' 
    199='State University of New York Athletic Conference' 
    134='Sun Belt Conference' 162='Sunshine State Conference' 
    120='The Summit League' 171='USA South Athletic Conference' 
    200='University Athletic Association' 354='Upper Midwest Athletic Conference' 
    201='Upstate Collegiate Athletic Association' 136='West Coast Conference' 
    163='West Virginia Intercollegiate Athletic Conference' 
    137='Western Athletic Conference' 
    202='Wisconsin Intercollegiate Ath Conference' 
    307='Wolverine-Hoosier Athletic Conference' 342='Other' 
-1='Not reported' 
-2='Not applicable';
  value sport3f 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value confno3f 214='Allegheny Mountain Collegiate Conference' 
    101='America East' 372='American Athletic Conference' 
    319='American Midwest Conference' 176='American Rivers Conference' 
    204='American Southwest Conference' 316='Appalachian Athletic Conference' 
    369='Association of Independent Institutions' 103='Atlantic 10 Conference' 
    102='Atlantic Coast Conference' 375='Atlantic East Conference' 
    135='Atlantic Sun Conference' 104='Big East Conference' 
    105='Big Sky Conference' 106='Big South Conference' 107='Big Ten Conference' 
    108='Big Twelve Conference' 109='Big West Conference' 
    138='California Collegiate Athletic Association' 
    301='California Pacific Conference' 164='Capital Athletic Conference' 
    328='Cascade Collegiate Conference' 165='Centennial Conference' 
    323='Central Atlantic Collegiate Conference' 
    140='Central Intercollegiate Athletic Association' 
    304='Chicagoland Collegiate Athletic Conference' 
    166='City University of New York Athletic Conference' 
    167='College Conference of Illinois and Wisconsin' 
    110='Colonial Athletic Association' 364='Colonial States Athletic Conference' 
    168='Commonwealth Coast Conference' 207='Commonwealth Conference' 
    139='Conference Carolinas' 111='Conference USA' 
    377='Continental Athletic Conference' 305='Crossroads League' 
    170='Division III Independents' 151='East Coast Conference' 
    115='Eastern College Athletic Conference' 
    332='Eastern Intercollegiate Athletic Conference' 172='Empire Eight' 
    333='The Sun Conference' 208='Freedom Conference' 
    302='Golden State Athletic Conference' 367='Great American Conference' 
    144='Great Lakes Intercollegiate Athletic Conference' 
    371='Great Midwest Athletic Conference' 145='Great Lakes Valley Conference' 
    174='Great Northeast Athletic Conference' 
    213='Great Northwest Athletic Conference' 
    311='Great Plains Athletic Conference' 337='Gulf Coast Athletic Conference' 
    146='Gulf South Conference' 320='Heart of America Athletic Conference' 
    175='Heartland Collegiate Athletic Conference' 209='Heartland Conference' 
    122='Horizon League' 318='Independent Mid-South Region' 
    322='Independent Midwest Region' 335='Independent Southeast Region' 
    117='Ivy Group' 309='Kansas Collegiate Athletic Conference' 
    361='Landmark Conference' 178='Little East Conference' 
    147='Lone Star Conference' 
    179='Massachusetts State College Athletic Association' 
    118='Metro Atlantic Athletic Conference' 
    180='Michigan Intercollegiate Athletic Association' 
    148='Mid-America Intercollegiate Athletic Association' 
    119='Mid-American Conference' 121='Mid-Eastern Athletic Conference' 
    315='Mid-South Conference' 181='Middle Atlantic States Athletic Corporation' 
    182='Midwest Conference' 183='Minnesota Intercollegiate Athletic Conference' 
    123='Missouri Valley Conference' 370='Mountain East Conference' 
    203='Mountain West Conference' 363='New England Collegiate Conference' 
    185='New England Small College Athletic Conference' 
    186='New England Women^s & Men^s Athletic Conference' 
    187='New Jersey Athletic Conference' 215='North Atlantic Conference' 
    189='North Coast Athletic Conference' 365='United East Conference' 
    373='North Star Athletic Association' 153='Northeast 10 Conference' 
    125='Northeast Conference' 359='Northern Athletics Collegiate Conference' 
    155='Northern Sun Intercollegiate Conference' 205='Northwest Conference' 
    191='Ohio Athletic Conference' 126='Ohio Valley Conference' 
    192='Old Dominion Athletic Conference' 156='Pacific West Conference' 
    127='Pacific-12 Conference' 128='Patriot League' 157='Peach Belt Conference' 
    158='Pennsylvania State Athletic Conference' 
    194='Presidents^ Athletic Conference' 353='Red River Athletic Conference' 
    374='River States' 159='Rocky Mountain Athletic Conference' 
    196='Skyline Conference' 340='Sooner Athletic Conference' 
    160='South Atlantic Conference' 130='Southeastern Conference' 
    368='Southern Athletic Association' 
    197='Southern California Intercollegiate Athletic Conference' 
    198='Southern Collegiate Athletic Conference' 131='Southern Conference' 
    161='Southern Intercollegiate Athletic Conference' 
    334='Southern States Athletic Conference' 132='Southland Conference' 
    133='Southwestern Athletic Conference' 
    195='St. Louis Intercollegiate Athletic Conference' 
    199='State University of New York Athletic Conference' 
    134='Sun Belt Conference' 162='Sunshine State Conference' 
    120='The Summit League' 171='USA South Athletic Conference' 
    200='University Athletic Association' 354='Upper Midwest Athletic Conference' 
    201='Upstate Collegiate Athletic Association' 136='West Coast Conference' 
    163='West Virginia Intercollegiate Athletic Conference' 
    137='Western Athletic Conference' 
    202='Wisconsin Intercollegiate Athletic Conference' 
    307='Wolverine-Hoosier Athletic Conference' 342='Other' 
-1='Not reported' 
-2='Not applicable';
  value sport4f 1='Yes' 2='No' 
-1='Not reported' 
-2='Not applicable';
  value confno4f 214='Allegheny Mountain Collegiate Conference' 
    101='America East' 372='American Athletic Conference' 
    376='American Collegiate Athletic Association' 
    319='American Midwest Conference' 176='American Rivers Conference' 
    204='American Southwest Conference' 316='Appalachian Athletic Conference' 
    103='Atlantic 10 Conference' 102='Atlantic Coast Conference' 
    375='Atlantic East Conference' 135='Atlantic Sun Conference' 
    104='Big East Conference' 105='Big Sky Conference' 106='Big South Conference' 
    107='Big Ten Conference' 108='Big Twelve Conference' 
    109='Big West Conference' 138='California Collegiate Athletic Association' 
    301='California Pacific Conference' 164='Capital Athletic Conference' 
    328='Cascade Collegiate Conference' 165='Centennial Conference' 
    323='Central Atlantic Collegiate Conference' 
    140='Central Intercollegiate Athletic Association' 
    304='Chicagoland Collegiate Athletic Conference' 
    166='City University of New York Athletic Conference' 
    167='College Conference of Illinois and Wisconsin' 
    110='Colonial Athletic Association' 364='Colonial States Athletic Conference' 
    168='Commonwealth Coast Conference' 207='Commonwealth Conference' 
    139='Conference Carolinas' 111='Conference USA' 
    377='Continental Athletic Conference' 305='Crossroads League' 
    141='Division II Independents' 170='Division III Independents' 
    151='East Coast Conference' 115='Eastern College Athletic Conference' 
    172='Empire Eight' 333='The Sun Conference' 208='Freedom Conference' 
    352='Frontier Conference' 302='Golden State Athletic Conference' 
    367='Great American Conference' 
    144='Great Lakes Intercollegiate Athletic Conference' 
    145='Great Lakes Valley Conference' 371='Great Midwest Athletic Conference' 
    174='Great Northeast Athletic Conference' 
    213='Great Northwest Athletic Conference' 
    311='Great Plains Athletic Conference' 337='Gulf Coast Athletic Conference' 
    146='Gulf South Conference' 320='Heart of America Athletic Conference' 
    175='Heartland Collegiate Athletic Conference' 122='Horizon League' 
    318='Independent Mid-South Region' 322='Independent Midwest Region' 
    335='Independent Southeast Region' 117='Ivy Group' 
    309='Kansas Collegiate Athletic Conference' 361='Landmark Conference' 
    178='Little East Conference' 147='Lone Star Conference' 
    179='Massachusetts State College Athletic Association' 
    118='Metro Atlantic Athletic Conference' 
    180='Michigan Intercollegiate Athletic Association' 
    148='Mid-America Intercollegiate Athletic Association' 
    119='Mid-American Conference' 121='Mid-Eastern Athletic Conference' 
    315='Mid-South Conference' 181='Middle Atlantic States Athletic Corporation' 
    182='Midwest Conference' 183='Minnesota Intercollegiate Athletic Conference' 
    123='Missouri Valley Conference' 370='Mountain East Conference' 
    124='Mountain Pacific Sports Federation' 203='Mountain West Conference' 
    363='New England Collegiate Conference' 
    185='New England Small College Athletic Conference' 
    186='New England Women^s & Men^s Athletic Conference' 
    187='New Jersey Athletic Conference' 215='North Atlantic Conference' 
    189='North Coast Athletic Conference' 365='United East Conference' 
    373='North Star Athletic Association' 153='Northeast 10 Conference' 
    125='Northeast Conference' 359='Northern Athletics Conference' 
    155='Northern Sun Intercollegiate Conference' 205='Northwest Conference' 
    191='Ohio Athletic Conference' 126='Ohio Valley Conference' 
    192='Old Dominion Athletic Conference' 156='Pacific West Conference' 
    127='Pacific-12 Conference' 128='Patriot League' 157='Peach Belt Conference' 
    158='Pennsylvania State Athletic Conference' 
    194='Presidents^ Athletic Conference' 353='Red River Athletic Conference' 
    374='River States' 159='Rocky Mountain Athletic Conference' 
    196='Skyline Conference' 340='Sooner Athletic Conference' 
    160='South Atlantic Conference' 130='Southeastern Conference' 
    368='Southern Athletic Association' 
    197='Southern California Intercollegiate Athletic Conference' 
    198='Southern Collegiate Athletic Conference' 131='Southern Conference' 
    161='Southern Intercollegiate Athletic Conference' 
    334='Southern States Athletic Conference' 132='Southland Conference' 
    133='Southwestern Athletic Conference' 
    195='St. Louis Intercollegiate Athletic Conference' 
    199='State University of New York Athletic Conference' 
    134='Sun Belt Conference' 162='Sunshine State Conference' 
    120='The Summit League' 171='USA South Athletic Conference' 
    200='University Athletic Association' 354='Upper Midwest Athletic Conference' 
    201='Upstate Collegiate Athletic Association' 136='West Coast Conference' 
    163='West Virginia Intercollegiate Athletic Conference' 
    137='Western Athletic Conference' 
    202='Wisconsin Intercollegiate Athletic Conference' 
    307='Wolverine-Hoosier Athletic Conference' 342='Other' 
-1='Not reported' 
-2='Not applicable';
    value $ximpflg 
  'A'='Not applicable'
  'B'='Institution left item blank' 
    'C'='Analyst corrected reported value'
    'D'='Do not know' 
    'G'='Data generated from other data values' 
    'H'='Value not derived - data not usable'
    'J'='Logical imputation' 
    'K'='Ratio adjustment'
    'L'='Imputed using the Group Median procedure' 
    'N'='Imputed using Nearest Neighbor procedure' 
    'P'='Imputed using Carry Forward procedure'
    'R'='Reported' 
    'Y'='Specific professional practice program n'
    'Z'='Implied zero';
    
options fmtsearch=(&libraryName);

data &libraryName..&file;
infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 
    lrecl=32736;
  informat unitid 6. peo1istr 2. peo2istr 2. peo3istr 2. peo4istr 2. peo5istr 2. 
    peo6istr 2. cntlaffi 2. pubprime 2. pubsecon 2. relaffil 3. level1 2. level1a 
    2. level1b 2. level2 2. level3 2. level4 2. level5 2. level6 2. level7 2. 
    level8 2. level12 2. level17 2. level18 2. level19 2. calsys 2. ft_ug 2. 
    ft_ftug 2. ftgdnidp 2. pt_ug 2. pt_ftug 2. ptgdnidp 2. docpp 2. docppsp 2. 
    openadmp 2. vet1 2. vet2 2. vet3 2. vet4 2. vet5 2. vet9 2. credits1 2. 
    credits2 2. credits3 2. credits4 2. slo5 2. slo51 2. slo52 2. slo53 2. slo6 
    2. slo7 2. slo8 2. slo81 2. slo82 2. slo83 2. slo9 2. yrscoll 2. stusrv1 2. 
    stusrv2 2. stusrv3 2. stusrv4 2. stusrv8 2. stusrv9 2. libres1 2. libres2 2. 
    libres3 2. libres4 2. libres5 2. libres6 2. libres9 2. tuitpl 2. tuitpl1 2. 
    tuitpl2 2. tuitpl3 2. tuitpl4 2. dstnugc 2. dstnugp 2. dstnugn 2. dstngc 2. 
    dstngp 2. dstngn 2. distcrs 2. distpgs 2. dstnced1 2. dstnced2 2. dstnced3 2. 
    distnced 2. disab 2. xdisabpc $1. disabpct 3. alloncam 2. tuitvary 2. room 2. 
    xroomcap $1. roomcap 5. board 2. xmealswk $1. mealswk 2. xroomamt $1. roomamt 
    5. xbordamt $1. boardamt 5. xrmbdamt $1. rmbrdamt 5. xappfeeu $1. applfeeu 3. 
    xappfeeg $1. applfeeg 3. athassoc 2. assoc1 2. assoc2 2. assoc3 2. assoc4 2. 
    assoc5 2. assoc6 2. sport1 2. confno1 3. sport2 2. confno2 3. sport3 2. 
    confno3 3. sport4 2. confno4 3.;
  input unitid peo1istr peo2istr peo3istr peo4istr peo5istr peo6istr cntlaffi 
    pubprime pubsecon relaffil level1 level1a level1b level2 level3 level4 level5 
    level6 level7 level8 level12 level17 level18 level19 calsys ft_ug ft_ftug 
    ftgdnidp pt_ug pt_ftug ptgdnidp docpp docppsp openadmp vet1 vet2 vet3 vet4 
    vet5 vet9 credits1 credits2 credits3 credits4 slo5 slo51 slo52 slo53 slo6 
    slo7 slo8 slo81 slo82 slo83 slo9 yrscoll stusrv1 stusrv2 stusrv3 stusrv4 
    stusrv8 stusrv9 libres1 libres2 libres3 libres4 libres5 libres6 libres9 
    tuitpl tuitpl1 tuitpl2 tuitpl3 tuitpl4 dstnugc dstnugp dstnugn dstngc dstngp 
    dstngn distcrs distpgs dstnced1 dstnced2 dstnced3 distnced disab xdisabpc $
disabpct alloncam tuitvary room xroomcap $
roomcap board xmealswk $
mealswk xroomamt $
roomamt xbordamt $
boardamt xrmbdamt $
rmbrdamt xappfeeu $
applfeeu xappfeeg $
applfeeg athassoc assoc1 assoc2 assoc3 assoc4 assoc5 assoc6 sport1 confno1 
    sport2 confno2 sport3 confno3 sport4 confno4;
  label unitid='Unique identification number of the institution' 
    peo1istr='Occupational' peo2istr='Academic' 
    peo3istr='Continuing professional' peo4istr='Recreational or avocational' 
    peo5istr='Adult basic remedial or high school equivalent' 
    peo6istr='Secondary (high school)' 
    cntlaffi='Institutional control or affiliation' 
    pubprime='Primary public control' pubsecon='Secondary public control' 
    relaffil='Religious affiliation' level1='Certificate of less than 1 year' 
    level1a='Certificate of less than 12 weeks' 
    level1b='Certificate of at least 12 weeks, but less than 1 year' 
    level2='Certifiicate of at least 1 year, but less than 2 years' 
    level3='Associate^s degree' 
    level4='Certificate of at least 2 years, but less than 4 years' 
    level5='Bachelor^s degree' level6='Postbaccalaureate certificate' 
    level7='Master^s degree' level8='Post-master^s certificate' 
    level12='Other degree' level17='Doctor^s degree - research/scholarship' 
    level18='Doctor^s degree - professional practice' 
    level19='Doctor^s degree - other' calsys='Calendar system' 
    ft_ug='Full-time undergraduate students are enrolled' ft_ftug='Full time first-time degree/certificate-seeking undergraduate students enrolled' ftgdnidp='Full-time graduate (not including doctor^s professional practice) students are enrolled' 
    pt_ug='Part-time undergraduate students are enrolled' pt_ftug='Part time first-time degree/certificate-seeking undergraduate students enrolled' ptgdnidp='Part-time graduate (not including doctor^s professional practice) students are enrolled' 
    docpp='Doctor^s professional practice students are enrolled' docppsp='Doctor^s professional practice students are enrolled in programs formerly designated as first-professional' 
    openadmp='Open admission policy' vet1='Yellow Ribbon Program (officially known as Post-9/11 GI Bill, Yellow Ribbon Program)' 
    vet2='Credit for military training' vet3='Dedicated point of contact for support services for veterans, military servicemembers, and their families' 
    vet4='Recognized student veteran organization' 
    vet5='Member of Servicemembers Opportunity Colleges' vet9='Services  and programs are not available to veterans, military servicemembers, or their families?' 
    credits1='Dual enrollment' credits2='Credit for life experiences' 
    credits3='Advanced placement (AP) credits' 
    credits4='Institution does not accept dual, credit for life, or AP credits' 
    slo5='ROTC' slo51='ROTC - Army' slo52='ROTC - Navy' slo53='ROTC - Air Force' 
    slo6='Study abroad' slo7='Weekend/evening  college' 
    slo8='Teacher certification (below the postsecondary level)' slo81='Teacher certification: Students can complete their preparation in certain areas of specialization' slo82='Teacher certification: Students must complete their preparation at another institution for certain areas of specialization' slo83='Teacher certification: Approved by the state for initial certifcation or licensure of teachers.' 
    slo9='None of the above special learning opportunities are offered' 
    yrscoll='Years of college-level work required' stusrv1='Remedial services' 
    stusrv2='Academic/career counseling service' 
    stusrv3='Employment services for students' 
    stusrv4='Placement services for completers' 
    stusrv8='On-campus day care for students^ children' 
    stusrv9='None of the above selected services are offered' 
    libres1='Library resources/services: Physical facilities' 
    libres2='Library resources/services: Organized collection of printed materials' 
    libres3='Library resources/services: Access to digital/electronic resources' libres4='Library resources/services: Staff trained to provide and interpret library materials' 
    libres5='Library resources/services: Established library hours' libres6='Library resources/services: Access to library collections that are shared with other institutions' 
    libres9='Library resources/services not provided' 
    tuitpl='Any alternative tuition plans offered by institution' 
    tuitpl1='Tuition guaranteed plan' tuitpl2='Prepaid tuition plan' 
    tuitpl3='Tuition payment plan' tuitpl4='Other alternative tuition plan' 
    dstnugc='Undergraduate level distance education courses offered' 
    dstnugp='Undergraduate level distance education programs offered' 
    dstnugn='Undergraduate level distance education not offered' 
    dstngc='Graduate level distance education courses offered' 
    dstngp='Graduate level distance education programs offered' 
    dstngn='Graduate level distance education not offered' 
    distcrs='Distance education courses offered' 
    distpgs='Distance education programs offered' dstnced1='Undergraduate level programs or courses are offered via distance education' dstnced2='Graduate level programs or courses are offered via distance education' 
    dstnced3='Does not offer distance education opportunities' 
    distnced='All programs offered completely via distance education' disab='Percent indicator of undergraduates formally registered as students with disabilities' xdisabpc='Imputation field for disabpct - Percent of undergraduates, who are formally registered as students with disabilities, when percentage is more than 3 percent' disabpct='Percent of undergraduates, who are formally registered as students with disabilities, when percentage is more than 3 percent' alloncam='Full-time, first-time degree/certificate-seeking students required to live on campus' tuitvary='Tuition charge varies for in-district, in-state, out-of-state students' 
    room='Institution provide on-campus housing' 
    xroomcap='Imputation field for roomcap - Total dormitory capacity' 
    roomcap='Total dormitory capacity' 
    board='Institution provides board or meal plan' xmealswk='Imputation field for mealswk - Number of meals per week in board charge' 
    mealswk='Number of meals per week in board charge' 
    xroomamt='Imputation field for roomamt - Typical room charge for academic year' 
    roomamt='Typical room charge for academic year' xbordamt='Imputation field for boardamt - Typical board charge for academic year' 
    boardamt='Typical board charge for academic year' 
    xrmbdamt='Imputation field for rmbrdamt - Combined charge for room and board' 
    rmbrdamt='Combined charge for room and board' 
    xappfeeu='Imputation field for applfeeu - Undergraduate application fee' 
    applfeeu='Undergraduate application fee' 
    xappfeeg='Imputation field for applfeeg - Graduate application fee' 
    applfeeg='Graduate application fee' 
    athassoc='Member of National Athletic Association' 
    assoc1='Member of National Collegiate Athletic Association (NCAA)' 
    assoc2='Member of National Association of Intercollegiate Athletics (NAIA)' 
    assoc3='Member of National Junior College Athletic  Association (NJCAA)' 
    assoc4='Member of National Small College Athletic Association (NSCAA)' 
    assoc5='Member of National Christian College Athletic Association (NCCAA)' 
    assoc6='Member of other national athletic association not listed above' 
    sport1='NCAA/NAIA member for football' 
    confno1='NCAA/NAIA conference number football' 
    sport2='NCAA/NAIA member for basketball' 
    confno2='NCAA/NAIA conference number basketball' 
    sport3='NCAA/NAIA member for baseball' 
    confno3='NCAA/NAIA conference number baseball' 
    sport4='NCAA/NAIA member for cross country/track' 
    confno4='NCAA/NAIA conference number cross country/track';
    
format xdisabpc-character-xappfeeg $ximpflg.
  peo1istr peo1istr.
  peo2istr peo2istr.
  peo3istr peo3istr.
  peo4istr peo4istr.
  peo5istr peo5istr.
  peo6istr peo6istr.
  cntlaffi cntlaffi.
  pubprime pubprime.
  pubsecon pubsecon.
  relaffil relaffil.
  level1 level1f.
  level1a level1a.
  level1b level1b.
  level2 level2f.
  level3 level3f.
  level4 level4f.
  level5 level5f.
  level6 level6f.
  level7 level7f.
  level8 level8f.
  level12 level12f.
  level17 level17f.
  level18 level18f.
  level19 level19f.
  calsys calsys.
  ft_ug ft_ug.
  ft_ftug ft_ftug.
  ftgdnidp ftgdnidp.
  pt_ug pt_ug.
  pt_ftug pt_ftug.
  ptgdnidp ptgdnidp.
  docpp docpp.
  docppsp docppsp.
  openadmp openadmp.
  vet1 vet1f.
  vet2 vet2f.
  vet3 vet3f.
  vet4 vet4f.
  vet5 vet5f.
  vet9 vet9f.
  credits1 credits1f.
  credits2 credits2f.
  credits3 credits3f.
  credits4 credits4f.
  slo5 slo5f.
  slo51 slo51f.
  slo52 slo52f.
  slo53 slo53f.
  slo6 slo6f.
  slo7 slo7f.
  slo8 slo8f.
  slo81 slo81f.
  slo82 slo82f.
  slo83 slo83f.
  slo9 slo9f.
  yrscoll yrscoll.
  stusrv1 stusrv1f.
  stusrv2 stusrv2f.
  stusrv3 stusrv3f.
  stusrv4 stusrv4f.
  stusrv8 stusrv8f.
  stusrv9 stusrv9f.
  libres1 libres1f.
  libres2 libres2f.
  libres3 libres3f.
  libres4 libres4f.
  libres5 libres5f.
  libres6 libres6f.
  libres9 libres9f.
  tuitpl tuitpl.
  tuitpl1 tuitpl1f.
  tuitpl2 tuitpl2f.
  tuitpl3 tuitpl3f.
  tuitpl4 tuitpl4f.
  dstnugc dstnugc.
  dstnugp dstnugp.
  dstnugn dstnugn.
  dstngc dstngc.
  dstngp dstngp.
  dstngn dstngn.
  distcrs distcrs.
  distpgs distpgs.
  dstnced1 dstnced1f.
  dstnced2 dstnced2f.
  dstnced3 dstnced3f.
  distnced distnced.
  disab disab.
  alloncam alloncam.
  tuitvary tuitvary.
  room room.
  board board.
  athassoc athassoc.
  assoc1 assoc1f.
  assoc2 assoc2f.
  assoc3 assoc3f.
  assoc4 assoc4f.
  assoc5 assoc5f.
  assoc6 assoc6f.
  sport1 sport1f.
  confno1 confno1f.
  sport2 sport2f.
  confno2 confno2f.
  sport3 sport3f.
  confno3 confno3f.
  sport4 sport4f.
  confno4 confno4f.;
run;

Proc Freq;
  Tables peo1istr peo2istr peo3istr peo4istr peo5istr peo6istr cntlaffi pubprime 
    pubsecon relaffil level1 level1a level1b level2 level3 level4 level5 level6 
    level7 level8 level12 level17 level18 level19 calsys ft_ug ft_ftug ftgdnidp 
    pt_ug pt_ftug ptgdnidp docpp docppsp openadmp vet1 vet2 vet3 vet4 vet5 vet9 
    credits1 credits2 credits3 credits4 slo5 slo51 slo52 slo53 slo6 slo7 slo8 
    slo81 slo82 slo83 slo9 yrscoll stusrv1 stusrv2 stusrv3 stusrv4 stusrv8 
    stusrv9 libres1 libres2 libres3 libres4 libres5 libres6 libres9 tuitpl 
    tuitpl1 tuitpl2 tuitpl3 tuitpl4 dstnugc dstnugp dstnugn dstngc dstngp dstngn 
    distcrs distpgs dstnced1 dstnced2 dstnced3 distnced disab xdisabpc alloncam 
    tuitvary room xroomcap board xmealswk xroomamt xbordamt xrmbdamt xappfeeu 
    xappfeeg athassoc assoc1 assoc2 assoc3 assoc4 assoc5 assoc6 sport1 confno1 
    sport2 confno2 sport3 confno3 sport4 confno4 / missing;

Proc Summary print n sum mean min max;
  var disabpct roomcap mealswk roomamt boardamt rmbrdamt applfeeu applfeeg;
run;

