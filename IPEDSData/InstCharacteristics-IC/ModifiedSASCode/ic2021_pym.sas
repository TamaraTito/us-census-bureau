%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let file=ic2021_py;
%let libraryName=IPEDICM;
libname &libraryName "&path/SASData";

proc format library=&libraryName;

Proc Format;
value $cipcode1f
'01.0307'='Horse Husbandry/Equine Science and Management' 
'01.0504'='Dog/Pet/Animal Grooming' 
'01.8101'='Veterinary Sciences/Veterinary Clinical Sciences, General' 
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'09.0701'='Radio and Television' 
'09.0702'='Digital Communication and Media/Multimedia' 
'09.0799'='Radio, Television, and Digital Communication, Other' 
'09.0903'='Advertising' 
'10.0202'='Radio and Television Broadcasting Technology/Technician' 
'10.0203'='Recording Arts Technology/Technician' 
'10.0299'='Audiovisual Communications Technologies/Technicians, Other' 
'10.0304'='Animation, Interactive Technology, Video Graphics, and Special Effects' 
'10.9999'='Communications Technologies/Technicians and Support Services, Other' 
'11.0101'='Computer and Information Sciences, General' 
'11.0103'='Information Technology' 
'11.0205'='Computer Programming, Specific Platforms' 
'11.0401'='Information Science/Studies' 
'11.0601'='Data Entry/Microcomputer Applications, General' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.1001'='Network and System Administration/Administrator' 
'11.1006'='Computer Support Specialist' 
'12.0302'='Funeral Direction/Service' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0500'='Cooking and Related Culinary Arts, General' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0503'='Culinary Arts/Chef Training' 
'12.0599'='Culinary Arts and Related Services, Other' 
'12.0602'='Casino Dealing' 
'13.0406'='Higher Education/Higher Education Administration' 
'13.1207'='Montessori Teacher Education' 
'14.1001'='Electrical and Electronics Engineering' 
'15.0403'='Electromechanical/Electromechanical Engineering Technology/Technician' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0799'='Quality Control and Safety Technologies/Technicians, Other' 
'15.0801'='Aeronautical/Aerospace Engineering Technology/Technician' 
'15.1201'='Computer Engineering Technology/Technician' 
'19.0708'='Child Care and Support Services Management' 
'19.0709'='Child Care Provider/Assistant' 
'22.0302'='Legal Assistant/Paralegal' 
'23.0101'='English Language and Literature, General' 
'26.0102'='Biomedical Sciences, General' 
'31.0507'='Physical Fitness Technician' 
'43.0102'='Corrections' 
'43.0107'='Criminal Justice/Police Science' 
'43.0203'='Fire Science/Fire-fighting' 
'46.0000'='Construction Trades, General' 
'46.0301'='Electrical and Power Transmission Installation/Installer, General' 
'46.0302'='Electrician' 
'46.0303'='Lineworker' 
'46.0401'='Building/Property Maintenance' 
'46.0415'='Building Construction Technology/Technician' 
'47.0101'='Electrical/Electronics Equipment Installation and Repair Technology/Technician, General' 
'47.0104'='Computer Installation and Repair Technology/Technician' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0402'='Gunsmithing/Gunsmith' 
'47.0403'='Locksmithing and Safe Repair' 
'47.0404'='Musical Instrument Fabrication and Repair' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0607'='Airframe Mechanics and Aircraft Maintenance Technology/Technician' 
'47.0608'='Aircraft Powerplant Technology/Technician' 
'47.0609'='Avionics Maintenance Technology/Technician' 
'47.0611'='Motorcycle Maintenance and Repair Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0615'='Engine Machinist' 
'47.0616'='Marine Maintenance/Fitter and Ship Repair Technology/Technician' 
'48.0501'='Machine Tool Technology/Machinist' 
'48.0508'='Welding Technology/Welder' 
'48.0511'='Metal Fabricator' 
'48.0702'='Furniture Design and Manufacturing' 
'49.0102'='Airline/Commercial/Professional Pilot and Flight Crew' 
'49.0199'='Air Transportation, Other' 
'49.0202'='Construction/Heavy Equipment/Earthmoving Equipment Operation' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'49.0304'='Diver, Professional and Instructor' 
'50.0401'='Design and Visual Communications, General' 
'50.0408'='Interior Design' 
'50.0410'='Illustration' 
'50.0602'='Cinematography and Film/Video Production' 
'50.0699'='Film/Video and Photographic Arts, Other' 
'50.0702'='Fine/Studio Arts, General' 
'50.0713'='Jewelry Arts' 
'50.0913'='Music Technology' 
'51.0000'='Health Services/Allied Health/Health Sciences, General' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0705'='Medical Office Management/Administration' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0799'='Health and Medical Administrative Services, Other' 
'51.0801'='Medical/Clinical Assistant' 
'51.0802'='Clinical/Medical Laboratory Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0806'='Physical Therapy Assistant' 
'51.0812'='Respiratory Therapy Technician/Assistant' 
'51.0899'='Allied Health and Medical Assisting Services, Other' 
'51.0901'='Cardiovascular Technology/Technologist' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0920'='Magnetic Resonance Imaging (MRI) Technology/Technician' 
'51.1001'='Blood Bank Technology Specialist' 
'51.1004'='Clinical/Medical Laboratory Technician' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.3203'='Nursing Education' 
'51.3306'='Holistic/Integrative Health' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3503'='Somatic Bodywork' 
'51.3603'='Hypnotherapy/Hypnotherapist' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3808'='Nursing Science' 
'51.3899'='Registered Nursing, Nursing Administration, Nursing Research and Clinical Nursing, Other' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0201'='Business Administration and Management, General' 
'52.0301'='Accounting' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0407'='Business/Office Automation/Technology/Data Entry' 
'52.0410'='Traffic, Customs, and Transportation Clerk/Technician';
value prgmsr1f
1='Clock hours' 
2='Credit hours';
value $cipcode2f
'01.0504'='Dog/Pet/Animal Grooming' 
'01.1103'='Horticultural Science' 
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'09.0702'='Digital Communication and Media/Multimedia' 
'09.0799'='Radio, Television, and Digital Communication, Other' 
'09.0906'='Sports Communication' 
'09.0907'='International and Intercultural Communication' 
'10.0202'='Radio and Television Broadcasting Technology/Technician' 
'10.0203'='Recording Arts Technology/Technician' 
'11.0101'='Computer and Information Sciences, General' 
'11.0205'='Computer Programming, Specific Platforms' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0802'='Data Modeling/Warehousing and Database Administration' 
'11.0803'='Computer Graphics' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.1001'='Network and System Administration/Administrator' 
'11.1003'='Computer and Information Systems Security/Auditing/Information Assurance' 
'11.1006'='Computer Support Specialist' 
'11.9999'='Computer and Information Sciences and Support Services, Other' 
'12.0301'='Funeral Service and Mortuary Science, General' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0412'='Salon/Beauty Salon Management/Manager' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0502'='Bartending/Bartender' 
'12.0503'='Culinary Arts/Chef Training' 
'13.1207'='Montessori Teacher Education' 
'13.1399'='Teacher Education and Professional Development, Specific Subject Areas, Other' 
'14.0102'='Pre-Engineering' 
'14.1001'='Electrical and Electronics Engineering' 
'15.0303'='Electrical, Electronic, and Communications Engineering Technology/Technician' 
'15.0399'='Electrical/Electronic Engineering Technologies/Technicians, Other' 
'15.0404'='Instrumentation Technology/Technician' 
'15.0499'='Electromechanical Technologies/Technicians, Other' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0613'='Manufacturing Engineering Technology/Technician' 
'15.0803'='Automotive Engineering Technology/Technician' 
'15.0899'='Mechanical Engineering Related Technologies/Technicians, Other' 
'15.1202'='Computer/Computer Systems Technology/Technician' 
'15.1301'='Drafting and Design Technology/Technician, General' 
'15.1307'='3-D Modeling and Design Technology/Technician' 
'16.1701'='English as a Second Language' 
'22.0302'='Legal Assistant/Paralegal' 
'22.0303'='Court Reporting and Captioning/Court Reporter' 
'23.1301'='Writing, General' 
'31.0507'='Physical Fitness Technician' 
'31.9999'='Parks, Recreation, Leisure, Fitness, and Kinesiology, Other' 
'43.0102'='Corrections' 
'43.0103'='Criminal Justice/Law Enforcement Administration' 
'43.0104'='Criminal Justice/Safety Studies' 
'43.0107'='Criminal Justice/Police Science' 
'43.0203'='Fire Science/Fire-fighting' 
'46.0000'='Construction Trades, General' 
'46.0201'='Carpentry/Carpenter' 
'46.0301'='Electrical and Power Transmission Installation/Installer, General' 
'46.0302'='Electrician' 
'46.0401'='Building/Property Maintenance' 
'46.0403'='Building/Home/Construction Inspection/Inspector' 
'46.0415'='Building Construction Technology/Technician' 
'46.0503'='Plumbing Technology/Plumber' 
'47.0101'='Electrical/Electronics Equipment Installation and Repair Technology/Technician, General' 
'47.0103'='Communications Systems Installation and Repair Technology/Technician' 
'47.0199'='Electrical/Electronics Maintenance and Repair Technologies/Technicians, Other' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0302'='Heavy Equipment Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0403'='Locksmithing and Safe Repair' 
'47.0408'='Watchmaking and Jewelrymaking' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0607'='Airframe Mechanics and Aircraft Maintenance Technology/Technician' 
'47.0608'='Aircraft Powerplant Technology/Technician' 
'47.0611'='Motorcycle Maintenance and Repair Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0615'='Engine Machinist' 
'47.0616'='Marine Maintenance/Fitter and Ship Repair Technology/Technician' 
'48.0303'='Upholstery/Upholsterer' 
'48.0508'='Welding Technology/Welder' 
'49.0102'='Airline/Commercial/Professional Pilot and Flight Crew' 
'49.0202'='Construction/Heavy Equipment/Earthmoving Equipment Operation' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'49.0304'='Diver, Professional and Instructor' 
'50.0402'='Commercial and Advertising Art' 
'50.0411'='Game and Interactive Media Design' 
'50.0506'='Acting' 
'50.0601'='Film/Cinema/Media Studies' 
'50.0602'='Cinematography and Film/Video Production' 
'50.0699'='Film/Video and Photographic Arts, Other' 
'50.0702'='Fine/Studio Arts, General' 
'50.0713'='Jewelry Arts' 
'50.0913'='Music Technology' 
'51.0000'='Health Services/Allied Health/Health Sciences, General' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0603'='Dental Laboratory Technology/Technician' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0705'='Medical Office Management/Administration' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0799'='Health and Medical Administrative Services, Other' 
'51.0801'='Medical/Clinical Assistant' 
'51.0802'='Clinical/Medical Laboratory Assistant' 
'51.0803'='Occupational Therapist Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0810'='Emergency Care Attendant (EMT Ambulance)' 
'51.0812'='Respiratory Therapy Technician/Assistant' 
'51.0899'='Allied Health and Medical Assisting Services, Other' 
'51.0901'='Cardiovascular Technology/Technologist' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0907'='Medical Radiologic Technology/Science - Radiation Therapist' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0913'='Athletic Training/Trainer' 
'51.0920'='Magnetic Resonance Imaging (MRI) Technology/Technician' 
'51.1001'='Blood Bank Technology Specialist' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.1012'='Sterile Processing Technology/Technician' 
'51.1501'='Substance Abuse/Addiction Counseling' 
'51.2309'='Therapeutic Recreation/Recreational Therapy' 
'51.2602'='Home Health Aide/Home Attendant' 
'51.3306'='Holistic/Integrative Health' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3502'='Asian Bodywork Therapy' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3818'='Nursing Practice' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0201'='Business Administration and Management, General' 
'52.0204'='Office Management and Supervision' 
'52.0213'='Organizational Leadership' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0408'='General Office Occupations and Clerical Services' 
'52.0999'='Hospitality Administration/Management, Other' 
'-2'='Not applicable';
value prgmsr2f
1='Clock hours' 
2='Credit hours' 
-2='Not applicable';
value $cipcode3f
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'01.8399'='Veterinary/Animal Health Technologies/Technicians, Other' 
'09.0702'='Digital Communication and Media/Multimedia' 
'09.0799'='Radio, Television, and Digital Communication, Other' 
'09.0900'='Public Relations, Advertising, and Applied Communication' 
'09.0906'='Sports Communication' 
'09.0907'='International and Intercultural Communication' 
'10.0203'='Recording Arts Technology/Technician' 
'10.0304'='Animation, Interactive Technology, Video Graphics, and Special Effects' 
'10.0305'='Graphic and Printing Equipment Operator, General Production' 
'11.0101'='Computer and Information Sciences, General' 
'11.0103'='Information Technology' 
'11.0199'='Computer and Information Sciences,  Other' 
'11.0201'='Computer Programming/Programmer, General' 
'11.0299'='Computer Programming, Other' 
'11.0601'='Data Entry/Microcomputer Applications, General' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.1001'='Network and System Administration/Administrator' 
'11.1002'='System, Networking, and LAN/WAN Management/Manager' 
'11.1003'='Computer and Information Systems Security/Auditing/Information Assurance' 
'11.1004'='Web/Multimedia Management and Webmaster' 
'11.1006'='Computer Support Specialist' 
'12.0399'='Funeral Service and Mortuary Science, Other' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0412'='Salon/Beauty Salon Management/Manager' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0503'='Culinary Arts/Chef Training' 
'12.0602'='Casino Dealing' 
'13.1207'='Montessori Teacher Education' 
'13.1320'='Trade and Industrial Teacher Education' 
'13.1399'='Teacher Education and Professional Development, Specific Subject Areas, Other' 
'14.0102'='Pre-Engineering' 
'15.0303'='Electrical, Electronic, and Communications Engineering Technology/Technician' 
'15.0403'='Electromechanical/Electromechanical Engineering Technology/Technician' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0607'='Plastics and Polymer Engineering Technology/Technician' 
'15.0617'='Composite Materials Technology/Technician' 
'15.0803'='Automotive Engineering Technology/Technician' 
'15.1703'='Solar Energy Technology/Technician' 
'16.1701'='English as a Second Language' 
'19.0709'='Child Care Provider/Assistant' 
'22.0302'='Legal Assistant/Paralegal' 
'22.0303'='Court Reporting and Captioning/Court Reporter' 
'23.0101'='English Language and Literature, General' 
'31.0507'='Physical Fitness Technician' 
'42.0101'='Psychology, General' 
'43.0103'='Criminal Justice/Law Enforcement Administration' 
'43.0104'='Criminal Justice/Safety Studies' 
'43.0107'='Criminal Justice/Police Science' 
'43.0203'='Fire Science/Fire-fighting' 
'43.9999'='Homeland Security, Law Enforcement, Firefighting and Related Protective Services, Other' 
'44.0000'='Human Services, General' 
'46.0000'='Construction Trades, General' 
'46.0201'='Carpentry/Carpenter' 
'46.0302'='Electrician' 
'46.0303'='Lineworker' 
'46.0399'='Electrical and Power Transmission Installers, Other' 
'46.0401'='Building/Property Maintenance' 
'46.0415'='Building Construction Technology/Technician' 
'46.0503'='Plumbing Technology/Plumber' 
'47.0101'='Electrical/Electronics Equipment Installation and Repair Technology/Technician, General' 
'47.0103'='Communications Systems Installation and Repair Technology/Technician' 
'47.0104'='Computer Installation and Repair Technology/Technician' 
'47.0105'='Industrial Electronics Technology/Technician' 
'47.0110'='Security System Installation, Repair, and Inspection Technology/Technician' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0302'='Heavy Equipment Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0403'='Locksmithing and Safe Repair' 
'47.0408'='Watchmaking and Jewelrymaking' 
'47.0603'='Autobody/Collision and Repair Technology/Technician' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0607'='Airframe Mechanics and Aircraft Maintenance Technology/Technician' 
'47.0609'='Avionics Maintenance Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0616'='Marine Maintenance/Fitter and Ship Repair Technology/Technician' 
'48.0501'='Machine Tool Technology/Machinist' 
'48.0503'='Machine Shop Technology/Assistant' 
'48.0508'='Welding Technology/Welder' 
'48.0510'='Computer Numerically Controlled (CNC) Machinist Technology/CNC Machinist' 
'49.0102'='Airline/Commercial/Professional Pilot and Flight Crew' 
'49.0202'='Construction/Heavy Equipment/Earthmoving Equipment Operation' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'49.0208'='Railroad and Railway Transportation' 
'50.0102'='Digital Arts' 
'50.0409'='Graphic Design' 
'50.0601'='Film/Cinema/Media Studies' 
'50.0699'='Film/Video and Photographic Arts, Other' 
'50.0713'='Jewelry Arts' 
'50.0913'='Music Technology' 
'50.1099'='Arts, Entertainment, and Media Management, Other' 
'51.0000'='Health Services/Allied Health/Health Sciences, General' 
'51.0001'='Health and Wellness, General' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0705'='Medical Office Management/Administration' 
'51.0706'='Health Information/Medical Records Administration/Administrator' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0711'='Medical/Health Management and Clinical Assistant/Specialist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0799'='Health and Medical Administrative Services, Other' 
'51.0801'='Medical/Clinical Assistant' 
'51.0802'='Clinical/Medical Laboratory Assistant' 
'51.0803'='Occupational Therapist Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0806'='Physical Therapy Assistant' 
'51.0810'='Emergency Care Attendant (EMT Ambulance)' 
'51.0899'='Allied Health and Medical Assisting Services, Other' 
'51.0902'='Electrocardiograph Technology/Technician' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0920'='Magnetic Resonance Imaging (MRI) Technology/Technician' 
'51.1001'='Blood Bank Technology Specialist' 
'51.1004'='Clinical/Medical Laboratory Technician' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.1011'='Renal/Dialysis Technologist/Technician' 
'51.1012'='Sterile Processing Technology/Technician' 
'51.1501'='Substance Abuse/Addiction Counseling' 
'51.1502'='Psychiatric/Mental Health Services Technician' 
'51.2201'='Public Health, General' 
'51.2399'='Rehabilitation and Therapeutic Professions, Other' 
'51.2602'='Home Health Aide/Home Attendant' 
'51.3101'='Dietetics/Dietitian' 
'51.3202'='Health Professions Education' 
'51.3203'='Nursing Education' 
'51.3305'='Ayurvedic Medicine/Ayurveda' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3503'='Somatic Bodywork' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'51.9999'='Health Professions and Related Clinical Sciences, Other' 
'52.0201'='Business Administration and Management, General' 
'52.0299'='Business Administration, Management and Operations, Other' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0407'='Business/Office Automation/Technology/Data Entry' 
'52.0408'='General Office Occupations and Clerical Services' 
'52.0410'='Traffic, Customs, and Transportation Clerk/Technician' 
'-2'='Not applicable';
value prgmsr3f
1='Clock hours' 
2='Credit hours' 
-2='Not applicable';
value $cipcode4f
'01.0504'='Dog/Pet/Animal Grooming' 
'01.8101'='Veterinary Sciences/Veterinary Clinical Sciences, General' 
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'09.0799'='Radio, Television, and Digital Communication, Other' 
'09.0900'='Public Relations, Advertising, and Applied Communication' 
'09.0906'='Sports Communication' 
'10.0203'='Recording Arts Technology/Technician' 
'10.0301'='Graphic Communications, General' 
'11.0103'='Information Technology' 
'11.0401'='Information Science/Studies' 
'11.0501'='Computer Systems Analysis/Analyst' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.1001'='Network and System Administration/Administrator' 
'11.1002'='System, Networking, and LAN/WAN Management/Manager' 
'11.1006'='Computer Support Specialist' 
'11.1099'='Computer/Information Technology Services Administration and Management, Other' 
'11.9999'='Computer and Information Sciences and Support Services, Other' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0412'='Salon/Beauty Salon Management/Manager' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0500'='Cooking and Related Culinary Arts, General' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0502'='Bartending/Bartender' 
'12.0503'='Culinary Arts/Chef Training' 
'12.0504'='Restaurant, Culinary, and Catering Management/Manager' 
'12.9999'='Culinary, Entertainment, and Personal Services, Other' 
'13.1206'='Teacher Education, Multiple Levels' 
'13.1210'='Early Childhood Education and Teaching' 
'13.1319'='Technical Teacher Education' 
'13.1399'='Teacher Education and Professional Development, Specific Subject Areas, Other' 
'14.0901'='Computer Engineering, General' 
'15.0303'='Electrical, Electronic, and Communications Engineering Technology/Technician' 
'15.0403'='Electromechanical/Electromechanical Engineering Technology/Technician' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0614'='Welding Engineering Technology/Technician' 
'15.0803'='Automotive Engineering Technology/Technician' 
'15.1201'='Computer Engineering Technology/Technician' 
'15.1202'='Computer/Computer Systems Technology/Technician' 
'15.1302'='CAD/CADD Drafting and/or Design Technology/Technician' 
'16.1701'='English as a Second Language' 
'19.0501'='Foods, Nutrition, and Wellness Studies, General' 
'19.0709'='Child Care Provider/Assistant' 
'22.0303'='Court Reporting and Captioning/Court Reporter' 
'30.1901'='Nutrition Sciences' 
'31.0504'='Sport and Fitness Administration/Management' 
'43.0102'='Corrections' 
'43.0103'='Criminal Justice/Law Enforcement Administration' 
'43.0107'='Criminal Justice/Police Science' 
'43.0199'='Corrections and Criminal Justice, Other' 
'43.0203'='Fire Science/Fire-fighting' 
'43.0403'='Cyber/Computer Forensics and Counterterrorism' 
'46.0000'='Construction Trades, General' 
'46.0201'='Carpentry/Carpenter' 
'46.0301'='Electrical and Power Transmission Installation/Installer, General' 
'46.0302'='Electrician' 
'46.0303'='Lineworker' 
'46.0399'='Electrical and Power Transmission Installers, Other' 
'46.0401'='Building/Property Maintenance' 
'46.0402'='Concrete Finishing/Concrete Finisher' 
'46.0415'='Building Construction Technology/Technician' 
'46.0503'='Plumbing Technology/Plumber' 
'46.9999'='Construction Trades, Other' 
'47.0101'='Electrical/Electronics Equipment Installation and Repair Technology/Technician, General' 
'47.0103'='Communications Systems Installation and Repair Technology/Technician' 
'47.0104'='Computer Installation and Repair Technology/Technician' 
'47.0106'='Appliance Installation and Repair Technology/Technician' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0402'='Gunsmithing/Gunsmith' 
'47.0404'='Musical Instrument Fabrication and Repair' 
'47.0408'='Watchmaking and Jewelrymaking' 
'47.0600'='Vehicle Maintenance and Repair Technology/Technician, General' 
'47.0603'='Autobody/Collision and Repair Technology/Technician' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0617'='High Performance and Custom Engine Technician/Mechanic' 
'48.0000'='Precision Production Trades, General' 
'48.0501'='Machine Tool Technology/Machinist' 
'48.0508'='Welding Technology/Welder' 
'48.0510'='Computer Numerically Controlled (CNC) Machinist Technology/CNC Machinist' 
'48.0511'='Metal Fabricator' 
'48.9999'='Precision Production, Other' 
'49.0102'='Airline/Commercial/Professional Pilot and Flight Crew' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'49.0299'='Ground Transportation, Other' 
'50.0102'='Digital Arts' 
'50.0406'='Commercial Photography' 
'50.0408'='Interior Design' 
'50.0602'='Cinematography and Film/Video Production' 
'50.0713'='Jewelry Arts' 
'50.0903'='Music Performance, General' 
'50.1099'='Arts, Entertainment, and Media Management, Other' 
'51.0000'='Health Services/Allied Health/Health Sciences, General' 
'51.0001'='Health and Wellness, General' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0706'='Health Information/Medical Records Administration/Administrator' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0711'='Medical/Health Management and Clinical Assistant/Specialist' 
'51.0712'='Medical Reception/Receptionist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0799'='Health and Medical Administrative Services, Other' 
'51.0801'='Medical/Clinical Assistant' 
'51.0803'='Occupational Therapist Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0806'='Physical Therapy Assistant' 
'51.0810'='Emergency Care Attendant (EMT Ambulance)' 
'51.0901'='Cardiovascular Technology/Technologist' 
'51.0902'='Electrocardiograph Technology/Technician' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0913'='Athletic Training/Trainer' 
'51.0917'='Polysomnography' 
'51.0920'='Magnetic Resonance Imaging (MRI) Technology/Technician' 
'51.1001'='Blood Bank Technology Specialist' 
'51.1004'='Clinical/Medical Laboratory Technician' 
'51.1005'='Clinical Laboratory Science/Medical Technology/Technologist' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.1012'='Sterile Processing Technology/Technician' 
'51.1501'='Substance Abuse/Addiction Counseling' 
'51.1502'='Psychiatric/Mental Health Services Technician' 
'51.2211'='Health Services Administration' 
'51.2602'='Home Health Aide/Home Attendant' 
'51.2603'='Medication Aide' 
'51.2699'='Health Aides/Attendants/Orderlies, Other' 
'51.3301'='Acupuncture and Oriental Medicine' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3599'='Somatic Bodywork and Related Therapeutic Services, Other' 
'51.3703'='Polarity Therapy' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0201'='Business Administration and Management, General' 
'52.0203'='Logistics, Materials, and Supply Chain Management' 
'52.0299'='Business Administration, Management and Operations, Other' 
'52.0301'='Accounting' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0305'='Accounting and Business/Management' 
'52.0399'='Accounting and Related Services, Other' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0402'='Executive Assistant/Executive Secretary' 
'52.0407'='Business/Office Automation/Technology/Data Entry' 
'52.0408'='General Office Occupations and Clerical Services' 
'52.1201'='Management Information Systems, General' 
'52.1401'='Marketing/Marketing Management, General' 
'-2'='Not applicable';
value prgmsr4f
1='Clock hours' 
2='Credit hours' 
-2='Not applicable';
value $cipcode5f
'01.0601'='Applied Horticulture/Horticulture Operations, General' 
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'09.0702'='Digital Communication and Media/Multimedia' 
'09.0900'='Public Relations, Advertising, and Applied Communication' 
'10.0203'='Recording Arts Technology/Technician' 
'10.0301'='Graphic Communications, General' 
'10.0303'='Prepress/Desktop Publishing and Digital Imaging Design' 
'10.0304'='Animation, Interactive Technology, Video Graphics, and Special Effects' 
'11.0101'='Computer and Information Sciences, General' 
'11.0103'='Information Technology' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.1001'='Network and System Administration/Administrator' 
'11.1003'='Computer and Information Systems Security/Auditing/Information Assurance' 
'11.1004'='Web/Multimedia Management and Webmaster' 
'11.1006'='Computer Support Specialist' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0411'='Permanent Cosmetics/Makeup and Tattooing' 
'12.0412'='Salon/Beauty Salon Management/Manager' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0500'='Cooking and Related Culinary Arts, General' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0503'='Culinary Arts/Chef Training' 
'12.0505'='Food Preparation/Professional Cooking/Kitchen Assistant' 
'12.0507'='Food Service, Waiter/Waitress, and Dining Room Management/Manager' 
'12.0599'='Culinary Arts and Related Services, Other' 
'13.1210'='Early Childhood Education and Teaching' 
'13.1319'='Technical Teacher Education' 
'13.1399'='Teacher Education and Professional Development, Specific Subject Areas, Other' 
'14.1001'='Electrical and Electronics Engineering' 
'15.0201'='Civil Engineering Technologies/Technicians' 
'15.0404'='Instrumentation Technology/Technician' 
'15.0405'='Robotics Technology/Technician' 
'15.0406'='Automation Engineer Technology/Technician' 
'15.0407'='Mechatronics, Robotics, and Automation Engineering Technology/Technician' 
'15.0499'='Electromechanical Technologies/Technicians, Other' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0613'='Manufacturing Engineering Technology/Technician' 
'15.1001'='Construction Engineering Technology/Technician' 
'15.1202'='Computer/Computer Systems Technology/Technician' 
'15.1204'='Computer Software Technology/Technician' 
'15.1701'='Energy Systems Technology/Technician' 
'19.0709'='Child Care Provider/Assistant' 
'19.0901'='Apparel and Textiles, General' 
'19.0902'='Apparel and Textile Manufacture' 
'22.0302'='Legal Assistant/Paralegal' 
'23.0101'='English Language and Literature, General' 
'31.0501'='Sports, Kinesiology, and Physical Education/Fitness, General' 
'41.0301'='Chemical Technology/Technician' 
'43.0109'='Security and Loss Prevention Services' 
'43.0203'='Fire Science/Fire-fighting' 
'43.0404'='Cybersecurity Defense Strategy/Policy' 
'44.0401'='Public Administration' 
'46.0000'='Construction Trades, General' 
'46.0101'='Mason/Masonry' 
'46.0201'='Carpentry/Carpenter' 
'46.0302'='Electrician' 
'46.0401'='Building/Property Maintenance' 
'46.0415'='Building Construction Technology/Technician' 
'46.0499'='Building/Construction Finishing, Management, and Inspection, Other' 
'46.0503'='Plumbing Technology/Plumber' 
'46.0599'='Plumbing and Related Water Supply Services, Other' 
'46.9999'='Construction Trades, Other' 
'47.0101'='Electrical/Electronics Equipment Installation and Repair Technology/Technician, General' 
'47.0104'='Computer Installation and Repair Technology/Technician' 
'47.0199'='Electrical/Electronics Maintenance and Repair Technologies/Technicians, Other' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0600'='Vehicle Maintenance and Repair Technology/Technician, General' 
'47.0603'='Autobody/Collision and Repair Technology/Technician' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0606'='Small Engine Mechanics and Repair Technology/Technician' 
'47.0607'='Airframe Mechanics and Aircraft Maintenance Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0615'='Engine Machinist' 
'47.0616'='Marine Maintenance/Fitter and Ship Repair Technology/Technician' 
'48.0000'='Precision Production Trades, General' 
'48.0501'='Machine Tool Technology/Machinist' 
'48.0503'='Machine Shop Technology/Assistant' 
'48.0508'='Welding Technology/Welder' 
'48.0510'='Computer Numerically Controlled (CNC) Machinist Technology/CNC Machinist' 
'48.9999'='Precision Production, Other' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'50.0409'='Graphic Design' 
'50.0713'='Jewelry Arts' 
'50.0913'='Music Technology' 
'50.1099'='Arts, Entertainment, and Media Management, Other' 
'51.0000'='Health Services/Allied Health/Health Sciences, General' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0705'='Medical Office Management/Administration' 
'51.0706'='Health Information/Medical Records Administration/Administrator' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0711'='Medical/Health Management and Clinical Assistant/Specialist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0799'='Health and Medical Administrative Services, Other' 
'51.0801'='Medical/Clinical Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0806'='Physical Therapy Assistant' 
'51.0810'='Emergency Care Attendant (EMT Ambulance)' 
'51.0812'='Respiratory Therapy Technician/Assistant' 
'51.0902'='Electrocardiograph Technology/Technician' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0907'='Medical Radiologic Technology/Science - Radiation Therapist' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0920'='Magnetic Resonance Imaging (MRI) Technology/Technician' 
'51.1004'='Clinical/Medical Laboratory Technician' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.1012'='Sterile Processing Technology/Technician' 
'51.1802'='Optometric Technician/Assistant' 
'51.2211'='Health Services Administration' 
'51.2306'='Occupational Therapy/Therapist' 
'51.2602'='Home Health Aide/Home Attendant' 
'51.2603'='Medication Aide' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3805'='Family Practice Nurse/Nursing' 
'51.3899'='Registered Nursing, Nursing Administration, Nursing Research and Clinical Nursing, Other' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0101'='Business/Commerce, General' 
'52.0201'='Business Administration and Management, General' 
'52.0204'='Office Management and Supervision' 
'52.0205'='Operations Management and Supervision' 
'52.0301'='Accounting' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0305'='Accounting and Business/Management' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0402'='Executive Assistant/Executive Secretary' 
'52.0408'='General Office Occupations and Clerical Services' 
'52.0499'='Business Operations Support and Secretarial Services, Other' 
'52.0904'='Hotel/Motel Administration/Management' 
'52.1001'='Human Resources Management/Personnel Administration, General' 
'52.1803'='Retailing and Retail Operations' 
'-2'='Not applicable';
value prgmsr5f
1='Clock hours' 
2='Credit hours' 
-2='Not applicable';
value $cipcode6f
'01.0308'='Agroecology and Sustainable Agriculture' 
'01.8301'='Veterinary/Animal Health Technology/Technician and Veterinary Assistant' 
'09.0701'='Radio and Television' 
'09.0702'='Digital Communication and Media/Multimedia' 
'10.0201'='Photographic and Film/Video Technology/Technician' 
'10.0203'='Recording Arts Technology/Technician' 
'10.0303'='Prepress/Desktop Publishing and Digital Imaging Design' 
'10.0304'='Animation, Interactive Technology, Video Graphics, and Special Effects' 
'11.0101'='Computer and Information Sciences, General' 
'11.0201'='Computer Programming/Programmer, General' 
'11.0801'='Web Page, Digital/Multimedia and Information Resources Design' 
'11.0899'='Computer Software and Media Applications, Other' 
'11.0901'='Computer Systems Networking and Telecommunications' 
'11.0999'='Computer Systems Networking and Telecommunications, Other' 
'11.1001'='Network and System Administration/Administrator' 
'11.1002'='System, Networking, and LAN/WAN Management/Manager' 
'11.1003'='Computer and Information Systems Security/Auditing/Information Assurance' 
'11.1006'='Computer Support Specialist' 
'11.9999'='Computer and Information Sciences and Support Services, Other' 
'12.0401'='Cosmetology/Cosmetologist, General' 
'12.0402'='Barbering/Barber' 
'12.0404'='Electrolysis/Electrology and Electrolysis Technician' 
'12.0406'='Make-Up Artist/Specialist' 
'12.0407'='Hair Styling/Stylist and Hair Design' 
'12.0408'='Facial Treatment Specialist/Facialist' 
'12.0409'='Aesthetician/Esthetician and Skin Care Specialist' 
'12.0410'='Nail Technician/Specialist and Manicurist' 
'12.0412'='Salon/Beauty Salon Management/Manager' 
'12.0413'='Cosmetology, Barber/Styling, and Nail Instructor' 
'12.0414'='Master Aesthetician/Esthetician' 
'12.0499'='Cosmetology and Related Personal Grooming Arts, Other' 
'12.0500'='Cooking and Related Culinary Arts, General' 
'12.0501'='Baking and Pastry Arts/Baker/Pastry Chef' 
'12.0503'='Culinary Arts/Chef Training' 
'12.0504'='Restaurant, Culinary, and Catering Management/Manager' 
'13.1399'='Teacher Education and Professional Development, Specific Subject Areas, Other' 
'14.1001'='Electrical and Electronics Engineering' 
'15.0399'='Electrical/Electronic Engineering Technologies/Technicians, Other' 
'15.0401'='Biomedical Technology/Technician' 
'15.0404'='Instrumentation Technology/Technician' 
'15.0501'='Heating, Ventilation, Air Conditioning and Refrigeration Engineering Technology/Technician' 
'15.0617'='Composite Materials Technology/Technician' 
'15.0803'='Automotive Engineering Technology/Technician' 
'15.1202'='Computer/Computer Systems Technology/Technician' 
'15.1203'='Computer Hardware Technology/Technician' 
'15.1301'='Drafting and Design Technology/Technician, General' 
'15.1307'='3-D Modeling and Design Technology/Technician' 
'16.0103'='Language Interpretation and Translation' 
'19.0708'='Child Care and Support Services Management' 
'19.0709'='Child Care Provider/Assistant' 
'22.0301'='Legal Administrative Assistant/Secretary' 
'22.0302'='Legal Assistant/Paralegal' 
'22.0303'='Court Reporting and Captioning/Court Reporter' 
'41.0101'='Biology/Biotechnology Technology/Technician' 
'43.0100'='Criminal Justice and Corrections, General' 
'43.0102'='Corrections' 
'43.0107'='Criminal Justice/Police Science' 
'43.0109'='Security and Loss Prevention Services' 
'43.0203'='Fire Science/Fire-fighting' 
'43.9999'='Homeland Security, Law Enforcement, Firefighting and Related Protective Services, Other' 
'46.0000'='Construction Trades, General' 
'46.0101'='Mason/Masonry' 
'46.0201'='Carpentry/Carpenter' 
'46.0301'='Electrical and Power Transmission Installation/Installer, General' 
'46.0302'='Electrician' 
'46.0399'='Electrical and Power Transmission Installers, Other' 
'46.0401'='Building/Property Maintenance' 
'46.0415'='Building Construction Technology/Technician' 
'46.0502'='Pipefitting/Pipefitter and Sprinkler Fitter' 
'46.0503'='Plumbing Technology/Plumber' 
'46.9999'='Construction Trades, Other' 
'47.0104'='Computer Installation and Repair Technology/Technician' 
'47.0105'='Industrial Electronics Technology/Technician' 
'47.0201'='Heating, Air Conditioning, Ventilation and Refrigeration Maintenance Technology/Technician' 
'47.0303'='Industrial Mechanics and Maintenance Technology/Technician' 
'47.0399'='Heavy/Industrial Equipment Maintenance Technologies/Technicians, Other' 
'47.0403'='Locksmithing and Safe Repair' 
'47.0603'='Autobody/Collision and Repair Technology/Technician' 
'47.0604'='Automobile/Automotive Mechanics Technology/Technician' 
'47.0605'='Diesel Mechanics Technology/Technician' 
'47.0608'='Aircraft Powerplant Technology/Technician' 
'47.0613'='Medium/Heavy Vehicle and Truck Technology/Technician' 
'47.0615'='Engine Machinist' 
'47.0616'='Marine Maintenance/Fitter and Ship Repair Technology/Technician' 
'48.0303'='Upholstery/Upholsterer' 
'48.0501'='Machine Tool Technology/Machinist' 
'48.0503'='Machine Shop Technology/Assistant' 
'48.0508'='Welding Technology/Welder' 
'48.0510'='Computer Numerically Controlled (CNC) Machinist Technology/CNC Machinist' 
'49.0205'='Truck and Bus Driver/Commercial Vehicle Operator and Instructor' 
'50.0401'='Design and Visual Communications, General' 
'50.0402'='Commercial and Advertising Art' 
'50.0406'='Commercial Photography' 
'50.0409'='Graphic Design' 
'50.0506'='Acting' 
'50.0713'='Jewelry Arts' 
'51.0601'='Dental Assisting/Assistant' 
'51.0602'='Dental Hygiene/Hygienist' 
'51.0701'='Health/Health Care Administration/Management' 
'51.0705'='Medical Office Management/Administration' 
'51.0707'='Health Information/Medical Records Technology/Technician' 
'51.0710'='Medical Office Assistant/Specialist' 
'51.0711'='Medical/Health Management and Clinical Assistant/Specialist' 
'51.0712'='Medical Reception/Receptionist' 
'51.0713'='Medical Insurance Coding Specialist/Coder' 
'51.0714'='Medical Insurance Specialist/Medical Biller' 
'51.0716'='Medical Administrative/Executive Assistant and Medical Secretary' 
'51.0801'='Medical/Clinical Assistant' 
'51.0803'='Occupational Therapist Assistant' 
'51.0805'='Pharmacy Technician/Assistant' 
'51.0806'='Physical Therapy Assistant' 
'51.0812'='Respiratory Therapy Technician/Assistant' 
'51.0899'='Allied Health and Medical Assisting Services, Other' 
'51.0902'='Electrocardiograph Technology/Technician' 
'51.0903'='Electroneurodiagnostic/Electroencephalographic Technology/Technologist' 
'51.0904'='Emergency Medical Technology/Technician (EMT Paramedic)' 
'51.0905'='Nuclear Medical Technology/Technologist' 
'51.0908'='Respiratory Care Therapy/Therapist' 
'51.0909'='Surgical Technology/Technologist' 
'51.0910'='Diagnostic Medical Sonography/Sonographer and Ultrasound Technician' 
'51.0911'='Radiologic Technology/Science - Radiographer' 
'51.0917'='Polysomnography' 
'51.1009'='Phlebotomy Technician/Phlebotomist' 
'51.1011'='Renal/Dialysis Technologist/Technician' 
'51.1012'='Sterile Processing Technology/Technician' 
'51.2211'='Health Services Administration' 
'51.2602'='Home Health Aide/Home Attendant' 
'51.2604'='Rehabilitation Aide' 
'51.3305'='Ayurvedic Medicine/Ayurveda' 
'51.3306'='Holistic/Integrative Health' 
'51.3501'='Massage Therapy/Therapeutic Massage' 
'51.3801'='Registered Nursing/Registered Nurse' 
'51.3808'='Nursing Science' 
'51.3901'='Licensed Practical/Vocational Nurse Training' 
'51.3902'='Nursing Assistant/Aide and Patient Care Assistant/Aide' 
'51.3999'='Practical Nursing, Vocational Nursing and Nursing Assistants, Other' 
'52.0201'='Business Administration and Management, General' 
'52.0299'='Business Administration, Management and Operations, Other' 
'52.0301'='Accounting' 
'52.0302'='Accounting Technology/Technician and Bookkeeping' 
'52.0305'='Accounting and Business/Management' 
'52.0401'='Administrative Assistant and Secretarial Science, General' 
'52.0402'='Executive Assistant/Executive Secretary' 
'52.0408'='General Office Occupations and Clerical Services' 
'52.0803'='Banking and Financial Support Services' 
'-2'='Not applicable';
value prgmsr6f
1='Contact hours' 
2='Credit hours' 
-2='Not applicable';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

options fmtsearch=(&libraryName);


data &libraryName..&file;
infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 
		lrecl=32736;

informat
unitid   6. 
prgmofr  3. 
cipcode1 $7. 
xciptui1 $1.
ciptuit1 5. 
xcipsup1 $1.
cipsupp1 5. 
xciplgt1 $1.
ciplgth1 4. 
prgmsr1  3. 
xmthcmp1 $1.
mthcmp1  3. 
xwkcmp1  $1.
wkcmp1   4. 
xlnayhr1 $1.
lnayhr1  4. 
xlnaywk1 $1.
lnaywk1  4. 
xchg1py0 $1.
chg1py0  5. 
xchg1py1 $1.
chg1py1  5. 
xchg1py2 $1.
chg1py2  5. 
xchg1py3 $1.
chg1py3  5. 
xchg4py0 $1.
chg4py0  5. 
xchg4py1 $1.
chg4py1  5. 
xchg4py2 $1.
chg4py2  5. 
xchg4py3 $1.
chg4py3  5. 
xchg5py0 $1.
chg5py0  5. 
xchg5py1 $1.
chg5py1  5. 
xchg5py2 $1.
chg5py2  5. 
xchg5py3 $1.
chg5py3  5. 
xchg6py0 $1.
chg6py0  5. 
xchg6py1 $1.
chg6py1  5. 
xchg6py2 $1.
chg6py2  5. 
xchg6py3 $1.
chg6py3  5. 
xchg7py0 $1.
chg7py0  5. 
xchg7py1 $1.
chg7py1  5. 
xchg7py2 $1.
chg7py2  5. 
xchg7py3 $1.
chg7py3  5. 
xchg8py0 $1.
chg8py0  5. 
xchg8py1 $1.
chg8py1  5. 
xchg8py2 $1.
chg8py2  5. 
xchg8py3 $1.
chg8py3  5. 
xchg9py0 $1.
chg9py0  5. 
xchg9py1 $1.
chg9py1  5. 
xchg9py2 $1.
chg9py2  5. 
xchg9py3 $1.
chg9py3  5. 
cipcode2 $7. 
xciptui2 $1.
ciptuit2 5. 
xcipsup2 $1.
cipsupp2 5. 
xciplgt2 $1.
ciplgth2 4. 
prgmsr2  3. 
xmthcmp2 $1.
mthcmp2  4. 
cipcode3 $7. 
xciptui3 $1.
ciptuit3 5. 
xcipsup3 $1.
cipsupp3 5. 
xciplgt3 $1.
ciplgth3 4. 
prgmsr3  3. 
xmthcmp3 $1.
mthcmp3  4. 
cipcode4 $7. 
xciptui4 $1.
ciptuit4 5. 
xcipsup4 $1.
cipsupp4 5. 
xciplgt4 $1.
ciplgth4 4. 
prgmsr4  3. 
xmthcmp4 $1.
mthcmp4  4. 
cipcode5 $7. 
xciptui5 $1.
ciptuit5 5. 
xcipsup5 $1.
cipsupp5 5. 
xciplgt5 $1.
ciplgth5 4. 
prgmsr5  3. 
xmthcmp5 $1.
mthcmp5  4. 
cipcode6 $7. 
xciptui6 $1.
ciptuit6 5. 
xcipsup6 $1.
cipsupp6 5. 
xciplgt6 $1.
ciplgth6 4. 
prgmsr6  3. 
xmthcmp6 $1.
mthcmp6  4.;

input
unitid   
prgmofr  
cipcode1 $ 
xciptui1 $
ciptuit1 
xcipsup1 $
cipsupp1 
xciplgt1 $
ciplgth1 
prgmsr1  
xmthcmp1 $
mthcmp1  
xwkcmp1  $
wkcmp1   
xlnayhr1 $
lnayhr1  
xlnaywk1 $
lnaywk1  
xchg1py0 $
chg1py0  
xchg1py1 $
chg1py1  
xchg1py2 $
chg1py2  
xchg1py3 $
chg1py3  
xchg4py0 $
chg4py0  
xchg4py1 $
chg4py1  
xchg4py2 $
chg4py2  
xchg4py3 $
chg4py3  
xchg5py0 $
chg5py0  
xchg5py1 $
chg5py1  
xchg5py2 $
chg5py2  
xchg5py3 $
chg5py3  
xchg6py0 $
chg6py0  
xchg6py1 $
chg6py1  
xchg6py2 $
chg6py2  
xchg6py3 $
chg6py3  
xchg7py0 $
chg7py0  
xchg7py1 $
chg7py1  
xchg7py2 $
chg7py2  
xchg7py3 $
chg7py3  
xchg8py0 $
chg8py0  
xchg8py1 $
chg8py1  
xchg8py2 $
chg8py2  
xchg8py3 $
chg8py3  
xchg9py0 $
chg9py0  
xchg9py1 $
chg9py1  
xchg9py2 $
chg9py2  
xchg9py3 $
chg9py3  
cipcode2 $ 
xciptui2 $
ciptuit2 
xcipsup2 $
cipsupp2 
xciplgt2 $
ciplgth2 
prgmsr2  
xmthcmp2 $
mthcmp2  
cipcode3 $ 
xciptui3 $
ciptuit3 
xcipsup3 $
cipsupp3 
xciplgt3 $
ciplgth3 
prgmsr3  
xmthcmp3 $
mthcmp3  
cipcode4 $ 
xciptui4 $
ciptuit4 
xcipsup4 $
cipsupp4 
xciplgt4 $
ciplgth4 
prgmsr4  
xmthcmp4 $
mthcmp4  
cipcode5 $ 
xciptui5 $
ciptuit5 
xcipsup5 $
cipsupp5 
xciplgt5 $
ciplgth5 
prgmsr5  
xmthcmp5 $
mthcmp5  
cipcode6 $ 
xciptui6 $
ciptuit6 
xcipsup6 $
cipsupp6 
xciplgt6 $
ciplgth6 
prgmsr6  
xmthcmp6 $
mthcmp6 ;

label
unitid  ='Unique identification number of the institution' 
prgmofr ='Number of programs offered' 
cipcode1='CIP code of largest program' 
xciptui1='Imputation field for ciptuit1 - Tuition and fees 2021-22 (institutions with no full-time, first-time, undergraduate students)'
ciptuit1='Tuition and fees 2021-22 (institutions with no full-time, first-time, undergraduate students)' 
xcipsup1='Imputation field for cipsupp1 - Books and supplies 2021-22 (institutions with no full-time, first-time, undergraduate students)'
cipsupp1='Books and supplies 2021-22 (institutions with no full-time, first-time, undergraduate students)' 
xciplgt1='Imputation field for ciplgth1 - Total length of largest program'
ciplgth1='Total length of largest program' 
prgmsr1 ='Largest program measured in credit or clock hours' 
xmthcmp1='Imputation field for mthcmp1 - Average number of months to complete largest program'
mthcmp1 ='Average number of months to complete largest program' 
xwkcmp1 ='Imputation field for wkcmp1 - Total length of program in weeks, as completed by a student attending full-time'
wkcmp1  ='Total length of program in weeks, as completed by a student attending full-time' 
xlnayhr1='Imputation field for lnayhr1 - Total length of academic year (as used to calculate your Pell budget) in clock or credit hours'
lnayhr1 ='Total length of academic year (as used to calculate your Pell budget) in clock or credit hours' 
xlnaywk1='Imputation field for lnaywk1 - Total length of academic year (as used to calculate your Pell budget) in weeks'
lnaywk1 ='Total length of academic year (as used to calculate your Pell budget) in weeks' 
xchg1py0='Imputation field for chg1py0 - Published tuition and fees 2018-19'
chg1py0 ='Published tuition and fees 2018-19' 
xchg1py1='Imputation field for chg1py1 - Published tuition and fees 2019-20'
chg1py1 ='Published tuition and fees 2019-20' 
xchg1py2='Imputation field for chg1py2 - Published tuition and fees 2020-21'
chg1py2 ='Published tuition and fees 2020-21' 
xchg1py3='Imputation field for chg1py3 - Published tuition and fees 2021-22'
chg1py3 ='Published tuition and fees 2021-22' 
xchg4py0='Imputation field for chg4py0 - Books and supplies 2018-19'
chg4py0 ='Books and supplies 2018-19' 
xchg4py1='Imputation field for chg4py1 - Books and supplies 2019-20'
chg4py1 ='Books and supplies 2019-20' 
xchg4py2='Imputation field for chg4py2 - Books and supplies 2020-21'
chg4py2 ='Books and supplies 2020-21' 
xchg4py3='Imputation field for chg4py3 - Books and supplies 2021-22'
chg4py3 ='Books and supplies 2021-22' 
xchg5py0='Imputation field for chg5py0 - On campus, room and board 2018-19'
chg5py0 ='On campus, room and board 2018-19' 
xchg5py1='Imputation field for chg5py1 - On campus, room and board 2019-20'
chg5py1 ='On campus, room and board 2019-20' 
xchg5py2='Imputation field for chg5py2 - On campus, room and board 2020-21'
chg5py2 ='On campus, room and board 2020-21' 
xchg5py3='Imputation field for chg5py3 - On campus, room and board 2021-22'
chg5py3 ='On campus, room and board 2021-22' 
xchg6py0='Imputation field for chg6py0 - On campus, other expenses 2018-19'
chg6py0 ='On campus, other expenses 2018-19' 
xchg6py1='Imputation field for chg6py1 - On campus, other expenses 2019-20'
chg6py1 ='On campus, other expenses 2019-20' 
xchg6py2='Imputation field for chg6py2 - On campus, other expenses 2020-21'
chg6py2 ='On campus, other expenses 2020-21' 
xchg6py3='Imputation field for chg6py3 - On campus, other expenses 2021-22'
chg6py3 ='On campus, other expenses 2021-22' 
xchg7py0='Imputation field for chg7py0 - Off campus (not with family), room and board 2018-19'
chg7py0 ='Off campus (not with family), room and board 2018-19' 
xchg7py1='Imputation field for chg7py1 - Off campus (not with family), room and board 2019-20'
chg7py1 ='Off campus (not with family), room and board 2019-20' 
xchg7py2='Imputation field for chg7py2 - Off campus (not with family), room and board 2020-21'
chg7py2 ='Off campus (not with family), room and board 2020-21' 
xchg7py3='Imputation field for chg7py3 - Off campus (not with family), room and board 2021-22'
chg7py3 ='Off campus (not with family), room and board 2021-22' 
xchg8py0='Imputation field for chg8py0 - Off campus (not with family), other expenses 2018-19'
chg8py0 ='Off campus (not with family), other expenses 2018-19' 
xchg8py1='Imputation field for chg8py1 - Off campus (not with family), other expenses 2019-20'
chg8py1 ='Off campus (not with family), other expenses 2019-20' 
xchg8py2='Imputation field for chg8py2 - Off campus (not with family), other expenses 2020-21'
chg8py2 ='Off campus (not with family), other expenses 2020-21' 
xchg8py3='Imputation field for chg8py3 - Off campus (not with family), other expenses 2021-22'
chg8py3 ='Off campus (not with family), other expenses 2021-22' 
xchg9py0='Imputation field for chg9py0 - Off campus (with family), other expenses 2018-19'
chg9py0 ='Off campus (with family), other expenses 2018-19' 
xchg9py1='Imputation field for chg9py1 - Off campus (with family), other expenses 2019-20'
chg9py1 ='Off campus (with family), other expenses 2019-20' 
xchg9py2='Imputation field for chg9py2 - Off campus (with family), other expenses 2020-21'
chg9py2 ='Off campus (with family), other expenses 2020-21' 
xchg9py3='Imputation field for chg9py3 - Off campus (with family), other expenses 2021-22'
chg9py3 ='Off campus (with family), other expenses 2021-22' 
cipcode2='CIP code of 2nd largest program' 
xciptui2='Imputation field for ciptuit2 - Tuition and fees of 2nd largest program'
ciptuit2='Tuition and fees of 2nd largest program' 
xcipsup2='Imputation field for cipsupp2 - Cost of books and supplies of 2nd largest program'
cipsupp2='Cost of books and supplies of 2nd largest program' 
xciplgt2='Imputation field for ciplgth2 - Length of 2nd largest program'
ciplgth2='Length of 2nd largest program' 
prgmsr2 ='2nd largest program measured in credit or clock hours' 
xmthcmp2='Imputation field for mthcmp2 - Number of months to complete 2nd largest program'
mthcmp2 ='Number of months to complete 2nd largest program' 
cipcode3='CIP code of 3rd largest program' 
xciptui3='Imputation field for ciptuit3 - Tuition and fees of 3rd largest program'
ciptuit3='Tuition and fees of 3rd largest program' 
xcipsup3='Imputation field for cipsupp3 - Cost of books and supplies of 3rd largest program'
cipsupp3='Cost of books and supplies of 3rd largest program' 
xciplgt3='Imputation field for ciplgth3 - Length of 3rd largest program'
ciplgth3='Length of 3rd largest program' 
prgmsr3 ='3rd largest program measured in credit or clock hours' 
xmthcmp3='Imputation field for mthcmp3 - Number of months to complete 3rd largest program'
mthcmp3 ='Number of months to complete 3rd largest program' 
cipcode4='CIP code of 4th largest program' 
xciptui4='Imputation field for ciptuit4 - Tuition and fees of 4th largest program'
ciptuit4='Tuition and fees of 4th largest program' 
xcipsup4='Imputation field for cipsupp4 - Cost of books and supplies of 4th largest program'
cipsupp4='Cost of books and supplies of 4th largest program' 
xciplgt4='Imputation field for ciplgth4 - Length of 4th largest program'
ciplgth4='Length of 4th largest program' 
prgmsr4 ='4th largest program measured in credit or clock hours' 
xmthcmp4='Imputation field for mthcmp4 - Number of months to complete 4th largest program'
mthcmp4 ='Number of months to complete 4th largest program' 
cipcode5='CIP code of 5th largest program' 
xciptui5='Imputation field for ciptuit5 - Tuition and fees of 5th largest program'
ciptuit5='Tuition and fees of 5th largest program' 
xcipsup5='Imputation field for cipsupp5 - Cost of books and supplies of 5th largest program'
cipsupp5='Cost of books and supplies of 5th largest program' 
xciplgt5='Imputation field for ciplgth5 - Length of 5th largest program'
ciplgth5='Length of 5th largest program' 
prgmsr5 ='5th largest program measured in credit or clock hours' 
xmthcmp5='Imputation field for mthcmp5 - Number of months to complete 5th largest program'
mthcmp5 ='Number of months to complete 5th largest program' 
cipcode6='CIP code of 6th largest program' 
xciptui6='Imputation field for ciptuit6 - Tuition and fees of 6th largest program'
ciptuit6='Tuition and fees of 6th largest program' 
xcipsup6='Imputation field for cipsupp6 - Cost of books and supplies of 6th largest program'
cipsupp6='Cost of books and supplies of 6th largest program' 
xciplgt6='Imputation field for ciplgth6 - Length of 6th largest program'
ciplgth6='Length of 6th largest program' 
prgmsr6 ='6th largest program measured in credit or clock hours' 
xmthcmp6='Imputation field for mthcmp6 - Number of months to complete 6th largest program'
mthcmp6 ='Number of months to complete 6th largest program';
run;

Proc Freq;
Tables
cipcode1 xciptui1 xcipsup1 xciplgt1 prgmsr1  xmthcmp1 xwkcmp1  xlnayhr1
xlnaywk1 xchg1py0 xchg1py1 xchg1py2 xchg1py3 xchg4py0 xchg4py1 xchg4py2 xchg4py3 xchg5py0
xchg5py1 xchg5py2 xchg5py3 xchg6py0 xchg6py1 xchg6py2 xchg6py3 xchg7py0 xchg7py1 xchg7py2
xchg7py3 xchg8py0 xchg8py1 xchg8py2 xchg8py3 xchg9py0 xchg9py1 xchg9py2 xchg9py3 cipcode2
xciptui2 xcipsup2 xciplgt2 prgmsr2  xmthcmp2 cipcode3 xciptui3 xcipsup3 xciplgt3 prgmsr3 
xmthcmp3 cipcode4 xciptui4 xcipsup4 xciplgt4 prgmsr4  xmthcmp4 cipcode5 xciptui5 xcipsup5
xciplgt5 prgmsr5  xmthcmp5 cipcode6 xciptui6 xcipsup6 xciplgt6 prgmsr6  xmthcmp6  / missing;
format xciptui1-character-xmthcmp6 $ximpflg.
cipcode1  $cipcode1f.
prgmsr1  prgmsr1f.
cipcode2  $cipcode2f.
prgmsr2  prgmsr2f.
cipcode3  $cipcode3f.
prgmsr3  prgmsr3f.
cipcode4  $cipcode4f.
prgmsr4  prgmsr4f.
cipcode5  $cipcode5f.
prgmsr5  prgmsr5f.
cipcode6  $cipcode6f.
prgmsr6  prgmsr6f.
;

Proc Summary print n sum mean min max;
var
prgmofr  ciptuit1 cipsupp1 ciplgth1 mthcmp1  wkcmp1   lnayhr1 
lnaywk1  chg1py0  chg1py1  chg1py2  chg1py3  chg4py0  chg4py1  chg4py2  chg4py3  chg5py0 
chg5py1  chg5py2  chg5py3  chg6py0  chg6py1  chg6py2  chg6py3  chg7py0  chg7py1  chg7py2 
chg7py3  chg8py0  chg8py1  chg8py2  chg8py3  chg9py0  chg9py1  chg9py2  chg9py3 
ciptuit2 cipsupp2 ciplgth2 mthcmp2  ciptuit3 cipsupp3 ciplgth3
mthcmp3  ciptuit4 cipsupp4 ciplgth4 mthcmp4  ciptuit5 cipsupp5
ciplgth5 mthcmp5  ciptuit6 cipsupp6 ciplgth6 mthcmp6  ;
run;
