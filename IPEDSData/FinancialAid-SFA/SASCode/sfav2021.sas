*** Created:    August 23, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\sfav2021.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xugpo9_n $1.
ugpo9_n  6. 
xugpo9_t $1.
ugpo9_t  10. 
xugpo9_a $1.
ugpo9_a  6. 
xgpo9_n  $1.
gpo9_n   6. 
xgpo9_t  $1.
gpo9_t   10. 
xgpo9_a  $1.
gpo9_a   6. 
xpo9_n   $1.
po9_n    6. 
xpo9_t   $1.
po9_t    10. 
xpo9_a   $1.
po9_a    6. 
xugdod_n $1.
ugdod_n  6. 
xugdod_t $1.
ugdod_t  10. 
xugdod_a $1.
ugdod_a  6. 
xgdod_n  $1.
gdod_n   6. 
xgdod_t  $1.
gdod_t   10. 
xgdod_a  $1.
gdod_a   6. 
xdod_n   $1.
dod_n    6. 
xdod_t   $1.
dod_t    10. 
xdod_a   $1.
dod_a    6.;

input
unitid   
xugpo9_n $
ugpo9_n  
xugpo9_t $
ugpo9_t  
xugpo9_a $
ugpo9_a  
xgpo9_n  $
gpo9_n   
xgpo9_t  $
gpo9_t   
xgpo9_a  $
gpo9_a   
xpo9_n   $
po9_n    
xpo9_t   $
po9_t    
xpo9_a   $
po9_a    
xugdod_n $
ugdod_n  
xugdod_t $
ugdod_t  
xugdod_a $
ugdod_a  
xgdod_n  $
gdod_n   
xgdod_t  $
gdod_t   
xgdod_a  $
gdod_a   
xdod_n   $
dod_n    
xdod_t   $
dod_t    
xdod_a   $
dod_a   ;

label
unitid  ='Unique identification number of the institution' 
xugpo9_n='Imputation field for ugpo9_n - Number receiving Post-9/11 GI Bill Benefits - undergraduate students'
ugpo9_n ='Number receiving Post-9/11 GI Bill Benefits - undergraduate students' 
xugpo9_t='Imputation field for ugpo9_t - Total amount of Post-9/11 GI Bill Benefits awarded - undergraduate students'
ugpo9_t ='Total amount of Post-9/11 GI Bill Benefits awarded - undergraduate students' 
xugpo9_a='Imputation field for ugpo9_a - Average amount of Post-9/11 GI Bill Benefits awarded - undergraduate students'
ugpo9_a ='Average amount of Post-9/11 GI Bill Benefits awarded - undergraduate students' 
xgpo9_n ='Imputation field for gpo9_n - Number receiving Post-9/11 GI Bill Benefits - graduate students'
gpo9_n  ='Number receiving Post-9/11 GI Bill Benefits - graduate students' 
xgpo9_t ='Imputation field for gpo9_t - Total amount of Post-9/11 GI Bill Benefits awarded - graduate students'
gpo9_t  ='Total amount of Post-9/11 GI Bill Benefits awarded - graduate students' 
xgpo9_a ='Imputation field for gpo9_a - Average amount of Post-9/11 GI Bill Benefits awarded - graduate students'
gpo9_a  ='Average amount of Post-9/11 GI Bill Benefits awarded - graduate students' 
xpo9_n  ='Imputation field for po9_n - Number receiving Post-9/11 GI Bill Benefits - all students'
po9_n   ='Number receiving Post-9/11 GI Bill Benefits - all students' 
xpo9_t  ='Imputation field for po9_t - Total amount of Post-9/11 GI Bill Benefits awarded - all students'
po9_t   ='Total amount of Post-9/11 GI Bill Benefits awarded - all students' 
xpo9_a  ='Imputation field for po9_a - Average amount of Post-9/11 GI Bill Benefits awarded - all students'
po9_a   ='Average amount of Post-9/11 GI Bill Benefits awarded - all students' 
xugdod_n='Imputation field for ugdod_n - Number receiving Department of Defense Tuition Assistance Program benefits - undergraduate students'
ugdod_n ='Number receiving Department of Defense Tuition Assistance Program benefits - undergraduate students' 
xugdod_t='Imputation field for ugdod_t - Total amount of Department of Defense Tuition Assistance Program benefits  awarded - undergraduate students'
ugdod_t ='Total amount of Department of Defense Tuition Assistance Program benefits  awarded - undergraduate students' 
xugdod_a='Imputation field for ugdod_a - Average amount of Department of Defense Tuition Assistance Program benefits awarded- undergraduate students'
ugdod_a ='Average amount of Department of Defense Tuition Assistance Program benefits awarded- undergraduate students' 
xgdod_n ='Imputation field for gdod_n - Number receiving Department of Defense Tuition Assistance Program benefits - graduate students'
gdod_n  ='Number receiving Department of Defense Tuition Assistance Program benefits - graduate students' 
xgdod_t ='Imputation field for gdod_t - Total amount of Department of Defense Tuition Assistance Program benefits awarded - graduate students'
gdod_t  ='Total amount of Department of Defense Tuition Assistance Program benefits awarded - graduate students' 
xgdod_a ='Imputation field for gdod_a - Average amount of Department of Defense Tuition Assistance Program benefits awarded - graduate students'
gdod_a  ='Average amount of Department of Defense Tuition Assistance Program benefits awarded - graduate students' 
xdod_n  ='Imputation field for dod_n - Number receiving Department of Defense Tuition Assistance Program benefits - all students'
dod_n   ='Number receiving Department of Defense Tuition Assistance Program benefits - all students' 
xdod_t  ='Imputation field for dod_t - Total amount of Department of Defense Tuition Assistance Program benefits awarded - all students'
dod_t   ='Total amount of Department of Defense Tuition Assistance Program benefits awarded - all students' 
xdod_a  ='Imputation field for dod_a - Average amount of Department of Defense Tuition Assistance Program benefits awarded - all students'
dod_a   ='Average amount of Department of Defense Tuition Assistance Program benefits awarded - all students';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xugpo9_n xugpo9_t xugpo9_a xgpo9_n  xgpo9_t  xgpo9_a  xpo9_n   xpo9_t   xpo9_a  
xugdod_n xugdod_t xugdod_a xgdod_n  xgdod_t  xgdod_a  xdod_n   xdod_t   xdod_a    / missing;
format xugpo9_n-character-xdod_a   $ximpflg.;

Proc Summary print n sum mean min max;
var
ugpo9_n  ugpo9_t  ugpo9_a  gpo9_n   gpo9_t   gpo9_a   po9_n    po9_t    po9_a   
ugdod_n  ugdod_t  ugdod_a  gdod_n   gdod_t   gdod_a   dod_n    dod_t    dod_a    ;
run;
