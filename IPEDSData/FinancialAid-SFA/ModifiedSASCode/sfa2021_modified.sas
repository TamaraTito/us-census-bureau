%let path=/Data1/IPEDSData/DataFiles/FinancialAid-SFA;
%let file=sfa2021;
%let libraryName=IPEDSFA;
libname &libraryName "&path/SASData";

proc format;
  value $ximpflg  
  A='Not applicable' 
  B='Institution left item blank' 
  C='Analyst corrected reported value' 
  D='Do not know' 
  G='Data generated from other data values' 
  H='Value not derived - data not usable' 
  J='Logical imputation' 
  K='Ratio adjustment' 
  L='Imputed using the Group Median procedure' 
  N='Imputed using Nearest Neighbor procedure' 
  P='Imputed using Carry Forward procedure' 
  R='Reported' 
  Y='Specific professional practice program not applicable' 
  Z='Implied zero';
run;

options fmtsearch=(&libraryName);
  
data &libraryName..&file;
  infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;  
  informat
  unitid   6. 
  xscugrad $1.
  scugrad  6. 
  xscugffn $1.
  scugffn  6. 
  xscugffp $1.
  scugffp  6. 
  xscfa2   $1.
  scfa2    6. 
  xscfa1n  $1.
  scfa1n   6. 
  xscfa1p  $1.
  scfa1p   3. 
  xscfa11n $1.
  scfa11n  6. 
  xscfa11p $1.
  scfa11p  3. 
  xscfa12n $1.
  scfa12n  6. 
  xscfa12p $1.
  scfa12p  3. 
  xscfa13n $1.
  scfa13n  6. 
  xscfa13p $1.
  scfa13p  3. 
  xscfa14n $1.
  scfa14n  6. 
  xscfa14p $1.
  scfa14p  3. 
  xscfy2   $1.
  scfy2    6. 
  xscfy1n  $1.
  scfy1n   6. 
  xscfy1p  $1.
  scfy1p   3. 
  xuagrntn $1.
  uagrntn  6. 
  xuagrntp $1.
  uagrntp  3. 
  xuagrntt $1.
  uagrntt  10. 
  xuagrnta $1.
  uagrnta  6. 
  xupgrntn $1.
  upgrntn  6. 
  xupgrntp $1.
  upgrntp  3. 
  xupgrntt $1.
  upgrntt  10. 
  xupgrnta $1.
  upgrnta  6. 
  xufloann $1.
  ufloann  6. 
  xufloanp $1.
  ufloanp  3. 
  xufloant $1.
  ufloant  10. 
  xufloana $1.
  ufloana  6. 
  xanyaidn $1.
  anyaidn  6. 
  xanyaidp $1.
  anyaidp  3. 
  xaidfsin $1.
  aidfsin  6. 
  xaidfsip $1.
  aidfsip  3. 
  xagrnt_n $1.
  agrnt_n  6. 
  xagrnt_p $1.
  agrnt_p  6. 
  xagrnt_t $1.
  agrnt_t  10. 
  xagrnt_a $1.
  agrnt_a  6. 
  xfgrnt_n $1.
  fgrnt_n  6. 
  xfgrnt_p $1.
  fgrnt_p  3. 
  xfgrnt_t $1.
  fgrnt_t  10. 
  xfgrnt_a $1.
  fgrnt_a  6. 
  xpgrnt_n $1.
  pgrnt_n  6. 
  xpgrnt_p $1.
  pgrnt_p  6. 
  xpgrnt_t $1.
  pgrnt_t  10. 
  xpgrnt_a $1.
  pgrnt_a  6. 
  xofgrt_n $1.
  ofgrt_n  6. 
  xofgrt_p $1.
  ofgrt_p  6. 
  xofgrt_t $1.
  ofgrt_t  10. 
  xofgrt_a $1.
  ofgrt_a  6. 
  xsgrnt_n $1.
  sgrnt_n  6. 
  xsgrnt_p $1.
  sgrnt_p  3. 
  xsgrnt_t $1.
  sgrnt_t  10. 
  xsgrnt_a $1.
  sgrnt_a  6. 
  xigrnt_n $1.
  igrnt_n  6. 
  xigrnt_p $1.
  igrnt_p  3. 
  xigrnt_t $1.
  igrnt_t  10. 
  xigrnt_a $1.
  igrnt_a  6. 
  xloan_n  $1.
  loan_n   6. 
  xloan_p  $1.
  loan_p   3. 
  xloan_t  $1.
  loan_t   10. 
  xloan_a  $1.
  loan_a   6. 
  xfloan_n $1.
  floan_n  6. 
  xfloan_p $1.
  floan_p  6. 
  xfloan_t $1.
  floan_t  10. 
  xfloan_a $1.
  floan_a  6. 
  xoloan_n $1.
  oloan_n  6. 
  xoloan_p $1.
  oloan_p  6. 
  xoloan_t $1.
  oloan_t  10. 
  xoloan_a $1.
  oloan_a  6. 
  xgistn2  $1.
  gistn2   6. 
  xgiston2 $1.
  giston2  6. 
  xgistwf2 $1.
  gistwf2  6. 
  xgistof2 $1.
  gistof2  6. 
  xgistun2 $1.
  gistun2  6. 
  xgistt2  $1.
  gistt2   10. 
  xgista2  $1.
  gista2   6. 
  xgistn1  $1.
  gistn1   6. 
  xgiston1 $1.
  giston1  6. 
  xgistwf1 $1.
  gistwf1  6. 
  xgistof1 $1.
  gistof1  6. 
  xgistun1 $1.
  gistun1  6. 
  xgistt1  $1.
  gistt1   10. 
  xgista1  $1.
  gista1   6. 
  xgistn0  $1.
  gistn0   6. 
  xgiston0 $1.
  giston0  6. 
  xgistwf0 $1.
  gistwf0  6. 
  xgistof0 $1.
  gistof0  6. 
  xgistun0 $1.
  gistun0  6. 
  xgistt0  $1.
  gistt0   10. 
  xgista0  $1.
  gista0   6. 
  xgis4n2  $1.
  gis4n2   6. 
  xgis4on2 $1.
  gis4on2  6. 
  xgis4wf2 $1.
  gis4wf2  6. 
  xgis4of2 $1.
  gis4of2  6. 
  xgis4un2 $1.
  gis4un2  6. 
  xgis4t2  $1.
  gis4t2   10. 
  xgis4a2  $1.
  gis4a2   6. 
  xgis4n12 $1.
  gis4n12  6. 
  xgis4t12 $1.
  gis4t12  10. 
  xgis4a12 $1.
  gis4a12  6. 
  xgis4n22 $1.
  gis4n22  6. 
  xgis4t22 $1.
  gis4t22  10. 
  xgis4a22 $1.
  gis4a22  6. 
  xgis4n32 $1.
  gis4n32  6. 
  xgis4t32 $1.
  gis4t32  10. 
  xgis4a32 $1.
  gis4a32  6. 
  xgis4n42 $1.
  gis4n42  6. 
  xgis4t42 $1.
  gis4t42  10. 
  xgis4a42 $1.
  gis4a42  6. 
  xgis4n52 $1.
  gis4n52  6. 
  xgis4t52 $1.
  gis4t52  10. 
  xgis4a52 $1.
  gis4a52  6. 
  xgis4n1  $1.
  gis4n1   6. 
  xgis4on1 $1.
  gis4on1  6. 
  xgis4wf1 $1.
  gis4wf1  6. 
  xgis4of1 $1.
  gis4of1  6. 
  xgis4un1 $1.
  gis4un1  6. 
  xgis4t1  $1.
  gis4t1   10. 
  xgis4a1  $1.
  gis4a1   6. 
  xgis4n11 $1.
  gis4n11  6. 
  xgis4t11 $1.
  gis4t11  10. 
  xgis4a11 $1.
  gis4a11  6. 
  xgis4n21 $1.
  gis4n21  6. 
  xgis4t21 $1.
  gis4t21  10. 
  xgis4a21 $1.
  gis4a21  6. 
  xgis4n31 $1.
  gis4n31  6. 
  xgis4t31 $1.
  gis4t31  10. 
  xgis4a31 $1.
  gis4a31  6. 
  xgis4n41 $1.
  gis4n41  6. 
  xgis4t41 $1.
  gis4t41  10. 
  xgis4a41 $1.
  gis4a41  6. 
  xgis4n51 $1.
  gis4n51  6. 
  xgis4t51 $1.
  gis4t51  10. 
  xgis4a51 $1.
  gis4a51  6. 
  xgis4n0  $1.
  gis4n0   6. 
  xgis4on0 $1.
  gis4on0  6. 
  xgis4wf0 $1.
  gis4wf0  6. 
  xgis4of0 $1.
  gis4of0  6. 
  xgis4un0 $1.
  gis4un0  6. 
  xgis4t0  $1.
  gis4t0   10. 
  xgis4a0  $1.
  gis4a0   6. 
  xgis4n10 $1.
  gis4n10  6. 
  xgis4t10 $1.
  gis4t10  10. 
  xgis4a10 $1.
  gis4a10  6. 
  xgis4n20 $1.
  gis4n20  6. 
  xgis4t20 $1.
  gis4t20  10. 
  xgis4a20 $1.
  gis4a20  6. 
  xgis4n30 $1.
  gis4n30  6. 
  xgis4t30 $1.
  gis4t30  10. 
  xgis4a30 $1.
  gis4a30  6. 
  xgis4n40 $1.
  gis4n40  6. 
  xgis4t40 $1.
  gis4t40  10. 
  xgis4a40 $1.
  gis4a40  6. 
  xgis4n50 $1.
  gis4n50  6. 
  xgis4t50 $1.
  gis4t50  10. 
  xgis4a50 $1.
  gis4a50  6. 
  xgis4g2  $1.
  gis4g2   6. 
  xgis4g12 $1.
  gis4g12  6. 
  xgis4g22 $1.
  gis4g22  6. 
  xgis4g32 $1.
  gis4g32  6. 
  xgis4g42 $1.
  gis4g42  6. 
  xgis4g52 $1.
  gis4g52  6. 
  xgis4g1  $1.
  gis4g1   6. 
  xgis4g11 $1.
  gis4g11  6. 
  xgis4g21 $1.
  gis4g21  6. 
  xgis4g31 $1.
  gis4g31  6. 
  xgis4g41 $1.
  gis4g41  6. 
  xgis4g51 $1.
  gis4g51  6. 
  xgis4g0  $1.
  gis4g0   6. 
  xgis4g10 $1.
  gis4g10  6. 
  xgis4g20 $1.
  gis4g20  6. 
  xgis4g30 $1.
  gis4g30  6. 
  xgis4g40 $1.
  gis4g40  6. 
  xgis4g50 $1.
  gis4g50  6. 
  xnpist2  $1.
  npist2   6. 
  xnpist1  $1.
  npist1   6. 
  xnpist0  $1.
  npist0   6. 
  xnpis412 $1.
  npis412  6. 
  xnpis422 $1.
  npis422  6. 
  xnpis432 $1.
  npis432  6. 
  xnpis442 $1.
  npis442  6. 
  xnpis452 $1.
  npis452  6. 
  xnpis411 $1.
  npis411  6. 
  xnpis421 $1.
  npis421  6. 
  xnpis431 $1.
  npis431  6. 
  xnpis441 $1.
  npis441  6. 
  xnpis451 $1.
  npis451  6. 
  xnpis410 $1.
  npis410  6. 
  xnpis420 $1.
  npis420  6. 
  xnpis430 $1.
  npis430  6. 
  xnpis440 $1.
  npis440  6. 
  xnpis450 $1.
  npis450  6. 
  xgrntn2  $1.
  grntn2   6. 
  xgrnton2 $1.
  grnton2  6. 
  xgrntwf2 $1.
  grntwf2  6. 
  xgrntof2 $1.
  grntof2  6. 
  xgrntun2 $1.
  grntun2  6. 
  xgrntt2  $1.
  grntt2   10. 
  xgrnta2  $1.
  grnta2   6. 
  xgrntn1  $1.
  grntn1   6. 
  xgrnton1 $1.
  grnton1  6. 
  xgrntwf1 $1.
  grntwf1  6. 
  xgrntof1 $1.
  grntof1  6. 
  xgrntun1 $1.
  grntun1  6. 
  xgrntt1  $1.
  grntt1   10. 
  xgrnta1  $1.
  grnta1   6. 
  xgrntn0  $1.
  grntn0   6. 
  xgrnton0 $1.
  grnton0  6. 
  xgrntwf0 $1.
  grntwf0  6. 
  xgrntof0 $1.
  grntof0  6. 
  xgrntun0 $1.
  grntun0  6. 
  xgrntt0  $1.
  grntt0   10. 
  xgrnta0  $1.
  grnta0   6. 
  xgrn4n2  $1.
  grn4n2   6. 
  xgrn4on2 $1.
  grn4on2  6. 
  xgrn4wf2 $1.
  grn4wf2  6. 
  xgrn4of2 $1.
  grn4of2  6. 
  xgrn4un2 $1.
  grn4un2  6. 
  xgrn4t2  $1.
  grn4t2   10. 
  xgrn4a2  $1.
  grn4a2   6. 
  xgrn4n12 $1.
  grn4n12  6. 
  xgrn4t12 $1.
  grn4t12  10. 
  xgrn4a12 $1.
  grn4a12  6. 
  xgrn4n22 $1.
  grn4n22  6. 
  xgrn4t22 $1.
  grn4t22  10. 
  xgrn4a22 $1.
  grn4a22  6. 
  xgrn4n32 $1.
  grn4n32  6. 
  xgrn4t32 $1.
  grn4t32  10. 
  xgrn4a32 $1.
  grn4a32  6. 
  xgrn4n42 $1.
  grn4n42  6. 
  xgrn4t42 $1.
  grn4t42  10. 
  xgrn4a42 $1.
  grn4a42  6. 
  xgrn4n52 $1.
  grn4n52  6. 
  xgrn4t52 $1.
  grn4t52  10. 
  xgrn4a52 $1.
  grn4a52  6. 
  xgrn4n1  $1.
  grn4n1   6. 
  xgrn4on1 $1.
  grn4on1  6. 
  xgrn4wf1 $1.
  grn4wf1  6. 
  xgrn4of1 $1.
  grn4of1  6. 
  xgrn4un1 $1.
  grn4un1  6. 
  xgrn4t1  $1.
  grn4t1   10. 
  xgrn4a1  $1.
  grn4a1   6. 
  xgrn4n11 $1.
  grn4n11  6. 
  xgrn4t11 $1.
  grn4t11  10. 
  xgrn4a11 $1.
  grn4a11  6. 
  xgrn4n21 $1.
  grn4n21  6. 
  xgrn4t21 $1.
  grn4t21  10. 
  xgrn4a21 $1.
  grn4a21  6. 
  xgrn4n31 $1.
  grn4n31  6. 
  xgrn4t31 $1.
  grn4t31  10. 
  xgrn4a31 $1.
  grn4a31  6. 
  xgrn4n41 $1.
  grn4n41  6. 
  xgrn4t41 $1.
  grn4t41  10. 
  xgrn4a41 $1.
  grn4a41  6. 
  xgrn4n51 $1.
  grn4n51  6. 
  xgrn4t51 $1.
  grn4t51  10. 
  xgrn4a51 $1.
  grn4a51  6. 
  xgrn4n0  $1.
  grn4n0   6. 
  xgrn4on0 $1.
  grn4on0  6. 
  xgrn4wf0 $1.
  grn4wf0  6. 
  xgrn4of0 $1.
  grn4of0  6. 
  xgrn4un0 $1.
  grn4un0  6. 
  xgrn4t0  $1.
  grn4t0   10. 
  xgrn4a0  $1.
  grn4a0   6. 
  xgrn4n10 $1.
  grn4n10  6. 
  xgrn4t10 $1.
  grn4t10  10. 
  xgrn4a10 $1.
  grn4a10  6. 
  xgrn4n20 $1.
  grn4n20  6. 
  xgrn4t20 $1.
  grn4t20  10. 
  xgrn4a20 $1.
  grn4a20  6. 
  xgrn4n30 $1.
  grn4n30  6. 
  xgrn4t30 $1.
  grn4t30  10. 
  xgrn4a30 $1.
  grn4a30  6. 
  xgrn4n40 $1.
  grn4n40  6. 
  xgrn4t40 $1.
  grn4t40  10. 
  xgrn4a40 $1.
  grn4a40  6. 
  xgrn4n50 $1.
  grn4n50  6. 
  xgrn4t50 $1.
  grn4t50  10. 
  xgrn4a50 $1.
  grn4a50  6. 
  xgrn4g2  $1.
  grn4g2   6. 
  xgrn4g12 $1.
  grn4g12  6. 
  xgrn4g22 $1.
  grn4g22  6. 
  xgrn4g32 $1.
  grn4g32  6. 
  xgrn4g42 $1.
  grn4g42  6. 
  xgrn4g52 $1.
  grn4g52  6. 
  xgrn4g1  $1.
  grn4g1   6. 
  xgrn4g11 $1.
  grn4g11  6. 
  xgrn4g21 $1.
  grn4g21  6. 
  xgrn4g31 $1.
  grn4g31  6. 
  xgrn4g41 $1.
  grn4g41  6. 
  xgrn4g51 $1.
  grn4g51  6. 
  xgrn4g0  $1.
  grn4g0   6. 
  xgrn4g10 $1.
  grn4g10  6. 
  xgrn4g20 $1.
  grn4g20  6. 
  xgrn4g30 $1.
  grn4g30  6. 
  xgrn4g40 $1.
  grn4g40  6. 
  xgrn4g50 $1.
  grn4g50  6. 
  xnpgrn2  $1.
  npgrn2   6. 
  xnpgrn1  $1.
  npgrn1   6. 
  xnpgrn0  $1.
  npgrn0   6. 
  xnpt412  $1.
  npt412   6. 
  xnpt422  $1.
  npt422   6. 
  xnpt432  $1.
  npt432   6. 
  xnpt442  $1.
  npt442   6. 
  xnpt452  $1.
  npt452   6. 
  xnpt411  $1.
  npt411   6. 
  xnpt421  $1.
  npt421   6. 
  xnpt431  $1.
  npt431   6. 
  xnpt441  $1.
  npt441   6. 
  xnpt451  $1.
  npt451   6. 
  xnpt410  $1.
  npt410   6. 
  xnpt420  $1.
  npt420   6. 
  xnpt430  $1.
  npt430   6. 
  xnpt440  $1.
  npt440   6. 
  xnpt450  $1.
  npt450   6.;

input
  unitid   
  xscugrad $
  scugrad  
  xscugffn $
  scugffn  
  xscugffp $
  scugffp  
  xscfa2   $
  scfa2    
  xscfa1n  $
  scfa1n   
  xscfa1p  $
  scfa1p   
  xscfa11n $
  scfa11n  
  xscfa11p $
  scfa11p  
  xscfa12n $
  scfa12n  
  xscfa12p $
  scfa12p  
  xscfa13n $
  scfa13n  
  xscfa13p $
  scfa13p  
  xscfa14n $
  scfa14n  
  xscfa14p $
  scfa14p  
  xscfy2   $
  scfy2    
  xscfy1n  $
  scfy1n   
  xscfy1p  $
  scfy1p   
  xuagrntn $
  uagrntn  
  xuagrntp $
  uagrntp  
  xuagrntt $
  uagrntt  
  xuagrnta $
  uagrnta  
  xupgrntn $
  upgrntn  
  xupgrntp $
  upgrntp  
  xupgrntt $
  upgrntt  
  xupgrnta $
  upgrnta  
  xufloann $
  ufloann  
  xufloanp $
  ufloanp  
  xufloant $
  ufloant  
  xufloana $
  ufloana  
  xanyaidn $
  anyaidn  
  xanyaidp $
  anyaidp  
  xaidfsin $
  aidfsin  
  xaidfsip $
  aidfsip  
  xagrnt_n $
  agrnt_n  
  xagrnt_p $
  agrnt_p  
  xagrnt_t $
  agrnt_t  
  xagrnt_a $
  agrnt_a  
  xfgrnt_n $
  fgrnt_n  
  xfgrnt_p $
  fgrnt_p  
  xfgrnt_t $
  fgrnt_t  
  xfgrnt_a $
  fgrnt_a  
  xpgrnt_n $
  pgrnt_n  
  xpgrnt_p $
  pgrnt_p  
  xpgrnt_t $
  pgrnt_t  
  xpgrnt_a $
  pgrnt_a  
  xofgrt_n $
  ofgrt_n  
  xofgrt_p $
  ofgrt_p  
  xofgrt_t $
  ofgrt_t  
  xofgrt_a $
  ofgrt_a  
  xsgrnt_n $
  sgrnt_n  
  xsgrnt_p $
  sgrnt_p  
  xsgrnt_t $
  sgrnt_t  
  xsgrnt_a $
  sgrnt_a  
  xigrnt_n $
  igrnt_n  
  xigrnt_p $
  igrnt_p  
  xigrnt_t $
  igrnt_t  
  xigrnt_a $
  igrnt_a  
  xloan_n  $
  loan_n   
  xloan_p  $
  loan_p   
  xloan_t  $
  loan_t   
  xloan_a  $
  loan_a   
  xfloan_n $
  floan_n  
  xfloan_p $
  floan_p  
  xfloan_t $
  floan_t  
  xfloan_a $
  floan_a  
  xoloan_n $
  oloan_n  
  xoloan_p $
  oloan_p  
  xoloan_t $
  oloan_t  
  xoloan_a $
  oloan_a  
  xgistn2  $
  gistn2   
  xgiston2 $
  giston2  
  xgistwf2 $
  gistwf2  
  xgistof2 $
  gistof2  
  xgistun2 $
  gistun2  
  xgistt2  $
  gistt2   
  xgista2  $
  gista2   
  xgistn1  $
  gistn1   
  xgiston1 $
  giston1  
  xgistwf1 $
  gistwf1  
  xgistof1 $
  gistof1  
  xgistun1 $
  gistun1  
  xgistt1  $
  gistt1   
  xgista1  $
  gista1   
  xgistn0  $
  gistn0   
  xgiston0 $
  giston0  
  xgistwf0 $
  gistwf0  
  xgistof0 $
  gistof0  
  xgistun0 $
  gistun0  
  xgistt0  $
  gistt0   
  xgista0  $
  gista0   
  xgis4n2  $
  gis4n2   
  xgis4on2 $
  gis4on2  
  xgis4wf2 $
  gis4wf2  
  xgis4of2 $
  gis4of2  
  xgis4un2 $
  gis4un2  
  xgis4t2  $
  gis4t2   
  xgis4a2  $
  gis4a2   
  xgis4n12 $
  gis4n12  
  xgis4t12 $
  gis4t12  
  xgis4a12 $
  gis4a12  
  xgis4n22 $
  gis4n22  
  xgis4t22 $
  gis4t22  
  xgis4a22 $
  gis4a22  
  xgis4n32 $
  gis4n32  
  xgis4t32 $
  gis4t32  
  xgis4a32 $
  gis4a32  
  xgis4n42 $
  gis4n42  
  xgis4t42 $
  gis4t42  
  xgis4a42 $
  gis4a42  
  xgis4n52 $
  gis4n52  
  xgis4t52 $
  gis4t52  
  xgis4a52 $
  gis4a52  
  xgis4n1  $
  gis4n1   
  xgis4on1 $
  gis4on1  
  xgis4wf1 $
  gis4wf1  
  xgis4of1 $
  gis4of1  
  xgis4un1 $
  gis4un1  
  xgis4t1  $
  gis4t1   
  xgis4a1  $
  gis4a1   
  xgis4n11 $
  gis4n11  
  xgis4t11 $
  gis4t11  
  xgis4a11 $
  gis4a11  
  xgis4n21 $
  gis4n21  
  xgis4t21 $
  gis4t21  
  xgis4a21 $
  gis4a21  
  xgis4n31 $
  gis4n31  
  xgis4t31 $
  gis4t31  
  xgis4a31 $
  gis4a31  
  xgis4n41 $
  gis4n41  
  xgis4t41 $
  gis4t41  
  xgis4a41 $
  gis4a41  
  xgis4n51 $
  gis4n51  
  xgis4t51 $
  gis4t51  
  xgis4a51 $
  gis4a51  
  xgis4n0  $
  gis4n0   
  xgis4on0 $
  gis4on0  
  xgis4wf0 $
  gis4wf0  
  xgis4of0 $
  gis4of0  
  xgis4un0 $
  gis4un0  
  xgis4t0  $
  gis4t0   
  xgis4a0  $
  gis4a0   
  xgis4n10 $
  gis4n10  
  xgis4t10 $
  gis4t10  
  xgis4a10 $
  gis4a10  
  xgis4n20 $
  gis4n20  
  xgis4t20 $
  gis4t20  
  xgis4a20 $
  gis4a20  
  xgis4n30 $
  gis4n30  
  xgis4t30 $
  gis4t30  
  xgis4a30 $
  gis4a30  
  xgis4n40 $
  gis4n40  
  xgis4t40 $
  gis4t40  
  xgis4a40 $
  gis4a40  
  xgis4n50 $
  gis4n50  
  xgis4t50 $
  gis4t50  
  xgis4a50 $
  gis4a50  
  xgis4g2  $
  gis4g2   
  xgis4g12 $
  gis4g12  
  xgis4g22 $
  gis4g22  
  xgis4g32 $
  gis4g32  
  xgis4g42 $
  gis4g42  
  xgis4g52 $
  gis4g52  
  xgis4g1  $
  gis4g1   
  xgis4g11 $
  gis4g11  
  xgis4g21 $
  gis4g21  
  xgis4g31 $
  gis4g31  
  xgis4g41 $
  gis4g41  
  xgis4g51 $
  gis4g51  
  xgis4g0  $
  gis4g0   
  xgis4g10 $
  gis4g10  
  xgis4g20 $
  gis4g20  
  xgis4g30 $
  gis4g30  
  xgis4g40 $
  gis4g40  
  xgis4g50 $
  gis4g50  
  xnpist2  $
  npist2   
  xnpist1  $
  npist1   
  xnpist0  $
  npist0   
  xnpis412 $
  npis412  
  xnpis422 $
  npis422  
  xnpis432 $
  npis432  
  xnpis442 $
  npis442  
  xnpis452 $
  npis452  
  xnpis411 $
  npis411  
  xnpis421 $
  npis421  
  xnpis431 $
  npis431  
  xnpis441 $
  npis441  
  xnpis451 $
  npis451  
  xnpis410 $
  npis410  
  xnpis420 $
  npis420  
  xnpis430 $
  npis430  
  xnpis440 $
  npis440  
  xnpis450 $
  npis450  
  xgrntn2  $
  grntn2   
  xgrnton2 $
  grnton2  
  xgrntwf2 $
  grntwf2  
  xgrntof2 $
  grntof2  
  xgrntun2 $
  grntun2  
  xgrntt2  $
  grntt2   
  xgrnta2  $
  grnta2   
  xgrntn1  $
  grntn1   
  xgrnton1 $
  grnton1  
  xgrntwf1 $
  grntwf1  
  xgrntof1 $
  grntof1  
  xgrntun1 $
  grntun1  
  xgrntt1  $
  grntt1   
  xgrnta1  $
  grnta1   
  xgrntn0  $
  grntn0   
  xgrnton0 $
  grnton0  
  xgrntwf0 $
  grntwf0  
  xgrntof0 $
  grntof0  
  xgrntun0 $
  grntun0  
  xgrntt0  $
  grntt0   
  xgrnta0  $
  grnta0   
  xgrn4n2  $
  grn4n2   
  xgrn4on2 $
  grn4on2  
  xgrn4wf2 $
  grn4wf2  
  xgrn4of2 $
  grn4of2  
  xgrn4un2 $
  grn4un2  
  xgrn4t2  $
  grn4t2   
  xgrn4a2  $
  grn4a2   
  xgrn4n12 $
  grn4n12  
  xgrn4t12 $
  grn4t12  
  xgrn4a12 $
  grn4a12  
  xgrn4n22 $
  grn4n22  
  xgrn4t22 $
  grn4t22  
  xgrn4a22 $
  grn4a22  
  xgrn4n32 $
  grn4n32  
  xgrn4t32 $
  grn4t32  
  xgrn4a32 $
  grn4a32  
  xgrn4n42 $
  grn4n42  
  xgrn4t42 $
  grn4t42  
  xgrn4a42 $
  grn4a42  
  xgrn4n52 $
  grn4n52  
  xgrn4t52 $
  grn4t52  
  xgrn4a52 $
  grn4a52  
  xgrn4n1  $
  grn4n1   
  xgrn4on1 $
  grn4on1  
  xgrn4wf1 $
  grn4wf1  
  xgrn4of1 $
  grn4of1  
  xgrn4un1 $
  grn4un1  
  xgrn4t1  $
  grn4t1   
  xgrn4a1  $
  grn4a1   
  xgrn4n11 $
  grn4n11  
  xgrn4t11 $
  grn4t11  
  xgrn4a11 $
  grn4a11  
  xgrn4n21 $
  grn4n21  
  xgrn4t21 $
  grn4t21  
  xgrn4a21 $
  grn4a21  
  xgrn4n31 $
  grn4n31  
  xgrn4t31 $
  grn4t31  
  xgrn4a31 $
  grn4a31  
  xgrn4n41 $
  grn4n41  
  xgrn4t41 $
  grn4t41  
  xgrn4a41 $
  grn4a41  
  xgrn4n51 $
  grn4n51  
  xgrn4t51 $
  grn4t51  
  xgrn4a51 $
  grn4a51  
  xgrn4n0  $
  grn4n0   
  xgrn4on0 $
  grn4on0  
  xgrn4wf0 $
  grn4wf0  
  xgrn4of0 $
  grn4of0  
  xgrn4un0 $
  grn4un0  
  xgrn4t0  $
  grn4t0   
  xgrn4a0  $
  grn4a0   
  xgrn4n10 $
  grn4n10  
  xgrn4t10 $
  grn4t10  
  xgrn4a10 $
  grn4a10  
  xgrn4n20 $
  grn4n20  
  xgrn4t20 $
  grn4t20  
  xgrn4a20 $
  grn4a20  
  xgrn4n30 $
  grn4n30  
  xgrn4t30 $
  grn4t30  
  xgrn4a30 $
  grn4a30  
  xgrn4n40 $
  grn4n40  
  xgrn4t40 $
  grn4t40  
  xgrn4a40 $
  grn4a40  
  xgrn4n50 $
  grn4n50  
  xgrn4t50 $
  grn4t50  
  xgrn4a50 $
  grn4a50  
  xgrn4g2  $
  grn4g2   
  xgrn4g12 $
  grn4g12  
  xgrn4g22 $
  grn4g22  
  xgrn4g32 $
  grn4g32  
  xgrn4g42 $
  grn4g42  
  xgrn4g52 $
  grn4g52  
  xgrn4g1  $
  grn4g1   
  xgrn4g11 $
  grn4g11  
  xgrn4g21 $
  grn4g21  
  xgrn4g31 $
  grn4g31  
  xgrn4g41 $
  grn4g41  
  xgrn4g51 $
  grn4g51  
  xgrn4g0  $
  grn4g0   
  xgrn4g10 $
  grn4g10  
  xgrn4g20 $
  grn4g20  
  xgrn4g30 $
  grn4g30  
  xgrn4g40 $
  grn4g40  
  xgrn4g50 $
  grn4g50  
  xnpgrn2  $
  npgrn2   
  xnpgrn1  $
  npgrn1   
  xnpgrn0  $
  npgrn0   
  xnpt412  $
  npt412   
  xnpt422  $
  npt422   
  xnpt432  $
  npt432   
  xnpt442  $
  npt442   
  xnpt452  $
  npt452   
  xnpt411  $
  npt411   
  xnpt421  $
  npt421   
  xnpt431  $
  npt431   
  xnpt441  $
  npt441   
  xnpt451  $
  npt451   
  xnpt410  $
  npt410   
  xnpt420  $
  npt420   
  xnpt430  $
  npt430   
  xnpt440  $
  npt440   
  xnpt450  $
  npt450  ;

label
  unitid  ='Unique identification number of the institution' 
  xscugrad='Imputation field for scugrad - Total number of undergraduates - financial aid cohort'
  scugrad ='Total number of undergraduates - financial aid cohort' 
  xscugffn='Imputation field for scugffn - Total number of full-time first-time degree/certificate seeking undergraduates - financial aid cohort'
  scugffn ='Total number of full-time first-time degree/certificate seeking undergraduates - financial aid cohort' 
  xscugffp='Imputation field for scugffp - Full-time first-time degree/certificate seeking undergraduates as a percent of all undergraduates - financial aid cohort'
  scugffp ='Full-time first-time degree/certificate seeking undergraduates as a percent of all undergraduates - financial aid cohort' 
  xscfa2  ='Imputation field for scfa2 - Total number of undergraduates - fall cohort'
  scfa2   ='Total number of undergraduates - fall cohort' 
  xscfa1n ='Imputation field for scfa1n - Number of students in fall cohort'
  scfa1n  ='Number of students in fall cohort' 
  xscfa1p ='Imputation field for scfa1p - Students in fall cohort as a percentage of all undergraduates'
  scfa1p  ='Students in fall cohort as a percentage of all undergraduates' 
  xscfa11n='Imputation field for scfa11n - Number of students in fall cohort who are paying in-district tuition rates'
  scfa11n ='Number of students in fall cohort who are paying in-district tuition rates' 
  xscfa11p='Imputation field for scfa11p - Percentage of students in fall cohort who are paying in-district tuition rates'
  scfa11p ='Percentage of students in fall cohort who are paying in-district tuition rates' 
  xscfa12n='Imputation field for scfa12n - Number of students in fall cohort who are paying in-state tuititon rates'
  scfa12n ='Number of students in fall cohort who are paying in-state tuititon rates' 
  xscfa12p='Imputation field for scfa12p - Percentage of students in fall cohort who paying in-state tuition rates'
  scfa12p ='Percentage of students in fall cohort who paying in-state tuition rates' 
  xscfa13n='Imputation field for scfa13n - Number of students in fall cohort who are paying out-of-state tuition rates'
  scfa13n ='Number of students in fall cohort who are paying out-of-state tuition rates' 
  xscfa13p='Imputation field for scfa13p - Percentage of students in fall cohort who are paying out-of-state tuition rates'
  scfa13p ='Percentage of students in fall cohort who are paying out-of-state tuition rates' 
  xscfa14n='Imputation field for scfa14n - Number of students in fall cohort whose residence/tuition rate is unknown'
  scfa14n ='Number of students in fall cohort whose residence/tuition rate is unknown' 
  xscfa14p='Imputation field for scfa14p - Percentage of students in fall cohort whose residence/ tuition rate is unknown'
  scfa14p ='Percentage of students in fall cohort whose residence/ tuition rate is unknown' 
  xscfy2  ='Imputation field for scfy2 - Total number of undergraduates - full-year cohort'
  scfy2   ='Total number of undergraduates - full-year cohort' 
  xscfy1n ='Imputation field for scfy1n - Number of students in full-year cohort'
  scfy1n  ='Number of students in full-year cohort' 
  xscfy1p ='Imputation field for scfy1p - Students in full-year cohort as a percentage of all  undergraduates'
  scfy1p  ='Students in full-year cohort as a percentage of all  undergraduates' 
  xuagrntn='Imputation field for uagrntn - Number of undergraduate students awarded federal, state, local, institutional or other sources of grant aid'
  uagrntn ='Number of undergraduate students awarded federal, state, local, institutional or other sources of grant aid' 
  xuagrntp='Imputation field for uagrntp - Percent of undergraduate students awarded federal, state, local, institutional or other sources of grant aid'
  uagrntp ='Percent of undergraduate students awarded federal, state, local, institutional or other sources of grant aid' 
  xuagrntt='Imputation field for uagrntt - Total amount of federal, state, local, institutional or other sources of grant aid awarded to undergraduate students'
  uagrntt ='Total amount of federal, state, local, institutional or other sources of grant aid awarded to undergraduate students' 
  xuagrnta='Imputation field for uagrnta - Average amount of federal, state, local, institutional or other sources of grant aid awarded to undergraduate students'
  uagrnta ='Average amount of federal, state, local, institutional or other sources of grant aid awarded to undergraduate students' 
  xupgrntn='Imputation field for upgrntn - Number of undergraduate students awarded Pell grants'
  upgrntn ='Number of undergraduate students awarded Pell grants' 
  xupgrntp='Imputation field for upgrntp - Percent of undergraduate students awarded Pell grants'
  upgrntp ='Percent of undergraduate students awarded Pell grants' 
  xupgrntt='Imputation field for upgrntt - Total amount of Pell grant aid awarded to undergraduate students'
  upgrntt ='Total amount of Pell grant aid awarded to undergraduate students' 
  xupgrnta='Imputation field for upgrnta - Average amount Pell grant aid awarded to undergraduate students'
  upgrnta ='Average amount Pell grant aid awarded to undergraduate students' 
  xufloann='Imputation field for ufloann - Number of undergraduate students awarded federal student loans'
  ufloann ='Number of undergraduate students awarded federal student loans' 
  xufloanp='Imputation field for ufloanp - Percent of undergraduate students awarded federal student loans'
  ufloanp ='Percent of undergraduate students awarded federal student loans' 
  xufloant='Imputation field for ufloant - Total amount of federal student loans awarded to undergraduate students'
  ufloant ='Total amount of federal student loans awarded to undergraduate students' 
  xufloana='Imputation field for ufloana - Average amount of federal student loans awarded to undergraduate students'
  ufloana ='Average amount of federal student loans awarded to undergraduate students' 
  xanyaidn='Imputation field for anyaidn - Number of full-time first-time undergraduates awarded any financial aid'
  anyaidn ='Number of full-time first-time undergraduates awarded any financial aid' 
  xanyaidp='Imputation field for anyaidp - Percent of full-time first-time undergraduates awarded any financial aid'
  anyaidp ='Percent of full-time first-time undergraduates awarded any financial aid' 
  xaidfsin='Imputation field for aidfsin - Number of full-time first-time undergraduates awarded any loans to students or grant aid  from federal state/local government or the institution'
  aidfsin ='Number of full-time first-time undergraduates awarded any loans to students or grant aid  from federal state/local government or the institution' 
  xaidfsip='Imputation field for aidfsip - Percent of full-time first-time undergraduates awarded any loans to students or grant aid  from federal state/local government or the institution'
  aidfsip ='Percent of full-time first-time undergraduates awarded any loans to students or grant aid  from federal state/local government or the institution' 
  xagrnt_n='Imputation field for agrnt_n - Number of full-time first-time undergraduates awarded federal, state, local or institutional grant aid'
  agrnt_n ='Number of full-time first-time undergraduates awarded federal, state, local or institutional grant aid' 
  xagrnt_p='Imputation field for agrnt_p - Percent of full-time first-time undergraduates awarded federal, state, local or institutional grant aid'
  agrnt_p ='Percent of full-time first-time undergraduates awarded federal, state, local or institutional grant aid' 
  xagrnt_t='Imputation field for agrnt_t - Total amount of federal, state, local or institutional grant aid awarded to full-time first-time undergraduates'
  agrnt_t ='Total amount of federal, state, local or institutional grant aid awarded to full-time first-time undergraduates' 
  xagrnt_a='Imputation field for agrnt_a - Average amount of federal, state, local or institutional grant aid awarded'
  agrnt_a ='Average amount of federal, state, local or institutional grant aid awarded' 
  xfgrnt_n='Imputation field for fgrnt_n - Number of full-time first-time undergraduates awarded federal grant aid'
  fgrnt_n ='Number of full-time first-time undergraduates awarded federal grant aid' 
  xfgrnt_p='Imputation field for fgrnt_p - Percent of full-time first-time undergraduates awarded federal grant aid'
  fgrnt_p ='Percent of full-time first-time undergraduates awarded federal grant aid' 
  xfgrnt_t='Imputation field for fgrnt_t - Total amount of federal grant aid awarded to full-time first-time undergraduates'
  fgrnt_t ='Total amount of federal grant aid awarded to full-time first-time undergraduates' 
  xfgrnt_a='Imputation field for fgrnt_a - Average amount of federal grant aid awarded to full-time first-time undergraduates'
  fgrnt_a ='Average amount of federal grant aid awarded to full-time first-time undergraduates' 
  xpgrnt_n='Imputation field for pgrnt_n - Number of full-time first-time undergraduates awarded Pell grants'
  pgrnt_n ='Number of full-time first-time undergraduates awarded Pell grants' 
  xpgrnt_p='Imputation field for pgrnt_p - Percent of full-time first-time undergraduates awarded Pell grants'
  pgrnt_p ='Percent of full-time first-time undergraduates awarded Pell grants' 
  xpgrnt_t='Imputation field for pgrnt_t - Total amount of Pell grant aid awarded to full-time first-time undergraduates'
  pgrnt_t ='Total amount of Pell grant aid awarded to full-time first-time undergraduates' 
  xpgrnt_a='Imputation field for pgrnt_a - Average amount of Pell grant aid awarded to full-time first-time undergraduates'
  pgrnt_a ='Average amount of Pell grant aid awarded to full-time first-time undergraduates' 
  xofgrt_n='Imputation field for ofgrt_n - Number of full-time first-time undergraduates awarded other federal grant aid'
  ofgrt_n ='Number of full-time first-time undergraduates awarded other federal grant aid' 
  xofgrt_p='Imputation field for ofgrt_p - Percent of full-time first-time undergraduates awarded other federal grant aid'
  ofgrt_p ='Percent of full-time first-time undergraduates awarded other federal grant aid' 
  xofgrt_t='Imputation field for ofgrt_t - Total amount of other federal grant aid awarded to full-time first-time undergraduates'
  ofgrt_t ='Total amount of other federal grant aid awarded to full-time first-time undergraduates' 
  xofgrt_a='Imputation field for ofgrt_a - Average amount of other federal grant aid awarded to full-time first-time undergraduates'
  ofgrt_a ='Average amount of other federal grant aid awarded to full-time first-time undergraduates' 
  xsgrnt_n='Imputation field for sgrnt_n - Number of full-time first-time undergraduates awarded state/local grant aid'
  sgrnt_n ='Number of full-time first-time undergraduates awarded state/local grant aid' 
  xsgrnt_p='Imputation field for sgrnt_p - Percent of full-time first-time undergraduates awarded state/local grant aid'
  sgrnt_p ='Percent of full-time first-time undergraduates awarded state/local grant aid' 
  xsgrnt_t='Imputation field for sgrnt_t - Total amount of state/local grant aid awarded to full-time first-time undergraduates'
  sgrnt_t ='Total amount of state/local grant aid awarded to full-time first-time undergraduates' 
  xsgrnt_a='Imputation field for sgrnt_a - Average amount of state/local grant aid awarded to full-time first-time undergraduates'
  sgrnt_a ='Average amount of state/local grant aid awarded to full-time first-time undergraduates' 
  xigrnt_n='Imputation field for igrnt_n - Number of full-time first-time undergraduates awarded  institutional grant aid'
  igrnt_n ='Number of full-time first-time undergraduates awarded  institutional grant aid' 
  xigrnt_p='Imputation field for igrnt_p - Percent of full-time first-time undergraduates awarded institutional grant aid'
  igrnt_p ='Percent of full-time first-time undergraduates awarded institutional grant aid' 
  xigrnt_t='Imputation field for igrnt_t - Total amount of institutional grant aid awarded to full-time first-time undergraduates'
  igrnt_t ='Total amount of institutional grant aid awarded to full-time first-time undergraduates' 
  xigrnt_a='Imputation field for igrnt_a - Average amount of institutional grant aid awarded to full-time first-time undergraduates'
  igrnt_a ='Average amount of institutional grant aid awarded to full-time first-time undergraduates' 
  xloan_n ='Imputation field for loan_n - Number of full-time first-time undergraduates awarded student loans'
  loan_n  ='Number of full-time first-time undergraduates awarded student loans' 
  xloan_p ='Imputation field for loan_p - Percent of full-time first-time undergraduates awarded student loans'
  loan_p  ='Percent of full-time first-time undergraduates awarded student loans' 
  xloan_t ='Imputation field for loan_t - Total amount of student loans awarded to full-time first-time undergraduates'
  loan_t  ='Total amount of student loans awarded to full-time first-time undergraduates' 
  xloan_a ='Imputation field for loan_a - Average amount of student loans awarded to full-time first-time undergraduates'
  loan_a  ='Average amount of student loans awarded to full-time first-time undergraduates' 
  xfloan_n='Imputation field for floan_n - Number of full-time first-time undergraduates awarded federal student loans'
  floan_n ='Number of full-time first-time undergraduates awarded federal student loans' 
  xfloan_p='Imputation field for floan_p - Percent of full-time first-time undergraduates awarded federal student loans'
  floan_p ='Percent of full-time first-time undergraduates awarded federal student loans' 
  xfloan_t='Imputation field for floan_t - Total amount of federal student loans awarded to full-time first-time undergraduates'
  floan_t ='Total amount of federal student loans awarded to full-time first-time undergraduates' 
  xfloan_a='Imputation field for floan_a - Average amount of federal student loans awarded to full-time first-time undergraduates'
  floan_a ='Average amount of federal student loans awarded to full-time first-time undergraduates' 
  xoloan_n='Imputation field for oloan_n - Number of full-time first-time undergraduates awarded other student loans'
  oloan_n ='Number of full-time first-time undergraduates awarded other student loans' 
  xoloan_p='Imputation field for oloan_p - Percent of full-time first-time undergraduates awarded other student loans'
  oloan_p ='Percent of full-time first-time undergraduates awarded other student loans' 
  xoloan_t='Imputation field for oloan_t - Total amount of other student loans awarded to full-time first-time undergraduates'
  oloan_t ='Total amount of other student loans awarded to full-time first-time undergraduates' 
  xoloan_a='Imputation field for oloan_a - Average amount of other student loans awarded to full-time first-time undergraduates'
  oloan_a ='Average amount of other student loans awarded to full-time first-time undergraduates' 
  xgistn2 ='Imputation field for gistn2 - Total number, 2020-21'
  gistn2  ='Total number, 2020-21' 
  xgiston2='Imputation field for giston2 - Number living on-campus, 2020-21'
  giston2 ='Number living on-campus, 2020-21' 
  xgistwf2='Imputation field for gistwf2 - Number living off-campus with family, 2020-21'
  gistwf2 ='Number living off-campus with family, 2020-21' 
  xgistof2='Imputation field for gistof2 - Number living off-campus not with family, 2020-21'
  gistof2 ='Number living off-campus not with family, 2020-21' 
  xgistun2='Imputation field for gistun2 - Number living arrangement unknown, 2020-21'
  gistun2 ='Number living arrangement unknown, 2020-21' 
  xgistt2 ='Imputation field for gistt2 - Total amount of grant and scholarship aid awarded, 2020-21'
  gistt2  ='Total amount of grant and scholarship aid awarded, 2020-21' 
  xgista2 ='Imputation field for gista2 - Average amount of grant and scholarship aid awarded, 2020-21'
  gista2  ='Average amount of grant and scholarship aid awarded, 2020-21' 
  xgistn1 ='Imputation field for gistn1 - Total number, 2019-20'
  gistn1  ='Total number, 2019-20' 
  xgiston1='Imputation field for giston1 - Number living on-campus, 2019-20'
  giston1 ='Number living on-campus, 2019-20' 
  xgistwf1='Imputation field for gistwf1 - Number living off-campus with family, 2019-20'
  gistwf1 ='Number living off-campus with family, 2019-20' 
  xgistof1='Imputation field for gistof1 - Number living off-campus not with family, 2019-20'
  gistof1 ='Number living off-campus not with family, 2019-20' 
  xgistun1='Imputation field for gistun1 - Number living arrangement unknown, 2019-20'
  gistun1 ='Number living arrangement unknown, 2019-20' 
  xgistt1 ='Imputation field for gistt1 - Total amount of grant and scholarship aid awarded, 2019-20'
  gistt1  ='Total amount of grant and scholarship aid awarded, 2019-20' 
  xgista1 ='Imputation field for gista1 - Average amount of grant and scholarship aid awarded, 2019-20'
  gista1  ='Average amount of grant and scholarship aid awarded, 2019-20' 
  xgistn0 ='Imputation field for gistn0 - Total number, 2018-19'
  gistn0  ='Total number, 2018-19' 
  xgiston0='Imputation field for giston0 - Number living on-campus, 2018-19'
  giston0 ='Number living on-campus, 2018-19' 
  xgistwf0='Imputation field for gistwf0 - Number living off-campus with family, 2018-19'
  gistwf0 ='Number living off-campus with family, 2018-19' 
  xgistof0='Imputation field for gistof0 - Number living off-campus not with family, 2018-19'
  gistof0 ='Number living off-campus not with family, 2018-19' 
  xgistun0='Imputation field for gistun0 - Number living arrangement unknown, 2018-19'
  gistun0 ='Number living arrangement unknown, 2018-19' 
  xgistt0 ='Imputation field for gistt0 - Total amount of grant and scholarship aid awarded, 2018-19'
  gistt0  ='Total amount of grant and scholarship aid awarded, 2018-19' 
  xgista0 ='Imputation field for gista0 - Average amount of grant and scholarship aid awarded, 2018-19'
  gista0  ='Average amount of grant and scholarship aid awarded, 2018-19' 
  xgis4n2 ='Imputation field for gis4n2 - Total number in all income levels, 2020-21'
  gis4n2  ='Total number in all income levels, 2020-21' 
  xgis4on2='Imputation field for gis4on2 - Number living on-campus in all income levels, 2020-21'
  gis4on2 ='Number living on-campus in all income levels, 2020-21' 
  xgis4wf2='Imputation field for gis4wf2 - Number living off-campus with family in all income levels, 2020-21'
  gis4wf2 ='Number living off-campus with family in all income levels, 2020-21' 
  xgis4of2='Imputation field for gis4of2 - Number living off-campus not with family in all income levels, 2020-21'
  gis4of2 ='Number living off-campus not with family in all income levels, 2020-21' 
  xgis4un2='Imputation field for gis4un2 - Number living arrangement unknown in all income levels, 2020-21'
  gis4un2 ='Number living arrangement unknown in all income levels, 2020-21' 
  xgis4t2 ='Imputation field for gis4t2 - Total amount of grant and scholarship aid awarded, all income levels, 2020-21'
  gis4t2  ='Total amount of grant and scholarship aid awarded, all income levels, 2020-21' 
  xgis4a2 ='Imputation field for gis4a2 - Average amount of grant and scholarship aid awarded, all income levels, 2020-21'
  gis4a2  ='Average amount of grant and scholarship aid awarded, all income levels, 2020-21' 
  xgis4n12='Imputation field for gis4n12 - Number in income level (0-30,000), 2020-21'
  gis4n12 ='Number in income level (0-30,000), 2020-21' 
  xgis4t12='Imputation field for gis4t12 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21'
  gis4t12 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21' 
  xgis4a12='Imputation field for gis4a12 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21'
  gis4a12 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21' 
  xgis4n22='Imputation field for gis4n22 - Number in income level (30,001-48,000), 2020-21'
  gis4n22 ='Number in income level (30,001-48,000), 2020-21' 
  xgis4t22='Imputation field for gis4t22 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21'
  gis4t22 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21' 
  xgis4a22='Imputation field for gis4a22 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21'
  gis4a22 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21' 
  xgis4n32='Imputation field for gis4n32 - Number in income level (48,001-75,000), 2020-21'
  gis4n32 ='Number in income level (48,001-75,000), 2020-21' 
  xgis4t32='Imputation field for gis4t32 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21'
  gis4t32 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21' 
  xgis4a32='Imputation field for gis4a32 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21'
  gis4a32 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21' 
  xgis4n42='Imputation field for gis4n42 - Number in income level (75,001-110,000), 2020-21'
  gis4n42 ='Number in income level (75,001-110,000), 2020-21' 
  xgis4t42='Imputation field for gis4t42 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21'
  gis4t42 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21' 
  xgis4a42='Imputation field for gis4a42 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21'
  gis4a42 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21' 
  xgis4n52='Imputation field for gis4n52 - Number in income level (110,001 or more), 2020-21'
  gis4n52 ='Number in income level (110,001 or more), 2020-21' 
  xgis4t52='Imputation field for gis4t52 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21'
  gis4t52 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21' 
  xgis4a52='Imputation field for gis4a52 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21'
  gis4a52 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21' 
  xgis4n1 ='Imputation field for gis4n1 - Total number in all income levels, 2019-20'
  gis4n1  ='Total number in all income levels, 2019-20' 
  xgis4on1='Imputation field for gis4on1 - Number living on-campus in all income levels, 2019-20'
  gis4on1 ='Number living on-campus in all income levels, 2019-20' 
  xgis4wf1='Imputation field for gis4wf1 - Number living off-campus with family in all income levels, 2019-20'
  gis4wf1 ='Number living off-campus with family in all income levels, 2019-20' 
  xgis4of1='Imputation field for gis4of1 - Number living off-campus not with family in all income levels, 2019-20'
  gis4of1 ='Number living off-campus not with family in all income levels, 2019-20' 
  xgis4un1='Imputation field for gis4un1 - Number living arrangement unknown in all income levels, 2019-20'
  gis4un1 ='Number living arrangement unknown in all income levels, 2019-20' 
  xgis4t1 ='Imputation field for gis4t1 - Total amount of grant and scholarship aid awarded, all income levels, 2019-20'
  gis4t1  ='Total amount of grant and scholarship aid awarded, all income levels, 2019-20' 
  xgis4a1 ='Imputation field for gis4a1 - Average amount of grant and scholarship aid awarded, all income levels, 2019-20'
  gis4a1  ='Average amount of grant and scholarship aid awarded, all income levels, 2019-20' 
  xgis4n11='Imputation field for gis4n11 - Number in income level (0-30,000), 2019-20'
  gis4n11 ='Number in income level (0-30,000), 2019-20' 
  xgis4t11='Imputation field for gis4t11 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20'
  gis4t11 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20' 
  xgis4a11='Imputation field for gis4a11 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20'
  gis4a11 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20' 
  xgis4n21='Imputation field for gis4n21 - Number in income level (30,001-48,000), 2019-20'
  gis4n21 ='Number in income level (30,001-48,000), 2019-20' 
  xgis4t21='Imputation field for gis4t21 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20'
  gis4t21 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20' 
  xgis4a21='Imputation field for gis4a21 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20'
  gis4a21 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20' 
  xgis4n31='Imputation field for gis4n31 - Number in income level (48,001-75,000), 2019-20'
  gis4n31 ='Number in income level (48,001-75,000), 2019-20' 
  xgis4t31='Imputation field for gis4t31 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20'
  gis4t31 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20' 
  xgis4a31='Imputation field for gis4a31 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20'
  gis4a31 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20' 
  xgis4n41='Imputation field for gis4n41 - Number in income level (75,001-110,000), 2019-20'
  gis4n41 ='Number in income level (75,001-110,000), 2019-20' 
  xgis4t41='Imputation field for gis4t41 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20'
  gis4t41 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20' 
  xgis4a41='Imputation field for gis4a41 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20'
  gis4a41 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20' 
  xgis4n51='Imputation field for gis4n51 - Number in income level (110,001 or more), 2019-20'
  gis4n51 ='Number in income level (110,001 or more), 2019-20' 
  xgis4t51='Imputation field for gis4t51 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20'
  gis4t51 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20' 
  xgis4a51='Imputation field for gis4a51 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20'
  gis4a51 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20' 
  xgis4n0 ='Imputation field for gis4n0 - Total number in all income levels, 2018-19'
  gis4n0  ='Total number in all income levels, 2018-19' 
  xgis4on0='Imputation field for gis4on0 - Number living on-campus in all income levels, 2018-19'
  gis4on0 ='Number living on-campus in all income levels, 2018-19' 
  xgis4wf0='Imputation field for gis4wf0 - Number living off-campus with family in all income levels, 2018-19'
  gis4wf0 ='Number living off-campus with family in all income levels, 2018-19' 
  xgis4of0='Imputation field for gis4of0 - Number living off-campus not with family in all income levels, 2018-19'
  gis4of0 ='Number living off-campus not with family in all income levels, 2018-19' 
  xgis4un0='Imputation field for gis4un0 - Number living arrangement unknown in all income levels, 2018-19'
  gis4un0 ='Number living arrangement unknown in all income levels, 2018-19' 
  xgis4t0 ='Imputation field for gis4t0 - Total amount of grant and scholarship aid awarded, all income levels, 2018-19'
  gis4t0  ='Total amount of grant and scholarship aid awarded, all income levels, 2018-19' 
  xgis4a0 ='Imputation field for gis4a0 - Average amount of grant and scholarship aid awarded, all income levels, 2018-19'
  gis4a0  ='Average amount of grant and scholarship aid awarded, all income levels, 2018-19' 
  xgis4n10='Imputation field for gis4n10 - Number in income level (0-30,000), 2018-19'
  gis4n10 ='Number in income level (0-30,000), 2018-19' 
  xgis4t10='Imputation field for gis4t10 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19'
  gis4t10 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19' 
  xgis4a10='Imputation field for gis4a10 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19'
  gis4a10 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19' 
  xgis4n20='Imputation field for gis4n20 - Number in income level (30,001-48,000), 2018-19'
  gis4n20 ='Number in income level (30,001-48,000), 2018-19' 
  xgis4t20='Imputation field for gis4t20 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19'
  gis4t20 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19' 
  xgis4a20='Imputation field for gis4a20 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19'
  gis4a20 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19' 
  xgis4n30='Imputation field for gis4n30 - Number in income level (48,001-75,000), 2018-19'
  gis4n30 ='Number in income level (48,001-75,000), 2018-19' 
  xgis4t30='Imputation field for gis4t30 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19'
  gis4t30 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19' 
  xgis4a30='Imputation field for gis4a30 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19'
  gis4a30 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19' 
  xgis4n40='Imputation field for gis4n40 - Number in income level (75,001-110,000), 2018-19'
  gis4n40 ='Number in income level (75,001-110,000), 2018-19' 
  xgis4t40='Imputation field for gis4t40 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19'
  gis4t40 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19' 
  xgis4a40='Imputation field for gis4a40 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19'
  gis4a40 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19' 
  xgis4n50='Imputation field for gis4n50 - Number in income level (110,001 or more), 2018-19'
  gis4n50 ='Number in income level (110,001 or more), 2018-19' 
  xgis4t50='Imputation field for gis4t50 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19'
  gis4t50 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19' 
  xgis4a50='Imputation field for gis4a50 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19'
  gis4a50 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19' 
  xgis4g2 ='Imputation field for gis4g2 - Number awarded grant and scholarship aid, all income levels, 2020-21'
  gis4g2  ='Number awarded grant and scholarship aid, all income levels, 2020-21' 
  xgis4g12='Imputation field for gis4g12 - Number awarded grant and scholarship aid, income level (0-30,000), 2020-21'
  gis4g12 ='Number awarded grant and scholarship aid, income level (0-30,000), 2020-21' 
  xgis4g22='Imputation field for gis4g22 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2020-21'
  gis4g22 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2020-21' 
  xgis4g32='Imputation field for gis4g32 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2020-21'
  gis4g32 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2020-21' 
  xgis4g42='Imputation field for gis4g42 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2020-21'
  gis4g42 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2020-21' 
  xgis4g52='Imputation field for gis4g52 - Number awarded grant and scholarship aid, income level (110,001 or more), 2020-21'
  gis4g52 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2020-21' 
  xgis4g1 ='Imputation field for gis4g1 - Number awarded grant and scholarship aid, all income levels, 2019-20'
  gis4g1  ='Number awarded grant and scholarship aid, all income levels, 2019-20' 
  xgis4g11='Imputation field for gis4g11 - Number awarded grant and scholarship aid, income level (0-30,000), 2019-20'
  gis4g11 ='Number awarded grant and scholarship aid, income level (0-30,000), 2019-20' 
  xgis4g21='Imputation field for gis4g21 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2019-20'
  gis4g21 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2019-20' 
  xgis4g31='Imputation field for gis4g31 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2019-20'
  gis4g31 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2019-20' 
  xgis4g41='Imputation field for gis4g41 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2019-20'
  gis4g41 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2019-20' 
  xgis4g51='Imputation field for gis4g51 - Number awarded grant and scholarship aid, income level (110,001 or more), 2019-20'
  gis4g51 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2019-20' 
  xgis4g0 ='Imputation field for gis4g0 - Number awarded grant and scholarship aid, all income levels, 2018-19'
  gis4g0  ='Number awarded grant and scholarship aid, all income levels, 2018-19' 
  xgis4g10='Imputation field for gis4g10 - Number awarded grant and scholarship aid, income level (0-30,000), 2018-19'
  gis4g10 ='Number awarded grant and scholarship aid, income level (0-30,000), 2018-19' 
  xgis4g20='Imputation field for gis4g20 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2018-19'
  gis4g20 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2018-19' 
  xgis4g30='Imputation field for gis4g30 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2018-19'
  gis4g30 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2018-19' 
  xgis4g40='Imputation field for gis4g40 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2018-19'
  gis4g40 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2018-19' 
  xgis4g50='Imputation field for gis4g50 - Number awarded grant and scholarship aid, income level (110,001 or more), 2018-19'
  gis4g50 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2018-19' 
  xnpist2 ='Imputation field for npist2 - Average net price-students awarded grant or scholarship aid, 2020-21'
  npist2  ='Average net price-students awarded grant or scholarship aid, 2020-21' 
  xnpist1 ='Imputation field for npist1 - Average net price-students awarded grant or scholarship aid, 2019-20'
  npist1  ='Average net price-students awarded grant or scholarship aid, 2019-20' 
  xnpist0 ='Imputation field for npist0 - Average net price-students awarded grant or scholarship aid, 2018-19'
  npist0  ='Average net price-students awarded grant or scholarship aid, 2018-19' 
  xnpis412='Imputation field for npis412 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2020-21'
  npis412 ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpis422='Imputation field for npis422 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2020-21'
  npis422 ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpis432='Imputation field for npis432 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2020-21'
  npis432 ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpis442='Imputation field for npis442 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2020-21'
  npis442 ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpis452='Imputation field for npis452 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2020-21'
  npis452 ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpis411='Imputation field for npis411 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2019-20'
  npis411 ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpis421='Imputation field for npis421 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2019-20'
  npis421 ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpis431='Imputation field for npis431 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2019-20'
  npis431 ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpis441='Imputation field for npis441 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2019-20'
  npis441 ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpis451='Imputation field for npis451 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2019-20'
  npis451 ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpis410='Imputation field for npis410 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2018-19'
  npis410 ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpis420='Imputation field for npis420 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2018-19'
  npis420 ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpis430='Imputation field for npis430 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2018-19'
  npis430 ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpis440='Imputation field for npis440 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2018-19'
  npis440 ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpis450='Imputation field for npis450 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2018-19'
  npis450 ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2018-19' 
  xgrntn2 ='Imputation field for grntn2 - Total number, 2020-21'
  grntn2  ='Total number, 2020-21' 
  xgrnton2='Imputation field for grnton2 - Number living on-campus, 2020-21'
  grnton2 ='Number living on-campus, 2020-21' 
  xgrntwf2='Imputation field for grntwf2 - Number living off-campus with family, 2020-21'
  grntwf2 ='Number living off-campus with family, 2020-21' 
  xgrntof2='Imputation field for grntof2 - Number living off-campus not with family, 2020-21'
  grntof2 ='Number living off-campus not with family, 2020-21' 
  xgrntun2='Imputation field for grntun2 - Number living arrangement unknown, 2020-21'
  grntun2 ='Number living arrangement unknown, 2020-21' 
  xgrntt2 ='Imputation field for grntt2 - Total amount of grant and scholarship aid awarded, 2020-21'
  grntt2  ='Total amount of grant and scholarship aid awarded, 2020-21' 
  xgrnta2 ='Imputation field for grnta2 - Average amount of grant and scholarship aid awarded, 2020-21'
  grnta2  ='Average amount of grant and scholarship aid awarded, 2020-21' 
  xgrntn1 ='Imputation field for grntn1 - Total number, 2019-20'
  grntn1  ='Total number, 2019-20' 
  xgrnton1='Imputation field for grnton1 - Number living on-campus, 2019-20'
  grnton1 ='Number living on-campus, 2019-20' 
  xgrntwf1='Imputation field for grntwf1 - Number living off-campus with family, 2019-20'
  grntwf1 ='Number living off-campus with family, 2019-20' 
  xgrntof1='Imputation field for grntof1 - Number living off-campus not with family, 2019-20'
  grntof1 ='Number living off-campus not with family, 2019-20' 
  xgrntun1='Imputation field for grntun1 - Number living arrangement unknown, 2019-20'
  grntun1 ='Number living arrangement unknown, 2019-20' 
  xgrntt1 ='Imputation field for grntt1 - Total amount of grant and scholarship aid awarded, 2019-20'
  grntt1  ='Total amount of grant and scholarship aid awarded, 2019-20' 
  xgrnta1 ='Imputation field for grnta1 - Average amount of grant and scholarship aid awarded, 2019-20'
  grnta1  ='Average amount of grant and scholarship aid awarded, 2019-20' 
  xgrntn0 ='Imputation field for grntn0 - Total number, 2018-19'
  grntn0  ='Total number, 2018-19' 
  xgrnton0='Imputation field for grnton0 - Number living on-campus, 2018-19'
  grnton0 ='Number living on-campus, 2018-19' 
  xgrntwf0='Imputation field for grntwf0 - Number living off-campus with family, 2018-19'
  grntwf0 ='Number living off-campus with family, 2018-19' 
  xgrntof0='Imputation field for grntof0 - Number living off-campus not with family, 2018-19'
  grntof0 ='Number living off-campus not with family, 2018-19' 
  xgrntun0='Imputation field for grntun0 - Number living arrangement unknown, 2018-19'
  grntun0 ='Number living arrangement unknown, 2018-19' 
  xgrntt0 ='Imputation field for grntt0 - Total amount of grant and scholarship aid awarded, 2018-19'
  grntt0  ='Total amount of grant and scholarship aid awarded, 2018-19' 
  xgrnta0 ='Imputation field for grnta0 - Average amount of grant and scholarship aid awarded, 2018-19'
  grnta0  ='Average amount of grant and scholarship aid awarded, 2018-19' 
  xgrn4n2 ='Imputation field for grn4n2 - Total number in all income levels, 2020-21'
  grn4n2  ='Total number in all income levels, 2020-21' 
  xgrn4on2='Imputation field for grn4on2 - Number living on-campus in all income levels, 2020-21'
  grn4on2 ='Number living on-campus in all income levels, 2020-21' 
  xgrn4wf2='Imputation field for grn4wf2 - Number living off-campus with family in all income levels, 2020-21'
  grn4wf2 ='Number living off-campus with family in all income levels, 2020-21' 
  xgrn4of2='Imputation field for grn4of2 - Number living off-campus not with family in all income levels, 2020-21'
  grn4of2 ='Number living off-campus not with family in all income levels, 2020-21' 
  xgrn4un2='Imputation field for grn4un2 - Number living arrangement unknown in all income levels, 2020-21'
  grn4un2 ='Number living arrangement unknown in all income levels, 2020-21' 
  xgrn4t2 ='Imputation field for grn4t2 - Total amount of grant and scholarship aid awarded, all income levels, 2020-21'
  grn4t2  ='Total amount of grant and scholarship aid awarded, all income levels, 2020-21' 
  xgrn4a2 ='Imputation field for grn4a2 - Average amount of grant and scholarship aid awarded, all income levels, 2020-21'
  grn4a2  ='Average amount of grant and scholarship aid awarded, all income levels, 2020-21' 
  xgrn4n12='Imputation field for grn4n12 - Number in income level (0-30,000), 2020-21'
  grn4n12 ='Number in income level (0-30,000), 2020-21' 
  xgrn4t12='Imputation field for grn4t12 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21'
  grn4t12 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21' 
  xgrn4a12='Imputation field for grn4a12 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21'
  grn4a12 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2020-21' 
  xgrn4n22='Imputation field for grn4n22 - Number in income level (30,001-48,000), 2020-21'
  grn4n22 ='Number in income level (30,001-48,000), 2020-21' 
  xgrn4t22='Imputation field for grn4t22 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21'
  grn4t22 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21' 
  xgrn4a22='Imputation field for grn4a22 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21'
  grn4a22 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2020-21' 
  xgrn4n32='Imputation field for grn4n32 - Number in income level (48,001-75,000), 2020-21'
  grn4n32 ='Number in income level (48,001-75,000), 2020-21' 
  xgrn4t32='Imputation field for grn4t32 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21'
  grn4t32 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21' 
  xgrn4a32='Imputation field for grn4a32 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21'
  grn4a32 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2020-21' 
  xgrn4n42='Imputation field for grn4n42 - Number in income level (75,001-110,000), 2020-21'
  grn4n42 ='Number in income level (75,001-110,000), 2020-21' 
  xgrn4t42='Imputation field for grn4t42 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21'
  grn4t42 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21' 
  xgrn4a42='Imputation field for grn4a42 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21'
  grn4a42 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2020-21' 
  xgrn4n52='Imputation field for grn4n52 - Number in income level (110,001 or more), 2020-21'
  grn4n52 ='Number in income level (110,001 or more), 2020-21' 
  xgrn4t52='Imputation field for grn4t52 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21'
  grn4t52 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21' 
  xgrn4a52='Imputation field for grn4a52 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21'
  grn4a52 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2020-21' 
  xgrn4n1 ='Imputation field for grn4n1 - Total number in all income levels, 2019-20'
  grn4n1  ='Total number in all income levels, 2019-20' 
  xgrn4on1='Imputation field for grn4on1 - Number living on-campus in all income levels, 2019-20'
  grn4on1 ='Number living on-campus in all income levels, 2019-20' 
  xgrn4wf1='Imputation field for grn4wf1 - Number living off-campus with family in all income levels, 2019-20'
  grn4wf1 ='Number living off-campus with family in all income levels, 2019-20' 
  xgrn4of1='Imputation field for grn4of1 - Number living off-campus not with family in all income levels, 2019-20'
  grn4of1 ='Number living off-campus not with family in all income levels, 2019-20' 
  xgrn4un1='Imputation field for grn4un1 - Number living arrangement unknown in all income levels, 2019-20'
  grn4un1 ='Number living arrangement unknown in all income levels, 2019-20' 
  xgrn4t1 ='Imputation field for grn4t1 - Total amount of grant and scholarship aid awarded, all income levels, 2019-20'
  grn4t1  ='Total amount of grant and scholarship aid awarded, all income levels, 2019-20' 
  xgrn4a1 ='Imputation field for grn4a1 - Average amount of grant and scholarship aid awarded, all income levels, 2019-20'
  grn4a1  ='Average amount of grant and scholarship aid awarded, all income levels, 2019-20' 
  xgrn4n11='Imputation field for grn4n11 - Number in income level (0-30,000), 2019-20'
  grn4n11 ='Number in income level (0-30,000), 2019-20' 
  xgrn4t11='Imputation field for grn4t11 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20'
  grn4t11 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20' 
  xgrn4a11='Imputation field for grn4a11 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20'
  grn4a11 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2019-20' 
  xgrn4n21='Imputation field for grn4n21 - Number in income level (30,001-48,000), 2019-20'
  grn4n21 ='Number in income level (30,001-48,000), 2019-20' 
  xgrn4t21='Imputation field for grn4t21 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20'
  grn4t21 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20' 
  xgrn4a21='Imputation field for grn4a21 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20'
  grn4a21 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2019-20' 
  xgrn4n31='Imputation field for grn4n31 - Number in income level (48,001-75,000), 2019-20'
  grn4n31 ='Number in income level (48,001-75,000), 2019-20' 
  xgrn4t31='Imputation field for grn4t31 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20'
  grn4t31 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20' 
  xgrn4a31='Imputation field for grn4a31 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20'
  grn4a31 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2019-20' 
  xgrn4n41='Imputation field for grn4n41 - Number in income level (75,001-110,000), 2019-20'
  grn4n41 ='Number in income level (75,001-110,000), 2019-20' 
  xgrn4t41='Imputation field for grn4t41 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20'
  grn4t41 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20' 
  xgrn4a41='Imputation field for grn4a41 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20'
  grn4a41 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2019-20' 
  xgrn4n51='Imputation field for grn4n51 - Number in income level (110,001 or more), 2019-20'
  grn4n51 ='Number in income level (110,001 or more), 2019-20' 
  xgrn4t51='Imputation field for grn4t51 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20'
  grn4t51 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20' 
  xgrn4a51='Imputation field for grn4a51 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20'
  grn4a51 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2019-20' 
  xgrn4n0 ='Imputation field for grn4n0 - Total number in all income levels, 2018-19'
  grn4n0  ='Total number in all income levels, 2018-19' 
  xgrn4on0='Imputation field for grn4on0 - Number living on-campus in all income levels, 2018-19'
  grn4on0 ='Number living on-campus in all income levels, 2018-19' 
  xgrn4wf0='Imputation field for grn4wf0 - Number living off-campus with family in all income levels, 2018-19'
  grn4wf0 ='Number living off-campus with family in all income levels, 2018-19' 
  xgrn4of0='Imputation field for grn4of0 - Number living off-campus not with family in all income levels, 2018-19'
  grn4of0 ='Number living off-campus not with family in all income levels, 2018-19' 
  xgrn4un0='Imputation field for grn4un0 - Number living arrangement unknown in all income levels, 2018-19'
  grn4un0 ='Number living arrangement unknown in all income levels, 2018-19' 
  xgrn4t0 ='Imputation field for grn4t0 - Total amount of grant and scholarship aid awarded, all income levels, 2018-19'
  grn4t0  ='Total amount of grant and scholarship aid awarded, all income levels, 2018-19' 
  xgrn4a0 ='Imputation field for grn4a0 - Average amount of grant and scholarship aid awarded, all income levels, 2018-19'
  grn4a0  ='Average amount of grant and scholarship aid awarded, all income levels, 2018-19' 
  xgrn4n10='Imputation field for grn4n10 - Number in income level (0-30,000), 2018-19'
  grn4n10 ='Number in income level (0-30,000), 2018-19' 
  xgrn4t10='Imputation field for grn4t10 - Total amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19'
  grn4t10 ='Total amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19' 
  xgrn4a10='Imputation field for grn4a10 - Average amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19'
  grn4a10 ='Average amount of grant and scholarship aid awarded, income level (0-30,000), 2018-19' 
  xgrn4n20='Imputation field for grn4n20 - Number in income level (30,001-48,000), 2018-19'
  grn4n20 ='Number in income level (30,001-48,000), 2018-19' 
  xgrn4t20='Imputation field for grn4t20 - Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19'
  grn4t20 ='Total amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19' 
  xgrn4a20='Imputation field for grn4a20 - Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19'
  grn4a20 ='Average amount of grant and scholarship aid awarded, income level (30,001-48,000), 2018-19' 
  xgrn4n30='Imputation field for grn4n30 - Number in income level (48,001-75,000), 2018-19'
  grn4n30 ='Number in income level (48,001-75,000), 2018-19' 
  xgrn4t30='Imputation field for grn4t30 - Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19'
  grn4t30 ='Total amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19' 
  xgrn4a30='Imputation field for grn4a30 - Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19'
  grn4a30 ='Average amount of grant and scholarship aid awarded, income level (48,001-75,000), 2018-19' 
  xgrn4n40='Imputation field for grn4n40 - Number in income level (75,001-110,000), 2018-19'
  grn4n40 ='Number in income level (75,001-110,000), 2018-19' 
  xgrn4t40='Imputation field for grn4t40 - Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19'
  grn4t40 ='Total amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19' 
  xgrn4a40='Imputation field for grn4a40 - Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19'
  grn4a40 ='Average amount of grant and scholarship aid awarded, income level (75,001-110,000), 2018-19' 
  xgrn4n50='Imputation field for grn4n50 - Number in income level (110,001 or more), 2018-19'
  grn4n50 ='Number in income level (110,001 or more), 2018-19' 
  xgrn4t50='Imputation field for grn4t50 - Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19'
  grn4t50 ='Total amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19' 
  xgrn4a50='Imputation field for grn4a50 - Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19'
  grn4a50 ='Average amount of grant and scholarship aid awarded, income level (110,001 or more), 2018-19' 
  xgrn4g2 ='Imputation field for grn4g2 - Number awarded grant and scholarship aid, all income levels, 2020-21'
  grn4g2  ='Number awarded grant and scholarship aid, all income levels, 2020-21' 
  xgrn4g12='Imputation field for grn4g12 - Number awarded grant and scholarship aid, income level (0-30,000), 2020-21'
  grn4g12 ='Number awarded grant and scholarship aid, income level (0-30,000), 2020-21' 
  xgrn4g22='Imputation field for grn4g22 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2020-21'
  grn4g22 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2020-21' 
  xgrn4g32='Imputation field for grn4g32 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2020-21'
  grn4g32 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2020-21' 
  xgrn4g42='Imputation field for grn4g42 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2020-21'
  grn4g42 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2020-21' 
  xgrn4g52='Imputation field for grn4g52 - Number awarded grant and scholarship aid, income level (110,001 or more), 2020-21'
  grn4g52 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2020-21' 
  xgrn4g1 ='Imputation field for grn4g1 - Number awarded grant and scholarship aid, all income levels, 2019-20'
  grn4g1  ='Number awarded grant and scholarship aid, all income levels, 2019-20' 
  xgrn4g11='Imputation field for grn4g11 - Number awarded grant and scholarship aid, income level (0-30,000), 2019-20'
  grn4g11 ='Number awarded grant and scholarship aid, income level (0-30,000), 2019-20' 
  xgrn4g21='Imputation field for grn4g21 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2019-20'
  grn4g21 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2019-20' 
  xgrn4g31='Imputation field for grn4g31 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2019-20'
  grn4g31 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2019-20' 
  xgrn4g41='Imputation field for grn4g41 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2019-20'
  grn4g41 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2019-20' 
  xgrn4g51='Imputation field for grn4g51 - Number awarded grant and scholarship aid, income level (110,001 or more), 2019-20'
  grn4g51 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2019-20' 
  xgrn4g0 ='Imputation field for grn4g0 - Number awarded grant and scholarship aid, all income levels, 2019-20'
  grn4g0  ='Number awarded grant and scholarship aid, all income levels, 2019-20' 
  xgrn4g10='Imputation field for grn4g10 - Number awarded grant and scholarship aid, income level (0-30,000), 2018-19'
  grn4g10 ='Number awarded grant and scholarship aid, income level (0-30,000), 2018-19' 
  xgrn4g20='Imputation field for grn4g20 - Number awarded grant and scholarship aid, income level (30,001-48,000), 2018-19'
  grn4g20 ='Number awarded grant and scholarship aid, income level (30,001-48,000), 2018-19' 
  xgrn4g30='Imputation field for grn4g30 - Number awarded grant and scholarship aid, income level (48,001-75,000), 2018-19'
  grn4g30 ='Number awarded grant and scholarship aid, income level (48,001-75,000), 2018-19' 
  xgrn4g40='Imputation field for grn4g40 - Number awarded grant and scholarship aid, income level (75,001-110,000), 2018-19'
  grn4g40 ='Number awarded grant and scholarship aid, income level (75,001-110,000), 2018-19' 
  xgrn4g50='Imputation field for grn4g50 - Number awarded grant and scholarship aid, income level (110,001 or more), 2018-19'
  grn4g50 ='Number awarded grant and scholarship aid, income level (110,001 or more), 2018-19' 
  xnpgrn2 ='Imputation field for npgrn2 - Average net price-students awarded grant or scholarship aid, 2020-21'
  npgrn2  ='Average net price-students awarded grant or scholarship aid, 2020-21' 
  xnpgrn1 ='Imputation field for npgrn1 - Average net price-students awarded grant or scholarship aid, 2019-20'
  npgrn1  ='Average net price-students awarded grant or scholarship aid, 2019-20' 
  xnpgrn0 ='Imputation field for npgrn0 - Average net price-students awarded grant or scholarship aid, 2018-19'
  npgrn0  ='Average net price-students awarded grant or scholarship aid, 2018-19' 
  xnpt412 ='Imputation field for npt412 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2020-21'
  npt412  ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpt422 ='Imputation field for npt422 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2020-21'
  npt422  ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpt432 ='Imputation field for npt432 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2020-21'
  npt432  ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpt442 ='Imputation field for npt442 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2020-21'
  npt442  ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpt452 ='Imputation field for npt452 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2020-21'
  npt452  ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2020-21' 
  xnpt411 ='Imputation field for npt411 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2019-20'
  npt411  ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpt421 ='Imputation field for npt421 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2019-20'
  npt421  ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpt431 ='Imputation field for npt431 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2019-20'
  npt431  ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpt441 ='Imputation field for npt441 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2019-20'
  npt441  ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpt451 ='Imputation field for npt451 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2019-20'
  npt451  ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2019-20' 
  xnpt410 ='Imputation field for npt410 - Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2018-19'
  npt410  ='Average net price (income 0-30,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpt420 ='Imputation field for npt420 - Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2018-19'
  npt420  ='Average net price (income 30,001-48,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpt430 ='Imputation field for npt430 - Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2018-19'
  npt430  ='Average net price (income 48,001-75,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpt440 ='Imputation field for npt440 - Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2018-19'
  npt440  ='Average net price (income 75,001-110,000)-students awarded Title IV federal financial aid, 2018-19' 
  xnpt450 ='Imputation field for npt450 - Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2018-19'
  npt450  ='Average net price (income over 110,000)-students awarded Title IV federal financial aid, 2018-19';
  format xscugrad-character-xnpt450  $ximpflg.;
run;

Proc Freq;
Tables
xscugrad xscugffn xscugffp xscfa2   xscfa1n  xscfa1p  xscfa11n xscfa11p xscfa12n
xscfa12p xscfa13n xscfa13p xscfa14n xscfa14p xscfy2   xscfy1n  xscfy1p  xuagrntn xuagrntp
xuagrntt xuagrnta xupgrntn xupgrntp xupgrntt xupgrnta xufloann xufloanp xufloant xufloana
xanyaidn xanyaidp xaidfsin xaidfsip xagrnt_n xagrnt_p xagrnt_t xagrnt_a xfgrnt_n xfgrnt_p
xfgrnt_t xfgrnt_a xpgrnt_n xpgrnt_p xpgrnt_t xpgrnt_a xofgrt_n xofgrt_p xofgrt_t xofgrt_a
xsgrnt_n xsgrnt_p xsgrnt_t xsgrnt_a xigrnt_n xigrnt_p xigrnt_t xigrnt_a xloan_n  xloan_p 
xloan_t  xloan_a  xfloan_n xfloan_p xfloan_t xfloan_a xoloan_n xoloan_p xoloan_t xoloan_a
xgistn2  xgiston2 xgistwf2 xgistof2 xgistun2 xgistt2  xgista2  xgistn1  xgiston1 xgistwf1
xgistof1 xgistun1 xgistt1  xgista1  xgistn0  xgiston0 xgistwf0 xgistof0 xgistun0 xgistt0 
xgista0  xgis4n2  xgis4on2 xgis4wf2 xgis4of2 xgis4un2 xgis4t2  xgis4a2  xgis4n12 xgis4t12
xgis4a12 xgis4n22 xgis4t22 xgis4a22 xgis4n32 xgis4t32 xgis4a32 xgis4n42 xgis4t42 xgis4a42
xgis4n52 xgis4t52 xgis4a52 xgis4n1  xgis4on1 xgis4wf1 xgis4of1 xgis4un1 xgis4t1  xgis4a1 
xgis4n11 xgis4t11 xgis4a11 xgis4n21 xgis4t21 xgis4a21 xgis4n31 xgis4t31 xgis4a31 xgis4n41
xgis4t41 xgis4a41 xgis4n51 xgis4t51 xgis4a51 xgis4n0  xgis4on0 xgis4wf0 xgis4of0 xgis4un0
xgis4t0  xgis4a0  xgis4n10 xgis4t10 xgis4a10 xgis4n20 xgis4t20 xgis4a20 xgis4n30 xgis4t30
xgis4a30 xgis4n40 xgis4t40 xgis4a40 xgis4n50 xgis4t50 xgis4a50 xgis4g2  xgis4g12 xgis4g22
xgis4g32 xgis4g42 xgis4g52 xgis4g1  xgis4g11 xgis4g21 xgis4g31 xgis4g41 xgis4g51 xgis4g0 
xgis4g10 xgis4g20 xgis4g30 xgis4g40 xgis4g50 xnpist2  xnpist1  xnpist0  xnpis412 xnpis422
xnpis432 xnpis442 xnpis452 xnpis411 xnpis421 xnpis431 xnpis441 xnpis451 xnpis410 xnpis420
xnpis430 xnpis440 xnpis450 xgrntn2  xgrnton2 xgrntwf2 xgrntof2 xgrntun2 xgrntt2  xgrnta2 
xgrntn1  xgrnton1 xgrntwf1 xgrntof1 xgrntun1 xgrntt1  xgrnta1  xgrntn0  xgrnton0 xgrntwf0
xgrntof0 xgrntun0 xgrntt0  xgrnta0  xgrn4n2  xgrn4on2 xgrn4wf2 xgrn4of2 xgrn4un2 xgrn4t2 
xgrn4a2  xgrn4n12 xgrn4t12 xgrn4a12 xgrn4n22 xgrn4t22 xgrn4a22 xgrn4n32 xgrn4t32 xgrn4a32
xgrn4n42 xgrn4t42 xgrn4a42 xgrn4n52 xgrn4t52 xgrn4a52 xgrn4n1  xgrn4on1 xgrn4wf1 xgrn4of1
xgrn4un1 xgrn4t1  xgrn4a1  xgrn4n11 xgrn4t11 xgrn4a11 xgrn4n21 xgrn4t21 xgrn4a21 xgrn4n31
xgrn4t31 xgrn4a31 xgrn4n41 xgrn4t41 xgrn4a41 xgrn4n51 xgrn4t51 xgrn4a51 xgrn4n0  xgrn4on0
xgrn4wf0 xgrn4of0 xgrn4un0 xgrn4t0  xgrn4a0  xgrn4n10 xgrn4t10 xgrn4a10 xgrn4n20 xgrn4t20
xgrn4a20 xgrn4n30 xgrn4t30 xgrn4a30 xgrn4n40 xgrn4t40 xgrn4a40 xgrn4n50 xgrn4t50 xgrn4a50
xgrn4g2  xgrn4g12 xgrn4g22 xgrn4g32 xgrn4g42 xgrn4g52 xgrn4g1  xgrn4g11 xgrn4g21 xgrn4g31
xgrn4g41 xgrn4g51 xgrn4g0  xgrn4g10 xgrn4g20 xgrn4g30 xgrn4g40 xgrn4g50 xnpgrn2  xnpgrn1 
xnpgrn0  xnpt412  xnpt422  xnpt432  xnpt442  xnpt452  xnpt411  xnpt421  xnpt431  xnpt441 
xnpt451  xnpt410  xnpt420  xnpt430  xnpt440  xnpt450   / missing;

Proc Summary print n sum mean min max;
var
scugrad  scugffn  scugffp  scfa2    scfa1n   scfa1p   scfa11n  scfa11p  scfa12n 
scfa12p  scfa13n  scfa13p  scfa14n  scfa14p  scfy2    scfy1n   scfy1p   uagrntn  uagrntp 
uagrntt  uagrnta  upgrntn  upgrntp  upgrntt  upgrnta  ufloann  ufloanp  ufloant  ufloana 
anyaidn  anyaidp  aidfsin  aidfsip  agrnt_n  agrnt_p  agrnt_t  agrnt_a  fgrnt_n  fgrnt_p 
fgrnt_t  fgrnt_a  pgrnt_n  pgrnt_p  pgrnt_t  pgrnt_a  ofgrt_n  ofgrt_p  ofgrt_t  ofgrt_a 
sgrnt_n  sgrnt_p  sgrnt_t  sgrnt_a  igrnt_n  igrnt_p  igrnt_t  igrnt_a  loan_n   loan_p  
loan_t   loan_a   floan_n  floan_p  floan_t  floan_a  oloan_n  oloan_p  oloan_t  oloan_a 
gistn2   giston2  gistwf2  gistof2  gistun2  gistt2   gista2   gistn1   giston1  gistwf1 
gistof1  gistun1  gistt1   gista1   gistn0   giston0  gistwf0  gistof0  gistun0  gistt0  
gista0   gis4n2   gis4on2  gis4wf2  gis4of2  gis4un2  gis4t2   gis4a2   gis4n12  gis4t12 
gis4a12  gis4n22  gis4t22  gis4a22  gis4n32  gis4t32  gis4a32  gis4n42  gis4t42  gis4a42 
gis4n52  gis4t52  gis4a52  gis4n1   gis4on1  gis4wf1  gis4of1  gis4un1  gis4t1   gis4a1  
gis4n11  gis4t11  gis4a11  gis4n21  gis4t21  gis4a21  gis4n31  gis4t31  gis4a31  gis4n41 
gis4t41  gis4a41  gis4n51  gis4t51  gis4a51  gis4n0   gis4on0  gis4wf0  gis4of0  gis4un0 
gis4t0   gis4a0   gis4n10  gis4t10  gis4a10  gis4n20  gis4t20  gis4a20  gis4n30  gis4t30 
gis4a30  gis4n40  gis4t40  gis4a40  gis4n50  gis4t50  gis4a50  gis4g2   gis4g12  gis4g22 
gis4g32  gis4g42  gis4g52  gis4g1   gis4g11  gis4g21  gis4g31  gis4g41  gis4g51  gis4g0  
gis4g10  gis4g20  gis4g30  gis4g40  gis4g50  npist2   npist1   npist0   npis412  npis422 
npis432  npis442  npis452  npis411  npis421  npis431  npis441  npis451  npis410  npis420 
npis430  npis440  npis450  grntn2   grnton2  grntwf2  grntof2  grntun2  grntt2   grnta2  
grntn1   grnton1  grntwf1  grntof1  grntun1  grntt1   grnta1   grntn0   grnton0  grntwf0 
grntof0  grntun0  grntt0   grnta0   grn4n2   grn4on2  grn4wf2  grn4of2  grn4un2  grn4t2  
grn4a2   grn4n12  grn4t12  grn4a12  grn4n22  grn4t22  grn4a22  grn4n32  grn4t32  grn4a32 
grn4n42  grn4t42  grn4a42  grn4n52  grn4t52  grn4a52  grn4n1   grn4on1  grn4wf1  grn4of1 
grn4un1  grn4t1   grn4a1   grn4n11  grn4t11  grn4a11  grn4n21  grn4t21  grn4a21  grn4n31 
grn4t31  grn4a31  grn4n41  grn4t41  grn4a41  grn4n51  grn4t51  grn4a51  grn4n0   grn4on0 
grn4wf0  grn4of0  grn4un0  grn4t0   grn4a0   grn4n10  grn4t10  grn4a10  grn4n20  grn4t20 
grn4a20  grn4n30  grn4t30  grn4a30  grn4n40  grn4t40  grn4a40  grn4n50  grn4t50  grn4a50 
grn4g2   grn4g12  grn4g22  grn4g32  grn4g42  grn4g52  grn4g1   grn4g11  grn4g21  grn4g31 
grn4g41  grn4g51  grn4g0   grn4g10  grn4g20  grn4g30  grn4g40  grn4g50  npgrn2   npgrn1  
npgrn0   npt412   npt422   npt432   npt442   npt452   npt411   npt421   npt431   npt441  
npt451   npt410   npt420   npt430   npt440   npt450   ;
run;

  
  