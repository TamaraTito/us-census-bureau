%let path=/Data1/IPEDSData/DataFiles/FallEnr-EF;
%let file=ef2021a_dist;
%let libraryName=IPEDEF;
libname &libraryName "&path";

Proc Format;
value efdelev   
1='All students total' 
2='Undergraduate total' 
3='Undergraduate, degree/certificate-seeking total' 
11='Undergraduate, non-degree/certificate-seeking' 
12='Graduate';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

data &libraryName..&file;
infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
informat
unitid   6. 
efdelev  3. 
xefdetot $1.
efdetot  6. 
xefdeexc $1.
efdeexc  6. 
xefdesom $1.
efdesom  6. 
xefdenon $1.
efdenon  6. 
xefdeex1 $1.
efdeex1  6. 
xefdeex2 $1.
efdeex2  6. 
xefdeex3 $1.
efdeex3  6. 
xefdeex4 $1.
efdeex4  6. 
xefdeex5 $1.
efdeex5  6.;

input
unitid   
efdelev  
xefdetot $
efdetot  
xefdeexc $
efdeexc  
xefdesom $
efdesom  
xefdenon $
efdenon  
xefdeex1 $
efdeex1  
xefdeex2 $
efdeex2  
xefdeex3 $
efdeex3  
xefdeex4 $
efdeex4  
xefdeex5 $
efdeex5 ;

label
unitid  ='Unique identification number of the institution' 
efdelev ='Level of student' 
xefdetot='Imputation field for efdetot - All students enrolled'
efdetot ='All students enrolled' 
xefdeexc='Imputation field for efdeexc - Students enrolled exclusively in distance education courses'
efdeexc ='Students enrolled exclusively in distance education courses' 
xefdesom='Imputation field for efdesom - Students enrolled in some but not all distance education courses'
efdesom ='Students enrolled in some but not all distance education courses' 
xefdenon='Imputation field for efdenon - Student not enrolled in any distance education courses'
efdenon ='Student not enrolled in any distance education courses' 
xefdeex1='Imputation field for efdeex1 - Students enrolled exclusively in distance education courses and are located in same state/jurisdiction as institution'
efdeex1 ='Students enrolled exclusively in distance education courses and are located in same state/jurisdiction as institution' 
xefdeex2='Imputation field for efdeex2 - Students enrolled exclusively in distance education courses and are located in U.S., not in same state/jurisdiction as institution'
efdeex2 ='Students enrolled exclusively in distance education courses and are located in U.S., not in same state/jurisdiction as institution' 
xefdeex3='Imputation field for efdeex3 - Students enrolled exclusively in distance education courses and are located in U.S., state/jurisdiction unknown'
efdeex3 ='Students enrolled exclusively in distance education courses and are located in U.S., state/jurisdiction unknown' 
xefdeex4='Imputation field for efdeex4 - Students enrolled exclusively in distance education courses and are located outside U.S.'
efdeex4 ='Students enrolled exclusively in distance education courses and are located outside U.S.' 
xefdeex5='Imputation field for efdeex5 - Students enrolled exclusively in distance education courses and location of student unknown/not reported'
efdeex5 ='Students enrolled exclusively in distance education courses and location of student unknown/not reported';
format  xefdetot-character-xefdeex5 $ximpflg.
efdelev  efdelev;
run;


Proc Freq;
Tables
efdelev  xefdetot xefdeexc xefdesom xefdenon xefdeex1 xefdeex2 xefdeex3 xefdeex4
xefdeex5  / missing;
format xefdetot-character-xefdeex5 $ximpflg.
efdelev  efdelev.
;

Proc Summary print n sum mean min max;
var
efdetot  efdeexc  efdesom  efdenon  efdeex1  efdeex2  efdeex3  efdeex4 
efdeex5  ;
run;


