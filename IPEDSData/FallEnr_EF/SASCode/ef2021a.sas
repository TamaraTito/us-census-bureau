*** Created: September 14, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\ef2021a.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
efalevel 2. 
line     2. 
section  1. 
lstudy   1. 
xeftotlt $1.
eftotlt  6. 
xeftotlm $1.
eftotlm  6. 
xeftotlw $1.
eftotlw  6. 
xefaiant $1.
efaiant  6. 
xefaianm $1.
efaianm  6. 
xefaianw $1.
efaianw  6. 
xefasiat $1.
efasiat  6. 
xefasiam $1.
efasiam  6. 
xefasiaw $1.
efasiaw  6. 
xefbkaat $1.
efbkaat  6. 
xefbkaam $1.
efbkaam  6. 
xefbkaaw $1.
efbkaaw  6. 
xefhispt $1.
efhispt  6. 
xefhispm $1.
efhispm  6. 
xefhispw $1.
efhispw  6. 
xefnhpit $1.
efnhpit  6. 
xefnhpim $1.
efnhpim  6. 
xefnhpiw $1.
efnhpiw  6. 
xefwhitt $1.
efwhitt  6. 
xefwhitm $1.
efwhitm  6. 
xefwhitw $1.
efwhitw  6. 
xef2mort $1.
ef2mort  6. 
xef2morm $1.
ef2morm  6. 
xef2morw $1.
ef2morw  6. 
xefunknt $1.
efunknt  6. 
xefunknm $1.
efunknm  6. 
xefunknw $1.
efunknw  6. 
xefnralt $1.
efnralt  6. 
xefnralm $1.
efnralm  6. 
xefnralw $1.
efnralw  6.;

input
unitid   
efalevel 
line     
section  
lstudy   
xeftotlt $
eftotlt  
xeftotlm $
eftotlm  
xeftotlw $
eftotlw  
xefaiant $
efaiant  
xefaianm $
efaianm  
xefaianw $
efaianw  
xefasiat $
efasiat  
xefasiam $
efasiam  
xefasiaw $
efasiaw  
xefbkaat $
efbkaat  
xefbkaam $
efbkaam  
xefbkaaw $
efbkaaw  
xefhispt $
efhispt  
xefhispm $
efhispm  
xefhispw $
efhispw  
xefnhpit $
efnhpit  
xefnhpim $
efnhpim  
xefnhpiw $
efnhpiw  
xefwhitt $
efwhitt  
xefwhitm $
efwhitm  
xefwhitw $
efwhitw  
xef2mort $
ef2mort  
xef2morm $
ef2morm  
xef2morw $
ef2morw  
xefunknt $
efunknt  
xefunknm $
efunknm  
xefunknw $
efunknw  
xefnralt $
efnralt  
xefnralm $
efnralm  
xefnralw $
efnralw ;

label
unitid  ='Unique identification number of the institution' 
efalevel='Level of student' 
line    ='Level of student (original line number on survey form)' 
section ='Attendance status of student' 
lstudy  ='Level of student' 
xeftotlt='Imputation field for eftotlt - Grand total'
eftotlt ='Grand total' 
xeftotlm='Imputation field for eftotlm - Grand total men'
eftotlm ='Grand total men' 
xeftotlw='Imputation field for eftotlw - Grand total women'
eftotlw ='Grand total women' 
xefaiant='Imputation field for efaiant - American Indian or Alaska Native total'
efaiant ='American Indian or Alaska Native total' 
xefaianm='Imputation field for efaianm - American Indian or Alaska Native men'
efaianm ='American Indian or Alaska Native men' 
xefaianw='Imputation field for efaianw - American Indian or Alaska Native women'
efaianw ='American Indian or Alaska Native women' 
xefasiat='Imputation field for efasiat - Asian total'
efasiat ='Asian total' 
xefasiam='Imputation field for efasiam - Asian men'
efasiam ='Asian men' 
xefasiaw='Imputation field for efasiaw - Asian women'
efasiaw ='Asian women' 
xefbkaat='Imputation field for efbkaat - Black or African American total'
efbkaat ='Black or African American total' 
xefbkaam='Imputation field for efbkaam - Black or African American men'
efbkaam ='Black or African American men' 
xefbkaaw='Imputation field for efbkaaw - Black or African American women'
efbkaaw ='Black or African American women' 
xefhispt='Imputation field for efhispt - Hispanic total'
efhispt ='Hispanic total' 
xefhispm='Imputation field for efhispm - Hispanic men'
efhispm ='Hispanic men' 
xefhispw='Imputation field for efhispw - Hispanic women'
efhispw ='Hispanic women' 
xefnhpit='Imputation field for efnhpit - Native Hawaiian or Other Pacific Islander total'
efnhpit ='Native Hawaiian or Other Pacific Islander total' 
xefnhpim='Imputation field for efnhpim - Native Hawaiian or Other Pacific Islander men'
efnhpim ='Native Hawaiian or Other Pacific Islander men' 
xefnhpiw='Imputation field for efnhpiw - Native Hawaiian or Other Pacific Islander women'
efnhpiw ='Native Hawaiian or Other Pacific Islander women' 
xefwhitt='Imputation field for efwhitt - White total'
efwhitt ='White total' 
xefwhitm='Imputation field for efwhitm - White men'
efwhitm ='White men' 
xefwhitw='Imputation field for efwhitw - White women'
efwhitw ='White women' 
xef2mort='Imputation field for ef2mort - Two or more races total'
ef2mort ='Two or more races total' 
xef2morm='Imputation field for ef2morm - Two or more races men'
ef2morm ='Two or more races men' 
xef2morw='Imputation field for ef2morw - Two or more races women'
ef2morw ='Two or more races women' 
xefunknt='Imputation field for efunknt - Race/ethnicity unknown total'
efunknt ='Race/ethnicity unknown total' 
xefunknm='Imputation field for efunknm - Race/ethnicity unknown men'
efunknm ='Race/ethnicity unknown men' 
xefunknw='Imputation field for efunknw - Race/ethnicity unknown women'
efunknw ='Race/ethnicity unknown women' 
xefnralt='Imputation field for efnralt - Nonresident alien total'
efnralt ='Nonresident alien total' 
xefnralm='Imputation field for efnralm - Nonresident alien men'
efnralm ='Nonresident alien men' 
xefnralw='Imputation field for efnralw - Nonresident alien women'
efnralw ='Nonresident alien women';
run;

Proc Format;
value efalevel  
1='All students total' 
2='All students, Undergraduate total' 
3='All students, Undergraduate, Degree/certificate-seeking total' 
4='All students, Undergraduate, Degree/certificate-seeking, First-time' 
5='All students, Undergraduate, Other degree/certificate-seeking' 
19='All students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 
20='All students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
11='All students, Undergraduate, Non-degree/certificate-seeking' 
12='All students, Graduate' 
21='Full-time students total' 
22='Full-time students, Undergraduate total' 
23='Full-time students, Undergraduate, Degree/certificate-seeking total' 
24='Full-time students, Undergraduate, Degree/certificate-seeking, First-time' 
25='Full-time students, Undergraduate, Degree/certificate-seeking, Other degree/certificate-seeking' 
39='Full-time students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 
40='Full-time students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
31='Full-time students, Undergraduate, Non-degree/certificate-seeking' 
32='Full-time students, Graduate' 
41='Part-time students total' 
42='Part-time students, Undergraduate total' 
43='Part-time students, Undergraduate, Degree/certificate-seeking total' 
44='Part-time students, Undergraduate, Degree/certificate-seeking, First-time' 
45='Part-time students, Undergraduate, Degree/certificate-seeking, Other degree/certificate-seeking' 
59='Part-time students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 
60='Part-time students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
51='Part-time students, Undergraduate, Non-degree/certificate-seeking' 
52='Part-time students, Graduate';
value line      
1='Full-time, first-time, first-year, degree-seeking undergraduates' 
2='Full-time undergraduate, other degree/certificatee-seeking, transfer-ins' 
3='Full-time  undergraduate, other degree/certificatee-seeking, continuing' 
6='Full-time degree-seeking undergraduates, total' 
7='Full-time nondegree-seeking undergraduates' 
8='Total  full-time undergraduates' 
11='Full-time graduates' 
14='Total full-time students' 
15='Part-time, first-time, first-year, degree-seeking undergraduates' 
16='Part-time undergraduate, other degree/certificatee-seeking, transfer-ins' 
17='Part-time undergraduate, other degree/certificatee-seeking, continuing' 
20='Part-time degree-seeking undergraduates, total' 
21='Part-time nondegree-seeking undergraduates' 
22='Total part-time undergraduates' 
25='Part-time graduates' 
28='Total part-time' 
29='Total enrollment' 
99='Generated record not on original survey';
value section   
1='Full-time' 
2='Part-time' 
3='All students';
value lstudy    
1='Undergraduate' 
3='Graduate' 
4='All students';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
efalevel line     section  lstudy   xeftotlt xeftotlm xeftotlw xefaiant xefaianm
xefaianw xefasiat xefasiam xefasiaw xefbkaat xefbkaam xefbkaaw xefhispt xefhispm xefhispw
xefnhpit xefnhpim xefnhpiw xefwhitt xefwhitm xefwhitw xef2mort xef2morm xef2morw xefunknt
xefunknm xefunknw xefnralt xefnralm xefnralw  / missing;
format xeftotlt-character-xefnralw $ximpflg.
efalevel  efalevel.
line  line.
section  section.
lstudy  lstudy.
;

Proc Summary print n sum mean min max;
var
eftotlt  eftotlm  eftotlw  efaiant  efaianm 
efaianw  efasiat  efasiam  efasiaw  efbkaat  efbkaam  efbkaaw  efhispt  efhispm  efhispw 
efnhpit  efnhpim  efnhpiw  efwhitt  efwhitm  efwhitw  ef2mort  ef2morm  ef2morw  efunknt 
efunknm  efunknw  efnralt  efnralm  efnralw  ;
run;
