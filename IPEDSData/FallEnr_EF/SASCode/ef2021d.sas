*** Created: September 14, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\ef2021d.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xgrcohrt $1.
grcohrt  6. 
xugenter $1.
ugentern 6. 
xpgrcohr $1.
pgrcohrt 3. 
xrrftct  $1.
rrftct   6. 
xrrftex  $1.
rrftex   6. 
xrrftin  $1.
rrftin   6. 
xrrftcta $1.
rrftcta  6. 
xret_nmf $1.
ret_nmf  6. 
xret_pcf $1.
ret_pcf  3. 
xrrptct  $1.
rrptct   6. 
xrrptex  $1.
rrptex   6. 
xrrptin  $1.
rrptin   6. 
xrrptcta $1.
rrptcta  6. 
xret_nmp $1.
ret_nmp  6. 
xret_pcp $1.
ret_pcp  3. 
xstufacr $1.
stufacr  6.;

input
unitid   
xgrcohrt $
grcohrt  
xugenter $
ugentern 
xpgrcohr $
pgrcohrt 
xrrftct  $
rrftct   
xrrftex  $
rrftex   
xrrftin  $
rrftin   
xrrftcta $
rrftcta  
xret_nmf $
ret_nmf  
xret_pcf $
ret_pcf  
xrrptct  $
rrptct   
xrrptex  $
rrptex   
xrrptin  $
rrptin   
xrrptcta $
rrptcta  
xret_nmp $
ret_nmp  
xret_pcp $
ret_pcp  
xstufacr $
stufacr ;

label
unitid  ='Unique identification number of the institution' 
xgrcohrt='Imputation field for grcohrt - Full-time first-time degree/certificate-seeking undergraduate (current year GRS cohort)'
grcohrt ='Full-time first-time degree/certificate-seeking undergraduate (current year GRS cohort)' 
xugenter='Imputation field for ugentern - Total entering students at the undergraduate level, fall 2021'
ugentern='Total entering students at the undergraduate level, fall 2021' 
xpgrcohr='Imputation field for pgrcohrt - Current year GRS cohort as a percent of entering class'
pgrcohrt='Current year GRS cohort as a percent of entering class' 
xrrftct ='Imputation field for rrftct - Full-time fall 2020 cohort'
rrftct  ='Full-time fall 2020 cohort' 
xrrftex ='Imputation field for rrftex - Exclusions from full-time fall 2020 cohort'
rrftex  ='Exclusions from full-time fall 2020 cohort' 
xrrftin ='Imputation field for rrftin - Inclusions to the full-time fall 2020 cohort'
rrftin  ='Inclusions to the full-time fall 2020 cohort' 
xrrftcta='Imputation field for rrftcta - Full-time adjusted fall 2020 cohort'
rrftcta ='Full-time adjusted fall 2020 cohort' 
xret_nmf='Imputation field for ret_nmf - Students from the full-time adjusted fall 2020 cohort enrolled in fall 2021'
ret_nmf ='Students from the full-time adjusted fall 2020 cohort enrolled in fall 2021' 
xret_pcf='Imputation field for ret_pcf - Full-time retention rate, 2021'
ret_pcf ='Full-time retention rate, 2021' 
xrrptct ='Imputation field for rrptct - Part-time fall 2020 cohort'
rrptct  ='Part-time fall 2020 cohort' 
xrrptex ='Imputation field for rrptex - Exclusions from part-time fall 2020 cohort'
rrptex  ='Exclusions from part-time fall 2020 cohort' 
xrrptin ='Imputation field for rrptin - Inclusions to the part-time fall 2020 cohort'
rrptin  ='Inclusions to the part-time fall 2020 cohort' 
xrrptcta='Imputation field for rrptcta - Part-time adjusted fall 2020 cohort'
rrptcta ='Part-time adjusted fall 2020 cohort' 
xret_nmp='Imputation field for ret_nmp - Students from the part-time adjusted fall 2020 cohort enrolled in fall 2021'
ret_nmp ='Students from the part-time adjusted fall 2020 cohort enrolled in fall 2021' 
xret_pcp='Imputation field for ret_pcp - Part-time retention rate, 2021'
ret_pcp ='Part-time retention rate, 2021' 
xstufacr='Imputation field for stufacr - Student-to-faculty ratio'
stufacr ='Student-to-faculty ratio';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xgrcohrt xugenter xpgrcohr xrrftct  xrrftex  xrrftin  xrrftcta xret_nmf xret_pcf
xrrptct  xrrptex  xrrptin  xrrptcta xret_nmp xret_pcp xstufacr  / missing;
format xgrcohrt-character-xstufacr $ximpflg.;

Proc Summary print n sum mean min max;
var
grcohrt  ugentern pgrcohrt rrftct   rrftex   rrftin   rrftcta  ret_nmf  ret_pcf 
rrptct   rrptex   rrptin   rrptcta  ret_nmp  ret_pcp  stufacr  ;
run;
