%let path=/Data1/IPEDSData/DataFiles/Libraries-AL;
%let file=al2021;
%let libraryName=IPEDADM;
libname &libraryName "&path/SASData";

Proc Format library=&libraryName;
value lexp100k  
1='Greater than or equal to $100,000' 
2='Less than $100,000';
value lcolelyn  
1='Yes' 
2='No';
value lilldyn   
1='Yes' 
2='No';
value lilsyn    
1='Yes' 
2='No';
value lfrngbyn  
1='Yes' 
2='No' 
-2='Not applicable';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';
run;
options fmtsearch=(&libraryName);

Data &libraryName..&file;;
	infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
lexp100k 2. 
lcolelyn 2. 
xlpbooks $1.
lpbooks  8. 
xlebooks $1.
lebooks  8. 
xledatab $1.
ledatab  8. 
xlpmedia $1.
lpmedia  8. 
xlemedia $1.
lemedia  8. 
xlpseria $1.
lpseria  8. 
xleseria $1.
leseria  8. 
xlpcollc $1.
lpcllct  8. 
xlecollc $1.
lecllct  8. 
xltcllct $1.
ltcllct  8. 
xlpcrclt $1.
lpcrclt  8. 
xlecrclt $1.
lecrclt  8. 
xltcrclt $1.
ltcrclt  8. 
lilldyn  2. 
xlilldpr $1.
lilldpr  8. 
xlilldrc $1.
lilldrc  8. 
lilsyn   2. 
xlstotal $1.
lstotal  5. 
xlslibrn $1.
lslibrn  5. 
xlsoprof $1.
lsoprof  5. 
xlsopaid $1.
lsopaid  5. 
xlsstast $1.
lsstast  5. 
xlbranch $1.
lbranch  3. 
xsalwag  $1.
lsalwag  12. 
lfrngbyn 2. 
xlfrngbn $1.
lfrngbn  12. 
xlexmsbb $1.
lexmsbb  12. 
xlexmscs $1.
lexmscs  12. 
xlexmsot $1.
lexmsot  12. 
xlexmstl $1.
lexmstl  12. 
xlexomps $1.
lexomps  12. 
xlexomot $1.
lexomot  12. 
xlexomtl $1.
lexomtl  12. 
xlexptot $1.
lexptot  12. 
lswmsom  12.;

input
unitid   
lexp100k 
lcolelyn 
xlpbooks $
lpbooks  
xlebooks $
lebooks  
xledatab $
ledatab  
xlpmedia $
lpmedia  
xlemedia $
lemedia  
xlpseria $
lpseria  
xleseria $
leseria  
xlpcollc $
lpcllct  
xlecollc $
lecllct  
xltcllct $
ltcllct  
xlpcrclt $
lpcrclt  
xlecrclt $
lecrclt  
xltcrclt $
ltcrclt  
lilldyn  
xlilldpr $
lilldpr  
xlilldrc $
lilldrc  
lilsyn   
xlstotal $
lstotal  
xlslibrn $
lslibrn  
xlsoprof $
lsoprof  
xlsopaid $
lsopaid  
xlsstast $
lsstast  
xlbranch $
lbranch  
xsalwag  $
lsalwag  
lfrngbyn 
xlfrngbn $
lfrngbn  
xlexmsbb $
lexmsbb  
xlexmscs $
lexmscs  
xlexmsot $
lexmsot  
xlexmstl $
lexmstl  
xlexomps $
lexomps  
xlexomot $
lexomot  
xlexomtl $
lexomtl  
xlexptot $
lexptot  
lswmsom ;

label
unitid  ='Unique identification number of the institution' 
lexp100k='Were annual total library expenses greater than or equal to $100,000' 
lcolelyn='Is the Library collection entirely electronic' 
xlpbooks='Imputation field for lpbooks - Number of physical books'
lpbooks ='Number of physical books' 
xlebooks='Imputation field for lebooks - Number of digital/electronic books'
lebooks ='Number of digital/electronic books' 
xledatab='Imputation field for ledatab - Number of digital/electronic databases'
ledatab ='Number of digital/electronic databases' 
xlpmedia='Imputation field for lpmedia - Number of physical media'
lpmedia ='Number of physical media' 
xlemedia='Imputation field for lemedia - Number of digital/electronic media'
lemedia ='Number of digital/electronic media' 
xlpseria='Imputation field for lpseria - Number of physical serials'
lpseria ='Number of physical serials' 
xleseria='Imputation field for leseria - Number of electronic serials'
leseria ='Number of electronic serials' 
xlpcollc='Imputation field for lpcllct - Total physical library collections (books, media and serials)'
lpcllct ='Total physical library collections (books, media and serials)' 
xlecollc='Imputation field for lecllct - Total electronic library collections (books, databases, media and serials)'
lecllct ='Total electronic library collections (books, databases, media and serials)' 
xltcllct='Imputation field for ltcllct - Total library collections (physical and electronic)'
ltcllct ='Total library collections (physical and electronic)' 
xlpcrclt='Imputation field for lpcrclt - Total physical library circulations (books and media)'
lpcrclt ='Total physical library circulations (books and media)' 
xlecrclt='Imputation field for lecrclt - Total digital/electronic circulations (books and media)'
lecrclt ='Total digital/electronic circulations (books and media)' 
xltcrclt='Imputation field for ltcrclt - Total library circulations (physical and digital/electronic)'
ltcrclt ='Total library circulations (physical and digital/electronic)' 
lilldyn ='Does institution have interlibrary loan services ?' 
xlilldpr='Imputation field for lilldpr - Total interlibrary loans and documents provided to other libraries'
lilldpr ='Total interlibrary loans and documents provided to other libraries' 
xlilldrc='Imputation field for lilldrc - Total interlibrary loans and documents received'
lilldrc ='Total interlibrary loans and documents received' 
lilsyn  ='Does  institution have Library Staff?' 
xlstotal='Imputation field for lstotal - Total library FTE staff'
lstotal ='Total library FTE staff' 
xlslibrn='Imputation field for lslibrn - Librarians FTE staff'
lslibrn ='Librarians FTE staff' 
xlsoprof='Imputation field for lsoprof - Other professional FTE staff'
lsoprof ='Other professional FTE staff' 
xlsopaid='Imputation field for lsopaid - All other paid FTE staff (Except Student Assistants)'
lsopaid ='All other paid FTE staff (Except Student Assistants)' 
xlsstast='Imputation field for lsstast - Student assistants FTE'
lsstast ='Student assistants FTE' 
xlbranch='Imputation field for lbranch - Number of branches and independent libraries'
lbranch ='Number of branches and independent libraries' 
xsalwag ='Imputation field for lsalwag - Total salaries and wages from the library budget'
lsalwag ='Total salaries and wages from the library budget' 
lfrngbyn='Are staff fringe benefits paid out of the library budget' 
xlfrngbn='Imputation field for lfrngbn - Total fringe benefits'
lfrngbn ='Total fringe benefits' 
xlexmsbb='Imputation field for lexmsbb - One-time purchases of books, serial backfiles, and other materials'
lexmsbb ='One-time purchases of books, serial backfiles, and other materials' 
xlexmscs='Imputation field for lexmscs - Ongoing commitments to subscriptions'
lexmscs ='Ongoing commitments to subscriptions' 
xlexmsot='Imputation field for lexmsot - Other materials/services expenditures'
lexmsot ='Other materials/services expenditures' 
xlexmstl='Imputation field for lexmstl - Total materials/services expenditures'
lexmstl ='Total materials/services expenditures' 
xlexomps='Imputation field for lexomps - Preservation services'
lexomps ='Preservation services' 
xlexomot='Imputation field for lexomot - Other operation and maintenance expenditures'
lexomot ='Other operation and maintenance expenditures' 
xlexomtl='Imputation field for lexomtl - Total operations and maintenance expenditures'
lexomtl ='Total operations and maintenance expenditures' 
xlexptot='Imputation field for lexptot - Total expenditures (salaries/wages, benefits, materials/services, and operations/maintenance)'
lexptot ='Total expenditures (salaries/wages, benefits, materials/services, and operations/maintenance)' 
lswmsom ='Salaries/wages, materials/services, and operations/maintenance';
format xlpbooks-character-xlexptot $ximpflg.
lexp100k  lexp100k.
lcolelyn  lcolelyn.
lilldyn  lilldyn.
lilsyn  lilsyn.
lfrngbyn  lfrngbyn.;
run;


Proc Freq;
Tables
lexp100k lcolelyn xlpbooks xlebooks xledatab xlpmedia xlemedia xlpseria xleseria
xlpcollc xlecollc xltcllct xlpcrclt xlecrclt xltcrclt lilldyn  xlilldpr xlilldrc lilsyn  
xlstotal xlslibrn xlsoprof xlsopaid xlsstast xlbranch xsalwag  lfrngbyn xlfrngbn xlexmsbb
xlexmscs xlexmsot xlexmstl xlexomps xlexomot xlexomtl xlexptot  / missing;
run;

Proc Summary print n sum mean min max;
var
lpbooks  lebooks  ledatab  lpmedia  lemedia  lpseria  leseria 
lpcllct  lecllct  ltcllct  lpcrclt  lecrclt  ltcrclt  lilldpr  lilldrc 
lstotal  lslibrn  lsoprof  lsopaid  lsstast  lbranch  lsalwag  lfrngbn  lexmsbb 
lexmscs  lexmsot  lexmstl  lexomps  lexomot  lexomtl  lexptot  lswmsom  ;
run;
