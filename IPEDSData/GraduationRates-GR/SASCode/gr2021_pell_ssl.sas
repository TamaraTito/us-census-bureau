*** Created:    August 22, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\gr2021_pell_ssl.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
psgrtype 3. 
xpgrevct $1.
pgrevct  6. 
xpgexclu $1.
pgexclu  6. 
xpgadjct $1.
pgadjct  6. 
xpgcmbac $1.
pgcmbac  6. 
xpgcmoba $1.
pgcmoba  6. 
xpgcmtot $1.
pgcmtot  6. 
xssrevct $1.
ssrevct  6. 
xssexclu $1.
ssexclu  6. 
xssadjct $1.
ssadjct  6. 
xsscmbac $1.
sscmbac  6. 
xsscmoba $1.
sscmoba  6. 
xsscmtot $1.
sscmtot  6. 
xnrrevct $1.
nrrevct  6. 
xnrexclu $1.
nrexclu  6. 
xnradjct $1.
nradjct  6. 
xnrcmbac $1.
nrcmbac  6. 
xnrcmoba $1.
nrcmoba  6. 
xnrcmtot $1.
nrcmtot  6. 
xttrevct $1.
ttrevct  6. 
xttexclu $1.
ttexclu  6. 
xttadjct $1.
ttadjct  6. 
xttcmbac $1.
ttcmbac  6. 
xttcmoba $1.
ttcmoba  6. 
xttcmtot $1.
ttcmtot  6.;

input
unitid   
psgrtype 
xpgrevct $
pgrevct  
xpgexclu $
pgexclu  
xpgadjct $
pgadjct  
xpgcmbac $
pgcmbac  
xpgcmoba $
pgcmoba  
xpgcmtot $
pgcmtot  
xssrevct $
ssrevct  
xssexclu $
ssexclu  
xssadjct $
ssadjct  
xsscmbac $
sscmbac  
xsscmoba $
sscmoba  
xsscmtot $
sscmtot  
xnrrevct $
nrrevct  
xnrexclu $
nrexclu  
xnradjct $
nradjct  
xnrcmbac $
nrcmbac  
xnrcmoba $
nrcmoba  
xnrcmtot $
nrcmtot  
xttrevct $
ttrevct  
xttexclu $
ttexclu  
xttadjct $
ttadjct  
xttcmbac $
ttcmbac  
xttcmoba $
ttcmoba  
xttcmtot $
ttcmtot ;

label
unitid  ='Unique identification number of the institution' 
psgrtype='Cohort type' 
xpgrevct='Imputation field for pgrevct - Pell Grant recipients - revised cohort'
pgrevct ='Pell Grant recipients - revised cohort' 
xpgexclu='Imputation field for pgexclu - Pell Grant recipients - exclusions'
pgexclu ='Pell Grant recipients - exclusions' 
xpgadjct='Imputation field for pgadjct - Pell Grant recipients - adjusted  cohort (revised minus exclusions)'
pgadjct ='Pell Grant recipients - adjusted  cohort (revised minus exclusions)' 
xpgcmbac='Imputation field for pgcmbac - Pell Grant  recipients - number that completed a bachelor^s or equivalent degree within 150% of normal time'
pgcmbac ='Pell Grant  recipients - number that completed a bachelor^s or equivalent degree within 150% of normal time' 
xpgcmoba='Imputation field for pgcmoba - Pell Grant  recipients - number that completed an award of less than 4 academic years or equivalent within 150% of normal time'
pgcmoba ='Pell Grant  recipients - number that completed an award of less than 4 academic years or equivalent within 150% of normal time' 
xpgcmtot='Imputation field for pgcmtot - Pell Grant  recipients - total number that completed an award  within 150% of normal time'
pgcmtot ='Pell Grant  recipients - total number that completed an award  within 150% of normal time' 
xssrevct='Imputation field for ssrevct - Subsidized Stafford Loan recipients not receiving Pell Grants - revised cohort'
ssrevct ='Subsidized Stafford Loan recipients not receiving Pell Grants - revised cohort' 
xssexclu='Imputation field for ssexclu - Subsidized Stafford Loan recipients not receiving Pell Grants - exclusions'
ssexclu ='Subsidized Stafford Loan recipients not receiving Pell Grants - exclusions' 
xssadjct='Imputation field for ssadjct - Subsidized Stafford Loan recipients not receiving Pell Grants - adjusted cohort (revised minus exclusions)'
ssadjct ='Subsidized Stafford Loan recipients not receiving Pell Grants - adjusted cohort (revised minus exclusions)' 
xsscmbac='Imputation field for sscmbac - Subsidized Stafford Loan recipients not receiving Pell Grants - number completed a bachelor^s or equivalent within 150% of normal time'
sscmbac ='Subsidized Stafford Loan recipients not receiving Pell Grants - number completed a bachelor^s or equivalent within 150% of normal time' 
xsscmoba='Imputation field for sscmoba - Subsidized Stafford Loan recipients not receiving Pell Grants - number completed an award of less than 4 academic years within 150% of normal time'
sscmoba ='Subsidized Stafford Loan recipients not receiving Pell Grants - number completed an award of less than 4 academic years within 150% of normal time' 
xsscmtot='Imputation field for sscmtot - Subsidized Stafford Loan recipients not receiving Pell Grants - total number completed an award within 150% of normal time'
sscmtot ='Subsidized Stafford Loan recipients not receiving Pell Grants - total number completed an award within 150% of normal time' 
xnrrevct='Imputation field for nrrevct - Did not receive Pell grant or subsidized Stafford loan - revised cohort'
nrrevct ='Did not receive Pell grant or subsidized Stafford loan - revised cohort' 
xnrexclu='Imputation field for nrexclu - Did not receive Pell grant or subsidized Stafford loan - exclusions'
nrexclu ='Did not receive Pell grant or subsidized Stafford loan - exclusions' 
xnradjct='Imputation field for nradjct - Did not receive Pell grant or subsidized Stafford loan - adjusted  cohort (revised minus exclusions)'
nradjct ='Did not receive Pell grant or subsidized Stafford loan - adjusted  cohort (revised minus exclusions)' 
xnrcmbac='Imputation field for nrcmbac - Did not receive Pell grant or subsidized Stafford loan - number that completed a bachelor^s or equivalent degree within 150% of normal time'
nrcmbac ='Did not receive Pell grant or subsidized Stafford loan - number that completed a bachelor^s or equivalent degree within 150% of normal time' 
xnrcmoba='Imputation field for nrcmoba - Did not receive Pell grant or subsidized Stafford loan - number that completed an award of less than 4 academic years within 150% of normal time'
nrcmoba ='Did not receive Pell grant or subsidized Stafford loan - number that completed an award of less than 4 academic years within 150% of normal time' 
xnrcmtot='Imputation field for nrcmtot - Did not receive Pell grant or subsidized Stafford loan - total number that completed an award  within 150% of normal time'
nrcmtot ='Did not receive Pell grant or subsidized Stafford loan - total number that completed an award  within 150% of normal time' 
xttrevct='Imputation field for ttrevct - Total revised cohort'
ttrevct ='Total revised cohort' 
xttexclu='Imputation field for ttexclu - Total exclusions'
ttexclu ='Total exclusions' 
xttadjct='Imputation field for ttadjct - Total adjusted cohort'
ttadjct ='Total adjusted cohort' 
xttcmbac='Imputation field for ttcmbac - Total number that completed a bachelor^s or equivalent degree within 150% of normal time'
ttcmbac ='Total number that completed a bachelor^s or equivalent degree within 150% of normal time' 
xttcmoba='Imputation field for ttcmoba - Total number that completed an award of less than 4 academic years within 150% of normal time'
ttcmoba ='Total number that completed an award of less than 4 academic years within 150% of normal time' 
xttcmtot='Imputation field for ttcmtot - Total number that completed an award  within 150% of normal time'
ttcmtot ='Total number that completed an award  within 150% of normal time';
run;

Proc Format;
value psgrtype  
1='Total 2015 cohort (Bachelor^s and other degree/certificate seeking) - four-year institutions' 
2='Bachelor^s degree seeking 2015 cohort - four-year institutions' 
3='Other degree/certificate seeking 2015 cohort - four-year institutions ' 
4='Degree/certificate seeking 2018 cohort (less than four-year institutions)';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
psgrtype xpgrevct xpgexclu xpgadjct xpgcmbac xpgcmoba xpgcmtot xssrevct xssexclu
xssadjct xsscmbac xsscmoba xsscmtot xnrrevct xnrexclu xnradjct xnrcmbac xnrcmoba xnrcmtot
xttrevct xttexclu xttadjct xttcmbac xttcmoba xttcmtot  / missing;
format xpgrevct-character-xttcmtot $ximpflg.
psgrtype  psgrtype.
;

Proc Summary print n sum mean min max;
var
pgrevct  pgexclu  pgadjct  pgcmbac  pgcmoba  pgcmtot  ssrevct  ssexclu 
ssadjct  sscmbac  sscmoba  sscmtot  nrrevct  nrexclu  nradjct  nrcmbac  nrcmoba  nrcmtot 
ttrevct  ttexclu  ttadjct  ttcmbac  ttcmoba  ttcmtot  ;
run;
