*** Created:    August 22, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\gr2021_l2.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xline_10 $1.
line_10  6. 
xline_45 $1.
line_45  6. 
xline_50 $1.
line_50  6. 
xline_55 $1.
line_55  6. 
xline_11 $1.
line_11  6. 
xline_30 $1.
line_30  6. 
xline_51 $1.
line_51  6. 
xline_52 $1.
line_52  6. 
xpglin10 $1.
pglin10  6. 
xpglin45 $1.
pglin45  6. 
xpglin50 $1.
pglin50  6. 
xpglin11 $1.
pglin11  6. 
xsslin10 $1.
sslin10  6. 
xsslin45 $1.
sslin45  6. 
xsslin50 $1.
sslin50  6. 
xsslin11 $1.
sslin11  6. 
xnrlin10 $1.
nrlin10  6. 
xnrlin45 $1.
nrlin45  6. 
xnrlin50 $1.
nrlin50  6. 
xnrlin11 $1.
nrlin11  6.;

input
unitid   
xline_10 $
line_10  
xline_45 $
line_45  
xline_50 $
line_50  
xline_55 $
line_55  
xline_11 $
line_11  
xline_30 $
line_30  
xline_51 $
line_51  
xline_52 $
line_52  
xpglin10 $
pglin10  
xpglin45 $
pglin45  
xpglin50 $
pglin50  
xpglin11 $
pglin11  
xsslin10 $
sslin10  
xsslin45 $
sslin45  
xsslin50 $
sslin50  
xsslin11 $
sslin11  
xnrlin10 $
nrlin10  
xnrlin45 $
nrlin45  
xnrlin50 $
nrlin50  
xnrlin11 $
nrlin11 ;

label
unitid  ='Unique identification number of the institution' 
xline_10='Imputation field for line_10 - Revised Cohort'
line_10 ='Revised Cohort' 
xline_45='Imputation field for line_45 - Exclusions'
line_45 ='Exclusions' 
xline_50='Imputation field for line_50 - Adjusted cohort (revised cohort minus exclusions)'
line_50 ='Adjusted cohort (revised cohort minus exclusions)' 
xline_55='Imputation field for line_55 - Completers within 100% of normal time'
line_55 ='Completers within 100% of normal time' 
xline_11='Imputation field for line_11 - Completers within 150% of normal time'
line_11 ='Completers within 150% of normal time' 
xline_30='Imputation field for line_30 - Transfer-out students'
line_30 ='Transfer-out students' 
xline_51='Imputation field for line_51 - Still enrolled'
line_51 ='Still enrolled' 
xline_52='Imputation field for line_52 - No longer enrolled'
line_52 ='No longer enrolled' 
xpglin10='Imputation field for pglin10 - Pell Grant recipients - revised cohort'
pglin10 ='Pell Grant recipients - revised cohort' 
xpglin45='Imputation field for pglin45 - Pell Grant recipients - exclusions'
pglin45 ='Pell Grant recipients - exclusions' 
xpglin50='Imputation field for pglin50 - Pell Grant recipients - adjusted  cohort (revised minus exclusions)'
pglin50 ='Pell Grant recipients - adjusted  cohort (revised minus exclusions)' 
xpglin11='Imputation field for pglin11 - Pell Grant  recipients - total number that completed an award  within 150% of normal time to completion'
pglin11 ='Pell Grant  recipients - total number that completed an award  within 150% of normal time to completion' 
xsslin10='Imputation field for sslin10 - Subsidized Stafford Loan recipients not receiving Pell Grants - revised cohort'
sslin10 ='Subsidized Stafford Loan recipients not receiving Pell Grants - revised cohort' 
xsslin45='Imputation field for sslin45 - Subsidized Stafford Loan recipients not receiving Pell Grants - exclusions'
sslin45 ='Subsidized Stafford Loan recipients not receiving Pell Grants - exclusions' 
xsslin50='Imputation field for sslin50 - Subsidized Stafford Loan recipients not receiving Pell Grants - adjusted cohort (revised minus exclusions)'
sslin50 ='Subsidized Stafford Loan recipients not receiving Pell Grants - adjusted cohort (revised minus exclusions)' 
xsslin11='Imputation field for sslin11 - Subsidized Stafford Loan recipients not receiving Pell Grants - total number completed an award within 150% of normal time to completion'
sslin11 ='Subsidized Stafford Loan recipients not receiving Pell Grants - total number completed an award within 150% of normal time to completion' 
xnrlin10='Imputation field for nrlin10 - Did not receive Pell grant or subsidized Stafford loan - revised cohort'
nrlin10 ='Did not receive Pell grant or subsidized Stafford loan - revised cohort' 
xnrlin45='Imputation field for nrlin45 - Did not receive Pell grant or subsidized Stafford loan - exclusions'
nrlin45 ='Did not receive Pell grant or subsidized Stafford loan - exclusions' 
xnrlin50='Imputation field for nrlin50 - Did not receive Pell grant or subsidized Stafford loan - adjusted cohort'
nrlin50 ='Did not receive Pell grant or subsidized Stafford loan - adjusted cohort' 
xnrlin11='Imputation field for nrlin11 - Did not receive Pell grant or subsidized Stafford loan -  total number completed an award within 150% of normal time to completion'
nrlin11 ='Did not receive Pell grant or subsidized Stafford loan -  total number completed an award within 150% of normal time to completion';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xline_10 xline_45 xline_50 xline_55 xline_11 xline_30 xline_51 xline_52 xpglin10
xpglin45 xpglin50 xpglin11 xsslin10 xsslin45 xsslin50 xsslin11 xnrlin10 xnrlin45 xnrlin50
xnrlin11  / missing;
format xline_10-character-xnrlin11 $ximpflg.;

Proc Summary print n sum mean min max;
var
line_10  line_45  line_50  line_55  line_11  line_30  line_51  line_52  pglin10 
pglin45  pglin50  pglin11  sslin10  sslin45  sslin50  sslin11  nrlin10  nrlin45  nrlin50 
nrlin11  ;
run;
