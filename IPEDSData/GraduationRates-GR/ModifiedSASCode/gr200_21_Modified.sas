%let path=/Data1/IPEDSData/DataFiles/GraduationRates-GR;
%let file=gr200_21;
%let libraryName=IPEDGR;
libname &libraryName "&path/SASData";

/* /Data1/IPEDSData/DataFiles/GraduationRates-GR/SASCode/gr200_21.csv */

proc format library=&libraryName;
	value eapcat 10000='All staff' 10010='All staff, With faculty status' 
		10020='All staff, Tenured' 10030='All staff, On Tenure Track' 
		10040='All staff, Not on Tenure Track/No Tenure system' 
		10042='All staff, Annual contract' 
		10043='All staff, Less-than-annual contract' 
		10041='All staff, Multi-year and indefinite contract' 
		10044='All staff, multi-year contract' 10045='All staff, indefinite contract' 
		10050='All staff, Without faculty status' 10060='All staff, faculty/tenure status not applicable nondegree-granting institutions' 
		20000='Instructional, research and public service' 
		20010='Instructional, research and public service, With faculty status' 
		20020='Instructional, research and public service, Tenured' 
		20030='Instructional, research and public service, On Tenure Track' 20040='Instructional, research and public service, Not on Tenure Track/No Tenure system' 
		20042='Instructional, research and public service, Annual contract' 
		20043='Instructional, research and public service, Less-than-annual contract' 20041='Instructional, research and public service, Multi-year and indefinite contract' 
		20044='Instructional, research and public service, Multi-year  contract' 
		20045='Instructional, research and public service, Indefinite contract' 
		20050='Instructional, research and public service, Without faculty status' 20060='Instructional, research and public service, Faculty/tenure status not applicable, nondegree-granting institutions' 
		21000='Instructional staff, total' 
		21010='Instructional staff, With faculty status' 
		21020='Instructional staff, Tenured' 
		21030='Instructional staff, On Tenure Track' 
		21040='Instructional staff, Not on Tenure Track/No Tenure system' 
		21042='Instructional staff, Annual contract' 
		21043='Instructional staff, Less-than-annual contract' 
		21041='Instructional staff, Multi-year and indefinite contract' 
		21044='Instructional staff, Multi-year contract' 
		21045='Instructional staff, Indefinite contract' 
		21050='Instructional staff, Without faculty status' 21060='Instructional staff, Faculty/tenure status not applicable, nondegree-granting institutions' 
		21100='Instructional staff, primarily instruction' 
		21110='Instructional staff, primarily instruction, With faculty status' 
		21120='Instructional staff, primarily instruction, Tenured' 
		21130='Instructional staff, primarily instruction, On Tenure Track' 21140='Instructional staff, primarily instruction, Not on Tenure Track/No Tenure system' 
		21142='Instructional staff, primarily instruction, Annual contract' 
		21143='Instructional staff, primarily instruction, Less-than-annual contract' 21141='Instructional staff, primarily instruction, Multi-year and indefinite contract' 
		21144='Instructional staff, primarily instruction, Multi-year contract' 
		21145='Instructional staff, primarily instruction, Indefinite contract' 
		21150='Instructional staff, primarily instruction, Without faculty status' 21160='Instructional staff, primarily instruction, Faculty/tenure status not applicable, nondegree-granting institutions' 
		21200='Instructional staff, primarily instruction, exclusively credit' 21210='Instructional staff, primarily instruction, exclusively credit With faculty status' 
		21220='Instructional staff, primarily instruction, exclusively credit Tenured' 21230='Instructional staff, primarily instruction, exclusively credit On Tenure Track' 21240='Instructional staff, primarily instruction, exclusively credit Not on Tenure Track/No Tenure system' 21242='Instructional staff, primarily instruction, exclusively credit Annual contract' 21243='Instructional staff, primarily instruction, exclusively credit Less-than-annual contract' 21241='Instructional staff, primarily instruction, exclusively credit Multi-year and indefinite contract' 21244='Instructional staff, primarily instruction, exclusively credit Multi-year contract' 21245='Instructional staff, primarily instruction, exclusively credit Indefinite contract' 21250='Instructional staff, primarily instruction, exclusively credit Without faculty status' 21260='Instructional staff, primarily instruction, exclusively credit, Faculty/tenure status not applicable, nondegree-granting institutions' 
		21300='Instructional staff, primarily instruction, exclusively not-for-credit' 21310='Instructional staff, primarily instruction, exclusively not-for-credit With faculty status' 21320='Instructional staff, primarily instruction, exclusively not-for-credit Tenured' 21330='Instructional staff, primarily instruction, exclusively not-for-credit On Tenure Track' 21340='Instructional staff, primarily instruction, exclusively not-for-credit Not on Tenure Track/No Tenure system' 21342='Instructional staff, primarily instruction, exclusively not-for-credit Annual contract' 21343='Instructional staff, primarily instruction, exclusively not-for-credit Less-than-annual contract' 21341='Instructional staff, primarily instruction, exclusively not-for-credit Multi-year and indefinite contract' 21344='Instructional staff, primarily instruction, exclusively not-for-credit Multi-year contract' 21345='Instructional staff, primarily instruction, exclusively not-for-credit Indefinite contract' 21350='Instructional staff, primarily instruction, exclusively not-for-credit Without faculty status' 21400='Instructional staff, primarily instruction, combined credit/not-for-credit' 21410='Instructional staff, primarily instruction, combined credit/not-for-credit With faculty status' 21420='Instructional staff, primarily instruction, combined credit/not-for-credit Tenured' 21430='Instructional staff, primarily instruction, combined credit/not-for-credit On Tenure Track' 21440='Instructional staff, primarily instruction, combined credit/not-for-credit Not on Tenure Track/No Tenure system' 21442='Instructional staff, primarily instruction, combined credit/not-for-credit Annual contract' 21443='Instructional staff, primarily instruction, combined credit/not-for-credit Less-than-annual contract' 21441='Instructional staff, primarily instruction, combined credit/not-for-credit Multi-year and indefinite contract' 21444='Instructional staff, primarily instruction, combined credit/not-for-credit Multi-year contract' 21445='Instructional staff, primarily instruction, combined credit/not-for-credit Indefinite contract' 21450='Instructional staff, primarily instruction, combined credit/not-for-credit Without faculty status' 
		21500='Instructional staff - Instruction/research/public service' 21510='Instructional staff - Instruction/research/public service With faculty status' 
		21520='Instructional staff - Instruction/research/public service Tenured' 21530='Instructional staff - Instruction/research/public service On Tenure Track' 21540='Instructional staff - Instruction/research/public service Not on Tenure Track/No Tenure system' 21542='Instructional staff - Instruction/research/public service Annual contract' 21543='Instructional staff - Instruction/research/public service Less-than-annual contract' 21541='Instructional staff - Instruction/research/public service Multi-year and indefinite contract' 21544='Instructional staff - Instruction/research/public service Multi-year contract' 21545='Instructional staff - Instruction/research/public service Indefinite contract' 21550='Instructional staff - Instruction/research/public service Without faculty status' 
		22000='Research' 22010='Research With faculty status' 
		22020='Research Tenured' 22030='Research On Tenure Track' 
		22040='Research Not on Tenure Track/No Tenure system' 
		22042='Research Annual contract' 22043='Research Less-than-annual contract' 
		22041='Research Multi-year and indefinite contract' 
		22044='Research Multi-year contract' 22045='Research Indefinite contract' 
		22050='Research Without faculty status' 22060='Research Faculty/tenure status not applicable nondegree-granting institutions' 
		23000='Public service' 23010='Public service With faculty status' 
		23020='Public service Tenured' 23030='Public service On Tenure Track' 
		23040='Public service Not on Tenure Track/No Tenure system' 
		23042='Public service Annual contract' 
		23043='Public service Less-than-annual contract' 
		23041='Public service Multi-year and indefinite contract' 
		23044='Public service Multi-year contract' 
		23045='Public service Indefinite contract' 
		23050='Public service Without faculty status' 23060='Public service Faculty/tenure status not applicable nondegree-granting institutions' 25000='Library/Archivists,Curators, and Museum/Student and Acacemic Affairs and Other Education Services' 25010='Library/Archivists,Curators, and Museum/Student and Acacemic Affairs and Other Education ServicesWith faculty status' 25020='Library/Archivists,Curators, and Museum/Student and Acacemic Affairs and Other Education ServicesTenured' 25030='Library/Archivists,Curators, and Museum/Student and Acacemic Affairs and Other Education Services On Tenure Track' 25040='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services Not on Tenure Track/No Tenure system' 25042='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Annual contract' 25043='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Less-than-annual contract' 25041='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Multi-year and indefinite contract' 25044='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Multi-year contract' 25045='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Indefinite contract' 25050='Librarians, Curators, Archivists and other teaching Instructional support Without faculty status' 25060='Library/Archivists,Curators, and Museum/ Sudent and Acacemic Affairs and Other Education Services  Faculty/tenure status not applicable nondegree-gran' 26000='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians' 26010='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians With faculty status' 26020='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Tenured' 26030='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians On Tenure Track' 26040='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Not on Tenure Track/No Tenure system' 26042='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Annual contract' 26043='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Less-than-annual contract' 26041='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Multi-year and indefinite contract' 26044='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Multi-year contract' 26045='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Indefinite contract' 26050='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians Without faculty status' 26060='Librarians/Library Technicians/Archivists, Curators, and Museum Technicians not applicable, nondegree-granting institutions' 
		26100='Archivists, Curators, and Museum Technicians' 
		26110='Archivists, Curators, and Museum Technicians With faculty status' 
		26120='Archivists, Curators, and Museum Technicians Tenured' 
		26130='Archivists, Curators, and Museum Technicians On Tenure Track' 26140='Archivists, Curators, and Museum Technicians Not on Tenure Track/No Tenure system' 
		26142='Archivists, Curators, and Museum Technicians Annual contract' 
		26143='Archivists, Curators, and Museum Technicians Less-than-annual contract' 26141='Archivists, Curators, and Museum Technicians Multi-year and indefinite contract' 
		26144='Archivists, Curators, and Museum Technicians Multi-year contract' 
		26145='Archivists, Curators, and Museum Technicians Indefinite contract' 
		26150='Archivists, Curators, and Museum Technicians Without faculty status' 
		26200='Librarians' 26210='Librarians With faculty status' 
		26220='Librarians Tenured' 26230='Librarians On Tenure Track' 
		26240='Librarians Not on Tenure Track/No Tenure system' 
		26242='Librarians Annual contract' 
		26243='Librarians Less-than-annual contract' 
		26241='Librarians Multi-year and indefinite contract' 
		26244='Librarians Multi-year contract' 26245='Librarians Indefinite contract' 
		26250='Librarians Without faculty status' 26300='Library Technicians' 
		26310='Library Technicians With faculty status' 
		26320='Library Technicians Tenured' 
		26330='Library Technicians On Tenure Track' 
		26340='Library Technicians Not on Tenure Track/No Tenure system' 
		26342='Library Technicians Annual contract' 
		26343='Library Technicians Less-than-annual contract' 
		26341='Library Technicians Multi-year and indefinite contract' 
		26344='Library Technicians Multi-year contract' 
		26345='Library Technicians Indefinite contract' 
		26350='Library Technicians Without faculty status' 26400='Detailed library occupations not available for non-degree-granting institutions' 26460='Detailed library occupations and faculty/tenure status not applicable, nondegree-granting institutions' 
		27000='Student and Academic Affairs and Other Education Services' 27010='Student and Academic Affairs and Other Education Services  With faculty status' 
		27020='Student and Academic Affairs and Other Education Services Tenured' 27030='Student and Academic Affairs and Other Education Services On Tenure Track' 27040='Student and Academic Affairs and Other Education Services  Not on Tenure Track/No Tenure system' 27042='Student and Academic Affairs and Other Education Services  Annual contract' 27043='Student and Academic Affairs and Other Education Services  Less-than-annual contract' 27041='Student and Academic Affairs and Other Education Services  Multi-year and indefinite contract' 27044='Student and Academic Affairs and Other Education Services  Multi-year contract' 27045='Student and Academic Affairs and Other Education Services  Indefinite contract' 27050='Student and Academic Affairs and Other Education Services  Without faculty status' 27060='Student and Academic Affairs and Other Education Services  Faculty/tenure status not applicable' 
		30000='Management' 30010='Management With faculty status' 
		30020='Management Tenured' 30030='Management On Tenure Track' 
		30040='Management Not on Tenure Track/No Tenure system' 
		30042='Management Annual contract' 
		30043='Management Less-than-annual contract' 
		30041='Management Multi-year and indefinite contract' 
		30044='Management Multi-year contract' 30045='Management Indefinite contract' 
		30050='Management Without faculty status' 30060='Management Faculty/tenure status not applicble non degree-granting institutions' 
		31000='Business and Financial Operations' 
		31010='Business and Financial Operations  With faculty status' 
		31020='Business and Financial Operations  Tenured' 
		31030='Business and Financial Operations  On Tenure Track' 
		31040='Business and Financial Operations  Not on Tenure Track/No Tenure system' 
		31042='Business and Financial Operations  Annual contract' 
		31043='Business and Financial Operations  Less-than-annual contract' 
		31041='Business and Financial Operations  Multi-year and indefinite contract' 
		31044='Business and Financial Operations  Multi-year contract' 
		31045='Business and Financial Operations  Indefinite contract' 
		31050='Business and Financial Operations  Without faculty status' 31060='Business and Financial Operations Faculty/tenure status not applicble non degree-granting institutions' 
		32000='Computer, Engineering, and Science' 
		32010='Computer, Engineering, and Science With faculty status' 
		32020='Computer, Engineering, and Science Tenured' 
		32030='Computer, Engineering, and Science On Tenure Track' 
		32040='Computer, Engineering, and Science Not on Tenure Track/No Tenure system' 
		32042='Computer, Engineering, and Science Annual contract' 
		32043='Computer, Engineering, and Science Less-than-annual contract' 
		32041='Computer, Engineering, and Science Multi-year and indefinite contract' 
		32044='Computer, Engineering, and Science Multi-year contract' 
		32045='Computer, Engineering, and Science Indefinite contract' 
		32050='Computer, Engineering, and Science Without faculty status' 32060='Computer, Engineering, and Science Faculty/tenure status not applicble non degree-granting institutions' 33000='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media' 33010='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media With faculty status' 33020='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Tenured' 33030='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media On Tenure Track' 33040='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Not on Tenure Track/No Tenure system' 33042='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Annual contract' 33043='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and MediaLess-than-annual contract' 33041='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Multi-year and indefinite contract' 33044='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Multi-year contract' 33045='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Indefinite contract' 33050='Community, Social Service, Legal, Arts, Design, Entertainment, Sports and Media Without faculty status' 33060='Community Service, Legal, Arts, and Media Faculty/tenure status not applicble non degree-granting institutions' 
		34000='Healthcare Practioners and Technical' 
		34010='Healthcare Practioners and Technical With faculty status' 
		34020='Healthcare Practioners and Technical Tenured' 
		34030='Healthcare Practioners and Technical On Tenure Track' 34040='Healthcare Practioners and Technical Not on Tenure Track/No Tenure system' 
		34042='Healthcare Practioners and Technical Annual contract' 
		34043='Healthcare Practioners and Technical Less-than-annual contract' 
		34041='Healthcare Practioners and Technical Multi-year and indefinite contract' 
		34044='Healthcare Practioners and Technical Multi-year contract' 
		34045='Healthcare Practioners and Technical Indefinite contract' 
		34050='Healthcare Practioners and Technical Without faculty status' 34060='Healthcare Practioners and Technical Faculty/tenure status not applicble non degree-granting institutions' 
		35000='Service Occupations' 35060='Service Occupations Faculty/tenure status not applicble non degree-granting institutions' 
		36000='Sales and Related Occupations' 36060='Sales and Related Occupations Faculty/tenure status not applicble non degree-granting institutions' 
		37000='Office and Administrative Support' 37060='Office and Administrative Support Faculty/tenure status not applicble non degree-granting institutions' 
		38000='Natural Resources, Construction, and Maintenance' 38060='Natural Resources, Construction, and Maintenance Faculty/tenure status not applicble non degree-granting institutions' 
		39000='Production, Transportation, and Material Moving' 39060='Production, Transportation, and Material Moving Faculty/tenure status not applicble non degree-granting institutions' 
		40000='Graduate Assistants' 41000='Graduate Assistants Teaching' 
		42000='Graduate Assistants Research' 49000='Graduate Assistants Other';
	value occupcat 100='All staff' 
		200='Instructional, research and public service staff' 
		210='Instructional staff, total' 
		211='Instructional staff, primarily instruction' 
		212='Instructional staff, primarily instruction, exclusively credit' 
		213='Instructional staff, primarily instruction, exclusively not-for-credit' 214='Instructional staff, primarily instruction, combined credit/not-for-credit' 
		215='Instructional staff - Instruction/research/public service' 
		220='Research' 230='Public service' 250='Librarians/Library Technicians/Archivists and Curators, and Museum technicians/Student and Academic Affairs and Other Education Services' 
		260='Librarians, Curators, and Archivists' 
		261='Archivists, Curators, and Museum Technicians' 262='Librarians' 
		263='Library Technicians' 264='Detailed library occupations not available for non-degree-granting institutions' 
		270='Student and Academic Affairs and Other Education Services' 
		300='Management' 310='Business and Financial Operations' 
		320='Computer, Engineering, and Science' 330='Community Social Service, Legal, Arts, Design, Entertainment, Sports and Media' 
		340='Healthcare Practioners and Technical' 350='Service Occupations' 
		360='Sales and Related Occupations' 370='Office and Administrative Support' 
		380='Natural Resources, Construction, and Maintenance' 
		390='Production, Transportation, and Material Moving' 
		400='Graduate Assistants Total' 410='Graduate Assistants Teaching' 
		420='Graduate Assistants Research' 490='Graduate Assistants Other';
	value facstat 0='All staff' 10='With faculty status, total' 
		20='With faculty status, tenured' 30='With faculty status, on tenure track' 
		40='With faculty status not on tenure track/No tenure system, total' 41='With faculty status not on tenure track/No tenure system, multi-year and indefinite contract' 44='With faculty status not on tenure track/No tenure system, multi-year contract' 45='With faculty status not on tenure track/No tenure system, indefinite contract' 
		42='With faculty status not on tenure track/No tenure system, annual contract' 43='With faculty status not on tenure track/No tenure system, less-than-annual contract' 
		50='Without faculty status' 
		60='Faculty/tenure status not applicable, nondegree-granting institutions';
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program n' Z='Implied zero';
run;

options fmtsearch=(&libraryName);

data &libraryName..&file;
	infile "&path/&file..csv" 
		delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
	informat unitid 6. xbarevct $1. barevct 6. xbaexclu $1. baexclu 6. 
		xbaac150 $1. baac150 6. xbanc100 $1. banc100 6. xbagr100 $1. bagr100 6. 
		xbanc150 $1. banc150 6. xbagr150 $1. bagr150 6. xbaaexcl $1. baaexcl 6. 
		xbaac200 $1. baac200 6. xbanc20a $1. banc200a 6. xbastend $1. bastend 6. 
		xbanc200 $1. banc200 6. xbagr200 $1. bagr200 6. xl4revct $1. l4revct 6. 
		xl4exclu $1. l4exclu 6. xl4ac150 $1. l4ac150 6. xl4nc100 $1. l4nc100 6. 
		xl4gr100 $1. l4gr100 6. xl4nc150 $1. l4nc150 6. xl4gr150 $1. l4gr150 6. 
		xl4aexcl $1. l4aexcl 6. xl4ac200 $1. l4ac200 6. xl4nc20a $1. l4nc200a 6. 
		xl4stend $1. l4stend 6. xl4nc200 $1. l4nc200 6. xl4gr200 $1. l4gr200 6.;
	input unitid xbarevct $
		barevct xbaexclu $
		baexclu xbaac150 $
		baac150 xbanc100 $
		banc100 xbagr100 $
		bagr100 xbanc150 $
		banc150 xbagr150 $
		bagr150 xbaaexcl $
		baaexcl xbaac200 $
		baac200 xbanc20a $
		banc200a xbastend $
		bastend xbanc200 $
		banc200 xbagr200 $
		bagr200 xl4revct $
		l4revct xl4exclu $
		l4exclu xl4ac150 $
		l4ac150 xl4nc100 $
		l4nc100 xl4gr100 $
		l4gr100 xl4nc150 $
		l4nc150 xl4gr150 $
		l4gr150 xl4aexcl $
		l4aexcl xl4ac200 $
		l4ac200 xl4nc20a $
		l4nc200a xl4stend $
		l4stend xl4nc200 $
		l4nc200 xl4gr200 $
		l4gr200;
	label unitid='Unique identification number of the institution' xbarevct='Imputation field for barevct - Revised bachelor^s degree-seeking cohort, (cohort year 2013)' 
		barevct='Revised bachelor^s degree-seeking cohort, (cohort year 2013)' xbaexclu='Imputation field for baexclu - Exclusions from bachelor^s degree-seeking cohort within 150% percent of normal time' baexclu='Exclusions from bachelor^s degree-seeking cohort within 150% percent of normal time' xbaac150='Imputation field for baac150 - Adjusted bachelor^s degree-seeking cohort within 150% of normal time' 
		baac150='Adjusted bachelor^s degree-seeking cohort within 150% of normal time' xbanc100='Imputation field for banc100 - Number completed a bachelor^s degree within 100% of normal time (4-years)' banc100='Number completed a bachelor^s degree within 100% of normal time (4-years)' xbagr100='Imputation field for bagr100 - 4-year Graduation rate - bachelor^s degree within 100% of normal time' 
		bagr100='4-year Graduation rate - bachelor^s degree within 100% of normal time' xbanc150='Imputation field for banc150 - Number completed a bachelor^s degree within 150% of normal time (6-years)' banc150='Number completed a bachelor^s degree within 150% of normal time (6-years)' xbagr150='Imputation field for bagr150 - 6-year Graduation rate - bachelor^s degree within 150% of normal time' 
		bagr150='6-year Graduation rate - bachelor^s degree within 150% of normal time' xbaaexcl='Imputation field for baaexcl - Additional exclusions from bachelor^s degree-seeking cohort' 
		baaexcl='Additional exclusions from bachelor^s degree-seeking cohort' xbaac200='Imputation field for baac200 - Adjusted bachelor^s degree-seeking cohort within 200% of normal time' 
		baac200='Adjusted bachelor^s degree-seeking cohort within 200% of normal time' xbanc20a='Imputation field for banc200a - Number completed a bachelor^s degree between 150% and 200% of normal time' banc200a='Number completed a bachelor^s degree between 150% and 200% of normal time' 
		xbastend='Imputation field for bastend - Still enrolled' 
		bastend='Still enrolled' xbanc200='Imputation field for banc200 - Number completed a bachelor^s degree within 200% of normal time (8-years)' banc200='Number completed a bachelor^s degree within 200% of normal time (8-years)' xbagr200='Imputation field for bagr200 - 8-year Graduation rate - bachelor^s degree within 200% of normal time' 
		bagr200='8-year Graduation rate - bachelor^s degree within 200% of normal time' xl4revct='Imputation field for l4revct - Revised degree/certificate-seeking cohort, (cohort year 2017)' 
		l4revct='Revised degree/certificate-seeking cohort, (cohort year 2017)' xl4exclu='Imputation field for l4exclu - Exclusions from degree/certificate-seeking cohort within 150% percent of normal time' l4exclu='Exclusions from degree/certificate-seeking cohort within 150% percent of normal time' xl4ac150='Imputation field for l4ac150 - Adjusted degree/certificate-seeking cohort within 150% of normal time' 
		l4ac150='Adjusted degree/certificate-seeking cohort within 150% of normal time' xl4nc100='Imputation field for l4nc100 - Number completed a degree/certificate within 100% of normal time' 
		l4nc100='Number completed a degree/certificate within 100% of normal time' xl4gr100='Imputation field for l4gr100 - Graduation rate - degree/certificate within 100% of normal time' 
		l4gr100='Graduation rate - degree/certificate within 100% of normal time' xl4nc150='Imputation field for l4nc150 - Number completed a degree/certificate  within 150% of normal time' 
		l4nc150='Number completed a degree/certificate  within 150% of normal time' xl4gr150='Imputation field for l4gr150 - Graduation rate - degree/certificate within 150% of normal time' 
		l4gr150='Graduation rate - degree/certificate within 150% of normal time' xl4aexcl='Imputation field for l4aexcl - Additional exclusions from degree/certificate-seeking cohort' 
		l4aexcl='Additional exclusions from degree/certificate-seeking cohort' xl4ac200='Imputation field for l4ac200 - Adjusted degree/certificate-seeking cohort within 200% of normal time' 
		l4ac200='Adjusted degree/certificate-seeking cohort within 200% of normal time' xl4nc20a='Imputation field for l4nc200a - Number completed a  degree/certificate between 150% and 200% of normal time' l4nc200a='Number completed a  degree/certificate between 150% and 200% of normal time' 
		xl4stend='Imputation field for l4stend - Still enrolled' 
		l4stend='Still enrolled' xl4nc200='Imputation field for l4nc200 - Number completed a degree/certificate within 200% of normal time' 
		l4nc200='Number completed a degree/certificate within 200% of normal time' xl4gr200='Imputation field for l4gr200 - Graduation rate - degree/certificate within 200% of normal time' 
		l4gr200='Graduation rate - degree/certificate within 200% of normal time';
run;

Proc Freq;
	Tables xbarevct xbaexclu xbaac150 xbanc100 xbagr100 xbanc150 xbagr150 xbaaexcl 
		xbaac200 xbanc20a xbastend xbanc200 xbagr200 xl4revct xl4exclu xl4ac150 
		xl4nc100 xl4gr100 xl4nc150 xl4gr150 xl4aexcl xl4ac200 xl4nc20a xl4stend 
		xl4nc200 xl4gr200 / missing;
run;

Proc Summary print n sum mean min max;
	var barevct baexclu baac150 banc100 bagr100 banc150 bagr150 baaexcl baac200 
		banc200a bastend banc200 bagr200 l4revct l4exclu l4ac150 l4nc100 l4gr100 
		l4nc150 l4gr150 l4aexcl l4ac200 l4nc200a l4stend l4nc200 l4gr200;
run;