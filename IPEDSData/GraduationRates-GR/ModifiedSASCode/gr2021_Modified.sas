%let path=/Data1/IPEDSData/DataFiles/GraduationRates-GR;
%let file=gr2021;
%let libraryName=IPEDGR;
libname &libraryName "&path/SASData";

/* Data DCT;
infile '/Data1/IPEDSData/DataFiles/GraduationRates-GR/gr2021.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
*/

proc format library=&libraryName;
  value grtype    
    40='Total exclusions 4-year schools' 
    2='4-year institutions, Adjusted cohort (revised cohort minus exclusions)' 
    3='4-year institutions, Completers within 150% of normal time' 
    4='4-year institutions, Transfer-out students' 
    41='4-year institutions, noncompleters still enrolled' 
    42='4-year institutions, No longer enrolled' 
    6='Bachelor^s or equiv subcohort (4-yr institution)' 
    7='Bachelor^s or equiv subcohort (4-yr institution) exclusions' 
    8='Bachelor^s or equiv subcohort (4-yr institution) adjusted cohort (revised cohort minus exclusions)' 
    9='Bachelor^s or equiv subcohort (4-yr institution) Completers within 150% of normal time total' 
    10='Bachelor^s or equiv subcohort (4-yr institution) Completers of programs of < 2 yrs (150% of normal time)' 
    11='Bachelor^s or equiv subcohort (4-yr institution) Completers of programs of 2 but <4 yrs (150% of normal time)' 
    12='Bachelor^s or equiv subcohort (4-yr institution) Completers of bachelor^s or equiv degrees total (150% of normal time)' 
    13='Bachelor^s or equiv subcohort (4-yr institution) Completers of bachelor^s or equiv degrees in 4 years or less' 
    14='Bachelor^s or equiv subcohort (4-yr institution) Completers of bachelor^s or equiv degrees in 5 years' 
    15='Bachelor^s or equiv subcohort (4-yr institution) Completers of bachelor^s or equiv degrees in 6 years' 
    16='Bachelor^s or equiv subcohort (4-yr institution) Transfer-out students' 
    43='Bachelor^s or equiv subcohort (4-yr institution) noncompleters still enrolled' 
    44='Bachelor^s or equiv subcohort (4-yr institution), No longer enrolled' 
    18='Other degree/certif-seeking subcohort (4-yr institution)' 
    19='Other degree/certificate-seeking subcohort(4-yr institution) exclusions' 
    20='Other degree/certif-seeking subcohort (4-yr institution) Adjusted cohort (revised cohort minus exclusions)' 
    21='Other degree/certif-seeking subcohort (4-yr institution) Completers within 150% of normal time total' 
    22='Other degree/certif-seeking subcohort (4-yr institution) Completers of programs < 2 yrs (150% of normal time)' 
    23='Other degree/certif-seeking subcohort (4-yr institution) Completers of programs of 2 but < 4 yrs (150% of normal time)' 
    24='Other degree/certif-seeking subcohort (4-yr institution) Completers of bachelor^s or equiv degrees (150% of normal time)' 
    25='Other degree/certif-seeking subcohort (4-yr institution) Transfer-out students' 
    45='Other degree/certif-seeking subcohort (4-yr institution) noncompleters still enrolled' 
    46='Other degree/certif-seeking subcohort (4-yr institution) No longer enrolled' 
    27='Degree/certif-seeking students ( 2-yr institution)' 
    28='Degree/certificate-seeking subcohort(2-yr institution) exclusions' 
    29='Degree/certif-seeking students ( 2-yr institution) Adjusted cohort (revised cohort minus exclusions)' 
    30='Degree/certif-seeking students ( 2-yr institution) Completers within 150% of normal time total' 
    31='Degree/certif-seeking students ( 2-yr institution) Completers of programs of < 2 yrs (150% of normal time)' 
    32='Degree/certificate-seeking students ( 2-yr institution) Completers of programs of 2 but < 4 yrs (150% of normal time)' 
    35='Degree/certif-seeking students ( 2-yr institution) Completers within 100% of normal time total' 
    36='Degree/certif-seeking students ( 2-yr institution) Completers of programs of < 2 yrs (100% of normal time)' 
    37='Degree/certificate-seeking students ( 2-yr institution) Completers of programs of 2 but < 4 yrs (100% of normal time)' 
    33='Degree/certif-seeking students ( 2-yr institution) Transfer-out students' 
    47='Degree/certif-seeking students ( 2-yr institution) noncompleters still enrolled' 
    48='Degree/certif-seeking students ( 2-yr institution) No longer enrolled';
  value chrtstat  
    10='Revised cohort' 
    11='Exclusions' 
    12='Adjusted cohort (revised cohort minus exclusions)' 
    13='Completers within 150% of normal time' 
    14='Completers of programs of less than 2 years (150% of normal time)' 
    15='Completers of programs of 2 but less than 4 years (150% of normal time)' 
    16='Completers of bachelor^s or equivalent degrees (150% of normal time)' 
    17='Completers of bachelor^s or equivalent degrees in 4 years or less' 
    18='Completers of bachelor^s or equivalent degrees in 5 years' 
    19='Completers of bachelor^s or equivalent degrees in 6 years' 
    20='Transfer-out students' 
    22='Completers of programs within 100% of normal time total' 
    23='Completers of programs of < 2 yrs within 100% of normal time (not available by race or gender)' 
    24='Completers of programs of 2 but < 4 yrs within 100% of normal time (not available by race or gender)' 
    31='Noncompleters, still enrolled' 
    32='Noncompleters, no longer enrolled';
  value section   
    1='Bachelor^s/ equiv +  other degree/certif-seeking 2015 subcohort (4-yr institution)' 
    2='Bachelor^s or equiv 2015 subcohort (4-yr institution)' 
    3='Other degree/certif-seeking 2015 subcohort (4-yr institution)' 
    4='Degree/certif-seeking students 2018 cohort ( 2-yr )';
  value cohort    
    1='Bachelor^s/ equiv +  other degree/certif-seeking 2015 subcohorts (4-yr institution)' 
    2='Bachelor^s or equiv 2015  subcohort (4-yr institution)' 
    3='Other degree/certif-seeking 2015 subcohort (4-yr institution)' 
    4='Degree/certif-seeking students 2018 cohort ( 2-yr institution)';
  value $line      
    '10'='Revised cohort' 
    '45'='Exclusions' 
    '50'='Adjusted cohort (revised cohort minus exclusions)' 
    '29A'='Completers within 150% of normal time' 
    '11A'='Completers of programs of less than 2 years (150% of normal time)' 
    '12A'='Completers of programs of 2 but less than 4 years (150% of normal time)' 
    '18A'='Completers of bachelor^s or equivalent degrees (150% of normal time)' 
    '19'='Completers of bachelor^s or equivalent degrees in 4 years or less' 
    '20'='Completers of bachelor^s or equivalent degrees in 5 years' 
    '21'='Completers of bachelor^s or equivalent degrees in 6 years' 
    '30'='Transfer-out students' 
    '999'='Generated record not on original survey form' 
    '57'='Completers of programs within 100% of normal time total (not available by race or gender)' 
    '55'='Completers of programs of less than 2 years within 100% of normal time (not available by race or gender)' 
    '56'='Completers of programs of 2 but less than 4 years within 100% of normal time (not available by race or gender)' 
    '22'='Noncompleters, still enrolled' 
    '23'='Noncompleters, no longer enrolled';
  value $ximpflg  
    A='Not applicable' 
    B='Institution left item blank' 
    C='Analyst corrected reported value' 
    D='Do not know' 
    G='Data generated from other data values' 
    H='Value not derived - data not usable' 
    J='Logical imputation' 
    K='Ratio adjustment' 
    L='Imputed using the Group Median procedure' 
    N='Imputed using Nearest Neighbor procedure' 
    P='Imputed using Carry Forward procedure' 
    R='Reported' 
    Y='Specific professional practice program n' 
    Z='Implied zero';
run;

options fmtsearch=(&libraryName);

data &libraryName..&file;
  infile "&path/&file..csv"
    delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
  informat
    unitid   6. 
    grtype   4. 
    chrtstat 2. 
    section  1. 
    cohort   1. 
    line     $3. 
    xgrtotlt $1.
    grtotlt  6. 
    xgrtotlm $1.
    grtotlm  6. 
    xgrtotlw $1.
    grtotlw  6. 
    xgraiant $1.
    graiant  6. 
    xgraianm $1.
    graianm  6. 
    xgraianw $1.
    graianw  6. 
    xgrasiat $1.
    grasiat  6. 
    xgrasiam $1.
    grasiam  6. 
    xgrasiaw $1.
    grasiaw  6. 
    xgrbkaat $1.
    grbkaat  6. 
    xgrbkaam $1.
    grbkaam  6. 
    xgrbkaaw $1.
    grbkaaw  6. 
    xgrhispt $1.
    grhispt  6. 
    xgrhispm $1.
    grhispm  6. 
    xgrhispw $1.
    grhispw  6. 
    xgrnhpit $1.
    grnhpit  6. 
    xgrnhpim $1.
    grnhpim  6. 
    xgrnhpiw $1.
    grnhpiw  6. 
    xgrwhitt $1.
    grwhitt  6. 
    xgrwhitm $1.
    grwhitm  6. 
    xgrwhitw $1.
    grwhitw  6. 
    xgr2mort $1.
    gr2mort  6. 
    xgr2morm $1.
    gr2morm  6. 
    xgr2morw $1.
    gr2morw  6. 
    xgrunknt $1.
    grunknt  6. 
    xgrunknm $1.
    grunknm  6. 
    xgrunknw $1.
    grunknw  6. 
    xgrnralt $1.
    grnralt  6. 
    xgrnralm $1.
    grnralm  6. 
    xgrnralw $1.
    grnralw  6.;
  input
    unitid   
    grtype   
    chrtstat 
    section  
    cohort   
    line     $ 
    xgrtotlt $
    grtotlt  
    xgrtotlm $
    grtotlm  
    xgrtotlw $
    grtotlw  
    xgraiant $
    graiant  
    xgraianm $
    graianm  
    xgraianw $
    graianw  
    xgrasiat $
    grasiat  
    xgrasiam $
    grasiam  
    xgrasiaw $
    grasiaw  
    xgrbkaat $
    grbkaat  
    xgrbkaam $
    grbkaam  
    xgrbkaaw $
    grbkaaw  
    xgrhispt $
    grhispt  
    xgrhispm $
    grhispm  
    xgrhispw $
    grhispw  
    xgrnhpit $
    grnhpit  
    xgrnhpim $
    grnhpim  
    xgrnhpiw $
    grnhpiw  
    xgrwhitt $
    grwhitt  
    xgrwhitm $
    grwhitm  
    xgrwhitw $
    grwhitw  
    xgr2mort $
    gr2mort  
    xgr2morm $
    gr2morm  
    xgr2morw $
    gr2morw  
    xgrunknt $
    grunknt  
    xgrunknm $
    grunknm  
    xgrunknw $
    grunknw  
    xgrnralt $
    grnralt  
    xgrnralm $
    grnralm  
    xgrnralw $
    grnralw ;
  label
    unitid  ='Unique identification number of the institution' 
    grtype  ='Cohort data' 
    chrtstat='Graduation rate status in cohort' 
    section ='Section of survey form' 
    cohort  ='Cohort' 
    line    ='Original line number of survey form' 
    xgrtotlt='Imputation field for grtotlt - Grand total'
    grtotlt ='Grand Total' 
    xgrtotlm='Imputation field for grtotlm - Total men'
    grtotlm ='Total men' 
    xgrtotlw='Imputation field for grtotlw - Total women'
    grtotlw ='Total women' 
    xgraiant='Imputation field for graiant - American Indian or Alaska Native total'
    graiant ='American Indian or Alaska Native total' 
    xgraianm='Imputation field for graianm - American Indian or Alaska Native men'
    graianm ='American Indian or Alaska Native men' 
    xgraianw='Imputation field for graianw - American Indian or Alaska Native women'
    graianw ='American Indian or Alaska Native women' 
    xgrasiat='Imputation field for grasiat - Asian total'
    grasiat ='Asian total' 
    xgrasiam='Imputation field for grasiam - Asian men'
    grasiam ='Asian men' 
    xgrasiaw='Imputation field for grasiaw - Asian women'
    grasiaw ='Asian women' 
    xgrbkaat='Imputation field for grbkaat - Black or African American total'
    grbkaat ='Black or African American total' 
    xgrbkaam='Imputation field for grbkaam - Black or African American men'
    grbkaam ='Black or African American men' 
    xgrbkaaw='Imputation field for grbkaaw - Black or African American women'
    grbkaaw ='Black or African American women' 
    xgrhispt='Imputation field for grhispt - Hispanic total'
    grhispt ='Hispanic total' 
    xgrhispm='Imputation field for grhispm - Hispanic men'
    grhispm ='Hispanic men' 
    xgrhispw='Imputation field for grhispw - Hispanic women'
    grhispw ='Hispanic women' 
    xgrnhpit='Imputation field for grnhpit - Native Hawaiian or Other Pacific Islander total'
    grnhpit ='Native Hawaiian or Other Pacific Islander total' 
    xgrnhpim='Imputation field for grnhpim - Native Hawaiian or Other Pacific Islander men'
    grnhpim ='Native Hawaiian or Other Pacific Islander men' 
    xgrnhpiw='Imputation field for grnhpiw - Native Hawaiian or Other Pacific Islander women'
    grnhpiw ='Native Hawaiian or Other Pacific Islander women' 
    xgrwhitt='Imputation field for grwhitt - White total'
    grwhitt ='White total' 
    xgrwhitm='Imputation field for grwhitm - White men'
    grwhitm ='White men' 
    xgrwhitw='Imputation field for grwhitw - White women'
    grwhitw ='White women' 
    xgr2mort='Imputation field for gr2mort - Two or more races total'
    gr2mort ='Two or more races total' 
    xgr2morm='Imputation field for gr2morm - Two or more races men'
    gr2morm ='Two or more races men' 
    xgr2morw='Imputation field for gr2morw - Two or more races women'
    gr2morw ='Two or more races women' 
    xgrunknt='Imputation field for grunknt - Race/ethnicity unknown total'
    grunknt ='Race/ethnicity unknown total' 
    xgrunknm='Imputation field for grunknm - Race/ethnicity unknown men'
    grunknm ='Race/ethnicity unknown men' 
    xgrunknw='Imputation field for grunknw - Race/ethnicity unknown women'
    grunknw ='Race/ethnicity unknown women' 
    xgrnralt='Imputation field for grnralt - Nonresident alien total'
    grnralt ='Nonresident alien total' 
    xgrnralm='Imputation field for grnralm - Nonresident alien men'
    grnralm ='Nonresident alien men' 
    xgrnralw='Imputation field for grnralw - Nonresident alien women'
    grnralw ='Nonresident alien women';
    format xgrtotlt-character-xgrnralw $ximpflg.
grtype  grtype.
chrtstat  chrtstat.
section  section.
cohort  cohort.
line  $line.
;
run;

Proc Freq;
Tables
grtype   chrtstat section  cohort   line     xgrtotlt xgrtotlm xgrtotlw xgraiant
xgraianm xgraianw xgrasiat xgrasiam xgrasiaw xgrbkaat xgrbkaam xgrbkaaw xgrhispt xgrhispm
xgrhispw xgrnhpit xgrnhpim xgrnhpiw xgrwhitt xgrwhitm xgrwhitw xgr2mort xgr2morm xgr2morw
xgrunknt xgrunknm xgrunknw xgrnralt xgrnralm xgrnralw  / missing;
run;


Proc Summary print n sum mean min max;
var
grtotlt  grtotlm  grtotlw  graiant 
graianm  graianw  grasiat  grasiam  grasiaw  grbkaat  grbkaam  grbkaaw  grhispt  grhispm 
grhispw  grnhpit  grnhpim  grnhpiw  grwhitt  grwhitm  grwhitw  gr2mort  gr2morm  gr2morw 
grunknt  grunknm  grunknw  grnralt  grnralm  grnralw  ;
run;
