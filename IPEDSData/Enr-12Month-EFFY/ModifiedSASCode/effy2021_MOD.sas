*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;

%let path=/Data1/IPEDSData/DataFiles/Enr-12Month-EFFY;
%let file=effy2021;
%let libraryName=IPEDEFFY;
libname &libraryName "&path/SASData";

Proc Format library=&libraryName;
	value effyalev 1='All students total' 2='All students, Undergraduate total' 
		3='All students, Undergraduate, Degree/certificate-seeking total' 
		4='All students, Undergraduate, Degree/certificate-seeking, First-time' 
		5='All students, Undergraduate, Other degree/certificate-seeking' 19='All students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 
		20='All students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
		11='All students, Undergraduate, Non-degree/certificate-seeking' 
		12='All students, Graduate' 22='Full-time students, Undergraduate total' 
		23='Full-time students, Undergraduate, Degree/certificate-seeking total' 
		24='Full-time students, Undergraduate, Degree/certificate-seeking, First-time' 25='Full-time students, Undergraduate, Degree/certificate-seeking, Other degree/certificate-seeking' 39='Full-time students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 40='Full-time students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
		31='Full-time students, Undergraduate, Non-degree/certificate-seeking' 
		42='Part-time students, Undergraduate total' 
		43='Part-time students, Undergraduate, Degree/certificate-seeking total' 
		44='Part-time students, Undergraduate, Degree/certificate-seeking, First-time' 45='Part-time students, Undergraduate, Degree/certificate-seeking, Other degree/certificate-seeking' 59='Part-time students, Undergraduate, Other degree/certificatee-seeking, Transfer-ins' 60='Part-time students, Undergraduate, Other degree/certificatee-seeking, Continuing' 
		51='Part-time students, Undergraduate, Non-degree/certificate-seeking';
	value effylev 1='All students total' 2='Undergraduate' 4='Graduate' 
-2='Not applicable, undergraduate detail';
	value lstudy 1='Undergraduate' 3='Graduate' 999='Generated total';
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program n' Z='Implied zero';


options fmtsearch=(&libraryName);

data &libraryName..&file;
  infile "&path/&file..csv" delimiter=',' DSD MISSOVER 
		firstobs=2 lrecl=32736;
	informat unitid 6. effyalev 2. effylev 2. lstudy 3. xeytotlt $1. efytotlt 6. 
		xeytotlm $1. efytotlm 6. xeytotlw $1. efytotlw 6. xefyaiat $1. efyaiant 6. 
		xefyaiam $1. efyaianm 6. xefyaiaw $1. efyaianw 6. xefyasit $1. efyasiat 6. 
		xefyasim $1. efyasiam 6. xefyasiw $1. efyasiaw 6. xefybkat $1. efybkaat 6. 
		xefybkam $1. efybkaam 6. xefybkaw $1. efybkaaw 6. xefyhist $1. efyhispt 6. 
		xefyhism $1. efyhispm 6. xefyhisw $1. efyhispw 6. xefynhpt $1. efynhpit 6. 
		xefynhpm $1. efynhpim 6. xefynhpw $1. efynhpiw 6. xefywhit $1. efywhitt 6. 
		xefywhim $1. efywhitm 6. xefywhiw $1. efywhitw 6. xefy2mot $1. efy2mort 6. 
		xefy2mom $1. efy2morm 6. xefy2mow $1. efy2morw 6. xeyunknt $1. efyunknt 6. 
		xeyunknm $1. efyunknm 6. xeyunknw $1. efyunknw 6. xeynralt $1. efynralt 6. 
		xeynralm $1. efynralm 6. xeynralw $1. efynralw 6.;
	input unitid effyalev effylev lstudy xeytotlt $
efytotlt xeytotlm $
efytotlm xeytotlw $
efytotlw xefyaiat $
efyaiant xefyaiam $
efyaianm xefyaiaw $
efyaianw xefyasit $
efyasiat xefyasim $
efyasiam xefyasiw $
efyasiaw xefybkat $
efybkaat xefybkam $
efybkaam xefybkaw $
efybkaaw xefyhist $
efyhispt xefyhism $
efyhispm xefyhisw $
efyhispw xefynhpt $
efynhpit xefynhpm $
efynhpim xefynhpw $
efynhpiw xefywhit $
efywhitt xefywhim $
efywhitm xefywhiw $
efywhitw xefy2mot $
efy2mort xefy2mom $
efy2morm xefy2mow $
efy2morw xeyunknt $
efyunknt xeyunknm $
efyunknm xeyunknw $
efyunknw xeynralt $
efynralt xeynralm $
efynralm xeynralw $
efynralw;
	label unitid='Unique identification number of the institution' 
		effyalev='Level and degree/certificate-seeking status of student' 
		effylev='Undergraduate or graduate level of student' 
		lstudy='Original level of study on survey form' 
		xeytotlt='Imputation field for efytotlt - Grand total' efytotlt='Grand total' 
		xeytotlm='Imputation field for efytotlm - Grand total men' 
		efytotlm='Grand total men' 
		xeytotlw='Imputation field for efytotlw - Grand total women' 
		efytotlw='Grand total women' xefyaiat='Imputation field for efyaiant - American Indian or Alaska Native total' 
		efyaiant='American Indian or Alaska Native total' 
		xefyaiam='Imputation field for efyaianm - American Indian or Alaska Native men' 
		efyaianm='American Indian or Alaska Native men' xefyaiaw='Imputation field for efyaianw - American Indian or Alaska Native women' 
		efyaianw='American Indian or Alaska Native women' 
		xefyasit='Imputation field for efyasiat - Asian total' efyasiat='Asian total' 
		xefyasim='Imputation field for efyasiam - Asian men' efyasiam='Asian men' 
		xefyasiw='Imputation field for efyasiaw - Asian women' efyasiaw='Asian women' 
		xefybkat='Imputation field for efybkaat - Black or African American total' 
		efybkaat='Black or African American total' 
		xefybkam='Imputation field for efybkaam - Black or African American men' 
		efybkaam='Black or African American men' 
		xefybkaw='Imputation field for efybkaaw - Black or African American women' 
		efybkaaw='Black or African American women' 
		xefyhist='Imputation field for efyhispt - Hispanic or Latino total' 
		efyhispt='Hispanic or Latino total' 
		xefyhism='Imputation field for efyhispm - Hispanic or Latino men' 
		efyhispm='Hispanic or Latino men' 
		xefyhisw='Imputation field for efyhispw - Hispanic or Latino women' 
		efyhispw='Hispanic or Latino women' xefynhpt='Imputation field for efynhpit - Native Hawaiian or Other Pacific Islander total' 
		efynhpit='Native Hawaiian or Other Pacific Islander total' xefynhpm='Imputation field for efynhpim - Native Hawaiian or Other Pacific Islander men' 
		efynhpim='Native Hawaiian or Other Pacific Islander men' xefynhpw='Imputation field for efynhpiw - Native Hawaiian or Other Pacific Islander women' 
		efynhpiw='Native Hawaiian or Other Pacific Islander women' 
		xefywhit='Imputation field for efywhitt - White total' efywhitt='White total' 
		xefywhim='Imputation field for efywhitm - White men' efywhitm='White men' 
		xefywhiw='Imputation field for efywhitw - White women' efywhitw='White women' 
		xefy2mot='Imputation field for efy2mort - Two or more races total' 
		efy2mort='Two or more races total' 
		xefy2mom='Imputation field for efy2morm - Two or more races men' 
		efy2morm='Two or more races men' 
		xefy2mow='Imputation field for efy2morw - Two or more races women' 
		efy2morw='Two or more races women' 
		xeyunknt='Imputation field for efyunknt - Race/ethnicity unknown total' 
		efyunknt='Race/ethnicity unknown total' 
		xeyunknm='Imputation field for efyunknm - Race/ethnicity unknown men' 
		efyunknm='Race/ethnicity unknown men' 
		xeyunknw='Imputation field for efyunknw - Race/ethnicity unknown women' 
		efyunknw='Race/ethnicity unknown women' 
		xeynralt='Imputation field for efynralt - Nonresident alien total' 
		efynralt='Nonresident alien total' 
		xeynralm='Imputation field for efynralm - Nonresident alien men' 
		efynralm='Nonresident alien men' 
		xeynralw='Imputation field for efynralw - Nonresident alien women' 
		efynralw='Nonresident alien women';
		format xeytotlt-character-xeynralw $ximpflg.
			effyalev effyalev.
			effylev effylev.
			lstudy lstudy.;
run;

Proc Freq;
	Tables effyalev effylev lstudy xeytotlt xeytotlm xeytotlw xefyaiat xefyaiam 
		xefyaiaw xefyasit xefyasim xefyasiw xefybkat xefybkam xefybkaw xefyhist 
		xefyhism xefyhisw xefynhpt xefynhpm xefynhpw xefywhit xefywhim xefywhiw 
		xefy2mot xefy2mom xefy2mow xeyunknt xeyunknm xeyunknw xeynralt xeynralm 
		xeynralw / missing;
	format xeytotlt-character-xeynralw $ximpflg.
effyalev effyalev.
effylev effylev.
lstudy lstudy.;

Proc Summary print n sum mean min max;
	var efytotlt efytotlm efytotlw efyaiant efyaianm efyaianw efyasiat efyasiam 
		efyasiaw efybkaat efybkaam efybkaaw efyhispt efyhispm efyhispw efynhpit 
		efynhpim efynhpiw efywhitt efywhitm efywhitw efy2mort efy2morm efy2morw 
		efyunknt efyunknm efyunknw efynralt efynralm efynralw;
run;