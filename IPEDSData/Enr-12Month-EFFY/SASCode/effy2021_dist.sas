*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\effy2021_dist.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
effydlev 2. 
xeydetot $1.
efydetot 6. 
xeydeexc $1.
efydeexc 6. 
xeydesom $1.
efydesom 6. 
xeydenon $1.
efydenon 6.;

input
unitid   
effydlev 
xeydetot $
efydetot 
xeydeexc $
efydeexc 
xeydesom $
efydesom 
xeydenon $
efydenon;

label
unitid  ='Unique identification number of the institution' 
effydlev='Level of student' 
xeydetot='Imputation field for efydetot - All students enrolled'
efydetot='All students enrolled' 
xeydeexc='Imputation field for efydeexc - Students enrolled exclusively in distance education courses'
efydeexc='Students enrolled exclusively in distance education courses' 
xeydesom='Imputation field for efydesom - Students enrolled in some but not all distance education courses'
efydesom='Students enrolled in some but not all distance education courses' 
xeydenon='Imputation field for efydenon - Students not enrolled in any distance education courses'
efydenon='Students not enrolled in any distance education courses';
run;

Proc Format;
value effydlev  
1='All students total' 
2='Undergraduate total' 
3='Undergraduate, degree/certificate-seeking' 
11='Undergraduate, non-degree/certificate-seeking' 
12='Graduate';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
effydlev xeydetot xeydeexc xeydesom xeydenon  / missing;
format xeydetot-character-xeydenon $ximpflg.
effydlev  effydlev.
;

Proc Summary print n sum mean min max;
var
efydetot efydeexc efydesom efydenon ;
run;
