*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\efia2021.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xcdactua $1.
cdactua  8. 
xcnactua $1.
cnactua  8. 
xcdactga $1.
cdactga  8. 
xefteug  $1.
efteug   8. 
xeftegd  $1.
eftegd   8. 
xfteug   $1.
fteug    8. 
xftegd   $1.
ftegd    8. 
xftedpp  $1.
ftedpp   8. 
acttype  8.;

input
unitid   
xcdactua $
cdactua  
xcnactua $
cnactua  
xcdactga $
cdactga  
xefteug  $
efteug   
xeftegd  $
eftegd   
xfteug   $
fteug    
xftegd   $
ftegd    
xftedpp  $
ftedpp   
acttype ;

label
unitid  ='Unique identification number of the institution' 
xcdactua='Imputation field for cdactua - 12-month instructional activity credit hours: undergraduates'
cdactua ='12-month instructional activity credit hours: undergraduates' 
xcnactua='Imputation field for cnactua - 12-month instructional activity clock hours: undergraduates'
cnactua ='12-month instructional activity clock hours: undergraduates' 
xcdactga='Imputation field for cdactga - 12-month instructional activity credit hours: graduates'
cdactga ='12-month instructional activity credit hours: graduates' 
xefteug ='Imputation field for efteug - Estimated full-time equivalent (FTE) undergraduate enrollment, 2020-21'
efteug  ='Estimated full-time equivalent (FTE) undergraduate enrollment, 2020-21' 
xeftegd ='Imputation field for eftegd - Estimated full-time equivalent (FTE) graduate enrollment, 2020-21'
eftegd  ='Estimated full-time equivalent (FTE) graduate enrollment, 2020-21' 
xfteug  ='Imputation field for fteug - Reported full-time equivalent (FTE) undergraduate enrollment, 2020-21'
fteug   ='Reported full-time equivalent (FTE) undergraduate enrollment, 2020-21' 
xftegd  ='Imputation field for ftegd - Reported full-time equivalent (FTE) graduate enrollment, 2020-21'
ftegd   ='Reported full-time equivalent (FTE) graduate enrollment, 2020-21' 
xftedpp ='Imputation field for ftedpp - Reported full-time equivalent (FTE) doctors professional practice, 2020-21'
ftedpp  ='Reported full-time equivalent (FTE) doctors professional practice, 2020-21' 
acttype ='Is instructional activity based on credit or clock hours';
run;

Proc Format;
value acttype   
1='Clock hours' 
2='Credit hours' 
3='Both clock and credit hours' 
-2='Not applicable';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
xcdactua xcnactua xcdactga xefteug  xeftegd  xfteug   xftegd   xftedpp  acttype 
 / missing;
format xcdactua-character-xftedpp  $ximpflg.
acttype  acttype.
;

Proc Summary print n sum mean min max;
var
cdactua  cnactua  cdactga  efteug   eftegd   fteug    ftegd    ftedpp  
;
run;
