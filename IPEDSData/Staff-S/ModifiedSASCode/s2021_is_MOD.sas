*** Created: September 16, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;

%let path=/Data1/IPEDSData/DataFiles/Staff-S;
%let file=s2021_is;
%let libraryName=IPEDS;
libname &libraryName "&path/SASData";

Proc Format library=&libraryName;
	value siscat 1='All full-time instructional staff' 
		100='Full-time instructional with faculty status' 
		101='Full-time instructional professors' 
		102='Full-time instructional associate professors' 
		103='Full-time instructional assistant professors' 
		104='Full-time instructional instructors' 
		105='Full-time instructional lecturers' 
		106='Full-time instructional no academic rank' 
		200='Full-time instructional tenured total' 
		201='Full-time instructional tenured, professors' 
		202='Full-time instructional tenured, associate professors' 
		203='Full-time instructional tenured, assistant professors' 
		204='Full-time instructional tenured, instructors' 
		205='Full-time instructional tenured, lecturers' 
		206='Full-time instructional tenured, no academic rank' 
		300='Full-time instructional on-tenure track total' 
		301='Full-time instructional on-tenure track, professors' 
		302='Full-time instructional on-tenure track, associate professors' 
		303='Full-time instructional on-tenure track, assistant professors' 
		304='Full-time instructional on-tenure track, instructors' 
		305='Full-time instructional on-tenure track, lecturers' 
		306='Full-time instructional on-tenure track, no academic rank' 
		400='Full-time instructional not on tenure track/no tenure system system total' 
		401='Full-time instructional not on tenure/no tenure system, professors' 402='Full-time instructional not on tenure/no tenure system, associate professors' 403='Full-time instructional not on tenure/no tenure system, assistant professors' 
		404='Full-time instructional not on tenure/no tenure system, instructors' 
		405='Full-time instructional not on tenure/no tenure system, lecturers' 
		406='Full-time instructional not on tenure/no tenure system, no academic rank' 410='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, total' 411='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, professors' 412='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, asssociate professors' 413='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, assistant professors' 414='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, instructors' 415='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts contract,lecturers' 416='Full-time instructional not on tenure track/no tenure system, multi-year and indefinite contracts, no academic rank' 420='Full-time instructional not on tenure track/no tenure system, annual contract, total' 421='Full-time instructional not on tenure track/no tenure system, annual contract, professors' 422='Full-time instructional not on tenure track/no tenure system, annual contract, asssociate professors' 423='Full-time instructional not on tenure track/no tenure system, annual contract, assistant professors' 424='Full-time instructional not on tenure track/no tenure system, annual contract, instructors' 425='Full-time instructional not on tenure track/no tenure system, annual contract,lecturers' 426='Full-time instructional not on tenure track/no tenure system, annual contract, no academic rank' 430='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, total' 431='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, professors' 432='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, asssociate professors' 433='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, assistant professors' 434='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, instructors' 435='Full-time instructional not on tenure track/no tenure system, less-than-annual contract,lecturers' 436='Full-time instructional not on tenure track/no tenure system, less-than-annual contract, no academic rank' 440='Full-time instructional not on tenure track/no tenure system, multi-year contract, total' 441='Full-time instructional not on tenure track/no tenure system, multi-year contract, professors' 442='Full-time instructional not on tenure track/no tenure system, multi-year contract, associate professors' 443='Full-time instructional not on tenure track/no tenure system, multi-year contract, assistant professors' 444='Full-time instructional not on tenure track/no tenure system, multi-year contract, instructors' 445='Full-time instructional not on tenure track/no tenure system, multi-year contract, lecturers' 446='Full-time instructional not on tenure track/no tenure system, multi-year contract, no academic rank' 450='Full-time instructional not on tenure track/no tenure system, multi-year contract, total' 451='Full-time instructional not on tenure track/no tenure system, indefinite contract, professors' 452='Full-time instructional not on tenure track/no tenure system, indefinite contract, associate professors' 453='Full-time instructional not on tenure track/no tenure system, indefinite contract, assistant professors' 454='Full-time instructional not on tenure track/no tenure system, indefinite contract, instructors' 455='Full-time instructional not on tenure track/no tenure system, indefinite contract, lecturers' 456='Full-time instructional not on tenure track/no tenure system, indefinite contract, no academic rank' 
		500='Full-time instructional without faculty status';
	value facstat 0='All full-time instructional staff' 
		10='With faculty status, total' 20='With faculty status, tenured' 
		30='With faculty status, on tenure track' 
		40='With faculty status not on tenure track/No tenure system, total' 41='With faculty status not on tenure track/No tenure system, multi-year and indefinite contract' 44='With faculty status not on tenure track/No tenure system, multi-year contract' 45='With faculty status not on tenure track/No tenure system, indefinite contract' 
		42='With faculty status not on tenure track/No tenure system, annual contract' 43='With faculty status not on tenure track/No tenure system, less-than-annual contract' 
		50='Without faculty status';
	value arank 0='All ranks' 1='Professors' 2='Associate professors' 
		3='Assistant professors' 4='Instructors' 5='Lecturers' 6='No academic rank';
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program n' Z='Implied zero';

options fmtsearch=(&libraryName);

Data &libraryName..&file;
	infile "&path/&file..csv" delimiter=',' DSD MISSOVER 
		firstobs=2 lrecl=32736;
	informat unitid 6. siscat 3. facstat 3. arank 3. xhrtotlt $1. hrtotlt 6. 
		xhrtotlm $1. hrtotlm 6. xhrtotlw $1. hrtotlw 6. xhraiant $1. hraiant 6. 
		xhraianm $1. hraianm 6. xhraianw $1. hraianw 6. xhrasiat $1. hrasiat 6. 
		xhrasiam $1. hrasiam 6. xhrasiaw $1. hrasiaw 6. xhrbkaat $1. hrbkaat 6. 
		xhrbkaam $1. hrbkaam 6. xhrbkaaw $1. hrbkaaw 6. xhrhispt $1. hrhispt 6. 
		xhrhispm $1. hrhispm 6. xhrhispw $1. hrhispw 6. xhrnhpit $1. hrnhpit 6. 
		xhrnhpim $1. hrnhpim 6. xhrnhpiw $1. hrnhpiw 6. xhrwhitt $1. hrwhitt 6. 
		xhrwhitm $1. hrwhitm 6. xhrwhitw $1. hrwhitw 6. xhr2mort $1. hr2mort 6. 
		xhr2morm $1. hr2morm 6. xhr2morw $1. hr2morw 6. xhrunknt $1. hrunknt 6. 
		xhrunknm $1. hrunknm 6. xhrunknw $1. hrunknw 6. xhrnralt $1. hrnralt 6. 
		xhrnralm $1. hrnralm 6. xhrnralw $1. hrnralw 6.;
	input unitid siscat facstat arank xhrtotlt $
hrtotlt xhrtotlm $
hrtotlm xhrtotlw $
hrtotlw xhraiant $
hraiant xhraianm $
hraianm xhraianw $
hraianw xhrasiat $
hrasiat xhrasiam $
hrasiam xhrasiaw $
hrasiaw xhrbkaat $
hrbkaat xhrbkaam $
hrbkaam xhrbkaaw $
hrbkaaw xhrhispt $
hrhispt xhrhispm $
hrhispm xhrhispw $
hrhispw xhrnhpit $
hrnhpit xhrnhpim $
hrnhpim xhrnhpiw $
hrnhpiw xhrwhitt $
hrwhitt xhrwhitm $
hrwhitm xhrwhitw $
hrwhitw xhr2mort $
hr2mort xhr2morm $
hr2morm xhr2morw $
hr2morw xhrunknt $
hrunknt xhrunknm $
hrunknm xhrunknw $
hrunknw xhrnralt $
hrnralt xhrnralm $
hrnralm xhrnralw $
hrnralw;
	label unitid='Unique identification number of the institution' 
		siscat='Instructional staff category' facstat='Faculty and tenure status' 
		arank='Academic rank' xhrtotlt='Imputation field for hrtotlt - Grand total' 
		hrtotlt='Grand total' 
		xhrtotlm='Imputation field for hrtotlm - Grand total men' 
		hrtotlm='Grand total men' 
		xhrtotlw='Imputation field for hrtotlw - Grand total women' 
		hrtotlw='Grand total women' xhraiant='Imputation field for hraiant - American Indian or Alaska Native total' 
		hraiant='American Indian or Alaska Native total' 
		xhraianm='Imputation field for hraianm - American Indian or Alaska Native men' 
		hraianm='American Indian or Alaska Native men' xhraianw='Imputation field for hraianw - American Indian or Alaska Native women' 
		hraianw='American Indian or Alaska Native women' 
		xhrasiat='Imputation field for hrasiat - Asian total' hrasiat='Asian total' 
		xhrasiam='Imputation field for hrasiam - Asian men' hrasiam='Asian men' 
		xhrasiaw='Imputation field for hrasiaw - Asian women' hrasiaw='Asian women' 
		xhrbkaat='Imputation field for hrbkaat - Black or African American total' 
		hrbkaat='Black or African American total' 
		xhrbkaam='Imputation field for hrbkaam - Black or African American men' 
		hrbkaam='Black or African American men' 
		xhrbkaaw='Imputation field for hrbkaaw - Black or African American women' 
		hrbkaaw='Black or African American women' 
		xhrhispt='Imputation field for hrhispt - Hispanic or Latino total' 
		hrhispt='Hispanic or Latino total' 
		xhrhispm='Imputation field for hrhispm - Hispanic or Latino men' 
		hrhispm='Hispanic or Latino men' 
		xhrhispw='Imputation field for hrhispw - Hispanic or Latino women' 
		hrhispw='Hispanic or Latino women' xhrnhpit='Imputation field for hrnhpit - Native Hawaiian or Other Pacific Islander total' 
		hrnhpit='Native Hawaiian or Other Pacific Islander total' xhrnhpim='Imputation field for hrnhpim - Native Hawaiian or Other Pacific Islander men' 
		hrnhpim='Native Hawaiian or Other Pacific Islander men' xhrnhpiw='Imputation field for hrnhpiw - Native Hawaiian or Other Pacific Islander women' 
		hrnhpiw='Native Hawaiian or Other Pacific Islander women' 
		xhrwhitt='Imputation field for hrwhitt - White total' hrwhitt='White total' 
		xhrwhitm='Imputation field for hrwhitm - White men' hrwhitm='White men' 
		xhrwhitw='Imputation field for hrwhitw - White women' hrwhitw='White women' 
		xhr2mort='Imputation field for hr2mort - Two or more races total' 
		hr2mort='Two or more races total' 
		xhr2morm='Imputation field for hr2morm - Two or more races men' 
		hr2morm='Two or more races men' 
		xhr2morw='Imputation field for hr2morw - Two or more races women' 
		hr2morw='Two or more races women' 
		xhrunknt='Imputation field for hrunknt - Race/ethnicity unknown total' 
		hrunknt='Race/ethnicity unknown total' 
		xhrunknm='Imputation field for hrunknm - Race/ethnicity unknown men' 
		hrunknm='Race/ethnicity unknown men' 
		xhrunknw='Imputation field for hrunknw - Race/ethnicity unknown women' 
		hrunknw='Race/ethnicity unknown women' 
		xhrnralt='Imputation field for hrnralt - Nonresident alien total' 
		hrnralt='Nonresident alien total' 
		xhrnralm='Imputation field for hrnralm - Nonresident alien men' 
		hrnralm='Nonresident alien men' 
		xhrnralw='Imputation field for hrnralw - Nonresident alien women' 
		hrnralw='Nonresident alien women';
		format xhrtotlt-character-xhrnralw $ximpflg.
		siscat siscat.
		facstat facstat.
		arank arank.;
run;

Proc Freq;
	Tables siscat facstat arank xhrtotlt xhrtotlm xhrtotlw xhraiant xhraianm 
		xhraianw xhrasiat xhrasiam xhrasiaw xhrbkaat xhrbkaam xhrbkaaw xhrhispt 
		xhrhispm xhrhispw xhrnhpit xhrnhpim xhrnhpiw xhrwhitt xhrwhitm xhrwhitw 
		xhr2mort xhr2morm xhr2morw xhrunknt xhrunknm xhrunknw xhrnralt xhrnralm 
		xhrnralw / missing;
	format xhrtotlt-character-xhrnralw $ximpflg.
siscat siscat.
facstat facstat.
arank arank.;

Proc Summary print n sum mean min max;
	var hrtotlt hrtotlm hrtotlw hraiant hraianm hraianw hrasiat hrasiam hrasiaw 
		hrbkaat hrbkaam hrbkaaw hrhispt hrhispm hrhispw hrnhpit hrnhpim hrnhpiw 
		hrwhitt hrwhitm hrwhitw hr2mort hr2morm hr2morw hrunknt hrunknm hrunknw 
		hrnralt hrnralm hrnralw;
run;