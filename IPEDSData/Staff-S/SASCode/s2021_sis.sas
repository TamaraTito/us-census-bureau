*** Created: September 16, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\s2021_sis.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
facstat  6. 
xsistotl $1.
sistotl  2. 
xsisprof $1.
sisprof  2. 
xsisascp $1.
sisascp  2. 
xsisastp $1.
sisastp  2. 
xsisinst $1.
sisinst  2. 
xsislect $1.
sislect  2. 
xsisnork $1.
sisnork  2.;

input
unitid   
facstat  
xsistotl $
sistotl  
xsisprof $
sisprof  
xsisascp $
sisascp  
xsisastp $
sisastp  
xsisinst $
sisinst  
xsislect $
sislect  
xsisnork $
sisnork ;

label
unitid  ='Unique identification number of the institution' 
facstat ='Faculty and tenure status' 
xsistotl='Imputation field for sistotl - All ranks'
sistotl ='All ranks' 
xsisprof='Imputation field for sisprof - Professors'
sisprof ='Professors' 
xsisascp='Imputation field for sisascp - Associate professors'
sisascp ='Associate professors' 
xsisastp='Imputation field for sisastp - Assistant professors'
sisastp ='Assistant professors' 
xsisinst='Imputation field for sisinst - Instructors'
sisinst ='Instructors' 
xsislect='Imputation field for sislect - Lecturers'
sislect ='Lecturers' 
xsisnork='Imputation field for sisnork - No academic rank'
sisnork ='No academic rank';
run;

Proc Format;
value facstat   
0='All full-time instructional staff' 
10='With faculty status, total' 
20='With faculty status, tenured' 
30='With faculty status, on tenure track' 
40='With faculty status not on tenure track/No tenure system, total' 
41='With faculty status not on tenure track/No tenure system, multi-year and indefinite contracts' 
44='With faculty status not on tenure track/No tenure system, multi-year contract' 
45='With faculty status not on tenure track/No tenure system, indefinite contract' 
42='With faculty status not on tenure track/No tenure system, annual contract' 
43='With faculty status not on tenure track/No tenure system, less-than-annual contract' 
50='Without faculty status';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
facstat  xsistotl xsisprof xsisascp xsisastp xsisinst xsislect xsisnork  / missing;
format xsistotl-character-xsisnork $ximpflg.
facstat  facstat.
;

Proc Summary print n sum mean min max;
var
sistotl  sisprof  sisascp  sisastp  sisinst  sislect  sisnork  ;
run;
