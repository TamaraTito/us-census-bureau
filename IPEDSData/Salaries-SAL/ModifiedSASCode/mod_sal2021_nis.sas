%let path=/Data1/IPEDSData/DataFiles/Salaries-SAL;
%let file=sal2021_nis;
%let libraryName=IPEDEAP;
libname &libraryName "&path/SASData";

proc format library=&libraryName;
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program not applicable' Z='Implied zero';
run;

options fmtsearch=(&libraryName);

data &libraryName..&file;
	infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
	informat unitid 6. xsanin01 $1. sanin01 6. xsanit01 $1. sanit01 10. 
		xsanin02 $1. sanin02 6. xsanit02 $1. sanit02 10. xsanin03 $1. sanin03 6. 
		xsanit03 $1. sanit03 10. xsanin04 $1. sanin04 6. xsanit04 $1. sanit04 10. 
		xsanin05 $1. sanin05 6. xsanit05 $1. sanit05 10. xsanin06 $1. sanin06 6. 
		xsanit06 $1. sanit06 10. xsanin07 $1. sanin07 6. xsanit07 $1. sanit07 10. 
		xsanin08 $1. sanin08 6. xsanit08 $1. sanit08 10. xsanin09 $1. sanin09 6. 
		xsanit09 $1. sanit09 10. xsanin10 $1. sanin10 6. xsanit10 $1. sanit10 10. 
		xsanin11 $1. sanin11 6. xsanit11 $1. sanit11 10. xsanin12 $1. sanin12 6. 
		xsanit12 $1. sanit12 10. xsanin13 $1. sanin13 6. xsanit13 $1. sanit13 10. 
		xsanin14 $1. sanin14 6. xsanit14 $1. sanit14 10.;
	input unitid xsanin01 $ sanin01 xsanit01 $ sanit01 xsanin02 $
		sanin02 xsanit02 $ sanit02 xsanin03 $ sanin03 xsanit03 $
		sanit03 xsanin04 $ sanin04 xsanit04 $ sanit04 xsanin05 $
		sanin05 xsanit05 $ sanit05 xsanin06 $ sanin06 xsanit06 $
		sanit06 xsanin07 $ sanin07 xsanit07 $ sanit07 xsanin08 $
		sanin08 xsanit08 $ sanit08 xsanin09 $ sanin09 xsanit09 $
		sanit09 xsanin10 $ sanin10 xsanit10 $ sanit10 xsanin11 $
		sanin11 xsanit11 $ sanit11 xsanin12 $ sanin12 xsanit12 $
		sanit12 xsanin13 $ sanin13 xsanit13 $ sanit13 xsanin14 $
		sanin14 xsanit14 $ sanit14;
	label unitid='Unique identification number of the institution' xsanin01='Imputation field for sanin01 - Full-time non-instructional staff - number' 
		sanin01='Full-time non-instructional staff - number' xsanit01='Imputation field for sanit01 - Full-time non-instructional staff - outlays' 
		sanit01='Full-time non-instructional staff - outlays' 
		xsanin02='Imputation field for sanin02 - Research - number' 
		sanin02='Research - number' 
		xsanit02='Imputation field for sanit02 - Research - outlays' 
		sanit02='Research - outlays' 
		xsanin03='Imputation field for sanin03 - Public service - number' 
		sanin03='Public service - number' 
		xsanit03='Imputation field for sanit03 - Public service - Outlays' 
		sanit03='Public service - Outlays' xsanin04='Imputation field for sanin04 - Librarians, Curators, Archivists, and Academic Affairs and Other Education Services  - number' sanin04='Librarians, Curators, Archivists, and Academic Affairs and Other Education Services  - number' xsanit04='Imputation field for sanit04 - Librarians, Curators, Archivists and Academic Affairs and Other Education Services - outlays' sanit04='Librarians, Curators, Archivists and Academic Affairs and Other Education Services - outlays' 
		xsanin05='Imputation field for sanin05 - Management - number' 
		sanin05='Management - number' 
		xsanit05='Imputation field for sanit05 - Management - outlays' 
		sanit05='Management - outlays' xsanin06='Imputation field for sanin06 - Business and Financial Operations - number' 
		sanin06='Business and Financial Operations - number' xsanit06='Imputation field for sanit06 - Business and Financial Operations - outlays' 
		sanit06='Business and Financial Operations - outlays' xsanin07='Imputation field for sanin07 - Computer, Engineering, and Science - number' 
		sanin07='Computer, Engineering, and Science - number' xsanit07='Imputation field for sanit07 - Computer, Engineering, and Science - outlays' 
		sanit07='Computer, Engineering, and Science - outlays' xsanin08='Imputation field for sanin08 - Community, Social  Service, Legal, Arts, Design, Entertainment, Sports and Media - number' sanin08='Community, Social  Service, Legal, Arts, Design, Entertainment, Sports and Media - number' xsanit08='Imputation field for sanit08 - Community, Social  Service, Legal, Arts, Design, Entertainment, Sports and Media - outlays' sanit08='Community, Social  Service, Legal, Arts, Design, Entertainment, Sports and Media - outlays' xsanin09='Imputation field for sanin09 - Healthcare Practioners and Technical -number' 
		sanin09='Healthcare Practioners and Technical -number' xsanit09='Imputation field for sanit09 - Healthcare Practioners and Technical - outlays' 
		sanit09='Healthcare Practioners and Technical - outlays' 
		xsanin10='Imputation field for sanin10 - Service - number' 
		sanin10='Service - number' 
		xsanit10='Imputation field for sanit10 - Service - outlays' 
		sanit10='Service - outlays' 
		xsanin11='Imputation field for sanin11 - Sales and related - number' 
		sanin11='Sales and related - number' 
		xsanit11='Imputation field for sanit11 - Sales and related - outlays' 
		sanit11='Sales and related - outlays' xsanin12='Imputation field for sanin12 - Office and Administrative Support - number' 
		sanin12='Office and Administrative Support - number' xsanit12='Imputation field for sanit12 - Office and Administrative Support - outlays' 
		sanit12='Office and Administrative Support - outlays' xsanin13='Imputation field for sanin13 - Natural Resources, Construction, and Maintenance - number' 
		sanin13='Natural Resources, Construction, and Maintenance - number' xsanit13='Imputation field for sanit13 - Natural Resources, Construction, and Maintenance - outlays' 
		sanit13='Natural Resources, Construction, and Maintenance - outlays' xsanin14='Imputation field for sanin14 - Production, Transportation, and Material Moving - number' 
		sanin14='Production, Transportation, and Material Moving - number' xsanit14='Imputation field for sanit14 - Production, Transportation, and Material Moving - outlays' 
		sanit14='Production, Transportation, and Material Moving - outlays';
	format xsanin01-character-xsanit14 $ximpflg.;
run;

Proc Freq;
	Tables xsanin01 xsanit01 xsanin02 xsanit02 xsanin03 xsanit03 xsanin04 xsanit04 
		xsanin05 xsanit05 xsanin06 xsanit06 xsanin07 xsanit07 xsanin08 xsanit08 
		xsanin09 xsanit09 xsanin10 xsanit10 xsanin11 xsanit11 xsanin12 xsanit12 
		xsanin13 xsanit13 xsanin14 xsanit14 / missing;

Proc Summary print n sum mean min max;
	var sanin01 sanit01 sanin02 sanit02 sanin03 sanit03 sanin04 sanit04 sanin05 
		sanit05 sanin06 sanit06 sanin07 sanit07 sanin08 sanit08 sanin09 sanit09 
		sanin10 sanit10 sanin11 sanit11 sanin12 sanit12 sanin13 sanit13 sanin14 
		sanit14;
run;