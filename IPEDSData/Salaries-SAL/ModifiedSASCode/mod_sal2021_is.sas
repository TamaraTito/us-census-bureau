%let path=/Data1/IPEDSData/DataFiles/Salaries-SAL;
%let file=sal2021_is;
%let libraryName=IPEDSAL;
libname &libraryName "&path/SASData";

/**Format**/
proc format library=&libraryName;
	value arank 7='All instructional staff total' 1='Professor' 
		2='Associate professor' 3='Assistant professor' 4='Instructor' 5='Lecturer' 
		6='No academic rank';
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program n' Z='Implied zero';
run;

options fmtsearch=(&libraryName);

/** Data Step **/
data &libraryName..&file;
	infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
	informat unitid 6. arank 2. xsainstt $1. sainstt 6. xsainstm $1. sainstm 6. 
		xsainstw $1. sainstw 6. xsa_9mct $1. sa_9mct 6. xsa_9mcm $1. sa_9mcm 6. 
		xsa_9mcw $1. sa_9mcw 6. xsatotlt $1. satotlt 6. xsatotlm $1. satotlm 6. 
		xsatotlw $1. satotlw 6. xsa09mct $1. sa09mct 6. xsa09mcm $1. sa09mcm 6. 
		xsa09mcw $1. sa09mcw 6. xsa10mct $1. sa10mct 6. xsa10mcm $1. sa10mcm 6. 
		xsa10mcw $1. sa10mcw 6. xsa11mct $1. sa11mct 6. xsa11mcm $1. sa11mcm 6. 
		xsa11mcw $1. sa11mcw 6. xsa12mct $1. sa12mct 6. xsa12mcm $1. sa12mcm 6. 
		xsa12mcw $1. sa12mcw 6. xsaoutlt $1. saoutlt 10. xsaoutlm $1. saoutlm 10. 
		xsaoutlw $1. saoutlw 10. xsa09mot $1. sa09mot 10. xsa09mom $1. sa09mom 10. 
		xsa09mow $1. sa09mow 10. xsa10mot $1. sa10mot 10. xsa10mom $1. sa10mom 10. 
		xsa10mow $1. sa10mow 10. xsa11mot $1. sa11mot 10. xsa11mom $1. sa11mom 10. 
		xsa11mow $1. sa11mow 10. xsa12mot $1. sa12mot 10. xsa12mom $1. sa12mom 10. 
		xsa12mow $1. sa12mow 10. xsaeq9ot $1. saeq9ot 10. xsaeq9om $1. saeq9om 10. 
		xsaeq9ow $1. saeq9ow 10. xsaeq9at $1. saeq9at 6. xsaeq9am $1. saeq9am 6. 
		xsaeq9aw $1. saeq9aw 6. xsa09mat $1. sa09mat 6. xsa09mam $1. sa09mam 6. 
		xsa09maw $1. sa09maw 6. xsa10mat $1. sa10mat 6. xsa10mam $1. sa10mam 6. 
		xsa10maw $1. sa10maw 6. xsa11mat $1. sa11mat 6. xsa11mam $1. sa11mam 6. 
		xsa11maw $1. sa11maw 6. xsa12mat $1. sa12mat 6. xsa12mam $1. sa12mam 6. 
		xsa12maw $1. sa12maw 6.;
	input unitid arank xsainstt $
sainstt xsainstm $
sainstm xsainstw $
sainstw xsa_9mct $
sa_9mct xsa_9mcm $
sa_9mcm xsa_9mcw $
sa_9mcw xsatotlt $
satotlt xsatotlm $
satotlm xsatotlw $
satotlw xsa09mct $
sa09mct xsa09mcm $
sa09mcm xsa09mcw $
sa09mcw xsa10mct $
sa10mct xsa10mcm $
sa10mcm xsa10mcw $
sa10mcw xsa11mct $
sa11mct xsa11mcm $
sa11mcm xsa11mcw $
sa11mcw xsa12mct $
sa12mct xsa12mcm $
sa12mcm xsa12mcw $
sa12mcw xsaoutlt $
saoutlt xsaoutlm $
saoutlm xsaoutlw $
saoutlw xsa09mot $
sa09mot xsa09mom $
sa09mom xsa09mow $
sa09mow xsa10mot $
sa10mot xsa10mom $
sa10mom xsa10mow $
sa10mow xsa11mot $
sa11mot xsa11mom $
sa11mom xsa11mow $
sa11mow xsa12mot $
sa12mot xsa12mom $
sa12mom xsa12mow $
sa12mow xsaeq9ot $
saeq9ot xsaeq9om $
saeq9om xsaeq9ow $
saeq9ow xsaeq9at $
saeq9at xsaeq9am $
saeq9am xsaeq9aw $
saeq9aw xsa09mat $
sa09mat xsa09mam $
sa09mam xsa09maw $
sa09maw xsa10mat $
sa10mat xsa10mam $
sa10mam xsa10maw $
sa10maw xsa11mat $
sa11mat xsa11mam $
sa11mam xsa11maw $
sa11maw xsa12mat $
sa12mat xsa12mam $
sa12mam xsa12maw $
sa12maw;
	label unitid='Unique identification number of the institution' 
		arank='Academic rank' 
		xsainstt='Imputation field for sainstt - Instructional staff - total' 
		sainstt='Instructional staff - total' 
		xsainstm='Imputation field for sainstm - Instructional staff - men' 
		sainstm='Instructional staff - men' 
		xsainstw='Imputation field for sainstw - Instructional staff - women' 
		sainstw='Instructional staff - women' xsa_9mct='Imputation field for sa_9mct - Instructional staff on less than 9-month contract-total' 
		sa_9mct='Instructional staff on less than 9-month contract-total' xsa_9mcm='Imputation field for sa_9mcm - Instructional staff on less than 9-month contract-men' 
		sa_9mcm='Instructional staff on less than 9-month contract-men' xsa_9mcw='Imputation field for sa_9mcw - Instructional staff on less than 9-month contract-women' 
		sa_9mcw='Instructional staff on less than 9-month contract-women' xsatotlt='Imputation field for satotlt - Instructional staff on 9, 10, 11 or 12 month contract-total' 
		satotlt='Instructional staff on 9, 10, 11 or 12 month contract-total' xsatotlm='Imputation field for satotlm - Instructional staff on 9, 10, 11 or 12 month contract-men' 
		satotlm='Instructional staff on 9, 10, 11 or 12 month contract-men' xsatotlw='Imputation field for satotlw - Instructional staff on 9, 10, 11 or 12 month contract-women' 
		satotlw='Instructional staff on 9, 10, 11 or 12 month contract-women' xsa09mct='Imputation field for sa09mct - Instructional staff on 9-month contract-total' 
		sa09mct='Instructional staff on 9-month contract-total' xsa09mcm='Imputation field for sa09mcm - Instructional staff on 9-month contract-men' 
		sa09mcm='Instructional staff on 9-month contract-men' xsa09mcw='Imputation field for sa09mcw - Instructional staff on 9-month contract-women' 
		sa09mcw='Instructional staff on 9-month contract-women' xsa10mct='Imputation field for sa10mct - Instructional staff on 10-month contract-total' 
		sa10mct='Instructional staff on 10-month contract-total' xsa10mcm='Imputation field for sa10mcm - Instructional staff on 10-month contract-men' 
		sa10mcm='Instructional staff on 10-month contract-men' xsa10mcw='Imputation field for sa10mcw - Instructional staff on 10-month contract-women' 
		sa10mcw='Instructional staff on 10-month contract-women' xsa11mct='Imputation field for sa11mct - Instructional staff on 11-month contract-total' 
		sa11mct='Instructional staff on 11-month contract-total' xsa11mcm='Imputation field for sa11mcm - Instructional staff on 11-month contract-men' 
		sa11mcm='Instructional staff on 11-month contract-men' xsa11mcw='Imputation field for sa11mcw - Instructional staff on 11-month contract-women' 
		sa11mcw='Instructional staff on 11-month contract-women' xsa12mct='Imputation field for sa12mct - Instructional staff on 12-month contract total' 
		sa12mct='Instructional staff on 12-month contract total' xsa12mcm='Imputation field for sa12mcm - Instructional staff on 12-month contract men' 
		sa12mcm='Instructional staff on 12-month contract men' xsa12mcw='Imputation field for sa12mcw - Instructional staff on 12--month contract women' 
		sa12mcw='Instructional staff on 12--month contract women' 
		xsaoutlt='Imputation field for saoutlt - Salary outlays - total' 
		saoutlt='Salary outlays - total' 
		xsaoutlm='Imputation field for saoutlm - Salary outlays - men' 
		saoutlm='Salary outlays - men' 
		xsaoutlw='Imputation field for saoutlw - Salary outlays - women' 
		saoutlw='Salary outlays - women' xsa09mot='Imputation field for sa09mot - Salary outlays for instructional staff on 9-month contract-total' 
		sa09mot='Salary outlays for instructional staff on 9-month contract-total' xsa09mom='Imputation field for sa09mom - Salary outlays for instructional staff on 9-month contract-men' 
		sa09mom='Salary outlays for instructional staff on 9-month contract-men' xsa09mow='Imputation field for sa09mow - Salary outlays for instructional staff on 9-month contract-women' 
		sa09mow='Salary outlays for instructional staff on 9-month contract-women' xsa10mot='Imputation field for sa10mot - Salary outlays for instructional staff on 10-month contract-total' 
		sa10mot='Salary outlays for instructional staff on 10-month contract-total' xsa10mom='Imputation field for sa10mom - Salary outlays for instructional staff on 10-month contract-men' 
		sa10mom='Salary outlays for instructional staff on 10-month contract-men' xsa10mow='Imputation field for sa10mow - Salary outlays for instructional staff on 10-month contract-women' 
		sa10mow='Salary outlays for instructional staff on 10-month contract-women' xsa11mot='Imputation field for sa11mot - Salary outlays for instructional staff on 11-month contract-total' 
		sa11mot='Salary outlays for instructional staff on 11-month contract-total' xsa11mom='Imputation field for sa11mom - Salary outlays for instructional staff on 11-month contract-men' 
		sa11mom='Salary outlays for instructional staff on 11-month contract-men' xsa11mow='Imputation field for sa11mow - Salary outlays for instructional staff on 11-month contract-women' 
		sa11mow='Salary outlays for instructional staff on 11-month contract-women' xsa12mot='Imputation field for sa12mot - Salary outlays for instructional staff on 12-month contract-total' 
		sa12mot='Salary outlays for instructional staff on 12-month contract-total' xsa12mom='Imputation field for sa12mom - Salary outlays for instructional staff on 12-month contract-men' 
		sa12mom='Salary outlays for instructional staff on 12-month contract-men' xsa12mow='Imputation field for sa12mow - Salary outlays for instructional staff on 12-month contract-women' 
		sa12mow='Salary outlays for instructional staff on 12-month contract-women' xsaeq9ot='Imputation field for saeq9ot - Salary outlays for instructional staff equated to a 9-month contract-total' saeq9ot='Salary outlays for instructional staff equated to a 9-month contract-total' xsaeq9om='Imputation field for saeq9om - Salary outlays for instructional staff equated to a 9-month contract-men' saeq9om='Salary outlays for instructional staff equated to a 9-month contract-men' xsaeq9ow='Imputation field for saeq9ow - Salary outlays for instructional staff equated to a 9-month contract-women' saeq9ow='Salary outlays for instructional staff equated to a 9-month contract-women' xsaeq9at='Imputation field for saeq9at - Average salary for instructional staff equated to a 9-month contract-total' saeq9at='Average salary for instructional staff equated to a 9-month contract-total' xsaeq9am='Imputation field for saeq9am - Average salary for instructional staff equated to a 9-month contract-men' saeq9am='Average salary for instructional staff equated to a 9-month contract-men' xsaeq9aw='Imputation field for saeq9aw - Average salary for instructional staff equated to a 9-month contract-women' saeq9aw='Average salary for instructional staff equated to a 9-month contract-women' xsa09mat='Imputation field for sa09mat - Average salary for instructional staff on 9-month contract-total' 
		sa09mat='Average salary for instructional staff on 9-month contract-total' xsa09mam='Imputation field for sa09mam - Average salary for instructional staff on 9-month contract-men' 
		sa09mam='Average salary for instructional staff on 9-month contract-men' xsa09maw='Imputation field for sa09maw - Average salary for instructional staff on 9-month contract-women' 
		sa09maw='Average salary for instructional staff on 9-month contract-women' xsa10mat='Imputation field for sa10mat - Average salary for instructional staff on 10-month contract-total' 
		sa10mat='Average salary for instructional staff on 10-month contract-total' xsa10mam='Imputation field for sa10mam - Average salary for instructional staff on 10-month contract-men' 
		sa10mam='Average salary for instructional staff on 10-month contract-men' xsa10maw='Imputation field for sa10maw - Average salary for instructional staff on 10-month contract-women' 
		sa10maw='Average salary for instructional staff on 10-month contract-women' xsa11mat='Imputation field for sa11mat - Average salary for instructional staff on 11-month contract-total' 
		sa11mat='Average salary for instructional staff on 11-month contract-total' xsa11mam='Imputation field for sa11mam - Average salary for instructional staff on 11-month contract-men' 
		sa11mam='Average salary for instructional staff on 11-month contract-men' xsa11maw='Imputation field for sa11maw - Average salary for instructional staff on 11-month contract-women' 
		sa11maw='Average salary for instructional staff on 11-month contract-women' xsa12mat='Imputation field for sa12mat - Average salary for instructional staff on 12-month contract-total' 
		sa12mat='Average salary for instructional staff on 12-month contract-total' xsa12mam='Imputation field for sa12mam - Average salary for instructional staff on 12-month contract-men' 
		sa12mam='Average salary for instructional staff on 12-month contract-men' xsa12maw='Imputation field for sa12maw - Average salary for instructional staff on 12-month contract-women' 
		sa12maw='Average salary for instructional staff on 12-month contract-women';
	format xsainstt-character-xsa12maw $ximpflg.
arank arank.;
run;

/** Proc Freq and Summary **/
Proc Freq;
	Tables arank xsainstt xsainstm xsainstw xsa_9mct xsa_9mcm xsa_9mcw xsatotlt 
		xsatotlm xsatotlw xsa09mct xsa09mcm xsa09mcw xsa10mct xsa10mcm xsa10mcw 
		xsa11mct xsa11mcm xsa11mcw xsa12mct xsa12mcm xsa12mcw xsaoutlt xsaoutlm 
		xsaoutlw xsa09mot xsa09mom xsa09mow xsa10mot xsa10mom xsa10mow xsa11mot 
		xsa11mom xsa11mow xsa12mot xsa12mom xsa12mow xsaeq9ot xsaeq9om xsaeq9ow 
		xsaeq9at xsaeq9am xsaeq9aw xsa09mat xsa09mam xsa09maw xsa10mat xsa10mam 
		xsa10maw xsa11mat xsa11mam xsa11maw xsa12mat xsa12mam xsa12maw / missing;

Proc Summary print n sum mean min max;
	var sainstt sainstm sainstw sa_9mct sa_9mcm sa_9mcw satotlt satotlm satotlw 
		sa09mct sa09mcm sa09mcw sa10mct sa10mcm sa10mcw sa11mct sa11mcm sa11mcw 
		sa12mct sa12mcm sa12mcw saoutlt saoutlm saoutlw sa09mot sa09mom sa09mow 
		sa10mot sa10mom sa10mow sa11mot sa11mom sa11mow sa12mot sa12mom sa12mow 
		saeq9ot saeq9om saeq9ow saeq9at saeq9am saeq9aw sa09mat sa09mam sa09maw 
		sa10mat sa10mam sa10maw sa11mat sa11mam sa11maw sa12mat sa12mam sa12maw;
run;