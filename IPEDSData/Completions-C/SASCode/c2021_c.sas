*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\c2021_c.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
awlevelc 2. 
xcstotlt $1.
cstotlt  6. 
xcstotlm $1.
cstotlm  6. 
xcstotlw $1.
cstotlw  6. 
xcsaiant $1.
csaiant  6. 
xcsasiat $1.
csasiat  6. 
xcsbkaat $1.
csbkaat  6. 
xcshispt $1.
cshispt  6. 
xcsnhpit $1.
csnhpit  6. 
xcswhitt $1.
cswhitt  6. 
xcs2mort $1.
cs2mort  6. 
xcsunknt $1.
csunknt  6. 
xcsnralt $1.
csnralt  6. 
xcsund18 $1.
csund18  6. 
xcs18_24 $1.
cs18_24  6. 
xcs25_39 $1.
cs25_39  6. 
xcsabv40 $1.
csabv40  6. 
xcsunkn  $1.
csunkn   6.;

input
unitid   
awlevelc 
xcstotlt $
cstotlt  
xcstotlm $
cstotlm  
xcstotlw $
cstotlw  
xcsaiant $
csaiant  
xcsasiat $
csasiat  
xcsbkaat $
csbkaat  
xcshispt $
cshispt  
xcsnhpit $
csnhpit  
xcswhitt $
cswhitt  
xcs2mort $
cs2mort  
xcsunknt $
csunknt  
xcsnralt $
csnralt  
xcsund18 $
csund18  
xcs18_24 $
cs18_24  
xcs25_39 $
cs25_39  
xcsabv40 $
csabv40  
xcsunkn  $
csunkn  ;

label
unitid  ='Unique identification number of the institution' 
awlevelc='Award Level code' 
xcstotlt='Imputation field for cstotlt - Grand total'
cstotlt ='Grand total' 
xcstotlm='Imputation field for cstotlm - Grand total men'
cstotlm ='Grand total men' 
xcstotlw='Imputation field for cstotlw - Grand total women'
cstotlw ='Grand total women' 
xcsaiant='Imputation field for csaiant - American Indian or Alaska Native total'
csaiant ='American Indian or Alaska Native total' 
xcsasiat='Imputation field for csasiat - Asian total'
csasiat ='Asian total' 
xcsbkaat='Imputation field for csbkaat - Black or African American total'
csbkaat ='Black or African American total' 
xcshispt='Imputation field for cshispt - Hispanic or Latino total'
cshispt ='Hispanic or Latino total' 
xcsnhpit='Imputation field for csnhpit - Native Hawaiian or Other Pacific Islander total'
csnhpit ='Native Hawaiian or Other Pacific Islander total' 
xcswhitt='Imputation field for cswhitt - White total'
cswhitt ='White total' 
xcs2mort='Imputation field for cs2mort - Two or more races total'
cs2mort ='Two or more races total' 
xcsunknt='Imputation field for csunknt - Race/ethnicity unknown total'
csunknt ='Race/ethnicity unknown total' 
xcsnralt='Imputation field for csnralt - Nonresident alien total'
csnralt ='Nonresident alien total' 
xcsund18='Imputation field for csund18 - Ages, under 18'
csund18 ='Ages, under 18' 
xcs18_24='Imputation field for cs18_24 - Ages, 18-24'
cs18_24 ='Ages, 18-24' 
xcs25_39='Imputation field for cs25_39 - Ages, 25-39'
cs25_39 ='Ages, 25-39' 
xcsabv40='Imputation field for csabv40 - Ages, 40 and above'
csabv40 ='Ages, 40 and above' 
xcsunkn ='Imputation field for csunkn - Age unknown'
csunkn  ='Age unknown';
run;

Proc Format;
value awlevelc  
3='Associate^s degree' 
5='Bachelor^s degree' 
7='Master^s degree' 
9='Doctor^s degree' 
10='Postbaccalaureate or Post-master^s certificate' 
11='Certificate of less than 12 weeks' 
12='Certificate of at least 12 weeks but less than 1 year' 
2='Certificate of at least 1 but less than 4 years';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
awlevelc xcstotlt xcstotlm xcstotlw xcsaiant xcsasiat xcsbkaat xcshispt xcsnhpit
xcswhitt xcs2mort xcsunknt xcsnralt xcsund18 xcs18_24 xcs25_39 xcsabv40 xcsunkn   / missing;
format xcstotlt-character-xcsunkn  $ximpflg.
awlevelc  awlevelc.
;

Proc Summary print n sum mean min max;
var
cstotlt  cstotlm  cstotlw  csaiant  csasiat  csbkaat  cshispt  csnhpit 
cswhitt  cs2mort  csunknt  csnralt  csund18  cs18_24  cs25_39  csabv40  csunkn   ;
run;
