*** Created:       June 8, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\c2021_b.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xcstotlt $1.
cstotlt  6. 
xcstotlm $1.
cstotlm  6. 
xcstotlw $1.
cstotlw  6. 
xcsaiant $1.
csaiant  6. 
xcsaianm $1.
csaianm  6. 
xcsaianw $1.
csaianw  6. 
xcsasiat $1.
csasiat  6. 
xcsasiam $1.
csasiam  6. 
xcsasiaw $1.
csasiaw  6. 
xcsbkaat $1.
csbkaat  6. 
xcsbkaam $1.
csbkaam  6. 
xcsbkaaw $1.
csbkaaw  6. 
xcshispt $1.
cshispt  6. 
xcshispm $1.
cshispm  6. 
xcshispw $1.
cshispw  6. 
xcsnhpit $1.
csnhpit  6. 
xcsnhpim $1.
csnhpim  6. 
xcsnhpiw $1.
csnhpiw  6. 
xcswhitt $1.
cswhitt  6. 
xcswhitm $1.
cswhitm  6. 
xcswhitw $1.
cswhitw  6. 
xcs2mort $1.
cs2mort  6. 
xcs2morm $1.
cs2morm  6. 
xcs2morw $1.
cs2morw  6. 
xcsunknt $1.
csunknt  6. 
xcsunknm $1.
csunknm  6. 
xcsunknw $1.
csunknw  6. 
xcsnralt $1.
csnralt  6. 
xcsnralm $1.
csnralm  6. 
xcsnralw $1.
csnralw  6.;

input
unitid   
xcstotlt $
cstotlt  
xcstotlm $
cstotlm  
xcstotlw $
cstotlw  
xcsaiant $
csaiant  
xcsaianm $
csaianm  
xcsaianw $
csaianw  
xcsasiat $
csasiat  
xcsasiam $
csasiam  
xcsasiaw $
csasiaw  
xcsbkaat $
csbkaat  
xcsbkaam $
csbkaam  
xcsbkaaw $
csbkaaw  
xcshispt $
cshispt  
xcshispm $
cshispm  
xcshispw $
cshispw  
xcsnhpit $
csnhpit  
xcsnhpim $
csnhpim  
xcsnhpiw $
csnhpiw  
xcswhitt $
cswhitt  
xcswhitm $
cswhitm  
xcswhitw $
cswhitw  
xcs2mort $
cs2mort  
xcs2morm $
cs2morm  
xcs2morw $
cs2morw  
xcsunknt $
csunknt  
xcsunknm $
csunknm  
xcsunknw $
csunknw  
xcsnralt $
csnralt  
xcsnralm $
csnralm  
xcsnralw $
csnralw ;

label
unitid  ='Unique identification number of the institution' 
xcstotlt='Imputation field for cstotlt - Grand total'
cstotlt ='Grand total' 
xcstotlm='Imputation field for cstotlm - Grand total men'
cstotlm ='Grand total men' 
xcstotlw='Imputation field for cstotlw - Grand total women'
cstotlw ='Grand total women' 
xcsaiant='Imputation field for csaiant - American Indian or Alaska Native total'
csaiant ='American Indian or Alaska Native total' 
xcsaianm='Imputation field for csaianm - American Indian or Alaska Native men'
csaianm ='American Indian or Alaska Native men' 
xcsaianw='Imputation field for csaianw - American Indian or Alaska Native women'
csaianw ='American Indian or Alaska Native women' 
xcsasiat='Imputation field for csasiat - Asian total'
csasiat ='Asian total' 
xcsasiam='Imputation field for csasiam - Asian men'
csasiam ='Asian men' 
xcsasiaw='Imputation field for csasiaw - Asian women'
csasiaw ='Asian women' 
xcsbkaat='Imputation field for csbkaat - Black or African American total'
csbkaat ='Black or African American total' 
xcsbkaam='Imputation field for csbkaam - Black or African American men'
csbkaam ='Black or African American men' 
xcsbkaaw='Imputation field for csbkaaw - Black or African American women'
csbkaaw ='Black or African American women' 
xcshispt='Imputation field for cshispt - Hispanic or Latino total'
cshispt ='Hispanic or Latino total' 
xcshispm='Imputation field for cshispm - Hispanic or Latino men'
cshispm ='Hispanic or Latino men' 
xcshispw='Imputation field for cshispw - Hispanic or Latino women'
cshispw ='Hispanic or Latino women' 
xcsnhpit='Imputation field for csnhpit - Native Hawaiian or Other Pacific Islander total'
csnhpit ='Native Hawaiian or Other Pacific Islander total' 
xcsnhpim='Imputation field for csnhpim - Native Hawaiian or Other Pacific Islander men'
csnhpim ='Native Hawaiian or Other Pacific Islander men' 
xcsnhpiw='Imputation field for csnhpiw - Native Hawaiian or Other Pacific Islander women'
csnhpiw ='Native Hawaiian or Other Pacific Islander women' 
xcswhitt='Imputation field for cswhitt - White total'
cswhitt ='White total' 
xcswhitm='Imputation field for cswhitm - White men'
cswhitm ='White men' 
xcswhitw='Imputation field for cswhitw - White women'
cswhitw ='White women' 
xcs2mort='Imputation field for cs2mort - Two or more races total'
cs2mort ='Two or more races total' 
xcs2morm='Imputation field for cs2morm - Two or more races men'
cs2morm ='Two or more races men' 
xcs2morw='Imputation field for cs2morw - Two or more races women'
cs2morw ='Two or more races women' 
xcsunknt='Imputation field for csunknt - Race/ethnicity unknown total'
csunknt ='Race/ethnicity unknown total' 
xcsunknm='Imputation field for csunknm - Race/ethnicity unknown men'
csunknm ='Race/ethnicity unknown men' 
xcsunknw='Imputation field for csunknw - Race/ethnicity unknown women'
csunknw ='Race/ethnicity unknown women' 
xcsnralt='Imputation field for csnralt - Nonresident alien total'
csnralt ='Nonresident alien total' 
xcsnralm='Imputation field for csnralm - Nonresident alien men'
csnralm ='Nonresident alien men' 
xcsnralw='Imputation field for csnralw - Nonresident alien women'
csnralw ='Nonresident alien women';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xcstotlt xcstotlm xcstotlw xcsaiant xcsaianm xcsaianw xcsasiat xcsasiam xcsasiaw
xcsbkaat xcsbkaam xcsbkaaw xcshispt xcshispm xcshispw xcsnhpit xcsnhpim xcsnhpiw xcswhitt
xcswhitm xcswhitw xcs2mort xcs2morm xcs2morw xcsunknt xcsunknm xcsunknw xcsnralt xcsnralm
xcsnralw  / missing;
format xcstotlt-character-xcsnralw $ximpflg.;

Proc Summary print n sum mean min max;
var
cstotlt  cstotlm  cstotlw  csaiant  csaianm  csaianw  csasiat  csasiam  csasiaw 
csbkaat  csbkaam  csbkaaw  cshispt  cshispm  cshispw  csnhpit  csnhpim  csnhpiw  cswhitt 
cswhitm  cswhitw  cs2mort  cs2morm  cs2morw  csunknt  csunknm  csunknw  csnralt  csnralm 
csnralw  ;
run;
