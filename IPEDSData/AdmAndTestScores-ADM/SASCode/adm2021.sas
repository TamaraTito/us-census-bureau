*** Created:    August 22, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\adm2021.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
admcon1  2. 
admcon2  2. 
admcon3  2. 
admcon4  2. 
admcon5  2. 
admcon6  2. 
admcon7  2. 
admcon8  2. 
admcon9  2. 
xapplcn  $1.
applcn   6. 
xapplcnm $1.
applcnm  6. 
xapplcnw $1.
applcnw  6. 
xadmssn  $1.
admssn   6. 
xadmssnm $1.
admssnm  6. 
xadmssnw $1.
admssnw  6. 
xenrlt   $1.
enrlt    6. 
xenrlm   $1.
enrlm    6. 
xenrlw   $1.
enrlw    6. 
xenrlft  $1.
enrlft   6. 
xenrlftm $1.
enrlftm  6. 
xenrlftw $1.
enrlftw  6. 
xenrlpt  $1.
enrlpt   6. 
xenrlptm $1.
enrlptm  6. 
xenrlptw $1.
enrlptw  6. 
xsatnum  $1.
satnum   6. 
xsatpct  $1.
satpct   3. 
xactnum  $1.
actnum   6. 
xactpct  $1.
actpct   3. 
xsatvr25 $1.
satvr25  3. 
xsatvr75 $1.
satvr75  3. 
xsatmt25 $1.
satmt25  3. 
xsatmt75 $1.
satmt75  3. 
xactcm25 $1.
actcm25  3. 
xactcm75 $1.
actcm75  3. 
xacten25 $1.
acten25  3. 
xacten75 $1.
acten75  3. 
xactmt25 $1.
actmt25  3. 
xactmt75 $1.
actmt75  3.;

input
unitid   
admcon1  
admcon2  
admcon3  
admcon4  
admcon5  
admcon6  
admcon7  
admcon8  
admcon9  
xapplcn  $
applcn   
xapplcnm $
applcnm  
xapplcnw $
applcnw  
xadmssn  $
admssn   
xadmssnm $
admssnm  
xadmssnw $
admssnw  
xenrlt   $
enrlt    
xenrlm   $
enrlm    
xenrlw   $
enrlw    
xenrlft  $
enrlft   
xenrlftm $
enrlftm  
xenrlftw $
enrlftw  
xenrlpt  $
enrlpt   
xenrlptm $
enrlptm  
xenrlptw $
enrlptw  
xsatnum  $
satnum   
xsatpct  $
satpct   
xactnum  $
actnum   
xactpct  $
actpct   
xsatvr25 $
satvr25  
xsatvr75 $
satvr75  
xsatmt25 $
satmt25  
xsatmt75 $
satmt75  
xactcm25 $
actcm25  
xactcm75 $
actcm75  
xacten25 $
acten25  
xacten75 $
acten75  
xactmt25 $
actmt25  
xactmt75 $
actmt75 ;

label
unitid  ='Unique identification number of the institution' 
admcon1 ='Secondary school GPA' 
admcon2 ='Secondary school rank' 
admcon3 ='Secondary school record' 
admcon4 ='Completion of college-preparatory program' 
admcon5 ='Recommendations' 
admcon6 ='Formal demonstration of competencies' 
admcon7 ='Admission test scores' 
admcon8 ='TOEFL (Test of English as a Foreign Language' 
admcon9 ='Other Test (Wonderlic, WISC-III, etc.)' 
xapplcn ='Imputation field for applcn - Applicants total'
applcn  ='Applicants total' 
xapplcnm='Imputation field for applcnm - Applicants men'
applcnm ='Applicants men' 
xapplcnw='Imputation field for applcnw - Applicants women'
applcnw ='Applicants women' 
xadmssn ='Imputation field for admssn - Admissions total'
admssn  ='Admissions total' 
xadmssnm='Imputation field for admssnm - Admissions men'
admssnm ='Admissions men' 
xadmssnw='Imputation field for admssnw - Admissions women'
admssnw ='Admissions women' 
xenrlt  ='Imputation field for enrlt - Enrolled total'
enrlt   ='Enrolled total' 
xenrlm  ='Imputation field for enrlm - Enrolled  men'
enrlm   ='Enrolled  men' 
xenrlw  ='Imputation field for enrlw - Enrolled  women'
enrlw   ='Enrolled  women' 
xenrlft ='Imputation field for enrlft - Enrolled full time total'
enrlft  ='Enrolled full time total' 
xenrlftm='Imputation field for enrlftm - Enrolled full time men'
enrlftm ='Enrolled full time men' 
xenrlftw='Imputation field for enrlftw - Enrolled full time women'
enrlftw ='Enrolled full time women' 
xenrlpt ='Imputation field for enrlpt - Enrolled part time total'
enrlpt  ='Enrolled part time total' 
xenrlptm='Imputation field for enrlptm - Enrolled part time men'
enrlptm ='Enrolled part time men' 
xenrlptw='Imputation field for enrlptw - Enrolled part time women'
enrlptw ='Enrolled part time women' 
xsatnum ='Imputation field for satnum - Number of first-time degree/certificate-seeking students submitting SAT scores'
satnum  ='Number of first-time degree/certificate-seeking students submitting SAT scores' 
xsatpct ='Imputation field for satpct - Percent of first-time degree/certificate-seeking students submitting SAT scores'
satpct  ='Percent of first-time degree/certificate-seeking students submitting SAT scores' 
xactnum ='Imputation field for actnum - Number of first-time degree/certificate-seeking students submitting ACT scores'
actnum  ='Number of first-time degree/certificate-seeking students submitting ACT scores' 
xactpct ='Imputation field for actpct - Percent of first-time degree/certificate-seeking students submitting ACT scores'
actpct  ='Percent of first-time degree/certificate-seeking students submitting ACT scores' 
xsatvr25='Imputation field for satvr25 - SAT Evidence-Based Reading and Writing 25th percentile score'
satvr25 ='SAT Evidence-Based Reading and Writing 25th percentile score' 
xsatvr75='Imputation field for satvr75 - SAT Evidence-Based Reading and Writing 75th percentile score'
satvr75 ='SAT Evidence-Based Reading and Writing 75th percentile score' 
xsatmt25='Imputation field for satmt25 - SAT Math 25th percentile score'
satmt25 ='SAT Math 25th percentile score' 
xsatmt75='Imputation field for satmt75 - SAT Math 75th percentile score'
satmt75 ='SAT Math 75th percentile score' 
xactcm25='Imputation field for actcm25 - ACT Composite 25th percentile score'
actcm25 ='ACT Composite 25th percentile score' 
xactcm75='Imputation field for actcm75 - ACT Composite 75th percentile score'
actcm75 ='ACT Composite 75th percentile score' 
xacten25='Imputation field for acten25 - ACT English 25th percentile score'
acten25 ='ACT English 25th percentile score' 
xacten75='Imputation field for acten75 - ACT English 75th percentile score'
acten75 ='ACT English 75th percentile score' 
xactmt25='Imputation field for actmt25 - ACT Math 25th percentile score'
actmt25 ='ACT Math 25th percentile score' 
xactmt75='Imputation field for actmt75 - ACT Math 75th percentile score'
actmt75 ='ACT Math 75th percentile score';
run;

Proc Format;
value admcon1f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon2f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon3f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon4f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon5f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon6f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon7f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon8f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value admcon9f
1='Required' 
5='Considered but not required' 
2='Recommended' 
3='Neither required nor recommended';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
admcon1  admcon2  admcon3  admcon4  admcon5  admcon6  admcon7  admcon8  admcon9 
xapplcn  xapplcnm xapplcnw xadmssn  xadmssnm xadmssnw xenrlt   xenrlm   xenrlw   xenrlft 
xenrlftm xenrlftw xenrlpt  xenrlptm xenrlptw xsatnum  xsatpct  xactnum  xactpct  xsatvr25
xsatvr75 xsatmt25 xsatmt75 xactcm25 xactcm75 xacten25 xacten75 xactmt25 xactmt75  / missing;
format xapplcn -character-xactmt75 $ximpflg.
admcon1  admcon1f.
admcon2  admcon2f.
admcon3  admcon3f.
admcon4  admcon4f.
admcon5  admcon5f.
admcon6  admcon6f.
admcon7  admcon7f.
admcon8  admcon8f.
admcon9  admcon9f.
;

Proc Summary print n sum mean min max;
var

applcn   applcnm  applcnw  admssn   admssnm  admssnw  enrlt    enrlm    enrlw    enrlft  
enrlftm  enrlftw  enrlpt   enrlptm  enrlptw  satnum   satpct   actnum   actpct   satvr25 
satvr75  satmt25  satmt75  actcm25  actcm75  acten25  acten75  actmt25  actmt75  ;
run;
