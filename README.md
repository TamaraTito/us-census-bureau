# Goal of the Project
The goal of this project is to identify key factors influencing graduation rates across different racial categories and other relevant predictors.

## Built with :
SAS on Demand for Academics

## Data size :
Narrow it to 1634 rows and 15 columns
