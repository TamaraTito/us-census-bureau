/*Graduation Rates By Race*/
/* Graduation rates stratified by race/ethnicity subsetting only to the grtype values
"8 = Bachelor^s or equiv subcohort (4-yr institution) adjusted cohort (revised cohort minus exclusions)" and
"9 = Bachelor's or equiv subcohort (4-yr institution) Completers within 150% of normal time total".*/

proc sort data=ipedgr.gr2021 out=gr2021B;
by unitid;
run;

/* Subtracting Unknown race/ethnicity and Non-resident Alien totals from Grand Total and labeling it as Total */
data gr2021m;
 set gr2021B;
 grtotlt2 = grtotlt - grunknt - grnralt;
 label grtotlt2='All';
run;

/*For a 4--year bachelor degree, count across race/ethnicity - Transposing the data*/
proc transpose data=gr2021m out=gr2021A(rename=(col1=count));
  by unitid grtype;
  var grtotlt2 graiant grasiat grbkaat grhispt grnhpit grwhitt gr2mort ;
  where grtype in (8,9);/*(and grtotlt2=0) for inspecting 0 total students*/
run;

proc sql;
  select count(DISTINCT unitid) as TotalInstitutions
  from gr2021A;
quit; /*2008*/

proc sort data=gr2021A;
	by unitid _label_ count;
run;

/*graduation rate across race/ethnicity*/
data gr2021C;
  set gr2021A;
  by unitID _label_;
  retain wt;
  _numerator=lag1(count);
  if last._label_ then do;
    if first._label_ eq last._label_ then delete;
    Category = propcase(scan(_label_,1));
    if count >10 then rate = _numerator/count;/*where count is not 0 and less then 10 students per race */
      else rate = .;
  if rate ne .; /* Not taking into account rows that have no students enrolled in a race category*/
  if category eq 'All' then do;
      wt = count;
     end;
    output;
  end;
  drop _: count grtype;
run;

/*Number of institutions*/
proc sql;
  select count(DISTINCT unitid) as TotalInstitutions
  from gr2021C;
quit; /*1799*/


data sal2021_is;
 set ipedsal.sal2021_is;
 where arank = 7;
 keep unitid saoutlt satotlt;
run;

data racemerge;
merge gr2021C(in=a) ipedicm.ic2021_ay(in=b) sal2021_is(in=c) ipedsfa.sfa2021(in=d) ipedicm.hd2021(in=e) ipedicm.ic2021(in=f);
by unitid;
if a and b and c and d and e and f;
drop x:;
run;

/*Predictors*/
/*Feature engineering for selecting Average amount of aid/grant variables*/
proc format;
value obereg  
0='U.S. Service schools' 
1,2='New England and Mid East' 
3,4='Great Lakes and Plains' 
5='Southeast (AL, AR, FL, GA, KY, LA, MS, NC, SC, TN, VA, WV)' 
6='Southwest (AZ, NM, OK, TX)' 
7='Rocky Mountains (CO, ID, MT, UT, WY)' 
8='Far West (AK, CA, HI, NV, OR, WA)' 
9='Other U.S. jurisdictions (AS, FM, GU, MH, MP, PR, PW, VI)'
;

value pubpriv
 1 = 'Public'
 2,3,4 = 'Private'
 ;

value instsize  
1='Under 1,000' 
2='1,000 - 4,999' 
3='5,000 - 9,999' 
4='10,000 - 19,999' 
5='20,000 and above'
;
/* -1='Not reported'  */
/* -2='Not applicable'; */

value $race
	'American'="American Indian or Alaska Native"
	'Black'="Black or African American"
	'Hispanic'="Hispanic or Latino"
	'Native'="Native Hawaiian or Other Pacific Islanders "
	'Two'="Two or more races"
	'White'="White"
	;
run;

data preds;
 set racemerge;
 where not missing(tuition2) and uagrnta ne . and upgrnta ne . and ufloana ne . and rate ne .;
 
instate = tuition2 + fee2;
outstate = tuition3 + fee3;
 
/*average cost per student*/
if scfa13p = . then do;
	AvgCostPerStudent = outstate;
	end;
else do;
	AvgCostPerStudent = ((outstate) * scfa13p/100) + ((instate) * (1-(scfa13p/100)));
	end;

 AvgSalOut = saoutlt/satotlt;
 AvgGrant = uagrnta;
 AvgPell = upgrnta;
 AvgFed = ufloana;
 cntlaffi2= put(cntlaffi,pubpriv.);
 obereg2 = put(obereg,obereg.);

format instsize instsize. category race.;  
keep unitid rate wt category instate outstate AvgSalOut AvgGrant AvgPell AvgFed obereg obereg2 cntlaffi cntlaffi2 instsize AvgCostPerStudent;
run;

proc means data=preds NMISS;
run;

/*Standardizing the data*/

proc standard data=preds mean=0 std=1 out=predsstd;
var instate outstate avgsalout avggrant avgpell avgfed AvgCostPerStudent;
run;

/*Number of institutions*/
proc sql;
  select count(DISTINCT unitid) as TotalInstitutions
  from preds;
quit;/*1688*/

/*correlation*/

proc corr data=preds;
  var instate outstate avgsalout avggrant avgpell avgfed instsize AvgCostPerStudent;
run; /*AvgCosPreStudent - instate-outstate-avggrant are highly correlataed between each other*/

proc corr data=preds;
  var avgsalout avggrant avgpell avgfed instsize AvgCostPerStudent;
run;/*taking out instate and outstate*/


/*Modelling*/

/*Model 1 - GLM*/
ods graphics off;
proc glm data=predsstd; 
 where category Not in ('All') and obereg not in (0,9);
 class category cntlaffi2 instsize obereg2;
 model rate = category AvgCostPerStudent AvgSalOut AvgGrant AvgPell AvgFed obereg2 cntlaffi2 instsize / solution;
 lsmeans category;
run;


/*Model 2 - GLMselect using cross product of category with quantitative variables, regions, institution size and
affiliation as a predictors*/
proc glmselect data=predsstd;
  where category Not in ('All') and obereg not in (0,9);
  class category obereg2 cntlaffi2;
  model rate = category|AvgCostPerStudent category|AvgSalOut category|AvgGrant category|
  AvgPell category|AvgFed category|obereg2 category|instsize category|cntlaffi2 /  selection=stepwise slentry=0.05 slstay=0.05;
  weight wt;
  ods select ParameterEstimates;
  output out=glmselectOutput;
run;


